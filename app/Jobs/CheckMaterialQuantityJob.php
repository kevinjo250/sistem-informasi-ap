<?php

namespace App\Jobs;

use App\Models\Produk;
use App\Models\User;
use App\Notifications\LowMaterialStockNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;

class CheckMaterialQuantityJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $materials = Produk::onlyMaterial()
            ->whereColumn('stok_produk', '<=', 'safety_stok')
            ->get();

        $users = User::whereIn('jabatan_id', [1, 9])->get();

        if ($materials) {
            foreach ($materials as $material) {
                Notification::sendNow($users, new LowMaterialStockNotification($material));
            }
        }
    }
}
