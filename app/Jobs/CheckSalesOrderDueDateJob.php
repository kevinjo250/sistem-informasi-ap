<?php

namespace App\Jobs;

use App\Models\SalesOrder;
use App\Models\User;
use App\Notifications\SalesOrderDueDateNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;

class CheckSalesOrderDueDateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $date = now()->format('Y-m-d');

        $salesOrders = SalesOrder::where('status_nota', 'done')
            ->where('tanggal_jatuh_tempo', $date)
            ->get();

        $users = User::whereIn('jabatan_id', [1, 4])->get();

        if ($salesOrders) {
            foreach ($salesOrders as $salesOrder) {
                Notification::sendNow($users, new SalesOrderDueDateNotification($salesOrder));

                $salesOrder->update(['status_pembayaran' => 'overdue']);
            }
        }
    }
}
