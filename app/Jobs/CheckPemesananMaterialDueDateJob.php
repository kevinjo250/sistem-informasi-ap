<?php

namespace App\Jobs;

use App\Models\PemesananMaterial;
use App\Models\User;
use App\Notifications\PemesananMaterialDueDateNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;

class CheckPemesananMaterialDueDateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $date = now()->format('Y-m-d');

        $pemesananMaterials = PemesananMaterial::where('status_nota', 'done')
            ->where('tanggal_jatuh_tempo', $date)
            ->get();

        $users = User::whereIn('jabatan_id', [1, 4])->get();

        if ($pemesananMaterials) {
            foreach ($pemesananMaterials as $pemesananMaterial) {
                Notification::sendNow($users, new PemesananMaterialDueDateNotification($pemesananMaterial));

                $pemesananMaterial->update(['status_pembayaran' => 'overdue']);
            }
        }
    }
}
