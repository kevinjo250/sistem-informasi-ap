<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        Blade::directive('date', function ($date) {
            return "<?php echo date('d/m/Y', strtotime($date)) ?>";
        });

        Blade::directive('periode', function ($periode) {
            return "<?php echo date('F Y', strtotime($periode . '-01')) ?>";
        });

        Blade::directive('datetime', function ($date) {
            return "<?php echo date('d/m/Y H:i:s', strtotime($date)) ?>";
        });

        Blade::directive('money', function ($money) {
            return "<?php echo 'Rp ' . number_format($money); ?>";
        });

        Blade::directive('number', function ($number) {
            return "<?php echo number_format($number); ?>";
        });
    }
}
