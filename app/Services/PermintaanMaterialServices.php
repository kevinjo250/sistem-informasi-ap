<?php

namespace App\Services;

use App\Models\BillOfMaterial;
use App\Models\Jurnal;
use App\Models\PermintaanMaterial;
use App\Models\SuratPerintahKerja;
use App\Models\User;
use App\Notifications\PermintaanMaterialCreatedNotification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class PermintaanMaterialServices
{
    public function handleStorePermintaanMaterial(array $data, int $userId): PermintaanMaterial
    {
        $suratPerintahKerja = SuratPerintahKerja::with(['produkSalesOrder'])
            ->findOrFail($data['surat_perintah_kerja_id']);

        $permintaanMaterial = PermintaanMaterial::create($data + [
            'no_nota' => $this->createNomorNota(),
            'user_id' => $userId,
        ]);

        $materialsNeeded = $this->handleTotalMaterialNeeded(
            $suratPerintahKerja->produkSalesOrder->produk_id,
            $suratPerintahKerja->kuantitas,
        );

        $permintaanMaterial->produks()->attach($materialsNeeded);

        $suratPerintahKerja->update(['status_produksi' => 'waiting for materials']);

        $users = User::whereIn('jabatan_id', [1, 9])->get();

        Notification::sendNow($users, new PermintaanMaterialCreatedNotification($permintaanMaterial));

        return $permintaanMaterial;
    }

    public function handleDestroyPermintaanMaterial(int $permintaanMaterialId): PermintaanMaterial
    {
        $permintaanMaterial = PermintaanMaterial::findOrFail($permintaanMaterialId);

        if ($permintaanMaterial->suratPerintahKerja->realisasiProduksis()->exists()) {
            throw new \Exception('Permintaan material gagal dihapus!');
        }

        if ($permintaanMaterial->status_nota != 'pending') {
            throw new \Exception('Permintaan material gagal dihapus!');
        }

        $permintaanMaterial->produks()->detach();

        $permintaanMaterial->delete();

        $permintaanMaterial->suratPerintahKerja->update(['status_produksi' => 'pending']);

        DB::table('notifications')
            ->where('type', 'App\Notifications\PermintaanMaterialCreatedNotification')
            ->where('data->no_nota', $permintaanMaterial->no_nota)
            ->delete();

        return $permintaanMaterial;
    }

    public function handleConfirmPermintaanMaterial(int $permintaanMaterialId): PermintaanMaterial
    {
        $permintaanMaterial = PermintaanMaterial::with(['produks'])
            ->findOrFail($permintaanMaterialId);

        foreach ($permintaanMaterial->produks as $detail) {
            if ($detail->stok_produk < $detail->pivot->kuantitas) {
                throw new \Exception("Permintaan material gagal dikonfirmasi! Terdapat material yang belum tersedia.");
            }
        }

        $permintaanMaterial->update(['status_nota' => 'approved']);

        $permintaanMaterial->suratPerintahKerja->update(['status_produksi' => 'in production']);

        $this->handleUpdateStokProduk($permintaanMaterialId);

        $this->handleJurnalAkuntansi($permintaanMaterialId);

        return $permintaanMaterial;
    }

    private function createNomorNota(): string
    {
        $date = now()->format('Y-m-d');

        $record = PermintaanMaterial::latest()->first();

        if (! $record) {
            return "PM/{$date}/1";
        }

        $expNum = explode('/', $record->no_nota);

        $number = $expNum[2] +1;

        return "PM/{$date}/{$number}";
    }

    private function handleTotalMaterialNeeded(int $produkId, int $qtyProduksi): array
    {
        $bom = BillOfMaterial::with(['produks'])->where('produk_id', $produkId)
            ->firstOrFail();

        $materialsNeeded = [];

        foreach ($bom->produks as $data) {
            $materialsNeeded[$data['id']] = [
                'kuantitas' => $data['pivot']['kuantitas'] * $qtyProduksi,
                'hpp' => $data['harga_pokok']
            ];
        }

        return $materialsNeeded;
    }

    private function handleUpdateStokProduk(int $permintaanMaterialId): void
    {
        $permintaanMaterial = PermintaanMaterial::with(['produks'])
            ->findOrFail($permintaanMaterialId);
            
        foreach ($permintaanMaterial->produks as $detail) {
            $detail->update(['stok_produk' => $detail->stok_produk - $detail->pivot->kuantitas]);
        }

        return;
    }

    private function handleJurnalAkuntansi(int $permintaanMaterialId): void
    {
        $permintaanMaterial = PermintaanMaterial::findOrFail($permintaanMaterialId);

        $jurnal = Jurnal::create([
            'tanggal' => now(),
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'permintaan material',
            'nomor_bukti' => $permintaanMaterial->no_nota,
            'keterangan' => 'Mencatat penggunaan bahan baku untuk produksi.'
        ]);

        $jurnal->akuns()->attach([
            '105' => [
                'debet' => $permintaanMaterial->biayaMaterial,
                'kredit' => 0
            ],
            '104' => [
                'debet' => 0,
                'kredit' => $permintaanMaterial->biayaMaterial
            ]
        ]);
    }
}