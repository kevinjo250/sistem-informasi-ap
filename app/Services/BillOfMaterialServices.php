<?php

namespace App\Services;

use App\Models\BillOfMaterial;

class BillOfMaterialServices
{
    public function handleStoreBillOfMaterial(array $data): BillOfMaterial
    {
        $syncData = $this->syncInputData(
            $data['material_id'], 
            $data['kuantitas'],
        );

        $billOfMaterial = BillOfMaterial::create($data);

        $billOfMaterial->produks()->attach($syncData);

        return $billOfMaterial;
    }

    public function handleUpdateBillOfMaterial(array $data, int $billOfMaterialId, PerkiraanHppServices $hppService): BillOfMaterial
    {
        $billOfMaterial = BillOfMaterial::findOrFail($billOfMaterialId);

        $syncData = $this->syncInputData(
            $data['material_id'], 
            $data['kuantitas'],
        );
        
        $billOfMaterial->update($data);

        $billOfMaterial->produks()->sync($syncData);

        if ($billOfMaterial->perkiraanHpp()->exists()) {
            $hppService->handleUpdatePerkiraanHpp(
                [
                    'biaya_material' => $hppService->hitungBiayaMaterial($billOfMaterial->id)
                ],
                $billOfMaterial->perkiraanHpp->id
            );
        }

        return $billOfMaterial;
    }

    public function handleDestroyBillOfMaterial($billOfMaterialId): BillOfMaterial
    {
        $billOfMaterial = BillOfMaterial::findOrFail($billOfMaterialId);

        if ($billOfMaterial->perkiraanHpp()->exists()) {
            $billOfMaterial->perkiraanHpp()->delete();
        }

        $billOfMaterial->produks()->detach();

        $billOfMaterial->delete();

        return $billOfMaterial;
    }

    private function syncInputData(array $materialIds, array $kuantitas): array
    {
        $syncData = [];

        for ($i = 0; $i < count($materialIds); $i++) {
            if (array_key_exists($materialIds[$i], $syncData)) {
                $syncData[$materialIds[$i]]['kuantitas'] += $kuantitas[$i];

                continue;
            }

            $syncData[$materialIds[$i]] = [
                'kuantitas' => $kuantitas[$i]
            ];
        }

        return $syncData;
    }
}