<?php

namespace App\Services;

use App\Models\BatalOrder;
use App\Models\Jurnal;
use App\Models\SalesOrder;

class BatalOrderServices
{
    public function handleStoreBatalOrder(array $data, int $userId, int $salesOrderId): BatalOrder
    {
        $batalOrder = BatalOrder::create($data + [
            'no_nota' => $this->createNomorNota(),
            'user_id' => $userId,
            'sales_order_id' => $salesOrderId,
        ]);

        $this->handleUpdateStatusSalesOrder($salesOrderId);

        $this->handleUpdateStatusOrder($salesOrderId);

        $this->handleJurnalAkuntansi($batalOrder->id);
        
        return $batalOrder;
    }

    private function handleUpdateStatusSalesOrder(int $salesOrderId): void
    {
        $salesOrder = SalesOrder::findOrFail($salesOrderId);

        $salesOrder->update([
            'status_nota' => 'cancel',
            'status_pembayaran' => 'cancel',
        ]);
    }

    private function handleUpdateStatusOrder(int $salesOrderId): void
    {
        $salesOrder = SalesOrder::findOrFail($salesOrderId);

        $salesOrder->produks()
            ->newPivotStatement()
            ->where('sales_order_id', $salesOrder->id)
            ->update(['status_order' => 'cancel']);
    }

    private function handleJurnalAkuntansi(int $batalOrderId): void
    {
        $batalOrder = BatalOrder::with(['salesOrder'])->findOrFail($batalOrderId);

        $jurnal = Jurnal::create([
            'tanggal' => now(),
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'pembatalan',
            'nomor_bukti' => $batalOrder->no_nota,
            'keterangan' => "Mencatat pembatalan order pelanggan. {$batalOrder->keterangan}"
        ]);

        $syncData = [
            '401' => [//Penjualan
                'debet' => $batalOrder->salesOrder->total,
                'kredit' => 0
            ],
            '103' => [//Piutang
                'debet' => 0,
                'kredit' => $batalOrder->salesOrder->grand_total
            ]
        ];

        if ($batalOrder->salesOrder->diskon) {
            $syncData['402'] = [
                'debet' => 0,
                'kredit' => $batalOrder->salesOrder->diskon
            ];
        }

        $jurnal->akuns()->attach($syncData);
    }

    private function createNomorNota(): string
    {
        $date = now()->format('Y-m-d');
        
        $record = BatalOrder::latest()->first();

        if (! $record) {
            return "OB/{$date}/1";
        }

        $expNum = explode('/', $record->no_nota);

        $number = $expNum[2] +1;

        return "OB/{$date}/{$number}";
    }
}