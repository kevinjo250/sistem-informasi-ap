<?php

namespace App\Services;

use App\Models\ProdukSalesOrder;
use App\Models\SalesOrder;
use App\Models\SuratPerintahKerja;

class SuratPerintahKerjaServices
{
    public function handleStoreSuratPerintahKerja(array $data): SuratPerintahKerja
    {
        $produkSalesOrder = ProdukSalesOrder::findOrFail($data['produk_sales_order_id']);

        $suratPerintahKerja = SuratPerintahKerja::create($data + [
            'no_nota' => $this->createNomorNota()
        ]);

        $this->handleUpdateStatusOrder($suratPerintahKerja->produk_sales_order_id);

        $this->handleUpdateStatusNota($produkSalesOrder->sales_order_id);

        return $suratPerintahKerja;
    }

    public function handleUpdateSuratPerintahKerja(array $data, int $suratPerintahKerjaId): SuratPerintahKerja
    {
        $suratPerintahKerja = SuratPerintahKerja::findOrFail($suratPerintahKerjaId);
        
        $suratPerintahKerja->update($data);

        $this->handleUpdateStatusOrder($suratPerintahKerja->produk_sales_order_id);

        $this->handleUpdateStatusNota($suratPerintahKerja->produkSalesOrder->sales_order_id);

        return $suratPerintahKerja;
    }

    public function handleDestroySuratPerintahKerja(int $suratPerintahKerjaId): SuratPerintahKerja
    {
        $suratPerintahKerja = SuratPerintahKerja::findOrFail($suratPerintahKerjaId);

        $suratPerintahKerja->delete();

        $this->handleUpdateStatusOrder($suratPerintahKerja->produk_sales_order_id);

        $this->handleUpdateStatusNota($suratPerintahKerja->produkSalesOrder->sales_order_id);

        return $suratPerintahKerja;
    }

    private function handleUpdateStatusNota(int $salesOrderId): void
    {
        $sumPendingOrder = ProdukSalesOrder::where([
            ['sales_order_id', $salesOrderId],
            ['status_order', 'pending']
        ])->count();

        if(! $sumPendingOrder) {
            SalesOrder::findOrFail($salesOrderId)->update([
                'status_nota' => 'on going'
            ]);

            return;
        }

        SalesOrder::findOrFail($salesOrderId)->update([
            'status_nota' => 'confirmed'
        ]);
    }

    private function handleUpdateStatusOrder(int $produkSalesOrderId): void
    {
        $produkSalesOrder = ProdukSalesOrder::findOrFail($produkSalesOrderId);
        
        $sumProcessedQty = SuratPerintahKerja::where('produk_sales_order_id', $produkSalesOrderId)
            ->sum('kuantitas');

        if ($sumProcessedQty == $produkSalesOrder->kuantitas) {
            $produkSalesOrder->update(['status_order' => 'process']);

            return;
        }

        if ($sumProcessedQty < $produkSalesOrder->kuantitas) {
            $produkSalesOrder->update(['status_order' => 'pending']);

            return;
        }
    }

    private function createNomorNota(): string
    {
        $date = now()->format('Y-m-d');

        $record = SuratPerintahKerja::latest()->first();

        if (! $record) {
            return "SPK/{$date}/1";
        }

        $expNum = explode('/', $record->no_nota);
        
        $number = $expNum[2] +1;

        return "SPK/{$date}/{$number}";
    }
}