<?php

namespace App\Services;

use App\Models\Jurnal;
use App\Models\NotaPelunasanSupplier;
use App\Models\PemesananMaterial;

class NotaPelunasanSupplierService
{
    public function handleStoreNotaPelunasanSupplier(array $data, int $userId, int $pemesananMaterialId): NotaPelunasanSupplier
    {
        $notaPelunasanSupplier = NotaPelunasanSupplier::create($data + [
            'no_nota' => $this->createNomorNota(),
            'user_id' => $userId,
            'pemesanan_material_id' => $pemesananMaterialId,
        ]);

        $this->handleJurnalAkuntansi($notaPelunasanSupplier->id, 'store');

        $this->handleUpdateStatusPembayaranPemesananMaterial($pemesananMaterialId);

         return $notaPelunasanSupplier;
    }

    public function handleUpdateNotaPelunasanSupplier(array $data, int $notaPelunasanSupplierId): NotaPelunasanSupplier
    {
        $notaPelunasanSupplier = NotaPelunasanSupplier::findOrFail($notaPelunasanSupplierId);

        $notaPelunasanSupplier->update($data);

        $this->handleJurnalAkuntansi($notaPelunasanSupplierId, 'update');

        $this->handleUpdateStatusPembayaranPemesananMaterial($notaPelunasanSupplier->pemesanan_material_id);

        return $notaPelunasanSupplier;
    }

    public function handleDestroyNotaPelunasanSupplier(int $notaPelunasanSupplierId): NotaPelunasanSupplier
    {
        $notaPelunasanSupplier = NotaPelunasanSupplier::findOrFail($notaPelunasanSupplierId);

        $this->handleJurnalAkuntansi($notaPelunasanSupplierId, 'destroy');

        $notaPelunasanSupplier->delete();

        $this->handleUpdateStatusPembayaranPemesananMaterial($notaPelunasanSupplier->pemesanan_material_id);

        return $notaPelunasanSupplier;
    }

    private function handleUpdateStatusPembayaranPemesananMaterial(int $pemesananMaterialId): void
    {
        $pemesananMaterial = PemesananMaterial::findOrFail($pemesananMaterialId);

        $totalPelunasan = NotaPelunasanSupplier::where('pemesanan_material_id', $pemesananMaterialId)
            ->sum('nominal_pelunasan');

        if ($totalPelunasan == 0) {
            $pemesananMaterial->update(['status_pembayaran' => 'unpaid']);

            return;
        }

        if ($totalPelunasan < $pemesananMaterial->grand_total) {
            $pemesananMaterial->update(['status_pembayaran' => 'partially paid']);

            return;
        }

        $pemesananMaterial->update(['status_pembayaran' => 'paid']);
    }

    private function handleJurnalAkuntansi(int $notaPelunasanSupplierId, string $type): void
    {
        $notaPelunasanSupplier = NotaPelunasanSupplier::findOrFail($notaPelunasanSupplierId);

        $syncData = [
            '201' => [
                'debet' => $notaPelunasanSupplier->nominal_pelunasan,
                'kredit' => 0
            ]
        ];

        if ($notaPelunasanSupplier->cara_bayar == 'Tunai') {
            $syncData['101'] = [
                'debet' => 0,
                'kredit' => $notaPelunasanSupplier->nominal_pelunasan
            ];
        }

        if ($notaPelunasanSupplier->cara_bayar == 'Transfer') {
            $syncData['102'] = [
                'debet' => 0,
                'kredit' => $notaPelunasanSupplier->nominal_pelunasan
            ];
        }
        
        if ($type == 'store') {
            $jurnal = Jurnal::create([
                'tanggal' => now(),
                'jenis_jurnal' => 'JU',
                'jenis_transaksi' => 'pelunasan supplier',
                'nomor_bukti' => $notaPelunasanSupplier->no_nota,
                'keterangan' => "Mencatat pelunasan tagihan kepada supplier. {$notaPelunasanSupplier->keterangan}"
            ]);

            $jurnal->akuns()->attach($syncData);

            return;
        }

        $jurnal = Jurnal::where('nomor_bukti', $notaPelunasanSupplier->no_nota)
            ->firstOrFail();

        if ($type == 'destroy') {
            $jurnal->akuns()->detach();

            $jurnal->delete();

            return;
        }

        if ($type == 'update') {
            $jurnal->update([
                'tanggal' => now(), 
                'keterangan' => empty($notaPelunasanSupplier->keterangan) ? 'Mencatat pelunasan tagihan kepada supplier.' : $notaPelunasanSupplier->keterangan
            ]);

            $jurnal->akuns()->sync($syncData);

            return;
        }
    }

    private function createNomorNota(): string
    {
        $date = now()->format('Y-m-d');
        
        $record = NotaPelunasanSupplier::latest()->first();

        if (! $record) {
            return "NPS/{$date}/1";
        }

        $expNum = explode('/', $record->no_nota);

        $number = $expNum[2] +1;
        
        return "NPS/{$date}/{$number}";
    }
}