<?php

namespace App\Services;

use App\Models\Akun;
use App\Models\Jurnal;
use App\Models\Periode;
use Illuminate\Database\Eloquent\Collection;

class JurnalServices
{
    public function handleStoreJurnal(array $data): Jurnal
    {
        $syncData = $this->syncInputData(
            $data['akun_id'],
            $data['debet'],
            $data['kredit']
        );

        $this->checkDebetAndKredit($syncData);

        $jurnal = Jurnal::create($data + ['jenis_jurnal' => 'JU']);

        $jurnal->akuns()->attach($syncData);

        return $jurnal;
    }

    public function handleFindJurnal(array $data): Collection
    {
        $periode = explode("-", $data['periode_akuntansi']);

        $jenisTransaksi = $data['jenis_transaksi'];

        $jurnals = Jurnal::with(['akuns'])
            ->when($jenisTransaksi != 'all', function($query) use($jenisTransaksi) {
                $query->where('jenis_transaksi', $jenisTransaksi);
            })
            ->whereMonth('tanggal', $periode[1])
            ->whereYear('tanggal', $periode[0])
            ->get();

        return $jurnals;
    }

    public function handleClosingJurnal(): void
    {
        $periode = explode('-', date('Y-m'));

        $jurnals = Jurnal::where('jenis_jurnal', 'JT')
            ->whereMonth('tanggal', $periode[1])
            ->whereYear('tanggal', $periode[0])
            ->get();

        if ($jurnals->count()) {
            throw new \Exception('Tidak dapat membuat jurnal penutup!');
        }

        $akuns = $this->handleGetAkuns($periode);

        $totalPrive = $this->totalPrive($akuns);

        $totalPendapatan = $this->totalPendapatan($akuns);

        $totalDiskonPenjualan = $this->totalDiskonPenjualan($akuns);

        $totalHpp = $this->totalHpp($akuns);

        $totalBiayaOh = $this->totalBiayaOh($akuns);

        $this->handlePenutupanPendapatan($totalPendapatan);

        $this->handlePenutupanBiaya($totalDiskonPenjualan, $totalHpp, $totalBiayaOh);

        $this->handlePenutupanModalLabaRugi($totalPendapatan, $totalDiskonPenjualan, $totalHpp, $totalBiayaOh);

        $this->handlePenutupanModalPrive($totalPrive);

        $this->handleUpdateSaldoAkhirPeriode($akuns, $periode);

        $this->handleNewSaldoAkun($akuns, $periode);
    }

    private function syncInputData(array $akunIds, array $debets, array $kredits): array
    {
        $syncData = [];

        foreach ($akunIds as $key => $akunId) {
            $syncData[$akunId] = [
                'debet' => $debets[$key],
                'kredit' => $kredits[$key]
            ];
        }

        return $syncData;
    }

    private function checkDebetAndKredit(array $syncData): void
    {
        $totalDebet = 0;

        $totalKredit = 0;

        //check nominal debet dan kredit
        foreach ($syncData as $data) {
            if ($data['debet'] == $data['kredit']) {
                throw new \Exception('Nominal debet dan kredit tidak dapat sama!');
            }

            $totalDebet += $data['debet'];

            $totalKredit += $data['kredit'];
        }

        if ($totalDebet != $totalKredit) {
            throw new \Exception('Total debet dan kredit harus sama!');
        }
    }

    private function handleGetAkuns(array $periode): Collection
    {
        $akuns = Akun::with(['periodes' => function($query) use($periode) {
            $query->whereMonth('tanggal_awal', $periode[1])
                ->whereYear('tanggal_awal', $periode[0]);
        }])->get();

        return $akuns;
    }

    private function handlePenutupanPendapatan(int $totalPendapatan): void
    {
        $jurnal = Jurnal::create([
            'tanggal' => date('Y-m-t H:i:s'),
            'jenis_jurnal' => 'JT',
            'jenis_transaksi' => 'penutup',
            'nomor_bukti' => '-',
            'keterangan' => 'Penutupan - Pendapatan',
        ]);

        $jurnal->akuns()->attach([
            '401' => [
                'debet' => $totalPendapatan,
                'kredit' => 0
            ],
            '504' => [
                'debet' => 0,
                'kredit' => $totalPendapatan
            ]
        ]);
    }

    private function handlePenutupanBiaya(int $totalDiskonPenjualan, int $totalHpp, int $totalBiayaOh): void
    {
        $jurnal = Jurnal::create([
            'tanggal' => date('Y-m-t H:i:s'),
            'jenis_jurnal' => 'JT',
            'jenis_transaksi' => 'penutup',
            'nomor_bukti' => '-',
            'keterangan' => 'Penutupan - Biaya',
        ]);

        $jurnal->akuns()->attach([
            '504' => [
                'debet' => $totalDiskonPenjualan + $totalHpp + $totalBiayaOh,
                'kredit' => 0
            ],
            '402' => [
                'debet' => 0,
                'kredit' => $totalDiskonPenjualan
            ],
            '501' => [
                'debet' => 0,
                'kredit' => $totalHpp
            ],
            '502' => [
                'debet' => 0,
                'kredit' => $totalBiayaOh
            ]
        ]);
    }

    private function handlePenutupanModalLabaRugi(int $totalPendapatan, int $totalDiskonPenjualan, int $totalHpp, int $totalBiayaOh): void
    {
        $jurnal = Jurnal::create([
            'tanggal' => date('Y-m-t H:i:s'),
            'jenis_jurnal' => 'JT',
            'jenis_transaksi' => 'penutup',
            'nomor_bukti' => '-',
            'keterangan' => 'Penutupan - Modal dan Laba Rugi',
        ]);

        if ($totalPendapatan - ($totalDiskonPenjualan + $totalHpp + $totalBiayaOh) <= 0) {
            $jurnal->akuns()->attach([
                '301' => [
                    'debet' => abs($totalPendapatan - ($totalDiskonPenjualan + $totalHpp + $totalBiayaOh)),
                    'kredit' => 0
                ],
                '504' => [
                    'debet' => 0,
                    'kredit' => abs($totalPendapatan - ($totalDiskonPenjualan + $totalHpp + $totalBiayaOh))
                ]
            ]);

            return;
        }

        if ($totalPendapatan - ($totalDiskonPenjualan + $totalHpp + $totalBiayaOh) > 0) {
            $jurnal->akuns()->attach([            
                '504' => [
                    'debet' => $totalPendapatan - ($totalDiskonPenjualan + $totalHpp + $totalBiayaOh),
                    'kredit' => 0
                ],
                '301' => [
                    'debet' => 0,
                    'kredit' => $totalPendapatan - ($totalDiskonPenjualan + $totalHpp + $totalBiayaOh)
                ]
            ]);

            return;
        }
    }

    private function handlePenutupanModalPrive(int $totalPrive): void
    {
        $jurnal = Jurnal::create([
            'tanggal' => date('Y-m-t H:i:s'),
            'jenis_jurnal' => 'JT',
            'jenis_transaksi' => 'penutup',
            'nomor_bukti' => '-',
            'keterangan' => 'Penutupan - Modal dan Prive',
        ]);

        $jurnal->akuns()->attach([
            '301' => [
                'debet' => $totalPrive,
                'kredit' => 0
            ],
            '302' => [
                'debet' => 0,
                'kredit' => $totalPrive
            ]
        ]);
    }

    private function handleUpdateSaldoAkhirPeriode(Collection $akuns, array $periode): void
    {
        $akuns = $akuns->fresh(['periodes' => function($query) use($periode) {
            $query->whereMonth('tanggal_awal', $periode[1])
                ->whereYear('tanggal_awal', $periode[0]);
        }]);

        foreach ($akuns as $akun) {
            if ($akun->id != 504) {
                if ($akun->tipe_akun == 1) {
                    $akun->periodes()
                        ->newPivotStatement()
                        ->where('akun_id', $akun->id)
                        ->update([
                            'saldo_akhir' => $akun->periodes[0]->pivot->saldo_awal + $akun->sum_debet - $akun->sum_kredit
                    ]);

                    continue;
                }

                if ($akun->tipe_akun == 0) {
                    $akun->periodes()
                        ->newPivotStatement()
                        ->where('akun_id', $akun->id)
                        ->update([
                            'saldo_akhir' => $akun->periodes[0]->pivot->saldo_awal + $akun->sum_kredit - $akun->sum_debet
                    ]);

                    continue;
                }
            }
        }
    }

    private function handleNewSaldoAkun(Collection $akuns, array $periode): void
    {
        $akuns = $akuns->fresh(['periodes' => function($query) use($periode) {
            $query->whereMonth('tanggal_awal', $periode[1])
                ->whereYear('tanggal_awal', $periode[0]);
        }]);

        $syncData = [];

        foreach ($akuns as $akun) {
            if ($akun->id != 504) {
                $syncData[$akun->id] = [
                    'saldo_awal' => $akun->periodes[0]->pivot->saldo_akhir,
                    'saldo_akhir' => 0
                ];
            }
        }

        $periode = Periode::create([
            'tanggal_awal' => date("Y-m-01", strtotime('next month')),
            'tanggal_akhir' => date("Y-m-t", strtotime('next month'))
        ]);

        $periode->akuns()->attach($syncData);
    }

    private function totalPrive(Collection $akuns): int
    {
        $totalPrive = 0;

        foreach ($akuns as $akun) {
            if ($akun->id == 302) {
                $totalPrive = $akun->periodes[0]->pivot->saldo_awal + $akun->sum_debet - $akun->sum_kredit;

                break;
            }
        }

        return $totalPrive;
    }

    private function totalPendapatan(Collection $akuns): int
    {
        $totalPendapatan = 0;

        foreach ($akuns as $akun) {
            if ($akun->id == 401) {
                $totalPendapatan = $akun->periodes[0]->pivot->saldo_awal + $akun->sum_kredit - $akun->sum_debet;

                break;
            }
        }

        return $totalPendapatan;
    }

    private function totalDiskonPenjualan(Collection $akuns): int
    {
        $totalDiskonPenjualan = 0;

        foreach ($akuns as $akun) {
            if ($akun->id == 402) {
                $totalDiskonPenjualan = $akun->periodes[0]->pivot->saldo_awal + $akun->sum_debet - $akun->sum_kredit;

                break;
            }
        }

        return $totalDiskonPenjualan;
    }

    private function totalHpp(Collection $akuns): int
    {
        $totalHpp = 0;

        foreach ($akuns as $akun) {
            if ($akun->id == 501) {
                $totalHpp = $akun->periodes[0]->pivot->saldo_awal + $akun->sum_debet - $akun->sum_kredit;

                break;
            }
        }

        return $totalHpp;
    }

    private function totalBiayaOh(Collection $akuns): int
    {
        $totalBiayaOh = 0;

        foreach ($akuns as $akun) {
            if ($akun->id == 502) {
                $totalBiayaOh = $akun->periodes[0]->pivot->saldo_awal + $akun->sum_debet - $akun->sum_kredit;

                break;
            }
        }

        return $totalBiayaOh;
    }
}