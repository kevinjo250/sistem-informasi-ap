<?php

namespace App\Services;

use App\Models\Jurnal;
use App\Models\Produk;
use App\Models\SalesOrder;

class SalesOrderServices
{
    public function handleStoreSalesOrder(array $data, int $userId): SalesOrder
    {
        $syncData = $this->syncInputData(
            $data['produk_id'], 
            $data['kuantitas']
        );

        $salesOrder = SalesOrder::create($data + [
            'no_nota' => $this->createNomorNota(),
            'user_id' => $userId,
            'total' => $this->calculateTotal($syncData),
        ]);

        $salesOrder->produks()->attach($syncData);

        return $salesOrder;
    }

    public function handleUpdateSalesOrder(array $data, int $salesOrderId): SalesOrder
    {
        $salesOrder = SalesOrder::findOrFail($salesOrderId);

        $syncData = $this->syncInputData(
            $data['produk_id'], 
            $data['kuantitas']
        );

        $salesOrder->update($data + [            
            'total' => $this->calculateTotal($syncData),
        ]);

        $salesOrder->produks()->sync($syncData);

        return $salesOrder;
    }

    public function handleDestroySalesOrder(int $salesOrderId): SalesOrder
    {
        $salesOrder = SalesOrder::findOrFail($salesOrderId);

        if ($salesOrder->status_nota != 'draft') {
            throw new \Exception('Sales order gagal dihapus!');
        }

        $salesOrder->produks()->detach();

        $salesOrder->delete();

        return $salesOrder;
    }

    public function handleConfirmSalesOrder(int $salesOrderId): SalesOrder
    {
        $salesOrder = SalesOrder::findOrFail($salesOrderId);

        $salesOrder->update(['status_nota' => 'confirmed']);

        $this->handleJurnalAkuntansi($salesOrderId);

        return $salesOrder;
    }

    private function handleJurnalAkuntansi(int $salesOrderId): void
    {
        $salesOrder = SalesOrder::findOrFail($salesOrderId);

        $jurnal = Jurnal::create([
            'tanggal' => now(),
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'penjualan',
            'nomor_bukti' => $salesOrder->no_nota,
            'keterangan' => "Mencatat transaksi penjualan kepada pelanggan. {$salesOrder->keterangan}"
        ]);

        $syncData = [
            '103' => [//Piutang
                'debet' => $salesOrder->grand_total,
                'kredit' => 0
            ],
            '401' => [//Penjualan
                'debet' => 0,
                'kredit' => $salesOrder->total
            ]
        ];

        if ($salesOrder->diskon) {
            $syncData['402'] = [
                'debet' => $salesOrder->diskon,
                'kredit' => 0
            ];
        }

        $jurnal->akuns()->attach($syncData);
    }

    private function createNomorNota(): string
    {
        $date = now()->format('Y-m-d');

        $record = SalesOrder::latest()->first();

        if (! $record) {
            return "SO/{$date}/1";
        }

        $expNum = explode('/', $record->no_nota);

        $number =  $expNum[2] +1;

        return "SO/{$date}/{$number}";
    }

    private function calculateTotal(array $syncData): int
    {
        $total = 0;

        foreach ($syncData as $data) {
            $total  += $data['harga'] * $data['kuantitas'];
        }

        return $total;
    }

    private function syncInputData(array $produkIds, array $kuantitas): array
    {
        $listProduks = Produk::onlyFinishedProduct()->get(['id', 'harga_jual']);

        $syncData = [];

        for ($i = 0; $i < count($produkIds); $i++) {
            if (array_key_exists($produkIds[$i], $syncData)) {
                $syncData[$produkIds[$i]]['kuantitas'] +=  $kuantitas[$i];

                continue;
            }
            
            $produk = $listProduks->where('id', $produkIds[$i])->firstOrFail();

            $syncData[$produkIds[$i]] = [
                'kuantitas' => $kuantitas[$i],
                'harga' => $produk->harga_jual,
            ];
        }

        return $syncData;
    }
}