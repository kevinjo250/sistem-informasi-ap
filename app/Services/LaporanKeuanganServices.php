<?php

namespace App\Services;

use App\Models\Akun;
use Illuminate\Database\Eloquent\Collection;

class LaporanKeuanganServices
{
    public function handleRetriveAkuns(array $data): Collection
    {
        $periode = explode("-", $data['periode_akuntansi']);

        $akuns = Akun::with([
            'periodes' => function($query) use($periode) {
                $query->whereMonth('tanggal_awal', $periode[1])
                    ->whereYear('tanggal_awal', $periode[0]);
            },
            'jurnals' => function($query) use($periode) {
                $query->where('jenis_jurnal', 'JU')
                    ->whereMonth('tanggal', $periode[1])
                    ->whereYear('tanggal', $periode[0]);
        }])->orderBy('urutan_akun', 'asc')->get();

        return $akuns;
    }

    public function hitungTotalPendapatan(Collection $akuns): int
    {
        $totalPendapatan = 0;

        foreach ($akuns as $akun) {
            if ($akun->id == 401) {
                $totalPendapatan += $akun->periodes[0]->pivot->saldo_awal + $akun->sum_kredit - $akun->sum_debet;

                break;
            }
        }

        return $totalPendapatan;
    }

    public function hitungTotalBiaya(Collection $akuns): int
    {
        $totalBiaya = 0;

        foreach ($akuns as $akun) {
            if ($akun->urutan_akun == 5 || $akun->id == 402) {
                $totalBiaya += $akun->periodes[0]->pivot->saldo_awal + $akun->sum_debet - $akun->sum_kredit;
            }
        }

        return $totalBiaya;
    }

    public function getTotalModal(Collection $akuns): int
    {
        $modal = 0;

        foreach ($akuns as $akun) {
            if ($akun->id == 301) {
                $modal = $akun->periodes[0]->pivot->saldo_awal + $akun->sum_kredit - $akun->sum_debet;

                break;
            }
        }

        return $modal;
    }

    public function getTotalPrive(Collection $akuns): int
    {
        $prive = 0;

        foreach ($akuns as $akun) {
            if ($akun->id == 302) {
                $prive = $akun->periodes[0]->pivot->saldo_awal + $akun->sum_debet - $akun->sum_kredit;

                break;
            }
        }

        return $prive;
    }

    public function getTotalAktiva(Collection $akuns): int
    {
        $totalAktiva = 0;
        
        foreach ($akuns as $akun) {
            if ($akun->urutan_akun == 1) {
                $totalAktiva += $akun->periodes[0]->pivot->saldo_awal + $akun->sum_debet - $akun->sum_kredit;
            }
        }
        
        return $totalAktiva;
    }

    public function getTotalKewajiban(Collection $akuns): int
    {
        $totalKewajiban = 0;

        foreach ($akuns as $akun) {
            if ($akun->urutan_akun == 2) {
                $totalKewajiban += $akun->periodes[0]->pivot->saldo_awal + $akun->sum_kredit - $akun->sum_debet;
            }
        }

        return $totalKewajiban;
    }
}