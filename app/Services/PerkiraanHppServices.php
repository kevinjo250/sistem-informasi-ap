<?php

namespace App\Services;

use App\Models\BillOfMaterial;
use App\Models\PerkiraanHpp;

class PerkiraanHppServices
{
    public function handleStorePerkiraanHpp(array $data, int $billOfMaterialId): PerkiraanHpp
    {
        $perkiraanHpp = PerkiraanHpp::create($data + [
            'bill_of_material_id' => $billOfMaterialId,
            'biaya_material' => $this->hitungBiayaMaterial($billOfMaterialId),
        ]);

        return $perkiraanHpp;
    }

    public function handleUpdatePerkiraanHpp(array $data, int $perkiraanHppId): PerkiraanHpp
    {
        $perkiraanHpp = PerkiraanHpp::findOrFail($perkiraanHppId);

        $perkiraanHpp->update($data);

        return $perkiraanHpp;
    }

    public function handleDestroyPerkiraanHpp(int $perkiraanHppId): PerkiraanHpp
    {
        $perkiraanHpp = PerkiraanHpp::findOrfail($perkiraanHppId);

        $perkiraanHpp->delete();

        return $perkiraanHpp;
    }

    public function hitungBiayaMaterial($billOfMaterialId): int
    {
        $billOfMaterial = BillOfMaterial::findOrFail($billOfMaterialId);

        $biayaMaterial = 0;

        foreach ($billOfMaterial->produks as $detail) {
            $biayaMaterial += $detail->pivot->kuantitas * $detail->harga_pokok;
        }

        return $biayaMaterial;
    }
}