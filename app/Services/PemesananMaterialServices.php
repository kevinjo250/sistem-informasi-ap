<?php

namespace App\Services;

use App\Models\PemesananMaterial;
use App\Models\User;
use App\Notifications\PemesananMaterialConfirmedNotification;
use App\Notifications\PemesananMaterialCreatedNotification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class PemesananMaterialServices
{
    public function handleStorePemesananMaterial(array $data, int $userId): PemesananMaterial
    {
        $syncData = $this->syncInputData(
            $data['produk_id'], 
            $data['kuantitas'], 
            $data['harga'],
        );

        $pemesananMaterial = PemesananMaterial::create($data + [
            'no_nota' => $this->createNomorNota(),
            'total' => $this->calculateTotal($syncData),
            'user_id' => $userId,
        ]);

        $pemesananMaterial->produks()->attach($syncData);

        $users = User::whereIn('jabatan_id', [1, 2])->get();
        
        Notification::sendNow($users, new PemesananMaterialCreatedNotification($pemesananMaterial));

        return $pemesananMaterial;
    }

    public function handleUpdatePemesananMaterial(array $data, int $pemesananMaterialId): PemesananMaterial
    {
        $syncData = $this->syncInputData(
            $data['produk_id'], 
            $data['kuantitas'], 
            $data['harga'],
        );

        $pemesananMaterial = PemesananMaterial::findOrFail($pemesananMaterialId);

        $pemesananMaterial->update($data + [            
            'total' => $this->calculateTotal($syncData),
        ]);

        $pemesananMaterial->produks()->sync($syncData);

        return $pemesananMaterial;
    }

    public function handleDestroyPemesananMaterial(int $pemesananMaterialId): PemesananMaterial
    {
        $pemesananMaterial = PemesananMaterial::findOrFail($pemesananMaterialId);

        if ($pemesananMaterial->penerimaanMaterials()->exists() || $pemesananMaterial->notaPelunasanSuppliers()->exists()) {
            throw new \Exception('Pemesanan material gagal dihapus!');
        }

        $pemesananMaterial->produks()->detach();

        $pemesananMaterial->delete();

        DB::table('notifications')
            ->where('type', 'App\Notifications\PemesananMaterialCreatedNotification')
            ->where('data->no_nota', $pemesananMaterial->no_nota)
            ->delete();

        return $pemesananMaterial;
    }

    public function handleConfirmStatusNota(int $pemesananMaterialId): PemesananMaterial
    {
        $pemesananMaterial = PemesananMaterial::findOrFail($pemesananMaterialId);

        if (! $pemesananMaterial->produks()->wherePivot('kuantitas', 0)->get()->isEmpty()) {
            throw new \Exception('Kuantitas material pesanan minimal 1!');
        }

        $pemesananMaterial->update(['status_nota' => 'approved']);

        $pemesananMaterial->produks()
            ->newPivotStatement()
            ->where('pemesanan_material_id', $pemesananMaterial->id)
            ->update(['status_material' => 'approved']);

        $users = User::whereIn('jabatan_id', [1, 3])->get();

        Notification::send($users, new PemesananMaterialConfirmedNotification($pemesananMaterial));

        return $pemesananMaterial;
    }

    public function handleOpenStatusNota(int $pemesananMaterialId): PemesananMaterial
    {
        $pemesananMaterial = PemesananMaterial::findOrFail($pemesananMaterialId);

        $pemesananMaterial->update(['status_nota' => 'open']);

        $pemesananMaterial->produks()
            ->newPivotStatement()
            ->where('pemesanan_material_id', $pemesananMaterial->id)
            ->update(['status_material' => 'open']);

        return $pemesananMaterial;
    }

    private function createNomorNota(): string
    {
        $date = now()->format('Y-m-d');
        
        $record = PemesananMaterial::latest()->first();

        if (! $record) {
            return "NPM/{$date}/1";
        }

        $expNum = explode('/', $record->no_nota);

        $number = $expNum[2] +1;

        return "NPM/{$date}/{$number}";
    }

    private function syncInputData(array $produkIds, array $kuantitas, array $hargas): array
    {
        $syncData = [];

        for ($i = 0; $i < count($produkIds); $i++) {
            if (array_key_exists($produkIds[$i], $syncData)) {
                $syncData[$produkIds[$i]]['kuantitas'] +=  $kuantitas[$i];

                continue;
            }
            
            $syncData[$produkIds[$i]] = [
                'kuantitas' => $kuantitas[$i],
                'harga' => $hargas[$i],
            ];
        }

        return $syncData;
    }

    private function calculateTotal(array $syncData): int
    {
        $total = 0;

        foreach ($syncData as $data) {
            $total += $data['harga'] * $data['kuantitas'];
        }

        return $total;
    }
}