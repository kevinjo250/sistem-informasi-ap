<?php

namespace App\Services;

use App\Models\Jurnal;
use App\Models\PemesananMaterial;
use App\Models\PenerimaanMaterial;

class PenerimaanMaterialServices
{
    public function handleStorePenerimaanMaterial(array $data, int $userId, int $pemesananMaterialId): PenerimaanMaterial
    {
        $syncData = $this->syncInputData(
            $data['produk_id'], 
            $data['kuantitas']
        );

        if (! $syncData) {
            throw new \Exception('Semua material yang diterima tidak dapat berkuantitas 0!');
        }

        $penerimaanMaterial = PenerimaanMaterial::create($data + [
            'no_nota' => $this->createNomorNota(),
            'pemesanan_material_id' => $pemesananMaterialId,
            'user_id' => $userId,
        ]);

        $penerimaanMaterial->produks()->attach($syncData);

        $this->handleUpdateStatusMaterial($pemesananMaterialId);

        $this->handleUpdateStatusNota($penerimaanMaterial->pemesanan_material_id);

        $this->handleUpdateHargaPokokProduk($penerimaanMaterial->id, 'increase');

        $this->handleUpdateStokProduk($penerimaanMaterial->id, 'increase');

        return $penerimaanMaterial;
    }

    public function handleUpdatePenerimaanMaterial(array $data, int $penerimaanMaterialId): PenerimaanMaterial
    {
        $penerimaanMaterial = PenerimaanMaterial::findOrFail($penerimaanMaterialId);

        $syncData = $this->syncInputData(
            $data['produk_id'], 
            $data['kuantitas']
        );

        if (! $syncData) {
            throw new \Exception('Semua material yang diterima tidak dapat berkuantitas 0!');
        }

        $this->handleUpdateHargaPokokProduk($penerimaanMaterialId, 'decrease');

        $this->handleUpdateStokProduk($penerimaanMaterial->id, 'decrease');

        $penerimaanMaterial->update($data);

        $penerimaanMaterial->produks()->sync($syncData);

        $this->handleUpdateStatusMaterial($penerimaanMaterial->pemesanan_material_id);

        $this->handleUpdateStatusNota($penerimaanMaterial->pemesanan_material_id);

        $this->handleUpdateHargaPokokProduk($penerimaanMaterialId, 'increase');

        $this->handleUpdateStokProduk($penerimaanMaterial->id, 'increase');

        return $penerimaanMaterial;
    }

    public function handleDestroyPenerimaanMaterial(int $penerimaanMaterialId): PenerimaanMaterial
    {
        $penerimaanMaterial = PenerimaanMaterial::findOrFail($penerimaanMaterialId);

        $this->handleUpdateHargaPokokProduk($penerimaanMaterialId, 'decrease');

        $this->handleUpdateStokProduk($penerimaanMaterial->id, 'decrease');

        $penerimaanMaterial->produks()->detach();

        $penerimaanMaterial->delete();

        $this->handleUpdateStatusMaterial($penerimaanMaterial->pemesanan_material_id);

        $this->handleUpdateStatusNota($penerimaanMaterial->pemesanan_material_id);

        return $penerimaanMaterial;
    }

    private function createNomorNota(): string
    {
        $date = now()->format('Y-m-d');

        $record = PenerimaanMaterial::latest()->first();

        if (! $record) {
            return "NPS/{$date}/1";
        }

        $expNum = explode('/', $record->no_nota);

        $number = $expNum[2] +1;

        return "NPS/{$date}/{$number}";
    }

    public function handleRetrieveReceivedMaterial(int $pemesananMaterialId): array
    {
        $penerimaanMaterials = PenerimaanMaterial::with(['produks'])
            ->where('pemesanan_material_id', $pemesananMaterialId)
            ->get();

        $received = [];

        if (! count($penerimaanMaterials)) {
            return $received;
        }

        $pemesananMaterial = PemesananMaterial::with(['produks'])->findOrFail($pemesananMaterialId);

        foreach ($pemesananMaterial->produks as $detailPemesanan) {
            foreach ($penerimaanMaterials as $penerimaanMaterial) {
                foreach ($penerimaanMaterial->produks as $detailPenerimaan) {
                    if ($detailPemesanan->pivot->produk_id == $detailPenerimaan->pivot->produk_id) {
                        if (array_key_exists($detailPenerimaan->pivot->produk_id, $received)) {
                            $received[$detailPenerimaan->pivot->produk_id]['kuantitas'] += $detailPenerimaan->pivot->kuantitas;

                            continue;
                        }

                        $received[$detailPenerimaan->pivot->produk_id] = [
                            'kuantitas' => $detailPenerimaan->pivot->kuantitas,
                            'kuantitas_pesan' => $detailPemesanan->pivot->kuantitas
                        ];
                    }
                }
            }
        }

        return $received;
    }

    private function handleUpdateStatusMaterial(int $pemesananMaterialId): void
    {
        $pemesananMaterial = PemesananMaterial::with(['produks'])->findOrFail($pemesananMaterialId);

        $received = $this->handleRetrieveReceivedMaterial($pemesananMaterialId);

        foreach ($pemesananMaterial->produks as $detail) {
            if (array_key_exists($detail->pivot->produk_id, $received)) {
                if ($detail->pivot->kuantitas == $received[$detail->pivot->produk_id]['kuantitas']) {
                    $pemesananMaterial->produks()->updateExistingPivot($detail->pivot->produk_id, [
                        'status_material' => 'done',
                    ]);

                    continue;
                }
                
                if ($received[$detail->pivot->produk_id]['kuantitas'] < $detail->pivot->kuantitas) {
                    $pemesananMaterial->produks()->updateExistingPivot($detail->pivot->produk_id, [
                        'status_material' => 'partial',
                    ]);

                    continue;
                }
            }

            $pemesananMaterial->produks()->updateExistingPivot($detail->pivot->produk_id, [
                'status_material' => 'open',
            ]);
        }
    }

    private function handleUpdateStatusNota(int $pemesananMaterialId): void
    {
        $pemesananMaterial = PemesananMaterial::with([
            'produks' => function($query) {
                $query->where('status_material', 'open')
                    ->orWhere('status_material', 'partial');
        }])->findOrFail($pemesananMaterialId);

        if (! count($pemesananMaterial->produks)) {
            $pemesananMaterial->update(['status_nota' => 'done']);

            $this->handleJurnalAkuntansi($pemesananMaterialId, 'store');
            
            return;
        }

        $pemesananMaterial->update(['status_nota' => 'open']);

        $this->handleJurnalAkuntansi($pemesananMaterialId, 'destroy');
    }

    private function handleUpdateStokProduk(int $penerimaanMaterialId, string $type): void
    {
        $penerimaanMaterial = PenerimaanMaterial::with(['produks'])->findOrFail($penerimaanMaterialId);

        foreach ($penerimaanMaterial->produks as $produk) {
            if ($type == 'increase') {
                $produk->update([
                    'stok_produk' => $produk->stok_produk + $produk->pivot->kuantitas
                ]);

                continue;
            }

            if ($type == 'decrease') {
                $produk->update([
                    'stok_produk' => $produk->stok_produk - $produk->pivot->kuantitas
                ]);

                continue;
            }
        }
    }

    private function handleUpdateHargaPokokProduk(int $penerimaanMaterialId, string $type): void
    {
        $penerimaanMaterial = PenerimaanMaterial::with(['produks'])->findOrFail($penerimaanMaterialId);

        foreach ($penerimaanMaterial->produks as $detail) {
            $totalHargaMaterialLama = $detail->stok_produk * $detail->harga_pokok;

            foreach ($penerimaanMaterial->pemesananMaterial->produks as $pemesananMaterialProduk) {
                if ($detail->pivot->produk_id == $pemesananMaterialProduk->pivot->produk_id) {
                    $totalHargaMaterialBaru = $detail->pivot->kuantitas * $pemesananMaterialProduk->pivot->harga;

                    if ($type == 'increase') {
                        $hargaPokokBaru = ($totalHargaMaterialLama + $totalHargaMaterialBaru) / ($detail->stok_produk + $detail->pivot->kuantitas);
                    }
                    
                    if ($type == 'decrease') {
                        $hargaPokokBaru = ($totalHargaMaterialLama - $totalHargaMaterialBaru) / ($detail->stok_produk - $detail->pivot->kuantitas);
                    }
        
                    $detail->update(['harga_pokok' => $hargaPokokBaru]);

                    break;
                }
            }            
        } 
    }

    private function handleJurnalAkuntansi(int $pemesananMaterialId, string $type): void
    {
        $pemesananMaterial = PemesananMaterial::findOrFail($pemesananMaterialId);

        if ($type == 'store') {
            $jurnal = Jurnal::create([
                'tanggal' => now(),
                'jenis_jurnal' => 'JU',
                'jenis_transaksi' => 'pembelian',
                'nomor_bukti' => $pemesananMaterial->no_nota,
                'keterangan' => "Mencatat pembelian material kepada supplier. {$pemesananMaterial->keterangan}"
            ]);
    
            $jurnal->akuns()->attach([
                '104' => [//Sediaan
                    'debet' => $pemesananMaterial->grand_total,
                    'kredit' => 0
                ],
                '201' => [//Hutang
                    'debet' => 0,
                    'kredit' => $pemesananMaterial->grand_total
                ]
            ]);

            return;
        }

        if ($type == 'destroy') {
            $jurnal = Jurnal::with(['akuns'])
                ->where('nomor_bukti', $pemesananMaterial->no_nota)
                ->firstOrFail();

            $jurnal->akuns()->detach();

            $jurnal->delete();

            return;
        }
    }

    private function syncInputData(array $produkIds, array $kuantitas): array
    {
        $syncData = [];

        if (count(array_keys($kuantitas, 0)) == count($kuantitas)) {
            return $syncData;
        }

        for ($i = 0; $i < count($produkIds); $i++) {
            if ($kuantitas[$i] != 0) {
                $syncData[$produkIds[$i]] = [
                    'kuantitas' => $kuantitas[$i]
                ];
            }
        }

        return $syncData;
    }
}