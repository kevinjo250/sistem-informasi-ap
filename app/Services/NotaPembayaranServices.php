<?php

namespace App\Services;

use App\Models\Jurnal;
use App\Models\NotaPembayaran;
use App\Models\SalesOrder;

class NotaPembayaranServices
{
    public function handleStoreNotaPembayaran(array $data, int $salesOrderId, int $userId): NotaPembayaran
    {
        $notaPembayaran = NotaPembayaran::create($data + [
            'no_nota' => $this->createNomorNota(),
            'user_id' => $userId,
            'sales_order_id' => $salesOrderId,
        ]);

        $this->handleUpdateStatusPembayaranSalesOrder($salesOrderId);

        $this->handleJurnalAkuntansi($notaPembayaran->id, 'store');

        return $notaPembayaran;
    }

    public function handleUpdateNotaPembayaran(array $data, int $notaPembayaranId): NotaPembayaran
    {
        $notaPembayaran = NotaPembayaran::findOrFail($notaPembayaranId);

        $notaPembayaran->update($data);

        $this->handleUpdateStatusPembayaranSalesOrder($notaPembayaran->sales_order_id);

        $this->handleJurnalAkuntansi($notaPembayaranId, 'update');

        return $notaPembayaran;
    }

    public function handleDestroyNotaPembayaran(int $notaPembayaranId): NotaPembayaran
    {
        $notaPembayaran = NotaPembayaran::findOrFail($notaPembayaranId);

        $this->handleJurnalAkuntansi($notaPembayaranId, 'destroy');

        $notaPembayaran->delete();

        $this->handleUpdateStatusPembayaranSalesOrder($notaPembayaran->sales_order_id);

        return $notaPembayaran;
    }

    private function handleUpdateStatusPembayaranSalesOrder(int $salesOrderId): void
    {
        $salesOrder = SalesOrder::findOrFail($salesOrderId);

        $totalPembayaran = NotaPembayaran::where('sales_order_id', $salesOrderId)
            ->sum('nominal_bayar');

        if ($totalPembayaran == 0) {
            $salesOrder->update(['status_pembayaran' => 'unpaid']);

            return;
        }

        if ($totalPembayaran < $salesOrder->grand_total) {
            $salesOrder->update(['status_pembayaran' => 'partially paid']);

            return;
        }
        
        $salesOrder->update(['status_pembayaran' => 'paid']);
    }

    private function handleJurnalAkuntansi(int $notaPembayaranId, string $type): void
    {
        $notaPembayaran = NotaPembayaran::findOrFail($notaPembayaranId);

        $syncData = [];

        if ($notaPembayaran->cara_bayar == 'Tunai') {
            $syncData['101'] = [
                'debet' => $notaPembayaran->nominal_bayar,
                'kredit' => 0
            ];
        }
        
        if ($notaPembayaran->cara_bayar == 'Transfer') {
            $syncData['102'] = [
                'debet' => $notaPembayaran->nominal_bayar,
                'kredit' => 0
            ];
        }

        $syncData['103'] = [
            'debet' => 0,
            'kredit' => $notaPembayaran->nominal_bayar
        ];

        if ($type == 'store') {
            $jurnal = Jurnal::create([
                'tanggal' => now(),
                'jenis_jurnal' => 'JU',
                'jenis_transaksi' => 'pembayaran pelanggan',
                'nomor_bukti' => $notaPembayaran->no_nota,
                'keterangan' => "Mencatat pembayaran dari pelanggan. {$notaPembayaran->keterangan}"
            ]);

            $jurnal->akuns()->attach($syncData);

            return;
        }

        $jurnal = Jurnal::with(['akuns'])
            ->where('nomor_bukti', $notaPembayaran->no_nota)
            ->firstOrFail();

        if ($type == 'destroy') {
            $jurnal->akuns()->detach();

            $jurnal->delete();

            return;
        }

        if ($type == 'update') {
            $jurnal->update([
                'tanggal' => now(),
                'keterangan' => empty($notaPembayaran->keterangan) ? 'Mencatat pembayaran dari pelanggan.' : $notaPembayaran->keterangan
            ]);

            $jurnal->akuns()->sync($syncData);

            return;
        }
    }

    private function createNomorNota(): string
    {
        $date = now()->format('Y-m-d');
        
        $record = NotaPembayaran::latest()->first();

        if (! $record) {
            return "NPO/{$date}/1";
        }

        $expNum = explode('/', $record->no_nota);

        $number = $expNum[2] +1;

        return "NPO/{$date}/{$number}";
    }
}