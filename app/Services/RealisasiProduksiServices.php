<?php

namespace App\Services;

use App\Models\Jurnal;
use App\Models\Produk;
use App\Models\ProdukSalesOrder;
use App\Models\RealisasiProduksi;
use App\Models\SalesOrder;
use App\Models\SuratPerintahKerja;

class RealisasiProduksiServices
{
    public function handleStoreRealisasiProduksi(array $data, int $userId): RealisasiProduksi
    {
        $syncData = $this->syncInputData(
            $data['produk_id'], 
            $data['kuantitas_sisa'], 
            $data['kuantitas_rusak']
        );

        $realisasiProduksi = RealisasiProduksi::create($data + [
            'no_nota' => $this->createNomorNota(),
            'user_id' => $userId,
        ]);

        if (! empty($syncData)) {
            $realisasiProduksi->produks()->attach($syncData);
        }

        $this->handleUpdateStatusForAll(
            $realisasiProduksi->surat_perintah_kerja_id,
            $realisasiProduksi->suratPerintahKerja->produk_sales_order_id,
            $realisasiProduksi->suratPerintahKerja->produkSalesOrder->sales_order_id,
        );

        $this->handleUpdateProduk($realisasiProduksi->id, 'increase');

        $this->handleJurnalAkuntansi($realisasiProduksi->id, 'store');

        return $realisasiProduksi;
    }

    public function handleUpdateRealisasiProduksi(array $data, int $realisasiProduksiId): RealisasiProduksi
    {
        $realisasiProduksi = RealisasiProduksi::findOrFail($realisasiProduksiId);

        $this->handleUpdateProduk($realisasiProduksi->id, 'decrease');

        $syncData = $this->syncInputData(
            $data['produk_id'], 
            $data['kuantitas_sisa'], 
            $data['kuantitas_rusak']
        );

        $realisasiProduksi->update($data);

        if (! empty($syncData)) {
            $realisasiProduksi->produks()->sync($syncData);
        }

        $this->handleUpdateStatusForAll(
            $realisasiProduksi->surat_perintah_kerja_id,
            $realisasiProduksi->suratPerintahKerja->produk_sales_order_id,
            $realisasiProduksi->suratPerintahKerja->produkSalesOrder->sales_order_id,
        );

        $this->handleUpdateProduk($realisasiProduksi->id, 'increase');

        $this->handleJurnalAkuntansi($realisasiProduksiId, 'update');

        return $realisasiProduksi;
    }

    public function handleDestroyRealisasiProduksi(int $realisasiProduksiId): RealisasiProduksi
    {
        $realisasiProduksi = RealisasiProduksi::findOrFail($realisasiProduksiId);

        $this->handleUpdateProduk($realisasiProduksi->id, 'decrease');

        $this->handleJurnalAkuntansi($realisasiProduksiId, 'destroy');

        $realisasiProduksi->produks()->detach();

        $realisasiProduksi->delete();

        $this->handleUpdateStatusForAll(
            $realisasiProduksi->surat_perintah_kerja_id,
            $realisasiProduksi->suratPerintahKerja->produk_sales_order_id,
            $realisasiProduksi->suratPerintahKerja->produkSalesOrder->sales_order_id,
        );

        return $realisasiProduksi;
    }

    private function handleUpdateStatusForAll(int $suratPerintahKerjaId, int $produkSalesOrderId, int $salesOrderId): void
    {
        $this->handleUpdateStatusProduksiSuratPerintahKerja($suratPerintahKerjaId);

        $this->handleUpdateStatusOrderProdukSalesOrder($produkSalesOrderId);

        $this->handleUpdateStatusNotaSalesOrder($salesOrderId);
    }

    private function handleUpdateStatusProduksiSuratPerintahKerja(int $suratPerintahKerjaId): void
    {
        $suratPerintahKerja = SuratPerintahKerja::with(['realisasiProduksis'])
            ->findOrFail($suratPerintahKerjaId);

        if ($suratPerintahKerja->kuantitas == $suratPerintahKerja->realisasiProduksis->sum('kuantitas_selesai')) {
            $suratPerintahKerja->update(['status_produksi' => 'done']);

            return;
        }

        if ($suratPerintahKerja->kuantitas > $suratPerintahKerja->realisasiProduksis->sum('kuantitas_selesai')) {
            $suratPerintahKerja->update(['status_produksi' => 'in production']);

            return;
        }
    }

    private function handleUpdateStatusOrderProdukSalesOrder(int $produkSalesOrderId): void
    {
        $produkSalesOrder = ProdukSalesOrder::with([
            'suratPerintahKerjas' => function($query) {
                $query->where('status_produksi', 'done');
            }
        ])->findOrFail($produkSalesOrderId);

        if ($produkSalesOrder->kuantitas == $produkSalesOrder->suratPerintahKerjas->sum('kuantitas')) {
            $produkSalesOrder->update(['status_order' => 'done']);
            
            return;
        }

        if ($produkSalesOrder->kuantitas > $produkSalesOrder->suratPerintahKerjas->sum('kuantitas')) {
            $produkSalesOrder->update(['status_order' => 'process']);

            return;
        }
    }

    private function handleUpdateStatusNotaSalesOrder(int $salesOrderId): void
    {
        $salesOrder = SalesOrder::with([
            'produks' => function($query) {
                $query->where('status_order', 'process');
            }
        ])->findOrFail($salesOrderId);

        if (! $salesOrder->produks->count()) {
            $salesOrder->update(['status_nota' => 'done']);

            return;
        }

        $salesOrder->update(['status_nota' => 'on going']);
    }

    private function handleUpdateProduk(int $realisasiProduksiId, string $type): void
    {
        $realisasiProduksi = RealisasiProduksi::with(['suratPerintahKerja.produkSalesOrder.produk'])
            ->findOrFail($realisasiProduksiId);

        $produk = Produk::findOrFail($realisasiProduksi->suratPerintahKerja->produkSalesOrder->produk_id);
        
        $hppLama = $produk->stok_produk * $produk->harga_pokok;

        if ($type == 'increase') {
            $hppBaru = ($hppLama + $realisasiProduksi->realisasi_biaya) / ($produk->stok_produk + $realisasiProduksi->kuantitas_selesai);

            $produk->update([
                'stok_produk' => $produk->stok_produk + $realisasiProduksi->kuantitas_selesai,
                'harga_pokok' => $hppBaru,
            ]);

            return;
        }

        if ($type == 'decrease') {
            $hppBaru = empty($produk->stok_produk - $realisasiProduksi->kuantitas_selesai) ? 
                0 : 
                ($hppLama - $realisasiProduksi->realisasi_biaya) / ($produk->stok_produk - $realisasiProduksi->kuantitas_selesai);

            $produk->update([
                'stok_produk' => $produk->stok_produk - $realisasiProduksi->kuantitas_selesai,
                'harga_pokok' => $hppBaru,
            ]);

            return;
        }
    }

    private function handleJurnalAkuntansi(int $realisasiProduksiId, string $type): void
    {
        $realisasiProduksi = RealisasiProduksi::findOrFail($realisasiProduksiId);

        if ($type == 'store') {
            $jurnal = Jurnal::create([
                'tanggal' => now(),
                'jenis_jurnal' => 'JU',
                'jenis_transaksi' => 'produksi',
                'nomor_bukti' => $realisasiProduksi->no_nota,
                'keterangan' => 'Mencatat biaya gaji ke barang dalam proses.'
            ]);
    
            $jurnal->akuns()->attach([
                '105' => [//Sediaan Dalam Proses
                    'debet' => $realisasiProduksi->biaya_pekerja,
                    'kredit' => 0
                ],
                '202' => [//Hutang Gaji
                    'debet' => 0,
                    'kredit' => $realisasiProduksi->biaya_pekerja
                ]
            ]);

            $jurnal = Jurnal::create([
                'tanggal' => now(),
                'jenis_jurnal' => 'JU',
                'jenis_transaksi' => 'produksi',
                'nomor_bukti' => $realisasiProduksi->no_nota,
                'keterangan' => 'Mencatat biaya overhead ke hutang usaha.'
            ]);
    
            $jurnal->akuns()->attach([
                '502' => [//Biaya Overhead
                    'debet' => $realisasiProduksi->biaya_overhead,
                    'kredit' => 0
                ],
                '201' => [//Hutang Usaha
                    'debet' => 0,
                    'kredit' => $realisasiProduksi->biaya_overhead
                ]
            ]);
    
            $jurnal = Jurnal::create([
                'tanggal' => now(),
                'jenis_jurnal' => 'JU',
                'jenis_transaksi' => 'produksi',
                'nomor_bukti' => $realisasiProduksi->no_nota,
                'keterangan' => 'Mencatat biaya overhead ke barang dalam proses.'
            ]);
    
            $jurnal->akuns()->attach([
                '105' => [//Sediaan Dalam Proses
                    'debet' => $realisasiProduksi->biaya_overhead,
                    'kredit' => 0
                ],
                '502' => [//Biaya Overhead
                    'debet' => 0,
                    'kredit' => $realisasiProduksi->biaya_overhead
                ]
            ]);
    
            $jurnal = Jurnal::create([
                'tanggal' => now(),
                'jenis_jurnal' => 'JU',
                'jenis_transaksi' => 'produksi',
                'nomor_bukti' => $realisasiProduksi->no_nota,
                'keterangan' => 'Mencatat barang jadi yang ditransfer ke gudang barang jadi.'
            ]);
    
            $jurnal->akuns()->attach([
                '106' => [//Sediaan Jadi
                    'debet' => $realisasiProduksi->realisasi_biaya,
                    'kredit' => 0
                ],
                '105' => [//Sediaan Dalam Proses
                    'debet' => 0,
                    'kredit' => $realisasiProduksi->realisasi_biaya
                ]
            ]);

            return;
        }

        $jurnals = Jurnal::with(['akuns'])
            ->where('nomor_bukti', $realisasiProduksi->no_nota)
            ->get();

        if ($type == 'destroy') {
            foreach ($jurnals as $jurnal) {
                $jurnal->akuns()->detach();
    
                $jurnal->delete();
            }

            return;
        }

        if ($type == 'update') {
            foreach ($jurnals as $key => $jurnal) {
                $jurnal->update(['tanggal' => now()]);
    
                if ($key == 0) {
                    $jurnal->akuns()->attach([
                        '105' => [//Sediaan Dalam Proses
                            'debet' => $realisasiProduksi->biaya_pekerja,
                            'kredit' => 0
                        ],
                        '202' => [//Hutang Gaji
                            'debet' => 0,
                            'kredit' => $realisasiProduksi->biaya_pekerja
                        ]
                    ]);
    
                    continue;
                }
    
                if ($key == 1) {
                    $jurnal->akuns()->attach([
                        '502' => [//Biaya Overhead
                            'debet' => $realisasiProduksi->biaya_overhead,
                            'kredit' => 0
                        ],
                        '201' => [//Hutang Usaha
                            'debet' => 0,
                            'kredit' => $realisasiProduksi->biaya_overhead
                        ]
                    ]);
    
                    continue;
                }
    
                if ($key == 2) {
                    $jurnal->akuns()->sync([
                        '105' => [//Sediaan Dalam Proses
                            'debet' => $realisasiProduksi->biaya_overhead,
                            'kredit' => 0
                        ],
                        '502' => [//Biaya Overhead
                            'debet' => 0,
                            'kredit' => $realisasiProduksi->biaya_overhead
                        ]
                    ]);
    
                    continue;
                }
    
                if ($key == 3) {
                    $jurnal->akuns()->sync([
                        '106' => [//Sediaan Jadi
                            'debet' => $realisasiProduksi->realisasi_biaya,
                            'kredit' => 0
                        ],
                        '105' => [//Sediaan Dalam Proses
                            'debet' => 0,
                            'kredit' => $realisasiProduksi->realisasi_biaya
                        ]
                    ]);
    
                    continue;
                }
            }

            return;
        }
    }

    private function createNomorNota(): string
    {
        $date = now()->format('Y-m-d');
        
        $record = RealisasiProduksi::latest()->first();

        if (! $record) {
            return "RP/{$date}/1";
        }

        $expNum = explode('/', $record->no_nota);

        $number = $expNum[2] +1;

        return "RP/{$date}/{$number}";
    }

    private function syncInputData(array $produkIds, array $kuantitasSisa, array $kuantitasRusak): array
    {
        $syncData = [];
        
        for ($i = 0; $i < count($produkIds); $i++) {
            if (isset($produkIds[$i])) {
                if (array_key_exists($produkIds[$i], $syncData)) {
                    $syncData[$produkIds[$i]]['kuantitas_rusak'] +=  $kuantitasRusak[$i];

                    $syncData[$produkIds[$i]]['kuantitas_sisa'] +=  $kuantitasSisa[$i];

                    continue;
                }

                $syncData[$produkIds[$i]] = [
                    'kuantitas_rusak' => $kuantitasRusak[$i],
                    'kuantitas_sisa' => $kuantitasSisa[$i],
                ];
            }
        }

        return $syncData;
    }
}