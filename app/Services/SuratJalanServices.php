<?php

namespace App\Services;

use App\Models\Jurnal;
use App\Models\SalesOrder;
use App\Models\SuratJalan;
use App\Models\User;
use App\Notifications\SuratJalanCreatedNotification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class SuratJalanServices
{
    public function handleStoreSuratJalan(array $data, int $userId, int $salesOrderId): SuratJalan
    {
        $syncData = $this->syncInputData(
            $data['kuantitas'], 
            $salesOrderId
        );

        if (! $syncData) {
            throw new \Exception('Semua produk yang dikirim tidak dapat berkuantitas 0!');
        }

        $suratJalan = SuratJalan::create($data + [
            'no_nota' => $this->createNomorNota(),
            'user_id' => $userId,
            'sales_order_id' => $salesOrderId,
        ]);

        $suratJalan->produks()->attach($syncData);

        $this->handleUpdateStokProduk($suratJalan->id, 'decrease');

        $this->handleUpdateStatusOrder($salesOrderId);

        $this->handleJurnalAkuntansi($suratJalan->id, 'store');

        $users = User::whereIn('jabatan_id', [1, 9])->get();

        Notification::sendNow($users, new SuratJalanCreatedNotification($suratJalan));

        return $suratJalan;
    }

    public function handleUpdateSuratJalan(array $data, int $suratJalanId): SuratJalan
    {
        $suratJalan = SuratJalan::findOrFail($suratJalanId);

        $syncData = $this->syncInputData(
            $data['kuantitas'], 
            $suratJalan->sales_order_id
        );

        if (! $syncData) {
            throw new \Exception('Semua produk yang dikirim tidak dapat berkuantitas 0!');
        }

        $this->handleUpdateStokProduk($suratJalanId, 'increase');

        $suratJalan->update($data);

        $suratJalan->produks()->sync($syncData);

        $this->handleUpdateStokProduk($suratJalanId, 'decrease');

        $this->handleUpdateStatusOrder($suratJalan->sales_order_id);

        $this->handleJurnalAkuntansi($suratJalanId, 'store');

        return $suratJalan;
    }

    public function handleDestroySuratJalan(int $suratJalanId): SuratJalan
    {
        $suratJalan = SuratJalan::findOrFail($suratJalanId);

        $this->handleJurnalAkuntansi($suratJalanId, 'destroy');

        $this->handleUpdateStokProduk($suratJalanId, 'increase');

        $suratJalan->produks()->detach();

        $suratJalan->delete();

        $this->handleUpdateStatusOrder($suratJalan->sales_order_id);

        DB::table('notifications')
            ->where('type', 'App\Notifications\SuratJalanCreatedNotification')
            ->where('data->no_nota', $suratJalan->no_nota)
            ->delete();

        return $suratJalan;
    }
    
    private function createNomorNota(): string
    {
        $date = now()->format('Y-m-d');
        
        $record = SuratJalan::latest()->first();

        if (! $record) {
            return "SJ/{$date}/1";
        }

        $expNum = explode('/', $record->no_nota);
        
        $number = $expNum[2] +1;

        return "SJ/{$date}/{$number}";
    }

    public function handleRetrieveShippedProduct(int $salesOrderId): array
    {
        $suratJalans = SuratJalan::with(['produks'])
            ->where('sales_order_id', $salesOrderId)
            ->get();

        $shipped = [];

        if (! count($suratJalans)) {
            return $shipped;
        }

        $salesOrder = SalesOrder::with(['produks'])->findOrFail($salesOrderId);

        foreach ($salesOrder->produks as $detailSalesOrder) {
            foreach ($suratJalans as $suratJalan) {
                foreach ($suratJalan->produks as $detailSuratJalan) {
                    if ($detailSalesOrder->pivot->produk_id == $detailSuratJalan->pivot->produk_id) {
                        if (array_key_exists($detailSuratJalan->pivot->produk_id, $shipped)) {
                            $shipped[$detailSuratJalan->pivot->produk_id]['kuantitas'] += $detailSuratJalan->pivot->kuantitas;

                            continue;
                        }

                        $shipped[$detailSuratJalan->pivot->produk_id] = [
                            'kuantitas' => $detailSuratJalan->pivot->kuantitas,
                            'kuantitas_beli' => $detailSalesOrder->pivot->kuantitas
                        ];
                    }
                }
            }
        }

        return $shipped;
    }

    private function handleUpdateStatusOrder(int $salesOrderId): void
    {
        $salesOrder = SalesOrder::findOrFail($salesOrderId);

        $shipped = $this->handleRetrieveShippedProduct($salesOrderId);

        foreach ($salesOrder->produks as $detail) {
            if (array_key_exists($detail->pivot->produk_id, $shipped)) {
                if ($detail->pivot->kuantitas == $shipped[$detail->pivot->produk_id]['kuantitas']) {
                    $salesOrder->produks()->updateExistingPivot($detail->pivot->produk_id, [
                        'status_order' => 'shipped',
                    ]);

                    continue;
                }
                
                if ($shipped[$detail->pivot->produk_id]['kuantitas'] < $detail->pivot->kuantitas) {
                    $salesOrder->produks()->updateExistingPivot($detail->pivot->produk_id, [
                        'status_order' => 'partially shipped',
                    ]);

                    continue;
                }
            }

            $salesOrder->produks()->updateExistingPivot($detail->pivot->produk_id, [
                'status_order' => 'done',
            ]);
        }
    }

    private function handleUpdateStokProduk(int $suratJalanId, string $type): void
    {
        $suratJalan = SuratJalan::with(['produks'])->findOrFail($suratJalanId);

        foreach ($suratJalan->produks as $produk) {
            if ($type == 'increase') {
                $produk->update([
                    'stok_produk' => $produk->stok_produk + $produk->pivot->kuantitas
                ]);

                continue;
            }

            if ($type == 'decrease') {
                $produk->update([
                    'stok_produk' => $produk->stok_produk - $produk->pivot->kuantitas
                ]);

                continue;
            }
        }
    }

    private function handleJurnalAkuntansi(int $suratJalanId, string $type): void
    {
        $suratJalan = SuratJalan::findOrFail($suratJalanId);

        $syncData = [
            '501' => [
                'debet' => $suratJalan->total_hpp,
                'kredit' => 0
            ],
            '106' => [
                'debet' => 0,
                'kredit' => $suratJalan->total_hpp
            ]
        ];

        if ($type == 'store') {
            $jurnal = Jurnal::create([
                'tanggal' => now(),
                'jenis_jurnal' => 'JU',
                'jenis_transaksi' => 'pengiriman',
                'nomor_bukti' => $suratJalan->no_nota,
                'keterangan' => "Mencatat pengiriman produk ke pelanggan. {$suratJalan->keterangan}"
            ]);
    
            $jurnal->akuns()->attach($syncData);

            return;
        }

        $jurnal = Jurnal::with(['akuns'])
            ->where('nomor_bukti', $suratJalan->no_nota)
            ->firstOrFail();
        
        if ($type == 'destroy') {
            $jurnal->akuns()->detach();

            $jurnal->delete();

            return;
        }

        if ($type == 'update') {
            $jurnal->update(['tanggal' => now()]);

            $jurnal->akuns()->sync($syncData);

            return;
        }
    }

    private function syncInputData(array $kuantitas, int $salesOrderId): array
    {
        $syncData = [];

        if (count(array_keys($kuantitas, 0)) == count($kuantitas)) {
            return $syncData;
        }

        $salesOrder = SalesOrder::findOrFail($salesOrderId);

        $produkIds = $salesOrder->produks()
            ->wherePivot('status_order', '!=', 'shipped')
            ->pluck('produk_id');

        if (! empty($produkIds)) {
            $produkIds = $salesOrder->produks()->pluck('produk_id');
        }

        for ($i = 0; $i < count($produkIds); $i++) {
            $syncData[$produkIds[$i]] = [
                'kuantitas' => $kuantitas[$i]
            ];
        }

         return $syncData;
    }
}