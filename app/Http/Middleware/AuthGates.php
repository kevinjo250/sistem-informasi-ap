<?php

namespace App\Http\Middleware;

use App\Models\Jabatan;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class AuthGates
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user();

        if($user)
        {
            $jabatans = Jabatan::with(['otorisasis'])->get();

            $otorisasisArray = [];

            foreach($jabatans as $jabatan) {
                foreach($jabatan->otorisasis as $otorisasis) {
                    $otorisasisArray[$otorisasis->nama_otorisasi][] = $otorisasis->id;
                }
            }

            foreach($otorisasisArray as $nama_otorisasi => $otorisasis)
            {
                Gate::define($nama_otorisasi, function($user) use ($otorisasis){
                    return count(array_intersect($user->jabatan->otorisasis->pluck('id')->toArray(), $otorisasis)) > 0;
                });
            }
        }

        return $next($request);
    }
}
