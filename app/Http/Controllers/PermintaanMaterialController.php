<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePermintaanMaterialRequest;
use App\Models\BillOfMaterial;
use App\Models\PermintaanMaterial;
use App\Models\SuratPerintahKerja;
use App\Services\PermintaanMaterialServices;
use Illuminate\Support\Facades\Gate;

class PermintaanMaterialController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('permintaanMaterial access'), 403);
        
        $permintaanMaterials = PermintaanMaterial::with([
            'suratPerintahKerja.produkSalesOrder.produk', 
            'user'
        ])->get();

        return view('permintaanMaterial.index', compact('permintaanMaterials'));
    }

    public function create()
    {
        abort_if(Gate::denies('permintaanMaterial create'), 403);
        
        $suratPerintahKerjas = SuratPerintahKerja::doesnthave('permintaanMaterial')
            ->with(['produkSalesOrder'])
            ->get();

        return view('permintaanMaterial.create', compact('suratPerintahKerjas'));
    }

    public function store(StorePermintaanMaterialRequest $request, PermintaanMaterialServices $service)
    {
        $permintaanMaterial = $service->handleStorePermintaanMaterial(
            $request->validated(), 
            auth()->id()
        );

        return redirect()
            ->route('permintaanMaterials.show', $permintaanMaterial)
            ->with('success', 'Permintaan material berhasil dibuat!');
    }

    public function show(PermintaanMaterial $permintaanMaterial)
    {
        $permintaanMaterial->load([
            'suratPerintahKerja.produkSalesOrder.produk', 
            'produks'
        ]);

        return view('permintaanMaterial.detail', compact('permintaanMaterial'));
    }

    public function destroy(PermintaanMaterial $permintaanMaterial, PermintaanMaterialServices $service)
    {
        abort_if(Gate::denies('permintaanMaterial delete'), 403);
        
        try {
            $permintaanMaterial = $service->handleDestroyPermintaanMaterial($permintaanMaterial->id);
        } catch (\Exception $ex) {
            return redirect()
                ->route('permintaanMaterials.show', $permintaanMaterial)
                ->with('error', $ex->getMessage());
        }        

        return redirect()
            ->route('permintaanMaterials.index')
            ->with('success', 'Permintaan material berhasil dihapus!');
    }

    public function getBillOfMaterials($id)
    {
        $suratPerintahKerja = SuratPerintahKerja::with(['produkSalesOrder'])->findOrFail($id);

        $bom = BillOfMaterial::with(['produks'])
            ->where('produk_id', $suratPerintahKerja->produkSalesOrder->produk_id)
            ->get();
        
        return json_encode($bom);
    }

    public function confirm(PermintaanMaterial $permintaanMaterial, PermintaanMaterialServices $service)
    {
        abort_if(Gate::denies('permintaanMaterial confirm'), 403);
        
        try {
            $permintaanMaterial = $service->handleConfirmPermintaanMaterial($permintaanMaterial->id);
        } catch (\Exception $e) {
            return redirect()
                ->route('permintaanMaterials.show', $permintaanMaterial)
                ->with('error', $e->getMessage());
        }

        return redirect()
            ->route('permintaanMaterials.show', $permintaanMaterial)
            ->with('success', 'Permintaan material berhasil dikonfirmasi!');
    }
}
