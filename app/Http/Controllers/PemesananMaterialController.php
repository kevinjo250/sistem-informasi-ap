<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePemesananMaterialRequest;
use App\Http\Requests\UpdatePemesananMaterialRequest;
use App\Models\PemesananMaterial;
use App\Models\Produk;
use App\Models\Supplier;
use App\Services\PemesananMaterialServices;
use Illuminate\Support\Facades\Gate;

class PemesananMaterialController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('pemesananMaterial access'), 403);

        $pemesananMaterials = PemesananMaterial::with(['supplier'])->get();

        return view('pemesananMaterial.index', compact('pemesananMaterials'));
    }

    public function create()
    {
        abort_if(Gate::denies('pemesananMaterial create'), 403);

        $suppliers = Supplier::all(['id', 'nama_supplier', 'alamat_supplier', 'kota_supplier']);

        $produks = Produk::onlyMaterial()
            ->get(['id', 'nama_produk', 'ukuran_produk', 'warna_produk', 'harga_pokok', 'stok_produk'
        ]);

        return view('pemesananMaterial.create', compact('suppliers', 'produks'));
    }

    public function store(StorePemesananMaterialRequest $request, PemesananMaterialServices $service)
    {
        $pemesananMaterial = $service->handleStorePemesananMaterial(
            $request->validated(), 
            auth()->id(),
        );

        return redirect()
            ->route('pemesananMaterials.show', $pemesananMaterial)
            ->with('success', 'Pemesanan material berhasil disimpan!');
    }

    public function show(PemesananMaterial $pemesananMaterial)
    {
        $pemesananMaterial->load(['supplier', 'produks', 'notaPelunasanSuppliers']);

        return view('pemesananMaterial.detail', compact('pemesananMaterial'));
    }

    public function edit(PemesananMaterial $pemesananMaterial)
    {
        abort_if(Gate::denies('pemesananMaterial edit'), 403);

        $pemesananMaterial->load(['supplier', 'produks']);

        $suppliers = Supplier::all(['id', 'nama_supplier', 'alamat_supplier', 'kota_supplier']);

        $produks = Produk::onlyMaterial()
            ->get(['id', 'nama_produk', 'ukuran_produk', 'warna_produk', 'harga_pokok', 'stok_produk'
        ]);

        return view('pemesananMaterial.edit', compact('suppliers', 'produks', 'pemesananMaterial'));
    }

    public function update(UpdatePemesananMaterialRequest $request, PemesananMaterial $pemesananMaterial, PemesananMaterialServices $service)
    {
        $pemesananMaterial = $service->handleUpdatePemesananMaterial(
            $request->validated(), 
            $pemesananMaterial->id,
        );

        return redirect()
            ->route('pemesananMaterials.show', $pemesananMaterial)
            ->with('success', 'Pemesanan material berhasil diubah!');
    }

    public function destroy(PemesananMaterial $pemesananMaterial, PemesananMaterialServices $service)
    {
        abort_if(Gate::denies('pemesananMaterial delete'), 403);

        try {
            $pemesananMaterial = $service->handleDestroyPemesananMaterial($pemesananMaterial->id);

            return redirect()
                ->route('pemesananMaterials.index')
                ->with('success', 'Pemesanan material berhasil dihapus!');
        } catch (\Exception $ex) {
            return redirect()
                ->route('pemesananMaterials.index')
                ->with('error', $ex->getMessage());
        }
    }

    public function confirm(PemesananMaterial $pemesananMaterial, PemesananMaterialServices $service)
    {
        abort_if(Gate::denies('pemesananMaterial confirm'), 403);

        try {
            $pemesananMaterial = $service->handleConfirmStatusNota($pemesananMaterial->id);
    
            return redirect()
                ->route('pemesananMaterials.show', $pemesananMaterial)
                ->with('success', 'Status nota berhasil diubah!');
        } catch (\Exception $ex) {
            return redirect()
                ->route('pemesananMaterials.show', $pemesananMaterial)
                ->with('error', $ex->getMessage());
        }
    }

    public function open(PemesananMaterial $pemesananMaterial, PemesananMaterialServices $service)
    {
        abort_if(Gate::denies('pemesananMaterial selesai'), 403);

        $pemesananMaterial = $service->handleOpenStatusNota($pemesananMaterial->id);
    
        return redirect()
            ->route('pemesananMaterials.show', $pemesananMaterial)
            ->with('success', 'Status nota berhasil diubah!');
    }
}
