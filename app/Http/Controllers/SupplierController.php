<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSupplierRequest;
use App\Http\Requests\UpdateSupplierRequest;
use App\Models\Supplier;
use Illuminate\Support\Facades\Gate;

class SupplierController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('supplier access'), 403);

        $suppliers = Supplier::all();

        return view('supplier.index', compact('suppliers'));
    }

    public function create()
    {
        abort_if(Gate::denies('supplier create'), 403);

        return view('supplier.create');
    }

    public function store(StoreSupplierRequest $request)
    {
        Supplier::create($request->validated());

        return redirect()
            ->route('suppliers.index')
            ->with('success', 'Data supplier berhasil disimpan!');
    }

    public function edit(Supplier $supplier)
    {
        abort_if(Gate::denies('supplier edit'), 403);

        return view('supplier.edit', compact('supplier'));
    }

    public function update(UpdateSupplierRequest $request, Supplier $supplier)
    {
        $supplier->update($request->validated());

        return redirect()
            ->route('suppliers.index')
            ->with('success', 'Data supplier berhasil diubah!');
    }

    public function destroy(Supplier $supplier)
    {
        abort_if(Gate::denies('supplier delete'), 403);

        if ($supplier->pemesananMaterials()->exists()) {
            return redirect()
                ->route('suppliers.index')
                ->with('error', 'Data supplier gagal dihapus!');
        }

        $supplier->delete();

        return redirect()
            ->route('suppliers.index')
            ->with('success', 'Data supplier berhasil dihapus!');
    }
}
