<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $totalProduks = Produk::count();
        
        $totalKaryawans = User::count();

        return view('dashboard', compact('totalProduks', 'totalKaryawans'));
    }

    public function markNotification(Request $request)
    {
        auth()->user()
            ->unreadNotifications
            ->when($request->input('id'), function($query) use($request) {
                return $query->where('id', $request->input('id'));
            })
            ->markAsRead();

            return response()->noContent();
    }
}
