<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNotaPelunasanSupplierRequest;
use App\Http\Requests\UpdateNotaPelunasanSupplierRequest;
use App\Models\NotaPelunasanSupplier;
use App\Models\PemesananMaterial;
use App\Services\NotaPelunasanSupplierService;
use Illuminate\Support\Facades\Gate;

class NotaPelunasanSupplierController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('notaPelunasanSupplier access'), 403);

        $notaPelunasanSuppliers = NotaPelunasanSupplier::with(['user', 'pemesananMaterial.supplier'])
            ->get();

        return view('notaPelunasanSupplier.index', compact('notaPelunasanSuppliers'));
    }

    public function store(StoreNotaPelunasanSupplierRequest $request, PemesananMaterial $pemesananMaterial, NotaPelunasanSupplierService $service)
    {
        $notaPelunasanSupplier = $service->handleStoreNotaPelunasanSupplier(
            $request->validated(), 
            auth()->id(), 
            $pemesananMaterial->id
        );

        return redirect()
            ->route('notaPelunasanSuppliers.show', $notaPelunasanSupplier)
            ->with('success', 'Nota pelunasan supplier berhasil disimpan!');
    }

    public function show(NotaPelunasanSupplier $notaPelunasanSupplier)
    {
        $notaPelunasanSupplier->load(['pemesananMaterial.supplier', 'user.jabatan']);

        return view('notaPelunasanSupplier.detail', compact('notaPelunasanSupplier'));
    }

    public function edit(NotaPelunasanSupplier $notaPelunasanSupplier)
    {
        abort_if(Gate::denies('notaPelunasanSupplier edit'), 403);

        $notaPelunasanSupplier->load(['pemesananMaterial']);
        
        return view('notaPelunasanSupplier.edit', compact('notaPelunasanSupplier'));
    }

    public function update(UpdateNotaPelunasanSupplierRequest $request, NotaPelunasanSupplier $notaPelunasanSupplier, NotaPelunasanSupplierService $service)
    {
        $notaPelunasanSupplier = $service->handleUpdateNotaPelunasanSupplier(
            $request->validated(), 
            $notaPelunasanSupplier->id
        );

        return redirect()
            ->route('notaPelunasanSuppliers.show', $notaPelunasanSupplier)
            ->with('success', 'Nota pelunasan supplier berhasil diubah!');
    }

    public function destroy(NotaPelunasanSupplier $notaPelunasanSupplier, NotaPelunasanSupplierService $service)
    {
        abort_if(Gate::denies('notaPelunasanSupplier delete'), 403);

        $notaPelunasanSupplier = $service->handleDestroyNotaPelunasanSupplier($notaPelunasanSupplier->id);

        return redirect()
            ->route('notaPelunasanSuppliers.index')
            ->with('success', 'Nota pelunasan supplier berhasil dihapus!');
    }
}
