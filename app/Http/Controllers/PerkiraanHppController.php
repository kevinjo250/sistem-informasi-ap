<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePerkiraanHppRequest;
use App\Http\Requests\UpdatePerkiraanHppRequest;
use App\Models\BillOfMaterial;
use App\Models\PerkiraanHpp;
use App\Services\PerkiraanHppServices;
use Illuminate\Support\Facades\Gate;

class PerkiraanHppController extends Controller
{
    public function create(BillOfMaterial $billOfMaterial, PerkiraanHppServices $service)
    {
        $billOfMaterial->load(['produk']);

        $biayaMaterial = $service->hitungBiayaMaterial($billOfMaterial->id);

        return view('perkiraanHpp.create', compact('billOfMaterial', 'biayaMaterial'));
    }

    public function store(StorePerkiraanHppRequest $request, BillOfMaterial $billOfMaterial, PerkiraanHppServices $service)
    {
        $perkiraanHpp = $service->handleStorePerkiraanHpp(
            $request->validated(),
            $billOfMaterial->id,
        );

        return redirect()
            ->route('perkiraanHpps.show', $perkiraanHpp)
            ->with('success', 'Perkiraan hpp berhasil disimpan!');
    }

    public function show(PerkiraanHpp $perkiraanHpp)
    {
        abort_if(Gate::denies('perkiraanHpp access'), 403);
        
        $perkiraanHpp->load(['billOfMaterial.produk', 'billOfMaterial.produks']);

        return view('perkiraanHpp.detail', compact('perkiraanHpp'));
    }

    public function edit(PerkiraanHpp $perkiraanHpp)
    {
        $perkiraanHpp->load(['billOfMaterial.produk']);

        return view('perkiraanHpp.edit', compact('perkiraanHpp'));
    }

    public function update(UpdatePerkiraanHppRequest $request, PerkiraanHpp $perkiraanHpp, PerkiraanHppServices $service)
    {
        $perkiraanHpp = $service->handleUpdatePerkiraanHpp(
            $request->validated(),
            $perkiraanHpp->id,
        );

        return redirect()
            ->route('perkiraanHpps.show', $perkiraanHpp)
            ->with('success', 'Perkiraan hpp berhasil diubah!');
    }

    public function destroy(PerkiraanHpp $perkiraanHpp, PerkiraanHppServices $service)
    {
        $perkiraanHpp = $service->handleDestroyPerkiraanHpp($perkiraanHpp->id);

        return redirect()
            ->route('billOfMaterials.show', $perkiraanHpp->bill_of_material_id)
            ->with('success', 'Perkiraan hpp berhasil dihapus!');
    }
}
