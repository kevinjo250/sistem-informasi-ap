<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePelangganRequest;
use App\Http\Requests\UpdatePelangganRequest;
use App\Models\Pelanggan;
use Illuminate\Support\Facades\Gate;

class PelangganController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('pelanggan access'), 403);

        $pelanggans = Pelanggan::all();

        return view('pelanggan.index', compact('pelanggans'));
    }

    public function create()
    {
        abort_if(Gate::denies('pelanggan create'), 403);

        return view('pelanggan.create');
    }

    public function store(StorePelangganRequest $request)
    {
        Pelanggan::create($request->validated());

        return redirect()
            ->route('pelanggans.index')
            ->with('success', 'Data pelanggan berhasil disimpan!');
    }

    public function edit(Pelanggan $pelanggan)
    {
        abort_if(Gate::denies('pelanggan edit'), 403);

        return view('pelanggan.edit', compact('pelanggan'));
    }

    public function update(UpdatePelangganRequest $request, Pelanggan $pelanggan)
    {
        $pelanggan->update($request->validated());

        return redirect()
            ->route('pelanggans.index')
            ->with('success', 'Data pelanggan berhasil diubah!');
    }

    public function destroy(Pelanggan $pelanggan)
    {
        abort_if(Gate::denies('pelanggan delete'), 403);

        if ($pelanggan->salesOrders()->exists()) {
            return redirect()
                ->route('pelanggans.index')
                ->with('error', 'Data pelanggan gagal dihapus!');
        }

        $pelanggan->delete();

        return redirect()
            ->route('pelanggans.index')
            ->with('success', 'Data pelanggan berhasil dihapus!');
    }
}
