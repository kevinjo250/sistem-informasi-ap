<?php

namespace App\Http\Controllers;

use App\Http\Requests\FindJurnalRequest;
use App\Http\Requests\StoreJurnalRequest;
use App\Models\Akun;
use App\Models\BatalOrder;
use App\Models\NotaPelunasanSupplier;
use App\Models\NotaPembayaran;
use App\Models\PemesananMaterial;
use App\Models\PenerimaanMaterial;
use App\Models\PermintaanMaterial;
use App\Models\RealisasiProduksi;
use App\Models\SalesOrder;
use App\Models\SuratJalan;
use App\Services\JurnalServices;

class JurnalController extends Controller
{
    public function index()
    {
        return view('jurnal.index');
    }

    public function create()
    {
        $akuns = Akun::where('id', '!=', 504)
            ->get(['id', 'nama_akun']);

        return view('jurnal.create', compact('akuns'));
    }

    public function store(StoreJurnalRequest $request, JurnalServices $service)
    {
        try {
            $service->handleStoreJurnal($request->validated());

            return redirect()
                ->route('jurnals.index')
                ->with('success', 'Jurnal telah berhasil dibuat!');
        } catch (\Exception $ex) {
            return redirect()
                ->route('jurnals.create')
                ->with('error', $ex->getMessage())
                ->withInput();
        }
    }

    public function find(FindJurnalRequest $request, JurnalServices $service)
    {
        $jenisTransaksi = $request->input('jenis_transaksi');

        $salesOrders = $jenisTransaksi == 'all' || $jenisTransaksi == 'penjualan' ? SalesOrder::all(['id', 'no_nota']) : '';

        $batalOrders = $jenisTransaksi == 'all' || $jenisTransaksi == 'pembatalan' ? BatalOrder::all(['id', 'no_nota']) : '';

        $notaPembayarans = $jenisTransaksi == 'all' || $jenisTransaksi == 'pembayaran pelanggan' ? NotaPembayaran::all(['id', 'no_nota']) : '';

        $suratJalans = $jenisTransaksi == 'all' || $jenisTransaksi == 'pengiriman' ? SuratJalan::all(['id', 'no_nota']) : '';

        $pemesananMaterials = $jenisTransaksi == 'all' || $jenisTransaksi == 'pemesanan material' ? PemesananMaterial::all(['id', 'no_nota']) : '';

        $penerimaanMaterials = $jenisTransaksi == 'all' || $jenisTransaksi == 'penerimaan material' ? PenerimaanMaterial::all(['id', 'no_nota']) : '';

        $pelunasanSuppliers = $jenisTransaksi == 'all' || $jenisTransaksi == 'pelunasan supplier' ? NotaPelunasanSupplier::all(['id', 'no_nota']) : '';

        $permintaanMaterials = $jenisTransaksi == 'all' || $jenisTransaksi == 'permintaan material' ? PermintaanMaterial::all(['id', 'no_nota']) : '';

        $realisasiProduksis = $jenisTransaksi == 'all' || $jenisTransaksi == 'produksi' ? RealisasiProduksi::all(['id', 'no_nota']) : '';

        $jurnals = $service->handleFindJurnal($request->validated());

        return view('jurnal.detail', 
            compact(
                'jurnals', 
                'salesOrders', 
                'batalOrders', 
                'notaPembayarans', 
                'suratJalans', 
                'pemesananMaterials', 
                'penerimaanMaterials', 
                'pelunasanSuppliers', 
                'permintaanMaterials', 
                'realisasiProduksis'
            ));
    }

    public function close(JurnalServices $service)
    {
        try {
            $service->handleClosingJurnal();

            return redirect()
                ->route('jurnals.index')
                ->with('success', 'Jurnal penutup berhasil dibuat!');
        } catch (\Exception $ex) {
            return redirect()
                ->route('jurnals.index')
                ->with('error', $ex->getMessage());
        }
    }
}
