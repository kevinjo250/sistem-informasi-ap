<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDivisiRequest;
use App\Http\Requests\UpdateDivisiRequest;
use App\Models\Divisi;
use Illuminate\Support\Facades\Gate;

class DivisiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('divisi access'), 403);

        $divisis = Divisi::all(['id', 'nama_divisi']);

        return view('divisi.index', compact('divisis'));
    }

    public function create()
    {
        abort_if(Gate::denies('divisi create'), 403);

        return view('divisi.create');
    }

    public function store(StoreDivisiRequest $request)
    {
        Divisi::create($request->validated());

        return redirect()
            ->route('divisis.index')
            ->with('success', 'Data divisi berhasil disimpan!');
    }

    public function edit(Divisi $divisi)
    {
        abort_if(Gate::denies('divisi edit'), 403);

        return view('divisi.edit', compact('divisi'));
    }

    public function update(UpdateDivisiRequest $request, Divisi $divisi)
    {
        $divisi->update($request->validated());

        return redirect()
            ->route('divisis.index')
            ->with('success', 'Data divisi berhasil diubah!');
    }

    public function destroy(Divisi $divisi)
    {
        abort_if(Gate::denies('divisi delete'), 403);

        if ($divisi->users()->exists()) {
            return redirect()
                ->route('divisis.index')
                ->with('error', 'Data divisi gagal dihapus!');
        }

        $divisi->delete();

        return redirect()
            ->route('divisis.index')
            ->with('success', 'Data divisi berhasil dihapus!');
    }
}
