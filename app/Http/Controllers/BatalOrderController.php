<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBatalOrderRequest;
use App\Http\Requests\UpdateBatalOrderRequest;
use App\Models\BatalOrder;
use App\Models\SalesOrder;
use App\Services\BatalOrderServices;
use Illuminate\Support\Facades\Gate;

class BatalOrderController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('batalOrder access'), 403);

        $batalOrders = BatalOrder::with(['salesOrder', 'user'])->get();

        return view('batalOrder.index', compact('batalOrders'));
    }

    public function store(StoreBatalOrderRequest $request, SalesOrder $salesOrder, BatalOrderServices $service)
    {
        $batalOrder = $service->handleStoreBatalOrder(
            $request->validated(), 
            auth()->id(), 
            $salesOrder->id
        );

        return redirect()
            ->route('batalOrders.index')
            ->with('success', 'Pembatalan order berhasil disimpan!');
    }

    public function edit(BatalOrder $batalOrder)
    {
        abort_if(Gate::denies('batalOrder edit'), 403);

        $batalOrder->load(['salesOrder']);

        return view('batalOrder.edit', compact('batalOrder'));
    }

    public function update(UpdateBatalOrderRequest $request, BatalOrder $batalOrder)
    {
        $batalOrder->update($request->validated());

        return redirect()
            ->route('batalOrders.index')
            ->with('success', 'Pembatalan order berhasil diubah!');
    }
}
