<?php

namespace App\Http\Controllers;

use App\Http\Requests\FindBukuBesarRequest;
use App\Models\Akun;
use App\Models\BatalOrder;
use App\Models\NotaPelunasanSupplier;
use App\Models\NotaPembayaran;
use App\Models\PemesananMaterial;
use App\Models\PenerimaanMaterial;
use App\Models\PermintaanMaterial;
use App\Models\RealisasiProduksi;
use App\Models\SalesOrder;
use App\Models\SuratJalan;

class BukuBesarController extends Controller
{
    public function index()
    {
        return view('bukuBesar.index');
    }

    public function find(FindBukuBesarRequest $request)
    {
        $periode = explode("-", $request->input('periode_akuntansi'));

        $akuns = Akun::with([
            'periodes' => function($query) use($periode) {
                $query->whereMonth('tanggal_awal', $periode[1])
                    ->whereYear('tanggal_awal', $periode[0]);
            }, 
            'jurnals' => function($query) use($periode) {
                $query->whereMonth('tanggal', $periode[1])
                    ->whereYear('tanggal', $periode[0]);
        }])->get();

        $salesOrders = SalesOrder::all(['id', 'no_nota']);

        $batalOrders = BatalOrder::all(['id', 'no_nota']);

        $notaPembayarans = NotaPembayaran::all(['id', 'no_nota']);

        $suratJalans = SuratJalan::all(['id', 'no_nota']);

        $pemesananMaterials = PemesananMaterial::all(['id', 'no_nota']);

        $penerimaanMaterials = PenerimaanMaterial::all(['id', 'no_nota']);

        $pelunasanSuppliers = NotaPelunasanSupplier::all(['id', 'no_nota']);

        $permintaanMaterials = PermintaanMaterial::all(['id', 'no_nota']);

        $realisasiProduksis = RealisasiProduksi::all(['id', 'no_nota']);

        return view('bukuBesar.detail', 
            compact(
                'akuns',
                'salesOrders', 
                'batalOrders', 
                'notaPembayarans', 
                'suratJalans', 
                'pemesananMaterials', 
                'penerimaanMaterials', 
                'pelunasanSuppliers', 
                'permintaanMaterials', 
                'realisasiProduksis'
            ));
    }
}
