<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSalesOrderRequest;
use App\Http\Requests\UpdateSalesOrderRequest;
use App\Models\Pelanggan;
use App\Models\Produk;
use App\Models\SalesOrder;
use App\Services\SalesOrderServices;
use Illuminate\Support\Facades\Gate;

class SalesOrderController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('salesOrder access'), 403);

        $salesOrders = SalesOrder::with(['pelanggan', 'user'])->get();

        return view('salesOrder.index', compact('salesOrders'));
    }

    public function create()
    {
        abort_if(Gate::denies('salesOrder create'), 403);

        $pelanggans = Pelanggan::all(['id', 'nama_pelanggan', 'alamat_pelanggan', 'kota_pelanggan']);

        $produks = Produk::onlyFinishedProduct()
            ->get(['id', 'nama_produk', 'ukuran_produk', 'warna_produk', 'harga_jual']);

        return view('salesOrder.create', compact('pelanggans', 'produks'));
    }

    public function store(StoreSalesOrderRequest $request, SalesOrderServices $service)
    {
        $salesOrder = $service->handleStoreSalesOrder(
            $request->validated(), 
            auth()->id(),
        );

        return redirect()
            ->route('salesOrders.show', $salesOrder)
            ->with('success', 'Sales order berhasil disimpan!');
    }

    public function show(SalesOrder $salesOrder)
    {
        $salesOrder->load(['produks', 'pelanggan', 'notaPembayarans']);

        return view('salesOrder.detail', compact('salesOrder'));
    }

    public function edit(SalesOrder $salesOrder)
    {
        abort_if(Gate::denies('salesOrder edit'), 403);

        $salesOrder->load(['produks', 'pelanggan']);

        $pelanggans = Pelanggan::all(['id', 'nama_pelanggan', 'alamat_pelanggan', 'kota_pelanggan']);

        $produks = Produk::onlyFinishedProduct()
            ->get(['id', 'nama_produk', 'ukuran_produk', 'warna_produk', 'harga_jual']);

        return view('salesOrder.edit', compact('salesOrder', 'pelanggans', 'produks'));
    }

    public function update(UpdateSalesOrderRequest $request, SalesOrder $salesOrder, SalesOrderServices $service)
    {
        $salesOrder = $service->handleUpdateSalesOrder(
            $request->validated(), 
            $salesOrder->id,
        );

        return redirect()
            ->route('salesOrders.show', $salesOrder)
            ->with('success', 'Sales order berhasil diubah!');
    }

    public function destroy(SalesOrder $salesOrder, SalesOrderServices $service)
    {
        abort_if(Gate::denies('salesOrder delete'), 403);

        try {
            $salesOrder = $service->handleDestroySalesOrder($salesOrder->id);
            
            return redirect()
                ->route('salesOrders.index')
                ->with('success', 'Sales order berhasil dihapus!');
        } catch (\Exception $ex) {
            return redirect()
                ->route('salesOrders.show', $salesOrder)
                ->with('error', $ex->getMessage());
        }
    }

    public function confirm(SalesOrder $salesOrder, SalesOrderServices $service)
    {
        abort_if(Gate::denies('salesOrder confirm'), 403);

        $salesOrder = $service->handleConfirmSalesOrder($salesOrder->id);

        return redirect()
            ->route('salesOrders.show', $salesOrder)
            ->with('success', 'Sales order berhasil dikonfirmasi!');
    }
}
