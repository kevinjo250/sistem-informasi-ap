<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSuratPerintahKerjaRequest;
use App\Http\Requests\UpdateSuratPerintahKerjaRequest;
use App\Models\SalesOrder;
use App\Models\SuratPerintahKerja;
use App\Services\SuratPerintahKerjaServices;
use Illuminate\Support\Facades\Gate;

class SuratPerintahKerjaController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('suratPerintahKerja access'), 403);
        
        $suratPerintahKerjas = SuratPerintahKerja::with([
            'produkSalesOrder.salesOrder', 
            'produkSalesOrder.produk'
        ])->get();

        return view('suratPerintahKerja.index', compact('suratPerintahKerjas'));
    }

    public function create()
    {
        abort_if(Gate::denies('suratPerintahKerja create'), 403);
        
        $salesOrders = SalesOrder::with([
            'produks' => function($query) {
                $query->where('status_order', 'pending');
            }
        ])->confirmed()->get();
        
        return view('suratPerintahKerja.create', compact('salesOrders'));
    }

    public function store(StoreSuratPerintahKerjaRequest $request, SuratPerintahKerjaServices $service)
    {
        $suratPerintahKerja = $service->handleStoreSuratPerintahKerja($request->validated());

        return redirect()
            ->route('suratPerintahKerjas.show', $suratPerintahKerja)
            ->with('success', 'Surat perintah kerja berhasil dibuat!');
    }

    public function show(SuratPerintahKerja $suratPerintahKerja)
    {
        $suratPerintahKerja->load(['produkSalesOrder.produk']);

        return view('suratPerintahKerja.detail', compact('suratPerintahKerja'));
    }

    public function edit(SuratPerintahKerja $suratPerintahKerja)
    {
        abort_if(Gate::denies('suratPerintahKerja edit'), 403);
        
        $suratPerintahKerja->load(['produkSalesOrder.salesOrder', 'produkSalesOrder.produk']);
        
        return view('suratPerintahKerja.edit', compact('suratPerintahKerja'));
    }

    public function update(UpdateSuratPerintahKerjaRequest $request, SuratPerintahKerja $suratPerintahKerja, SuratPerintahKerjaServices $service)
    {
        $suratPerintahKerja = $service->handleUpdateSuratPerintahKerja(
            $request->validated(), 
            $suratPerintahKerja->id
        );

        return redirect()
            ->route('suratPerintahKerjas.show', $suratPerintahKerja)
            ->with('success', 'Surat perintah kerja berhasil diubah!');
    }

    public function destroy(SuratPerintahKerja $suratPerintahKerja, SuratPerintahKerjaServices $service)
    {
        abort_if(Gate::denies('suratPerintahKerja delete'), 403);
        
        if ($suratPerintahKerja->permintaanMaterial()->exists()) {
            return redirect()
                ->route('suratPerintahKerjas.index')
                ->with('error', 'Surat Perintah Kerja gagal dihapus!');
        }

        $suratPerintahKerja = $service->handleDestroySuratPerintahKerja($suratPerintahKerja->id);

        return redirect()
            ->route('suratPerintahKerjas.index')
            ->with('success', 'Surat Perintah Kerja berhasil dihapus!');
    }
}
