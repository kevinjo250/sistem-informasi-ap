<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Divisi;
use App\Models\Jabatan;
use App\Models\User;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('karyawan access'), 403);

        $users = User::with(['divisi', 'jabatan'])->get();
        
        return view('karyawan.index', compact('users'));
    }

    public function create()
    {
        abort_if(Gate::denies('karyawan create'), 403);

        $divisis = Divisi::all(['id', 'nama_divisi']);

        $jabatans = Jabatan::all(['id', 'nama_jabatan']);

        return view('karyawan.create', compact('divisis', 'jabatans'));
    }

    public function store(StoreUserRequest $request)
    {
        User::create($request->validated());

        return redirect()
            ->route('users.index')
            ->with('success', 'Data karyawan berhasil disimpan!');
    }

    public function edit(User $user)
    {
        abort_if(Gate::denies('karyawan edit'), 403);

        $user->load(['divisi', 'jabatan']);

        $divisis = Divisi::all(['id', 'nama_divisi']);

        $jabatans = Jabatan::all(['id', 'nama_jabatan']);

        return view('karyawan.edit', compact('user', 'divisis', 'jabatans'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->validated());

        return redirect()
            ->route('users.index')
            ->with('success', 'Data karyawan berhasil diubah!');
    }
}
