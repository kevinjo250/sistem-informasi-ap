<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBillOfMaterialRequest;
use App\Http\Requests\UpdateBillOfMaterialRequest;
use App\Models\BillOfMaterial;
use App\Models\Produk;
use App\Services\BillOfMaterialServices;
use App\Services\PerkiraanHppServices;
use Illuminate\Support\Facades\Gate;

class BillOfMaterialController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('billOfMaterial access'), 403);

        $billOfMaterials = BillOfMaterial::with(['produk'])->get();

        return view('billOfMaterial.index', compact('billOfMaterials'));
    }

    public function create()
    {
        abort_if(Gate::denies('billOfMaterial create'), 403);
        
        $produks = Produk::doesnthave('billOfMaterial')
            ->get(['id', 'nama_produk', 'jenis_produk', 'ukuran_produk', 'warna_produk', 'satuan_produk']);

        return view('billOfMaterial.create', compact('produks'));
    }

    public function store(StoreBillOfMaterialRequest $request, BillOfMaterialServices $service)
    {
        $billOfMaterial = $service->handleStoreBillOfMaterial($request->validated());

        return redirect()
            ->route('billOfMaterials.show', $billOfMaterial)
            ->with('success', 'Bill of material berhasil disimpan!');
    }

    public function show(BillOfMaterial $billOfMaterial)
    {
        $billOfMaterial->load(['produk', 'produks']);

        return view('billOfMaterial.detail', compact('billOfMaterial'));
    }

    public function edit(BillOfMaterial $billOfMaterial)
    {
        abort_if(Gate::denies('billOfMaterial edit'), 403);
        
        $billOfMaterial->load(['produk', 'produks']);

        $materials = Produk::onlyMaterial()
            ->get(['id', 'nama_produk', 'ukuran_produk', 'warna_produk', 'satuan_produk']);

        return view('billOfMaterial.edit', compact('billOfMaterial', 'materials'));
    }

    public function update(UpdateBillOfMaterialRequest $request, BillOfMaterial $billOfMaterial, BillOfMaterialServices $service, PerkiraanHppServices $hppService)
    {
        $billOfMaterial = $service->handleUpdateBillOfMaterial(
            $request->validated(), 
            $billOfMaterial->id,
            $hppService,
        );

        return redirect()
            ->route('billOfMaterials.show', $billOfMaterial)
            ->with('success', 'Bill of material berhasil disimpan!');
    }

    public function destroy(BillOfMaterial $billOfMaterial, BillOfMaterialServices $service)
    {
        abort_if(Gate::denies('billOfMaterial delete'), 403);
        
        try {
            $billOfMaterial = $service->handleDestroyBillOfMaterial($billOfMaterial->id);

            return redirect()
                ->route('billOfMaterials.index')
                ->with('success', 'Bill of material berhasil dihapus!');
        } catch (\Exception $ex) {
            return redirect()
                ->route('billOfMaterials.show', $billOfMaterial)
                ->with('error', $ex->getMessage());
        }
    }
}
