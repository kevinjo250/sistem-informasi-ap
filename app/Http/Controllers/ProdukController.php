<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProdukRequest;
use App\Http\Requests\UpdateProdukRequest;
use App\Models\Produk;
use App\Services\PerkiraanHppServices;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;

class ProdukController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('produk access'), 403);

        $produks = Produk::all();

         return view('produk.index', compact('produks'));
    }

    public function create()
    {
        abort_if(Gate::denies('produk create'), 403);

        return view('produk.create');
    }

    public function store(StoreProdukRequest $request)
    {
        $produk = Produk::create($request->validated());

        if ($request->hasFile('gambar_produk')) {
            $date = now()->format('Y-m-d');

            $newImageName = "{$date}-{$request->nama_produk}-{$produk->id}.{$request->gambar_produk->extension()}";

            $produk->update(['gambar_produk' => $newImageName]);

            $request->gambar_produk->move(public_path('images'), $newImageName);
        }

         return redirect()
            ->route('produks.index')
            ->with('success', 'Data produk berhasil disimpan!');
    }

    public function edit(Produk $produk)
    {
        abort_if(Gate::denies('produk edit'), 403);

        return view('produk.edit', compact('produk'));
    }

    public function update(UpdateProdukRequest $request, Produk $produk, PerkiraanHppServices $service)
    {
        if ($request->hasFile('gambar_produk')) {
            Storage::delete($produk->gambar_produk);
        }

        $produk->update($request->validated());

        if ($request->hasFile('gambar_produk')) {
            $date = now()->format('Y-m-d');
    
            $newImageName = "{$date}-{$request->nama_produk}-{$produk->id}.{$request->gambar_produk->extension()}";
    
            $produk->update(['gambar_produk' => $newImageName]);
    
            $request->gambar_produk->move(public_path('images'), $newImageName);
        }

        foreach ($produk->billOfMaterials as $detail) {
            if ($detail->perkiraanHpp()->exists()) {
                $service->handleUpdatePerkiraanHpp(
                    [
                        'biaya_material' => $service->hitungBiayaMaterial($detail->id)
                    ],
                    $detail->perkiraanHpp->id
                );
            }
        }
        
        return redirect()
            ->route('produks.index')
            ->with('success', 'Data produk berhasil diubah!');
    }
}
