<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdatePasswordRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class PasswordController extends Controller
{
    public function update(UpdatePasswordRequest $request, User $user)
    {
        $user->update([
            'password' => Hash::make($request->password)
        ]);

        return redirect()
            ->route('profile.edit', $user)
            ->with('success', 'Password telah diubah!');
    }
}