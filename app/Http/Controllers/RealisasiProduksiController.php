<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRealisasiProduksiRequest;
use App\Http\Requests\UpdateRealisasiProduksiRequest;
use App\Models\Produk;
use App\Models\RealisasiProduksi;
use App\Models\SuratPerintahKerja;
use App\Services\RealisasiProduksiServices;
use Illuminate\Support\Facades\Gate;

class RealisasiProduksiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('realisasiProduksi access'), 403);
        
        $realisasiProduksis = RealisasiProduksi::with([
            'suratPerintahKerja.produkSalesOrder.produk',
            'user',
        ])->get();

        return view('realisasiProduksi.index', compact('realisasiProduksis'));
    }

    public function create()
    {
        abort_if(Gate::denies('realisasiProduksi create'), 403);
        
        $suratPerintahKerjas = SuratPerintahKerja::with([
            'produkSalesOrder.produk.billOfMaterial.perkiraanHpp',
            'realisasiProduksis',
            'permintaanMaterial'
        ])->inProduction()->get();
        
        $materials = Produk::onlyMaterial()->get();

        return view('realisasiProduksi.create', compact('suratPerintahKerjas', 'materials'));
    }

    public function store(StoreRealisasiProduksiRequest $request, RealisasiProduksiServices $service)
    {
        $realisasiProduksi = $service->handleStoreRealisasiProduksi(
            $request->validated(), 
            auth()->id()
        );

        return redirect()
            ->route('realisasiProduksis.show', $realisasiProduksi)
            ->with('success', 'Realisasi produksi berhasil disimpan!');
    }

    public function show(RealisasiProduksi $realisasiProduksi)
    {
        $realisasiProduksi->load([
            'produks', 
            'suratPerintahKerja.produkSalesOrder.produk'
        ]);

        return view('realisasiProduksi.detail', compact('realisasiProduksi'));
    }

    public function edit(RealisasiProduksi $realisasiProduksi)
    {
        abort_if(Gate::denies('realisasiProduksi edit'), 403);
        
        $realisasiProduksi->load([
            'produks', 
            'suratPerintahKerja.produkSalesOrder.produk.billOfMaterial.perkiraanHpp'
        ]);

        $materials = Produk::onlyMaterial()->get();

        return view('realisasiProduksi.edit', compact('realisasiProduksi', 'materials'));
    }

    public function update(UpdateRealisasiProduksiRequest $request, RealisasiProduksi $realisasiProduksi, RealisasiProduksiServices $service)
    {
        $realisasiProduksi = $service->handleUpdateRealisasiProduksi(
            $request->validated(), 
            $realisasiProduksi->id
        );

        return redirect()
            ->route('realisasiProduksis.show', $realisasiProduksi)
            ->with('success', 'Realisasi produksi berhasil diubah!');
    }

    public function destroy(RealisasiProduksi $realisasiProduksi, RealisasiProduksiServices $service)
    {
        abort_if(Gate::denies('realisasiProduksi delete'), 403);
        
        $realisasiProduksi = $service->handleDestroyRealisasiProduksi($realisasiProduksi->id);

        return redirect()
            ->route('realisasiProduksis.index')
            ->with('success', 'Data realisasi produksi berhasil dihapus!');
    }
}
