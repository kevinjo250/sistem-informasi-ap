<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreJabatanRequest;
use App\Http\Requests\UpdateJabatanRequest;
use App\Models\Jabatan;
use App\Models\Otorisasi;
use Illuminate\Support\Facades\Gate;

class JabatanController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('jabatan access'), 403);

        $jabatans = Jabatan::with(['otorisasis'])->get();

        return view('jabatan.index', compact('jabatans'));
    }

    public function create()
    {
        abort_if(Gate::denies('jabatan create'), 403);

        $otorisasis = Otorisasi::all(['id', 'nama_otorisasi']);

        return view('jabatan.create', compact('otorisasis'));
    }

    public function store(StoreJabatanRequest $request)
    {
        $jabatan = Jabatan::create($request->validated());

        $jabatan->otorisasis()->attach($request->input('otorisasi_ids'));

        return redirect()
            ->route('jabatans.index')
            ->with('success', 'Data jabatan berhasil disimpan!');
    }

    public function edit(Jabatan $jabatan)
    {
        abort_if(Gate::denies('jabatan edit'), 403);

        $otorisasis = Otorisasi::all(['id', 'nama_otorisasi']);
        
        $jabatan->load(['otorisasis']);

        return view('jabatan.edit', compact('jabatan', 'otorisasis'));
    }

    public function update(UpdateJabatanRequest $request, Jabatan $jabatan)
    {
        $jabatan->update($request->validated());
        
        $jabatan->otorisasis()->sync($request->input('otorisasi_ids'));

        return redirect()
            ->route('jabatans.index')
            ->with('success', 'Data jabatan berhasil diubah!');
    }

    public function destroy(Jabatan $jabatan)
    {
        abort_if(Gate::denies('jabatan delete'), 403);

        if ($jabatan->users()->exists()) {
            return redirect()
                ->route('jabatans.index')
                ->with('error', 'Data jabatan gagal dihapus!');
        }

        $jabatan->otorisasis()->detach();
        
        $jabatan->delete();

        return redirect()
            ->route('jabatans.index')
            ->with('success', 'Data jabatan berhasil dihapus!');
    }
}
