<?php

namespace App\Http\Controllers;

use App\Models\Otorisasi;
use Illuminate\Support\Facades\Gate;

class OtorisasiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('otorisasi access'), 403);

        $otorisasis = Otorisasi::all(['nama_otorisasi']);

        return view('otorisasi.index', compact('otorisasis'));
    }
}
