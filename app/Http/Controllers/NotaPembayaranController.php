<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNotaPembayaranRequest;
use App\Http\Requests\UpdateNotaPembayaranRequest;
use App\Models\NotaPembayaran;
use App\Models\SalesOrder;
use App\Services\NotaPembayaranServices;
use Illuminate\Support\Facades\Gate;

class NotaPembayaranController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('notaPembayaran access'), 403);

        $notaPembayarans = NotaPembayaran::with(['user', 'salesOrder.pelanggan'])->get();

        return view('notaPembayaran.index', compact('notaPembayarans'));
    }

    public function store(StoreNotaPembayaranRequest $request, SalesOrder $salesOrder, NotaPembayaranServices $service)
    {
        $notaPembayaran = $service->handleStoreNotaPembayaran(
            $request->validated(), 
            $salesOrder->id, 
            auth()->id()
        );

        return redirect()
            ->route('notaPembayarans.show', $notaPembayaran)
            ->with('success', 'Nota pembayaran berhasil dibuat!');
    }

    public function show(NotaPembayaran $notaPembayaran)
    {
        $notaPembayaran->load(['salesOrder.pelanggan', 'user.jabatan']);

        return view('notaPembayaran.detail', compact('notaPembayaran'));
    }

    public function edit(NotaPembayaran $notaPembayaran)
    {
        abort_if(Gate::denies('notaPembayaran edit'), 403);

        $notaPembayaran->load(['salesOrder']);

        return view('notaPembayaran.edit', compact('notaPembayaran'));
    }

    public function update(UpdateNotaPembayaranRequest $request, NotaPembayaran $notaPembayaran, NotaPembayaranServices $service)
    {
        $notaPembayaran = $service->handleUpdateNotaPembayaran(
            $request->validated(), $notaPembayaran->id
        );

        return redirect()
            ->route('notaPembayarans.show', $notaPembayaran)
            ->with('success', 'Nota pembayaran berhasil diubah!');
    }

    public function destroy(NotaPembayaran $notaPembayaran, NotaPembayaranServices $service)
    {
        abort_if(Gate::denies('notaPembayaran delete'), 403);

        $notaPembayaran = $service->handleDestroyNotaPembayaran($notaPembayaran->id);

        return redirect()
            ->route('notaPembayarans.index')
            ->with('success', 'Nota pembayaran berhasil dihapus!');
    }
}
