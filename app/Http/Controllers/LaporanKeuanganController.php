<?php

namespace App\Http\Controllers;

use App\Http\Requests\FindLaporanKeuanganRequest;
use App\Services\LaporanKeuanganServices;

class LaporanKeuanganController extends Controller
{
    public function index()
    {
        return view('laporan.index');
    }

    public function find(FindLaporanKeuanganRequest $request, LaporanKeuanganServices $service)
    {
        $periode = explode("-", $request->input('periode_akuntansi'));

        $jenisLaporan = $request->input('jenis_laporan');

        $akuns = $service->handleRetriveAkuns($request->validated());

        $totalPendapatan = $service->hitungTotalPendapatan($akuns);

        $totalBiaya = $service->hitungTotalBiaya($akuns);

        $labaRugi = $totalPendapatan - $totalBiaya;

        $periode = $request->input('periode_akuntansi');

        if ($jenisLaporan == 'neraca') {
            $modal = $service->getTotalModal($akuns);

            $prive = $service->getTotalPrive($akuns);

            $totalKewajiban = $service->getTotalKewajiban($akuns);

            $totalAktiva = $service->getTotalAktiva($akuns);

            $totalPasiva = $totalKewajiban + $modal - $prive + $labaRugi;

            return view('laporan.neraca', compact('akuns', 'akuns', 'modal', 'prive', 'labaRugi', 'totalAktiva', 'totalPasiva', 'periode'));
        }

        if ($jenisLaporan == 'labaRugi') {
            return view('laporan.labaRugi', compact('akuns', 'totalPendapatan', 'totalBiaya', 'labaRugi', 'periode'));
        }

        if ($jenisLaporan == 'perubahanEkuitas') {
            $modal = $service->getTotalModal($akuns);

            $prive = $service->getTotalPrive($akuns);

            return view('laporan.perubahanEkuitas', compact('akuns', 'labaRugi', 'modal', 'prive', 'periode'));
        }
    }
}
