<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePenerimaanMaterialRequest;
use App\Http\Requests\UpdatePenerimaanMaterialRequest;
use App\Models\PemesananMaterial;
use App\Models\PenerimaanMaterial;
use App\Services\PenerimaanMaterialServices;
use Illuminate\Support\Facades\Gate;

class PenerimaanMaterialController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('penerimaanMaterial access'), 403);

        $penerimaanMaterials = PenerimaanMaterial::with(['pemesananMaterial.supplier', 'user'])->get();

        return view('penerimaanMaterial.index', compact('penerimaanMaterials'));
    }

    public function create(PemesananMaterial $pemesananMaterial, PenerimaanMaterialServices $service)
    {
        abort_if(Gate::denies('penerimaanMaterial create'), 403);

        if (! count($pemesananMaterial->produks)) {
            return redirect()
                ->route('pemesananMaterials.show', $pemesananMaterial)
                ->with('error', 'Tidak dapat membuat penerimaan!');
        }

        $pemesananMaterial->load(['supplier', 'produks' => function($query) {
            $query->where('status_material', '=', 'partial')
                ->orWhere('status_material', '=', 'open');
        }]);

        $received = $service->handleRetrieveReceivedMaterial($pemesananMaterial->id);

        return view('penerimaanMaterial.create', compact('pemesananMaterial', 'received'));
    }

    public function store(StorePenerimaanMaterialRequest $request, PemesananMaterial $pemesananMaterial, PenerimaanMaterialServices $service)
    {
        try {
            $penerimaanMaterial = $service->handleStorePenerimaanMaterial(
                $request->validated(), 
                auth()->id(), 
                $pemesananMaterial->id
            );
    
            return redirect()
                ->route('penerimaanMaterials.show', $penerimaanMaterial)
                ->with('success', 'Penerimaan material berhasil disimpan!');
        } catch (\Exception $ex) {
            return redirect()
                ->route('penerimaanMaterials.create', $pemesananMaterial)
                ->with('error', $ex->getMessage());
        }        
    }

    public function show(PenerimaanMaterial $penerimaanMaterial)
    {
        $penerimaanMaterial->load(['produks', 'pemesananMaterial']);

        return view('penerimaanMaterial.detail', compact('penerimaanMaterial'));
    }

    public function edit(PenerimaanMaterial $penerimaanMaterial, PenerimaanMaterialServices $service)
    {
        abort_if(Gate::denies('penerimaanMaterial edit'), 403);

        $penerimaanMaterial->load(['produks', 'pemesananMaterial']);

        $received = $service->handleRetrieveReceivedMaterial($penerimaanMaterial->pemesanan_material_id);

        return view('penerimaanMaterial.edit', compact('penerimaanMaterial', 'received'));
    }

    public function update(UpdatePenerimaanMaterialRequest $request, PenerimaanMaterial $penerimaanMaterial, PenerimaanMaterialServices $service)
    {
        try {
            $penerimaanMaterial = $service->handleUpdatePenerimaanMaterial(
                $request->validated(), 
                $penerimaanMaterial->id
            );
    
            return redirect()
                ->route('penerimaanMaterials.show', $penerimaanMaterial)
                ->with('success', 'Penerimaan material berhasil diubah!');
        } catch (\Exception $ex) {
            return redirect()
                ->route('penerimaanMaterials.edit', $penerimaanMaterial)
                ->with('error', $ex->getMessage());
        }
    }

    public function destroy(PenerimaanMaterial $penerimaanMaterial, PenerimaanMaterialServices $service)
    {
        abort_if(Gate::denies('penerimaanMaterial delete'), 403);

        $penerimaanMaterial = $service->handleDestroyPenerimaanMaterial($penerimaanMaterial->id);

        return redirect()
            ->route('penerimaanMaterials.index')
            ->with('success', 'Penerimaan material berhasil dihapus!');
    }
}
