<?php

namespace App\Http\Controllers;

use App\Models\Akun;
use Illuminate\Support\Facades\Gate;

class AkunController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('akun access'), 403);

        $akuns = Akun::all(['id', 'nama_akun', 'tipe_akun']);

        return view('akun.index', compact('akuns'));
    }
}
