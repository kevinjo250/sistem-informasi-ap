<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfileRequest;
use App\Models\User;

class ProfileController extends Controller
{
    public function edit(User $user)
    {
        abort_if(auth()->user()->id != $user->id, 403);

        return view('profile.profile', compact('user'));
    }

    public function update(UpdateProfileRequest $request, User $user)
    {        
        $user->update($request->validated());
        
        return redirect()
            ->route('profile.edit', $user)
            ->with('success', 'Data profile berhasil diubah!');
    }
}
