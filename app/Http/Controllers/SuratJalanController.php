<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSuratJalanRequest;
use App\Http\Requests\UpdateSuratJalanRequest;
use App\Models\SalesOrder;
use App\Models\SuratJalan;
use App\Services\SuratJalanServices;
use Illuminate\Support\Facades\Gate;

class SuratJalanController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('suratJalan access'), 403);

        $suratJalans = SuratJalan::with(['user', 'salesOrder.pelanggan'])->get();

        return view('suratJalan.index', compact('suratJalans'));
    }

    public function create(SalesOrder $salesOrder, SuratJalanServices $service)
    {
        abort_if(Gate::denies('suratJalan create'), 403);

        if (! $salesOrder->un_shipped_product) {
            return redirect()
                ->route('salesOrders.show', $salesOrder)
                ->with('error', 'Tidak dapat membuat surat jalan!');
        }

        $salesOrder->load(['pelanggan', 'produks' => function($query) {
            $query->where('status_order', '=', 'done')
                ->orWhere('status_order', '=', 'partially shipped');
        }]);

        $shipped = $service->handleRetrieveShippedProduct($salesOrder->id);

        return view('suratJalan.create', compact('salesOrder', 'shipped'));
    }

    public function store(StoreSuratJalanRequest $request, SalesOrder $salesOrder, SuratJalanServices $service)
    {
        try {
            $suratJalan = $service->handleStoreSuratJalan(
                $request->validated(), 
                auth()->id(), 
                $salesOrder->id
            );        
    
            return redirect()
                ->route('suratJalans.show', $suratJalan)
                ->with('success', 'Surat jalan berhasil disimpan!');
        } catch (\Exception $ex) {
            return redirect()
                ->route('suratJalans.create', $salesOrder)
                ->with('error', $ex->getMessage());
        }
    }

    public function show(SuratJalan $suratJalan)
    {
        $suratJalan->load(['produks', 'salesOrder']);

        return view('suratJalan.detail', compact('suratJalan'));
    }

    public function edit(SuratJalan $suratJalan, SuratJalanServices $service)
    {
        abort_if(Gate::denies('suratJalan edit'), 403);

        $suratJalan->load(['produks', 'salesOrder']);

        $shipped = $service->handleRetrieveShippedProduct($suratJalan->sales_order_id);

        return view('suratJalan.edit', compact('suratJalan', 'shipped'));
    }

    public function update(UpdateSuratJalanRequest $request, SuratJalan $suratJalan, SuratJalanServices $service)
    {
        try {
            $suratJalan = $service->handleUpdateSuratJalan(
                $request->validated(), 
                $suratJalan->id
            );
    
            return redirect()
                ->route('suratJalans.show', $suratJalan)
                ->with('success', 'Surat jalan berhasil diubah!');
        } catch (\Exception $ex) {
            return redirect()
                ->route('suratJalans.show', $suratJalan)
                ->with('error', $ex->getMessage());
        }
    }

    public function destroy(SuratJalan $suratJalan, SuratJalanServices $service)
    {
        abort_if(Gate::denies('suratJalan delete'), 403);

        $suratJalan = $service->handleDestroySuratJalan($suratJalan->id);

        return redirect()
            ->route('suratJalans.index')
            ->with('success', 'Surat jalan berhasil dihapus!');
    }
}
