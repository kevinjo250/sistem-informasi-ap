<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePemesananMaterialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal_jatuh_tempo' => ['nullable', 'date'],
            'diskon' => ['nullable', 'integer', 'min:0'],
            'kuantitas.*' => ['required', 'integer', 'min:0'],
            'harga.*' => ['required', 'integer', 'min:0'],
            'keterangan' => ['nullable'],
            'produk_id.*' => ['required'],
            'supplier_id' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'tanggal_jatuh_tempo.date' => 'Tanggal jatuh tempo harus berformat tanggal.',
            'diskon.integer' => 'Diskon harus berupa angka.',
            'diskon.min' => 'Nominal diskon minimal Rp 0.',
            'kuantitas.*.integer' => 'Kuantitas material harus berupa angka.',
            'kuantitas.*.min' => 'Kuantitas material minimal 0.',
            'harga.*.integer' => 'Harga material harus berupa angka.',
            'harga.*.min' => 'Harga material minimal Rp 0.',
            'produk_id.*.required' => 'Produk tidak dapat dikosongi.',
            'supplier_id.required' => 'Supplier tidak dapat dikosongi.',
        ];
    }
}
