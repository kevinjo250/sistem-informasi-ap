<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSuratPerintahKerjaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal_produksi' => ['required', 'date'],
            'kuantitas' => ['required', 'integer', 'min:1'],
            'keterangan' => ['nullable'],
        ];
    }

    public function messages()
    {
        return [
            'tanggal_produksi.required' => 'Tanggal produksi tidak dapat dikosongi.',
            'tanggal_produksi.date' => 'Tanggal produksi harus berformat tanggal.',
            'kuantitas.required' => 'Kuantitas produksi tidak dapat dikosongi.',
            'kuantitas.integer' => 'Kuantitas produksi harus berupa angka.',
            'kuantitas.min' => 'Kuantitas produksi minimal 1.',
        ];
    }
}
