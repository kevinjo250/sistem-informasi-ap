<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSalesOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal_kirim' => ['required', 'date'],
            'tanggal_jatuh_tempo' => ['required', 'date'],
            'diskon' => ['integer', 'min:0'],
            'keterangan' => ['nullable'],
            'pelanggan_id' => ['sometimes', 'required'],
            'produk_id.*'=> ['required'],
            'kuantitas.*' => ['required', 'integer', 'min:1'],
        ];
    }

    public function messages()
    {
        return [
            'tanggal_kirim.required' => 'Tanggal kirim tidak dapat dikosongi.',
            'tanggal_kirim.date' => 'Tanggal kirim harus berformat tanggal.',
            'tanggal_jatuh_tempo.required' => 'Tanggal jatuh tempo tidak dapat dikosongi.',
            'tanggal_jatuh_tempo.date' => 'Tanggal jatuh tempo harus berformat tanggal.',
            'diskon.integer' => 'Diskon harus berupa angka.',
            'diskon.min' => 'Nominal diskon minimal Rp 0.',
            'pelanggan_id.required' => 'Data pelanggan tidak dapat dikosongi.',
            'produk_id.*.required' => 'Produk tidak dapat dikosongi.',
            'kuantitas.*.required' => 'Kuantitas produk tidak dapat dikosongi.',
            'kuantitas.*.integer' => 'Kuantitas produk harus berupa integer.',
            'kuantitas.*.min' => 'Kuantitas produk minimal 1.',
        ];
    }
}
