<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreJabatanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_jabatan' => ['required'],
            'otorisasi_ids' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'nama_jabatan.required' => 'Nama jabatan tidak dapat dikosongi.',
            'otorisasi_ids.required' => 'Daftar otorisasi tidak dapat dikosongi.',
        ];
    }
}
