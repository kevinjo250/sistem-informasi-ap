<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePerkiraanHppRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'biaya_pekerja' => ['required', 'integer', 'min:1'],
            'biaya_overhead' => ['required', 'integer', 'min:1'],
        ];
    }

    public function messages()
    {
        return [
            'biaya_pekerja.required' => 'Biaya pekerja tidak dapat dikosongi.',
            'biaya_pekerja.integer' => 'Biaya pekerja harus berupa angka.',
            'biaya_pekerja.min' => 'Biaya pekerja minimal Rp 1.',
            'biaya_overhead.required' => 'Biaya overhead tidak dapat dikosongi.',
            'biaya_overhead.integer' => 'Biaya overhead harus berupa angka.',
            'biaya_overhead.min' => 'Biaya overhead minimal Rp 1.',
        ];
    }
}
