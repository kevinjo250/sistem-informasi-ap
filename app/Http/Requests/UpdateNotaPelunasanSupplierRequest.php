<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateNotaPelunasanSupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nominal_pelunasan' => ['required', 'integer', 'min:1'],
            'keterangan' => ['nullable'],
            'cara_bayar' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'nominal_pelunasan.required' => 'Nominal pelunasan tidak dapat dikosongi.',
            'nominal_pelunasan.integer' => 'Nominal pelunasan harus berupa angka.',
            'nominal_pelunasan.min' => 'Nominal pelunasan minimal Rp 1.',
            'cara_bayar.required' => 'Cara bayar tidak dapat dikosongi.',
        ];
    }
}
