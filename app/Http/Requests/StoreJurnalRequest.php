<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreJurnalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal' => ['required', 'date'],
            'jenis_transaksi' => ['required'],
            'nomor_bukti' => ['required'],
            'keterangan' => ['nullable'],
            'akun_id.*' => ['required'],
            'debet.*' => ['required', 'integer', 'min:0'],
            'kredit.*' => ['required', 'integer', 'min:0'],
        ];
    }

    public function messages()
    {
        return [
            'tanggal.required' => 'Tanggal transaksi tidak dapat dikosongi.',
            'tanggal.date' => 'Tanggal transaksi harus berformat tanggal.',
            'jenis_transaksi.required' => 'Jenis transaksi tidak dapat dikosongi.',
            'nomor_bukti.required' => 'Nomor bukti tidak dapat dikosongi.',
            'akun_id.*.required' => 'Nama akun tidak dapat dikosongi.',
            'debet.*.required' => 'Nominal debet tidak dapat dikosongi.',
            'debet.*.integer' => 'Nominal debet harus berformat integer.',
            'debet.*.min' => 'Nominal debet minimal Rp 0.',
            'kredit.*.required' => 'Nominal kredit tidak dapat dikosongi.',
            'kredit.*.integer' => 'Nominal kredit harus berformat integer.',
            'kredit.*.min' => 'Nominal kredit minimal Rp 0.',
        ];
    }
}
