<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProdukRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_produk' => ['required'],
            'jenis_produk' => ['required'],
            'ukuran_produk' => ['required'],
            'warna_produk' => ['required'],
            'safety_stok' => ['required', 'integer', 'min:1'],
            'satuan_produk' => ['required'],
            'harga_jual' => ['required', 'integer', 'min:0'],
            'keterangan' => ['nullable'],
            'gambar_produk' => ['nullable', 'image', 'max:2048']
        ];
    }

    public function messages()
    {
        return [
            'nama_produk.required' => 'Nama produk tidak dapat dikosongi.',
            'jenis_produk.required' => 'Jenis produk tidak dapat dikosongi.',
            'ukuran_produk.required' => 'Ukuran produk tidak dapat dikosongi.',
            'warna_produk.required' => 'Warna produk tidak dapat dikosongi.',
            'safety_stok.required' => 'Safety stok tidak dapat dikosongi.',
            'safety_stok.integer' => 'Safety stok harus berupa angka.',
            'safety_stok.min' => 'Safety stok minimal 1.',
            'satuan_produk.required' => 'Satuan produk tidak dapat dikosongi.',
            'harga_jual.required' => 'Harga jual tidak dapat dikosongi.',
            'harga_jual.integer' => 'Harga jual harus berupa angka.',
            'harga_jual.min' => 'Harga jual minimal 0.',
            'gambar_produk.image' => 'Gambar produk harus berformat jpg, jpeg atau png.',
            'gambar_produk.max' => 'Ukuran gambar maximal berukuran 2 MB.',
        ];
    }
}
