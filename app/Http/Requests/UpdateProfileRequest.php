<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProfileRequest extends FormRequest
{
    protected $errorBag = 'update-profile';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->id == $this->user()->id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => ['required', Rule::unique('users', 'username')->ignore($this->user())],
            'nama_karyawan' => ['required'],
            'alamat_karyawan' => ['required'],
            'email_karyawan' => ['nullable', 'email', Rule::unique('users', 'email_karyawan')->ignore($this->user())],
            'no_ktp' => ['required', 'size:16', Rule::unique('users', 'no_ktp')->ignore($this->user())],
            'no_telepon' => ['required', 'min:9', 'max:12', Rule::unique('users', 'no_telepon')->ignore($this->user())],
            'tanggal_lahir' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'Username tidak dapat dikosongi',
            'username.unique' => 'Username telah terdaftar pada database.',
            'nama_karyawan.required' => 'Nama karyawan tidak dapat dikosongi.',
            'email_karyawan.email' => 'Email harus berformat example@example.com.',
            'email_karyawan.unique' => 'Email telah terdaftar pada database.',
            'alamat_karyawan.required' => 'Alamat tidak dapat dikosongi.',
            'no_ktp.required' => 'Nomor KTP tidak dapat dikosongi.',
            'no_ktp.size' => 'Nomor KTP harus memiliki 16 digit.',
            'no_ktp.unique' => 'Nomor KTP telah terdaftar pada database.',
            'no_telepon.required' => 'Nomor telepon tidak dapat dikosongi.',
            'no_telepon.min' => 'Nomor telepon harus lebih dari 9 digit.',
            'no_telepon.max' => 'Nomor telepon harus kurang dari 12 digit.',
            'no_telepon.unique' => 'Nomor telepon telah terdaftar pada database.',
            'tanggal_lahir.required' => 'Tanggal lahir tidak dapat dikosongi.',
        ];
    }
}
