<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSuratJalanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal_kirim' => ['required', 'date'],
            'keterangan' => ['nullable'],
            'kuantitas.*' => ['required', 'integer', 'min:0'],
        ];
    }

    public function messages()
    {
        return [
            'tanggal_kirim.required' => 'Tanggal kirim tidak dapat dikosongi.',
            'tanggal_kirim.date' => 'Tanggal kirim harus berformat tanggal.',
            'kuantitas.*.required' => 'Kuantitas produk tidak dapat dikosongi.',
            'kuantitas.*.integer' => 'Kuantitas produk harus berupa angka.',
            'kuantitas.*.min' => 'Kuantitas produk minimal 0.',
        ];
    }
}
