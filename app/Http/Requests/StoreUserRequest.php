<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => ['required', 'unique:users,username'],
            'nama_karyawan' => ['required'],
            'email_karyawan' => ['nullable', 'email', 'unique:users,email_karyawan'],
            'alamat_karyawan' => ['required'],
            'no_ktp' => ['required', 'size:16', 'unique:users,no_ktp'],
            'no_telepon' => ['required', 'min:9', 'max:12', 'unique:users,no_telepon'],
            'tanggal_lahir' => ['required', 'date'],
            'tanggal_masuk' => ['required', 'date'],
            'password' => ['required', 'min:8', 'confirmed'],
            'divisi_id' => ['required'],
            'jabatan_id' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'Username tidak dapat dikosongi.',
            'username.unique' => 'Username telah dipakai.',
            'nama_karyawan.required' => 'Nama karyawan tidak dapat dikosongi.',
            'email_karyawan.email' => 'Email harus berformat example@example.com.',
            'email_karyawan.unique' => 'Email telah dipakai.',
            'alamat_karyawan.required' => 'Alamat tidak dapat dikosongi.',
            'no_ktp.required' => 'Nomor KTP tidak dapat dikosongi.',
            'no_ktp.size' => 'Nomor KTP harus memiliki 16 digit.',
            'no_ktp.unique' => 'Nomor KTP telah dipakai.',
            'no_telepon.required' => 'Nomor telepon tidak dapat dikosongi.',
            'no_telepon.min' => 'Nomor telepon harus lebih dari 9 digit.',
            'no_telepon.max' => 'Nomor telepon harus kurang dari 12 digit.',
            'no_telepon.unique' => 'Nomor telepon telah dipakai.',
            'tanggal_lahir.required' => 'Tanggal lahir tidak dapat dikosongi.',
            'tanggal_lahir.date' => 'Tanggal lahir harus berformat tanggal.',
            'tanggal_masuk.required' => 'Tanggal masuk tidak dapat dikosongi.',
            'tanggal_masuk.date' => 'Tanggal masuk harus berformat tanggal.',
            'password.required' => 'Password tidak dapat dikosongi.',
            'password.min' => 'Password harus memiliki setidaknya 8 karakter.',
            'password.confirmed' => 'Password yang dimasukkan tidak cocok.',
            'divisi_id.required' => 'Divisi karyawan tidak dapat dikosongi.',
            'jabatan_id.required' => 'Jabatan karyawan tidak dapat dikosongi.',
        ];
    }
}
