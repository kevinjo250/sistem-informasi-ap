<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdatePelangganRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_pelanggan' => ['required'],
            'alamat_pelanggan' => ['required'],
            'kota_pelanggan' => ['required'],
            'email_pelanggan' => ['nullable', 'email', Rule::unique('pelanggans', 'email_pelanggan')->ignore($this->pelanggan)],
            'no_ktp' => ['required', 'size:16', Rule::unique('pelanggans', 'no_ktp')->ignore($this->pelanggan)],
            'no_npwp' => ['nullable', 'size:15', Rule::unique('pelanggans', 'no_npwp')->ignore($this->pelanggan)],
            'no_telepon' => ['required', 'min:9', 'max:12', Rule::unique('pelanggans', 'no_telepon')->ignore($this->pelanggan)],
        ];
    }

    public function messages()
    {
        return [
            'nama_pelanggan.required' => 'Nama pelanggan tidak dapat dikosongi.',
            'alamat_pelanggan.required' => 'Alamat pelanggan tidak dapat dikosongi.',
            'kota_pelanggan.required' => 'Kota pelanggan tidak dapat dikosongi.',
            'email_pelanggan.email' => 'Email harus berformat example@example.com.',
            'email_pelanggan.unique' => 'Email telah terdaftar pada database.',
            'no_ktp.required' => 'Nomor KTP tidak dapat dikosongi.',
            'no_ktp.size' => 'Nomor KTP harus memiliki 16 digit.',
            'no_ktp.unique' => 'Nomor KTP telah terdaftar pada database.',
            'no_npwp.size' => 'Nomor NPWP harus memiliki 15 digit.',
            'no_npwp.unique' => 'Nomor NPWP telah terdaftar pada database.',
            'no_telepon.required' => 'Nomor telepon tidak dapat dikosongi.',
            'no_telepon.min' => 'Nomor telepon harus lebih dari 9 digit.',
            'no_telepon.max' => 'Nomor telepon harus kurang dari 12 digit.',
            'no_telepon.unique' => 'Nomor telepon telah terdaftar pada database.',
        ];
    }
}
