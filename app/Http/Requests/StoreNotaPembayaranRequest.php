<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNotaPembayaranRequest extends FormRequest
{
    protected $errorBag = 'store-nota-pembayaran';
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nominal_bayar' => ['required', 'integer', 'min:1'],
            'keterangan' => ['nullable'],
            'cara_bayar' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'nominal_bayar.required' => 'Nominal bayar tidak dapat dikosongi.',
            'nominal_bayar.integer' => 'Nominal bayar harus berupa angka.',
            'nominal_bayar.min' => 'Nominal bayar minimal Rp 1.',
            'cara_bayar.required' => 'Cara bayar tidak dapat dikosongi.',
        ];
    }
}
