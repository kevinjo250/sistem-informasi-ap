<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FindBukuBesarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'periode_akuntansi' => ['required', 'date_format:Y-m'],
        ];
    }

    public function messages()
    {
        return [
            'periode_akuntansi.required' => 'Periode akuntansi tidak dapat dikosongi.',
            'periode_akuntansi.date_format' => 'Periode akuntansi berformat Y-m.',
        ];
    }
}
