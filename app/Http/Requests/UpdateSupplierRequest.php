<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateSupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_supplier' => ['required'],
            'alamat_supplier' => ['required'],
            'kota_supplier' => ['required'],
            'email_supplier' => ['nullable', 'email', Rule::unique('suppliers', 'email_supplier')->ignore($this->supplier)],
            'no_telepon' => ['required', 'min:9', 'max:12', Rule::unique('suppliers', 'no_telepon')->ignore($this->supplier)],
        ];
    }

    public function messages()
    {
        return [
            'nama_supplier.required' => 'Nama supplier tidak dapat dikosongi.',
            'alamat_supplier.required' => 'Alamat supplier tidak dapat dikosongi.',
            'kota_supplier.required' => 'Kota supplier tidak dapat dikosongi.',
            'email_supplier.email' => 'Email harus berformat example@example.com.',
            'email_supplier.unique' => 'Email telah terdaftar pada database.',
            'no_telepon.required' => 'Nomor telepon tidak dapat dikosongi.',
            'no_telepon.min' => 'Nomor telepon harus lebih dari 9 digit.',
            'no_telepon.max' => 'Nomor telepon harus kurang dari 12 digit.',
            'no_telepon.unique' => 'Nomor telepon telah terdaftar pada database.',
        ];
    }
}
