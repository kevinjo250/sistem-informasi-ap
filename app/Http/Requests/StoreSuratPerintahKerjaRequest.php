<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSuratPerintahKerjaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal_produksi' => ['required', 'date'],
            'produk_sales_order_id' => ['required'],
            'kuantitas' => ['required', 'integer', 'min:1'],
            'keterangan' => ['nullable'],
        ];
    }

    public function messages()
    {
        return [
            'tanggal_produksi.required' => 'Tanggal produksi tidak dapat dikosongi.',
            'tanggal_produksi.date' => 'Tanggal produksi harus berformat tanggal.',
            'produk_sales_order_id.required'=> 'Produk yang diproduksi tidak dapat dikosongi.',
            'kuantitas.required' => 'Kuantitas produksi tidak dapat dikosongi.',
            'kuantitas.integer' => 'Kuantitas produksi harus berupa angka.',
            'kuantitas.min' => 'Kuantitas produksi minimal 1.',
        ];
    }
}
