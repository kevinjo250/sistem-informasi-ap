<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePenerimaanMaterialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'produk_id.*' => ['required'],
            'kuantitas.*' => ['required', 'integer', 'min:0']
        ];
    }

    public function messages()
    {
        return [
            'produk_id.*.required' => 'Material tidak dapat dikosongi.',
            'kuantitas.*.required' => 'Kuantitas material tidak dapat dikosongi.',
            'kuantitas.*.integer' => 'Kuantitas material harus berupa angka.',
            'kuantitas.*.min' => 'Kuantitas material minimal 0.',
        ];
    }
}
