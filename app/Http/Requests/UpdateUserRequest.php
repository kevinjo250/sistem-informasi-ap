<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal_masuk' => ['required', 'date'],
            'divisi_id' => ['required'],
            'jabatan_id' => ['required'],
            'status_karyawan' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'tanggal_masuk.required' => 'Tanggal masuk tidak dapat dikosongi.',
            'tanggal_masuk.date' => 'Tanggal masuk harus memiliki format tanggal.',
            'divisi_id.required' => 'Divisi karyawan tidak dapat dikosongi.',
            'jabatan_id.required' => 'Jabatan karyawan tidak dapat dikosongi.',
            'status_karyawan.required' => 'Status karyawan tidak dapat dikosongi.',
        ];
    }
}
