<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBillOfMaterialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'material_id.*' => ['required'],
            'kuantitas.*' => ['required', 'integer', 'min:1'],
        ];
    }

    public function messages()
    {
        return [
            'material_id.*.required' => 'Material tidak dapat dikosongi.',
            'kuantitas.*.required' => 'Kuantitas material tidak dapat dikosongi.',
            'kuantitas.*.integer' => 'Kuantitas material harus berupa angka.',
            'kuantitas.*.min' => 'Kuantitas material minimal 1.',
        ];
    }
}
