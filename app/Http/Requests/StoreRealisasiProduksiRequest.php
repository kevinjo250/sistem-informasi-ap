<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRealisasiProduksiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'surat_perintah_kerja_id' => ['required'],
            'tanggal_mulai' => ['required', 'date'],
            'tanggal_selesai' => ['required', 'date', 'after:tanggal_mulai'],
            'kuantitas_selesai' => ['required', 'integer', 'min:1'],
            'biaya_material' => ['required', 'integer', 'min:1'],
            'biaya_pekerja' => ['required', 'integer', 'min:1'],
            'biaya_overhead' => ['required', 'integer', 'min:1'],
            'produk_id' => ['sometimes', 'present', 'required'],
            'kuantitas_rusak' => ['sometimes', 'present', 'required'],
            'kuantitas_sisa' => ['sometimes', 'present', 'required'],
            'kuantitas_rusak.*' => ['integer', 'min:0'],
            'kuantitas_sisa.*' => ['integer', 'min:0'],
        ];
    }

    public function messages()
    {
        return [
            'surat_perintah_kerja_id.required' => 'Nomor surat perintah kerja tidak dapat dikosongi.',
            'tanggal_mulai.required' => 'Tanggal mulai tidak dapat dikosongi.',
            'tanggal_mulai.date' => 'Tanggal mulai harus berformat tanggal.',
            'tanggal_selesai.required' => 'Tanggal selesai tidak dapat dikosongi.',
            'tanggal_selesai.date' => 'Tanggal selesai harus berformat tanggal.',
            'tanggal_selesai.after' => 'Tanggal selesai harus berbeda dengan tanggal mulai.',
            'kuantitas_selesai.required' => 'Kuantitas selesai tidak dapat dikosongi.',
            'kuantitas_selesai.integer' => 'Kuantitas selesai harus berupa angka.',
            'kuantitas_selesai.min' => 'Kuantitas selesai minimal 1.',
            'biaya_material.required' => 'Biaya material tidak dapat dikosongi.',
            'biaya_material.integer' => 'Biaya material harus berupa angka.',
            'biaya_material.min' => 'Biaya material minimal Rp 1.',
            'biaya_pekerja.required' => 'Biaya pekerja tidak dapat dikosongi.',
            'biaya_pekerja.integer' => 'Biaya pekerja harus berupa angka.',
            'biaya_pekerja.min' => 'Biaya pekerja minimal Rp 1.',
            'biaya_overhead.required' => 'Biaya overhead tidak dapat dikosongi.',
            'biaya_overhead.integer' => 'Biaya overhead harus berupa angka.',
            'biaya_overhead.min' => 'Biaya overhead minimal Rp 1.',
            'kuantitas_rusak.*.integer' => 'Kuantitas rusak harus berupa angka.',
            'kuantitas_rusak.*.min' => 'Kuantitas rusak minimal 0.',
            'kuantitas_sisa.*.integer' => 'Kuantitas sisa harus berupa angka.',
            'kuantitas_sisa.*.min' => 'Kuantitas sisa minimal 0.',
        ];
    }
}
