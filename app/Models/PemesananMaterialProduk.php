<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class PemesananMaterialProduk extends Pivot
{
    public function getSubTotalAttribute()
    {
        return $this->kuantitas * $this->harga;
    }
}
