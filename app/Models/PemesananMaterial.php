<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PemesananMaterial extends Model
{
    use HasFactory;

    protected $fillable = [
        'no_nota',
        'tanggal_jatuh_tempo',
        'total',
        'diskon',
        'keterangan',
        'status_nota',
        'status_pembayaran',
        'supplier_id',
        'user_id',
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function produks()
    {
        return $this->belongsToMany(Produk::class)
            ->using(PemesananMaterialProduk::class)
            ->withPivot(['kuantitas', 'harga', 'status_material']);
    }

    public function penerimaanMaterials()
    {
        return $this->hasMany(PenerimaanMaterial::class);
    }

    public function notaPelunasanSuppliers()
    {
        return $this->hasMany(NotaPelunasanSupplier::class);
    }

    public function getGrandTotalAttribute()
    {
        return ($this->total - $this->diskon);
    }

    public function getNominalPelunasanAttribute()
    {
        return $this->notapelunasansuppliers->sum('nominal_pelunasan');
    }
}
