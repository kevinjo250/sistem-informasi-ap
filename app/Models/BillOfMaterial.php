<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BillOfMaterial extends Model
{
    use HasFactory;

    protected $fillable = [
        'produk_id',
    ];

    public function produk()
    {
        return $this->belongsTo(Produk::class);
    }

    public function produks()
    {
        return $this->belongsToMany(Produk::class)
            ->withPivot(['kuantitas']);
    }

    public function perkiraanHpp()
    {
        return $this->hasOne(PerkiraanHpp::class);
    }
}
