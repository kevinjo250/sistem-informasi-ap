<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jurnal extends Model
{
    use HasFactory;

    protected $fillable = [
        'tanggal',
        'jenis_jurnal',
        'jenis_transaksi',
        'nomor_bukti',
        'keterangan',
    ];

    public function akuns()
    {
        return $this->belongsToMany(Akun::class)
            ->withPivot(['id', 'debet', 'kredit']);
    }
}
