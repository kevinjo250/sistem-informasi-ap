<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotaPelunasanSupplier extends Model
{
    use HasFactory;

    protected $fillable = [
        'no_nota',
        'cara_bayar',
        'nominal_pelunasan',
        'keterangan',
        'user_id',
        'pemesanan_material_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function pemesananMaterial()
    {
        return $this->belongsTo(PemesananMaterial::class);
    }
}
