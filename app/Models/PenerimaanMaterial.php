<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenerimaanMaterial extends Model
{
    use HasFactory;

    protected $fillable = [
        'no_nota',
        'pemesanan_material_id',
        'user_id',
    ];

    public function pemesananMaterial()
    {
        return $this->belongsTo(PemesananMaterial::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function produks()
    {
        return $this->belongsToMany(Produk::class)
            ->withPivot(['kuantitas']);
    }
}
