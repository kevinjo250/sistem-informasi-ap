<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Akun extends Model
{
    use HasFactory;

    protected $fillables = [
        'nama_akun',
        'tipe_akun',
        'urutan_akun',
    ];

    public function jurnals()
    {
        return $this->belongsToMany(Jurnal::class)
            ->withPivot(['id', 'debet', 'kredit']);
    }

    public function periodes()
    {
        return $this->belongsToMany(Periode::class)
            ->withPivot(['id', 'saldo_awal', 'saldo_akhir']);
    }

    public function getSumDebetAttribute()
    {
        return $this->jurnals->sum(function($jurnal) {
            return $jurnal->pivot->debet;
        });
    }

    public function getSumKreditAttribute()
    {
        return $this->jurnals->sum(function($jurnal) {
            return $jurnal->pivot->kredit;
        });
    }
}
