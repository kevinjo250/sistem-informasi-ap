<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_pelanggan',
        'alamat_pelanggan',
        'kota_pelanggan',
        'email_pelanggan',
        'no_ktp',
        'no_npwp',
        'no_telepon',
    ];

    public function salesOrders()
    {
        return $this->hasMany(SalesOrder::class);
    }
}
