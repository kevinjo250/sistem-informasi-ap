<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Otorisasi extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_otorisasi',
    ];

    public function jabatans()
    {
        return $this->belongsToMany(Jabatan::class);
    }
}
