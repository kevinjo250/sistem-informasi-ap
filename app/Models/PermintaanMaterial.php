<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermintaanMaterial extends Model
{
    use HasFactory;

    protected $fillable = [
        'no_nota',
        'surat_perintah_kerja_id',
        'user_id',
        'status_nota',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function suratPerintahKerja()
    {
        return $this->belongsTo(SuratPerintahKerja::class);
    }

    public function produks()
    {
        return $this->belongsToMany(Produk::class)
            ->withPivot(['kuantitas', 'hpp']);
    }

    public function getBiayaMaterialAttribute()
    {
        return $this->produks->sum(function($produk) {
            return $produk->pivot->kuantitas * $produk->pivot->hpp;
        });
    }
}
