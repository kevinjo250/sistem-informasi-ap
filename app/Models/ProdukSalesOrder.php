<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProdukSalesOrder extends Pivot
{
    public function suratPerintahKerjas()
    {
        return $this->hasMany(
            SuratPerintahKerja::class, 
            'produk_sales_order_id'
        );
    }

    public function salesOrder()
    {
        return $this->belongsTo(SalesOrder::class);
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class);
    }

    public function getSubTotalAttribute()
    {
        return $this->kuantitas * $this->harga;
    }
}
