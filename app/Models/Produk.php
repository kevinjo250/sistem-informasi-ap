<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_produk',
        'jenis_produk',
        'ukuran_produk',
        'warna_produk',
        'safety_stok',
        'stok_produk',
        'satuan_produk',
        'harga_pokok',
        'harga_jual',
        'keterangan',
        'gambar_produk'
    ];

    public function salesOrders()
    {
        return $this->belongsToMany(SalesOrder::class)
            ->using(ProdukSalesOrder::class)
            ->withPivot(['id', 'kuantitas', 'harga', 'status_order']);
    }

    public function suratJalans()
    {
        return $this->belongsToMany(SuratJalan::class)
            ->withPivot(['kuantitas']);
    }

    public function pemesananMaterials()
    {
        return $this->belongsToMany(PemesananMaterial::class)
            ->using(PemesananMaterialProduk::class)
            ->withPivot(['kuantitas', 'harga']);
    }

    public function penerimaanMaterials()
    {
        return $this->belongsToMany(PenerimaanMaterial::class)
            ->withPivot('kuantitas');
    }

    public function billOfMaterial()
    {
        return $this->hasOne(BillOfMaterial::class);
    }

    public function billOfMaterials()
    {
        return $this->belongsToMany(BillOfMaterial::class)
            ->withPivot('kuantitas');
    }

    public function permintaanMaterials()
    {
        return $this->belongsToMany(PermintaanMaterial::class)
            ->withPivot(['kuantitas', 'hpp']);
    }

    public function realisasiProduksis()
    {
        return $this->belongsToMany(RealisasiProduksi::class)
            ->withPivot(['id', 'kuantitas_rusak', 'kuantitas_selesai']);
    }

    public function scopeOnlyMaterial($query)
    {
        return $query->where('jenis_produk', 'material');
    }

    public function scopeOnlyFinishedProduct($query)
    {
        return $query->where('jenis_produk', 'finished product');
    }
}
