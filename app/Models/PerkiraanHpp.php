<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PerkiraanHpp extends Model
{
    use HasFactory;

    protected $fillable = [
        'bill_of_material_id',
        'biaya_material',
        'biaya_pekerja',
        'biaya_overhead',
    ];

    public function billOfMaterial()
    {
        return $this->belongsTo(BillOfMaterial::class);
    }

    public function getNominalHppAttribute()
    {
        return $this->biaya_material + $this->biaya_pekerja + $this->biaya_overhead;
    }
}
