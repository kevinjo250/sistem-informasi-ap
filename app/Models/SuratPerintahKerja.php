<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuratPerintahKerja extends Model
{
    use HasFactory;

    protected $fillable = [
        'no_nota',
        'tanggal_produksi',
        'produk_sales_order_id',
        'kuantitas',
        'status_produksi',
        'keterangan',
    ];

    public function produkSalesOrder()
    {
        return $this->belongsTo(ProdukSalesOrder::class);
    }

    public function salesOrder()
    {
        return $this->query()->with('produkSalesOrder.salesOrder')->first();
    }

    public function produk()
    {
        return $this->query()->with('produkSalesOrder.produk')->first();
    }

    public function permintaanMaterial()
    {
        return $this->hasOne(PermintaanMaterial::class);
    }

    public function realisasiProduksis()
    {
        return $this->hasMany(RealisasiProduksi::class);
    }

    public function scopeInProduction($query)
    {
        return $query->where('status_produksi', 'in production');
    }
}
