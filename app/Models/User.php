<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'nama_karyawan',
        'alamat_karyawan',
        'email_karyawan',
        'no_ktp',
        'no_telepon',
        'tanggal_lahir',
        'tanggal_masuk',
        'status_karyawan',
        'password',
        'divisi_id',
        'jabatan_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function divisi()
    {
        return $this->belongsTo(Divisi::class);
    }

    public function jabatan()
    {
        return $this->belongsTo(Jabatan::class);
    }

    public function salesOrders()
    {
        return $this->hasMany(SalesOrder::class);
    }

    public function batalOrders()
    {
        return $this->hasMany(BatalOrder::class);
    }

    public function notaPembayarans()
    {
        return $this->hasMany(NotaPembayaran::class);
    }

    public function suratJalans()
    {
        return $this->hasMany(SuratJalan::class);
    }

    public function pemesananMaterials()
    {
        return $this->hasMany(PemesananMaterial::class);
    }

    public function penerimaanMaterials()
    {
        return $this->hasMany(PenerimaanMaterial::class);
    }

    public function notaPelunasanSuppliers()
    {
        return $this->hasMany(NotaPelunasanSupplier::class);
    }

    public function permintaanMaterials()
    {
        return $this->hasMany(PermintaanMaterial::class);
    }

    public function realisasiProduksis()
    {
        return $this->hasMany(RealisasiProduksi::class);
    }
}
