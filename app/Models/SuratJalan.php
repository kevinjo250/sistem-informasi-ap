<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuratJalan extends Model
{
    use HasFactory;

    protected $fillable = [
        'no_nota',
        'tanggal_kirim',
        'keterangan',
        'sales_order_id',
        'user_id',
    ];

    public function salesOrder()
    {
        return $this->belongsTo(SalesOrder::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function produks()
    {
        return $this->belongsToMany(Produk::class)
            ->withPivot(['kuantitas']);
    }

    public function getTotalHppAttribute()
    {
        return $this->produks->sum(function($produk) {
            return $produk->pivot->kuantitas * $produk->harga_pokok;
        });
    }
}
