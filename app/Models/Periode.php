<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Periode extends Model
{
    use HasFactory;

    protected $fillable = [
        'tanggal_awal',
        'tanggal_akhir',
    ];

    public function akuns()
    {
        return $this->belongsToMany(Akun::class)
            ->withPivot(['id', 'saldo_awal', 'saldo_akhir']);
    }
}
