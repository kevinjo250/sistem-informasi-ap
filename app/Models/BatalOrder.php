<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BatalOrder extends Model
{
    use HasFactory;

    protected $fillable = [
        'no_nota',
        'keterangan',
        'user_id',
        'sales_order_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function salesOrder()
    {
        return $this->belongsTo(SalesOrder::class);
    }
}
