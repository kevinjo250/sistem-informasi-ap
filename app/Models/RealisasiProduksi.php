<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RealisasiProduksi extends Model
{
    use HasFactory;

    protected $fillable = [
        'no_nota',
        'surat_perintah_kerja_id',
        'tanggal_mulai',
        'tanggal_selesai',
        'kuantitas_selesai',
        'biaya_material',
        'biaya_pekerja',
        'biaya_overhead',
        'user_id',
    ];

    public function suratPerintahKerja()
    {
        return $this->belongsTo(SuratPerintahKerja::class);
    }

    public function produks()
    {
        return $this->belongsToMany(Produk::class)
            ->withPivot(['id', 'kuantitas_rusak', 'kuantitas_sisa']);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getRealisasiBiayaAttribute()
    {
        return $this->biaya_material + $this->biaya_pekerja + $this->biaya_overhead;
    }
}
