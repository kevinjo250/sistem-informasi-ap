<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_supplier',
        'alamat_supplier',
        'kota_supplier',
        'email_supplier',
        'no_telepon',
    ];

    public function pemesananMaterials()
    {
        return $this->hasMany(PemesananMaterial::class);
    }
}
