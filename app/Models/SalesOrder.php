<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class  SalesOrder extends Model
{
    use HasFactory;

    protected $fillable = [
        'no_nota',
        'tanggal_kirim',
        'tanggal_jatuh_tempo',
        'total',
        'diskon',
        'keterangan',
        'status_nota',
        'status_pembayaran',
        'user_id',
        'pelanggan_id',
    ];

    public function produks()
    {
        return $this->belongsToMany(Produk::class)
            ->using(ProdukSalesOrder::class)
            ->withPivot(['id', 'kuantitas', 'harga', 'status_order']);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class);
    }

    public function batalOrder()
    {
        return $this->hasOne(BatalOrder::class);
    }

    public function notaPembayarans()
    {
        return $this->hasMany(NotaPembayaran::class);
    }

    public function suratJalans()
    {
        return $this->hasMany(SuratJalan::class);
    }

    public function getGrandTotalAttribute()
    {
        return ($this->total - $this->diskon);
    }

    public function getNominalBayarAttribute()
    {
        return $this->notapembayarans->sum('nominal_bayar');
    }

    public function getUnShippedProductAttribute()
    {
        return $this->produks()
            ->wherePivotIn('status_order', ['done', 'partially shipped'])
            ->count();
    }

    public function scopeConfirmed($query)
    {
        return $query->where('status_nota', 'confirmed');
    }
}
