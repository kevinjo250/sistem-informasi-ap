## Instalation Steps Sistem Informasi Akuntansi dan Produksi

1. Clone the code using `git@gitlab.com:kevinjo250/sistem-informasi-ap.git` or `git clone https://gitlab.com/kevinjo250/sistem-informasi-ap.git`

2. After the clone process is done, run `composer update`

3. Find file `.env.example` and duplicate it. Change the file name to `.env`

4. After the update process is done, run `php artisan key:generate`

5. Laravel will generate new key for the application, then prepare database named with `sistem_informasi_ap` (Username: root, No password)

6. Then run, `php artisan migrate:fresh --seed`

7. (Username for access `administrator` and password for access `administrator123`) and (Username for access `bob` and password for access `password123`)