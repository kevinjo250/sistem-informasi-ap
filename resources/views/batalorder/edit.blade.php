@extends('layouts.app')

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Ubah Batal Order</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Batal Order
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-vertical-layouts">
    <div class="row match-height">
        <div class="col-md-6 col-4">
            <div class="card">
                <form class="form form-vertical" action="{{ route('batalOrders.update', $batalOrder) }}" method="post">
                    @csrf
                    @method('patch')

                    <div class="card-header">
                        <h5 class="card-title">{{ $batalOrder->no_nota }}</h5>
                    </div>
                    <div class="card-body">                        
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Nomor Sales Order</label>
                                        <input type="text" class="form-control" value="{{ $batalOrder->salesOrder->no_nota }}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Keterangan</label>
                                        <textarea aria-describedby="keteranganHelp" class="form-control @error('keterangan') is-invalid @enderror" name="keterangan" cols="20" rows="5" placeholder="Masukan keterangan tambahan">{{ old('keterangan', $batalOrder->keterangan) }}</textarea>

                                        @error('keterangan')
                                            <small id="keteranganHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row ">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-warning">Ubah Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection