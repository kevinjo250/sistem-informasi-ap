@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Buat Jabatan</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Jabatan
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-vertical-layouts">
    <div class="row match-height">
        <div class="col-md-6 col-12">
            <div class="card">
                <form class="form form-vertical" action="{{ route('jabatans.store') }}" method="post">
                    @csrf
                    
                    <div class="card-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Nama Jabatan</label>
                                        <input type="text" aria-describedby="namaHelp" class="form-control @error('nama_jabatan') is-invalid @enderror" name="nama_jabatan" placeholder="Masukan Nama Jabatan" value="{{ old('nama_jabatan') }}">

                                        @error('nama_jabatan')
                                            <small id="namaHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Otorisasi Jabatan</label>
                                        <select aria-describedby="otorisasiHelp" class="select2 form-control" name="otorisasi_ids[]" multiple="multiple">
                                            @foreach($otorisasis as $otorisasi)
                                                <option value="{{ $otorisasi->id }}" {{ (in_array($otorisasi->id, old('otorisasi_ids', []))) ? 'selected' : '' }}>{{ $otorisasi->nama_otorisasi }}</option>
                                            @endforeach
                                        </select>

                                        @error('otorisasi_ids')
                                            <small id="otorisasiHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row ">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Simpan Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
@endsection