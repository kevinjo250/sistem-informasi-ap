@extends('layouts.app')

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Detail Jurnal</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Jurnal
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row" id="table-bordered">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Jurnal Umum</h4>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered mb-0">
                    <thead>
                        <tr>
                            <th>tanggal</th>
                            <th>no bukti</th>
                            <th>keterangan</th>
                            <th>akun</th>
                            <th>debet</th>
                            <th>kredit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($jurnals as $jurnal)
                            @if ($jurnal->jenis_jurnal == 'JU')
                                <tr>
                                    <td rowspan="{{ $jurnal->akuns->count() }}">
                                        <small class="text-muted">@datetime($jurnal->tanggal)</small>
                                    </td>
                                    <td rowspan="{{ $jurnal->akuns->count() }}">
                                        @switch($jurnal->jenis_transaksi)
                                            @case('penjualan')                                            
                                                <a href="{{ route('salesOrders.show', $salesOrders->where('no_nota', $jurnal->nomor_bukti)->first()->id) }}">{{ $jurnal->nomor_bukti }}</a>
                                                @break
                                            @case('pembatalan')
                                                <a href="#">{{ $jurnal->nomor_bukti }}</a>
                                            @break
                                            @case('pembayaran pelanggan')                                            
                                                <a href="{{ route('notaPembayarans.show', $notaPembayarans->where('no_nota', $jurnal->nomor_bukti)->first()->id) }}">{{ $jurnal->nomor_bukti }}</a>
                                                @break
                                            @case('pengiriman')                                            
                                                <a href="{{ route('suratJalans.show', $suratJalans->where('no_nota', $jurnal->nomor_bukti)->first()->id) }}">{{ $jurnal->nomor_bukti }}</a>
                                                @break
                                            @case('pembelian')                                            
                                                <a href="{{ route('pemesananMaterials.show', $pemesananMaterials->where('no_nota', $jurnal->nomor_bukti)->first()->id) }}">{{ $jurnal->nomor_bukti }}</a>
                                                @break
                                            @case('penerimaan material')                                            
                                                <a href="{{ route('penerimaanMaterials.show', $penerimaanMaterials->where('no_nota', $jurnal->nomor_bukti)->first()->id) }}">{{ $jurnal->nomor_bukti }}</a>
                                                @break
                                            @case('pelunasan supplier')                                            
                                                <a href="{{ route('notaPelunasanSuppliers.show', $pelunasanSuppliers->where('no_nota', $jurnal->nomor_bukti)->first()->id) }}">{{ $jurnal->nomor_bukti }}</a>
                                                @break
                                            @case('permintaan material')                                            
                                                <a href="{{ route('permintaanMaterials.show', $permintaanMaterials->where('no_nota', $jurnal->nomor_bukti)->first()->id) }}">{{ $jurnal->nomor_bukti }}</a>
                                                @break
                                            @case('produksi')
                                                <a href="{{ route('realisasiProduksis.show', $realisasiProduksis->where('no_nota', $jurnal->nomor_bukti)->first()->id) }}">{{ $jurnal->nomor_bukti }}</a>
                                                @break
                                            @default
                                                {{ $jurnal->nomor_bukti }}
                                        @endswitch
                                    </td>
                                    <td rowspan="{{ $jurnal->akuns->count() }}">
                                        @switch($jurnal->keterangan)
                                            @case(null)
                                                <small class="text-muted">-</small>
                                                @break
                                            @default
                                                <small class="text-muted">{{ $jurnal->keterangan }}</small>
                                        @endswitch
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $jurnal->akuns[0]->nama_akun }}</small>
                                    </td>
                                    <td class="text-right">
                                        <small class="text-muted">@money($jurnal->akuns[0]->pivot->debet)</small>
                                    </td>
                                    <td class="text-right">
                                        <small class="text-muted">@money($jurnal->akuns[0]->pivot->kredit)</small>
                                    </td>
                                </tr>
                                @for ($i = 1; $i < $jurnal->akuns->count(); $i++)
                                    <tr>
                                        <td>
                                            <small class="text-muted">{{ $jurnal->akuns[$i]->nama_akun }}</small>
                                        </td>
                                        <td class="text-right">
                                            <small class="text-muted">@money($jurnal->akuns[$i]->pivot->debet)</small>
                                        </td>
                                        <td class="text-right">
                                            <small class="text-muted">@money($jurnal->akuns[$i]->pivot->kredit)</small>
                                        </td>
                                    </tr>
                                @endfor
                            @endif                            
                        @empty
                            <tr>
                                <td colspan="6" class="text-muted text-center">No Data Available.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Jurnal Penutup</h4>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered mb-0">
                    <thead>
                        <tr>
                            <th>tanggal</th>
                            <th>no bukti</th>
                            <th>keterangan</th>
                            <th>akun</th>
                            <th>debet</th>
                            <th>kredit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($jurnals as $jurnal)
                            @if ($jurnal->jenis_jurnal == 'JT')
                                <tr>
                                    <td rowspan="{{ $jurnal->akuns->count() }}">
                                        <small class="text-muted">@datetime($jurnal->tanggal)</small>
                                    </td>
                                    <td rowspan="{{ $jurnal->akuns->count() }}">
                                        <small class="text-muted">-</small>
                                    </td>
                                    <td rowspan="{{ $jurnal->akuns->count() }}">
                                        <small class="text-muted">{{ $jurnal->keterangan }}</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $jurnal->akuns[0]->nama_akun }}</small>
                                    </td>
                                    <td class="text-right">
                                        <small class="text-muted">@money($jurnal->akuns[0]->pivot->debet)</small>
                                    </td>
                                    <td class="text-right">
                                        <small class="text-muted">@money($jurnal->akuns[0]->pivot->kredit)</small>
                                    </td>
                                </tr>
                                @for ($i = 1; $i < $jurnal->akuns->count(); $i++)
                                    <tr>
                                        <td>
                                            <small class="text-muted">{{ $jurnal->akuns[$i]->nama_akun }}</small>
                                        </td>
                                        <td class="text-right">
                                            <small class="text-muted">@money($jurnal->akuns[$i]->pivot->debet)</small>
                                        </td>
                                        <td class="text-right">
                                            <small class="text-muted">@money($jurnal->akuns[$i]->pivot->kredit)</small>
                                        </td>
                                    </tr>
                                @endfor
                            @endif
                        @empty
                            <tr>
                                <td colspan="6" class="text-muted text-center">No Data Available.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection