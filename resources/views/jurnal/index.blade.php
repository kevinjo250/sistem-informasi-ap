@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Jurnal</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Jurnal
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-vertical-layouts">
    <div class="invoice-create-btn mb-1">
        <a href="{{ route('jurnals.create') }}" class="btn btn-primary btn-sm round glow align-items-center">Buat Jurnal</a>
    </div>
    <div class="row">
        <div class="col-md-4 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Jurnal Akuntansi</h4>
                </div>
                <form class="form form-vertical" action="{{ route('jurnals.find') }}" method="post">
                    @csrf
                    
                    <div class="card-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Periode Akuntansi</label>
                                        <input type="month" class="form-control @error('periode_akuntansi') is-invalid @enderror" aria-describedby="periodeHelp" name="periode_akuntansi">

                                        @error('periode_akuntansi')
                                            <small id="periodeHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Jenis Transaksi</label>
                                        <select class="form-control @error('jenis_transaksi') is-invalid @enderror" aria-describedby="jenisHelp" name="jenis_transaksi">
                                            <option value="all" selected>Seluruh Transaksi</option>
                                            <option value="penjualan">Penjualan</option>
                                            <option value="pembatalan">Pembatalan</option>
                                            <option value="pembayaran pelanggan">Pembayaran Pelanggan</option>
                                            <option value="pengiriman">Pengiriman</option>
                                            <option value="pembelian">Pembelian</option>
                                            <option value="penerimaan material">Penerimaan Material</option>
                                            <option value="pelunasan supplier">Pelunasan Supplier</option>
                                            <option value="permintaan material">Permintaan Material</option>
                                            <option value="produksi">Produksi</option>
                                            <option value="lain-lain">Lain-lain</option>
                                        </select>

                                        @error('jenis_transaksi')
                                            <small id="jenisHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Tampilkan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Jurnal Penutup</h4>
                </div>
                <form class="form form-vertical">                    
                    <div class="card-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Periode Akuntansi</label>
                                        <input type="month" class="form-control" value="{{ date('Y-m') }}" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-6">

                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <a href="{{ route('jurnals.close') }}" class="btn btn-danger">Tutup Jurnal</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if(Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif

    @if(Session::has('error'))
        toastr['error']("{{ session('error') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection