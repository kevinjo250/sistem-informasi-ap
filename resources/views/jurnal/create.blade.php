@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Buat Jurnal</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Jurnal
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-vertical-layouts">
    <div class="row">
        <div class="col-md-6 col-12">
            <div class="card">
                <form class="form form-vertical" action="{{ route('jurnals.store') }}" method="post">
                    @csrf
                    
                    <div class="card-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Tanggal Transaksi</label>
                                        <input type="datetime-local" 
                                            aria-describedby="tanggalHelp" 
                                            class="form-control @error('tanggal') is-invalid @enderror" 
                                            name="tanggal"
                                            value="{{ old('tanggal') }}"
                                        >

                                        @error('tanggal')
                                            <small id="tanggalHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Nomor Bukti</label>
                                        <input type="text" 
                                            aria-describedby="nomorBuktiHelp" 
                                            class="form-control @error('nomor_bukti') is-invalid @enderror" 
                                            name="nomor_bukti" 
                                            placeholder="Nomor Bukti Transaksi" 
                                            value="{{ old('nomor_bukti') }}"
                                        >

                                        @error('nomor_bukti')
                                            <small id="nomorBuktiHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Jenis Transaksi</label>
                                        <select aria-describedby="jenisHelp" 
                                            class="form-control @error('jenis_transaksi') is-invalid @enderror" 
                                            name="jenis_transaksi"
                                        >
                                            <option value="penjualan">Penjualan</option>
                                            <option value="pembatalan">Pembatalan</option>
                                            <option value="pembayaran pelanggan">Pembayaran Pelanggan</option>
                                            <option value="pengiriman">Pengiriman</option>
                                            <option value="pembelian">Pembelian</option>
                                            <option value="penerimaan material">Penerimaan Material</option>
                                            <option value="pelunasan supplier">Pelunasan Supplier</option>
                                            <option value="permintaan material">Permintaan Material</option>
                                            <option value="produksi">Produksi</option>
                                            <option value="lain-lain">Lain-lain</option>
                                        </select>

                                        @error('jenis_transaksi')
                                            <small id="jenisHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Keterangan (Opsional)</label>
                                        <textarea class="form-control" name="keterangan" cols="30" rows="3">{{ old('keterangan') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-50">
                        <div class="table-responsive">
                            <table class="table mb-0" id="detail-akun-table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Nama Akun</th>
                                        <th class="text-right">Nominal Debet</th>
                                        <th class="text-right">Nominal Kredit</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse (old('akun_id', []) as $detail)
                                        <tr class="detail-akun" id="akun{{ $loop->index +1 }}">
                                            <td>
                                                <select aria-describedby="akunHelp" 
                                                    class="select2 form-control @error('akun_id.' . $loop->index) is-invalid @enderror" 
                                                    name="akun_id[]"
                                                >
                                                    <option></option>
                                                    @foreach ($akuns as $akun)
                                                        <option value="{{ $akun->id }}" 
                                                            {{ old('akun_id.' . $loop->parent->index) == $akun->id ? 'selected' : '' }}>
                                                            {{ $akun->nama_akun }}
                                                        </option>
                                                    @endforeach
                                                </select>

                                                @error('akun_id.' . $loop->index)
                                                    <small id="akunHelp" class="form-text text-danger">{{ $message }}</small>
                                                @enderror
                                            </td>
                                            <td>
                                                <input data-inputmask="
                                                    'alias': 'currency', 
                                                    'prefix': 'Rp ',
                                                    'placeholder': 'Rp 0',
                                                    'rightAlign': 'true',
                                                    'autoUnmask': 'true',
                                                    'allowMinus': 'false',
                                                    'removeMaskOnSubmit': 'true',
                                                    'digits': '0'" 
                                                    aria-describedby="debetHelp" class="form-control nominal-debet" name="debet[]" value="{{ old('debet.' . $loop->index, 0) }}">

                                                @error('debet.' . $loop->index)
                                                    <small id="debetHelp" class="form-text text-danger">{{ $message }}</small>
                                                @enderror
                                            </td>
                                            <td>
                                                <input data-inputmask="
                                                    'alias': 'currency', 
                                                    'prefix': 'Rp ',
                                                    'placeholder': 'Rp 0',
                                                    'rightAlign': 'true',
                                                    'autoUnmask': 'true',
                                                    'allowMinus': 'false',
                                                    'removeMaskOnSubmit': 'true',
                                                    'digits': '0'" 
                                                    aria-describedby="kreditHelp" class="form-control nominal-kredit" name="kredit[]" value="{{ old('kredit.' . $loop->index, 0) }}">

                                                @error('kredit.' . $loop->index)
                                                    <small id="kreditHelp" class="form-text text-danger">{{ $message }}</small>
                                                @enderror
                                            </td>
                                            @if ($loop->index +1  > 2)
                                                <td>
                                                    <button type="button" class="btn btn-icon btn-danger text-nowrap px-1 hapus-akun">
                                                        <i class="bx bx-trash"></i>
                                                    </button>
                                                </td>
                                            @endif
                                        </tr>
                                    @empty
                                        <tr class="detail-akun" id="akun1">
                                            <td>
                                                <select aria-describedby="akunHelp" 
                                                    class="select2 form-control @error('akun_id') is-invalid @enderror" 
                                                    name="akun_id[]"
                                                >
                                                    <option></option>
                                                    @foreach ($akuns as $akun)
                                                        <option value="{{ $akun->id }}" 
                                                            {{ old('akun_id') == $akun->id ? 'selected' : '' }}>
                                                            {{ $akun->nama_akun }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input data-inputmask="
                                                    'alias': 'currency', 
                                                    'prefix': 'Rp ',
                                                    'placeholder': 'Rp 0',
                                                    'rightAlign': 'true',
                                                    'autoUnmask': 'true',
                                                    'allowMinus': 'false',
                                                    'removeMaskOnSubmit': 'true',
                                                    'digits': '0'" 
                                                    aria-describedby="debetHelp" class="form-control nominal-debet" name="debet[]" value="0">
                                            </td>
                                            <td>
                                                <input data-inputmask="
                                                    'alias': 'currency', 
                                                    'prefix': 'Rp ',
                                                    'placeholder': 'Rp 0',
                                                    'rightAlign': 'true',
                                                    'autoUnmask': 'true',
                                                    'allowMinus': 'false',
                                                    'removeMaskOnSubmit': 'true',
                                                    'digits': '0'" 
                                                    aria-describedby="kreditHelp" class="form-control nominal-kredit" name="kredit[]" value="0">
                                            </td>
                                        </tr>
                                        <tr class="detail-akun" id="akun2">
                                            <td>
                                                <select aria-describedby="akunHelp" 
                                                    class="select2 form-control @error('akun_id') is-invalid @enderror" 
                                                    name="akun_id[]"
                                                >
                                                    <option></option>
                                                    @foreach ($akuns as $akun)
                                                        <option value="{{ $akun->id }}" 
                                                            {{ old('akun_id') == $akun->id ? 'selected' : '' }}>
                                                            {{ $akun->nama_akun }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input data-inputmask="
                                                    'alias': 'currency', 
                                                    'prefix': 'Rp ',
                                                    'placeholder': 'Rp 0',
                                                    'rightAlign': 'true',
                                                    'autoUnmask': 'true',
                                                    'allowMinus': 'false',
                                                    'removeMaskOnSubmit': 'true',
                                                    'digits': '0'" 
                                                    aria-describedby="debetHelp" class="form-control nominal-debet" name="debet[]" value="0">
                                            </td>
                                            <td>
                                                <input data-inputmask="
                                                    'alias': 'currency', 
                                                    'prefix': 'Rp ',
                                                    'placeholder': 'Rp 0',
                                                    'rightAlign': 'true',
                                                    'autoUnmask': 'true',
                                                    'allowMinus': 'false',
                                                    'removeMaskOnSubmit': 'true',
                                                    'digits': '0'" 
                                                    aria-describedby="kreditHelp" class="form-control nominal-kredit" name="kredit[]" value="0">
                                            </td>
                                        </tr>
                                    @endforelse                                    
                                </tbody>
                            </table>
                        </div>
                        <div class="col p-0">
                            <button type="button" class="btn btn-info" id="tambah-akun">
                                Tambah Akun
                            </button>
                        </div>
                    </div>

                    <div class="card-footer">                
                        <div class="row">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Simpan Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js" 
    integrity="sha512-6Jym48dWwVjfmvB0Hu3/4jn4TODd6uvkxdi9GNbBHwZ4nGcRxJUCaTkL3pVY6XUQABqFo3T58EMXFQztbjvAFQ==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/bindings/inputmask.binding.js" 
    integrity="sha512-J6WEJE0No+5Qqm9/T93q88yRQjvoAioXG4gzJ+eqZtLi+ZBgimZDkTiLWiljwrwnoQw+xwECQm282RJ6CrJnlw==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if(Session::has('error'))
        toastr['error']("{{ session('error') }}", {
            tapToDismiss: true,
        });
    @endif

    $( document ).ready(function() {
        var row_number = $(".detail-akun").length;

        $("#tambah-akun").on("click", function(e) {
            e.preventDefault();

            let new_row_number = row_number + 1;

            $('#detail-akun-table').append('<tr class="detail-akun" id="akun' + (new_row_number) + '"></tr>');

            $('#akun' + new_row_number).html($('#akun' + row_number).html()).find('td:first-child');

            if (row_number == 2) {
                $("#akun" + new_row_number).append('<td> <button type="button" class="btn btn-icon btn-danger text-nowrap px-1 hapus-akun"> <i class="bx bx-trash"></i> </button> </td>');
            }

            $("#akun" + new_row_number + " .nominal-debet").inputmask({
                alias: 'currency',
                prefix: 'Rp ',
                placeholder: 'Rp 0',
                rightAlign: true,
                autoUnmask: true,
                allowMinus: false,
                removeMaskOnSubmit: true,
                digits: '0',
            });

            $("#akun" + new_row_number + " .nominal-kredit").inputmask({
                alias: 'currency',
                prefix: 'Rp ',
                placeholder: 'Rp 0',
                rightAlign: true,
                autoUnmask: true,
                allowMinus: false,
                removeMaskOnSubmit: true,
                digits: '0',
            });

            $(".select2-container").remove();

            $(".select2").select2({
                placeholder: "Pilih Data",
                dropdownAutoWidth: true,
                width: '100%'
            });

            $("#akun" + new_row_number + " .text-danger").hide();

            $("#akun" + new_row_number + " .nominal-debet").removeClass("is-invalid");

            $("#akun" + new_row_number + " .nominal-kredit").removeClass("is-invalid");

            $("#akun" + new_row_number + " .nominal-debet").val(0);

            $("#akun" + new_row_number + " .nominal-kredit").val(0);

            $("#akun" + new_row_number + " .select2").val(null).trigger('change.select2');

            row_number++;
        });

        $(document).on('click', '.hapus-akun', function () {
            $(this).parents('tr').remove();

            row_number--;

            calculateTotal();
        });
    });
</script>
@endsection('script')