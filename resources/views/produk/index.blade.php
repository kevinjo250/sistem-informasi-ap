@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Produk</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Produk
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-datatable">
    @can('produk create')
        <div class="invoice-create-btn mb-1">
            <a href="{{ route('produks.create') }}" class="btn btn-primary btn-sm round glow align-items-center">Buat Produk</a>
        </div>
    @endcan
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body card-dashboard">
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>nama</th>
                                    <th>jenis</th>
                                    <th>ukuran</th>
                                    <th>warna</th>
                                    <th>safety stok</th>
                                    <th>stok</th>
                                    <th>satuan</th>
                                    <th>harga pokok</th>
                                    <th>harga jual</th>
                                    <th>ket</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($produks as $produk)
                                    <tr>
                                        <td>
                                            <img class="img-responsive" src="{{ asset($produk->gambar_produk ? 'images/' . $produk->gambar_produk : 'images/default-image.jpg') }}" height="100px" width="100px">
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $produk->nama_produk }}</small>
                                        </td>
                                        <td>
                                            @switch($produk->jenis_produk)
                                                @case('material')
                                                    <span class="badge badge-light-info badge-pill">{{ $produk->jenis_produk }}</span>
                                                    @break
                                                @case('reject')
                                                    <span class="badge badge-light-secondary badge-pill">{{ $produk->jenis_produk }}</span>
                                                    @break
                                                @default
                                                    <span class="badge badge-light-primary badge-pill">{{ $produk->jenis_produk }}</span>
                                            @endswitch
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $produk->ukuran_produk }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $produk->warna_produk }}</small>
                                        </td>
                                        <td class="text-right">
                                            <small class="text-muted">@number($produk->safety_stok)</small>
                                        </td>
                                        <td class="text-right">
                                            @if ($produk->stok_produk < $produk->safety_stok)                                                
                                                <small class="text-muted badge badge-light-danger badge-pill">@number($produk->stok_produk)</small>
                                            @else                                                
                                                <small class="text-muted">@number($produk->stok_produk)</small>
                                            @endif
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $produk->satuan_produk }}</small>
                                        </td>
                                        <td class="text-right">
                                            <small class="text-muted">@money($produk->harga_pokok)</small>
                                        </td>
                                        <td class="text-right">
                                            <small class="text-muted">@money($produk->harga_jual)</small>
                                        </td>
                                        <td>
                                            @switch($produk->keterangan)
                                                @case(null)
                                                    <span class="badge badge-light-info badge-pill">not found</span>
                                                    @break
                                                @default                                                    
                                                    <small class="text-muted">{{ $produk->keterangan }}</small>
                                            @endswitch
                                        </td>
                                        <td>
                                            <div class="invoice-action">
                                                @can('produk edit')
                                                    <a href="{{ route('produks.edit', $produk) }}" class="invoice-action-edit cursor-pointer">
                                                        <i class="bx bx-edit-alt"></i>
                                                    </a>
                                                @endcan
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/datatables/datatable.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if(Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection