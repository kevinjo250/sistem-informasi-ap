@extends('layouts.app')

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Ubah Produk</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Produk
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-vertical-layouts">
    <div class="row match-height">
        <div class="col-lg-8 col-md-12 col-12">
            <div class="card">
                <form class="form form-vertical" action="{{ route('produks.update', $produk) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    
                    <div class="card-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Nama Produk</label>
                                        <input type="text" aria-describedby="namaHelp" class="form-control @error('nama_produk') is-invalid @enderror" name="nama_produk" placeholder="Masukan Nama Produk" value="{{ old('nama_produk', $produk->nama_produk) }}">

                                        @error('nama_produk')
                                            <small id="namaHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Jenis Produk</label>
                                        <select class="form-control @error('jenis_produk') is-invalid @enderror" aria-describedby="jenisHelp" name="jenis_produk">
                                            <option value="material" {{ old('jenis_produk', $produk->jenis_produk) == 'material' ? 'selected' : '' }}>Material</option>
                                            <option value="finished product" {{ old('jenis_produk', $produk->jenis_produk) == 'finished product' ? 'selected' : '' }}>Finished Product</option>
                                        </select>

                                        @error('jenis_produk')
                                            <small id="jenisHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Ukuran Produk</label>
                                        <input type="text" aria-describedby="ukuranHelp" class="form-control @error('ukuran_produk') is-invalid @enderror" name="ukuran_produk" placeholder="Masukan Ukuran Produk" value="{{ old('ukuran_produk', $produk->ukuran_produk) }}">

                                        @error('ukuran_produk')
                                            <small id="ukuranHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Warna Produk</label>
                                        <input type="text" aria-describedby="warnaHelp" class="form-control @error('warna_produk') is-invalid @enderror" name="warna_produk" placeholder="Masukan Warna Produk" value="{{ old('warna_produk', $produk->warna_produk) }}">

                                        @error('warna_produk')
                                            <small id="warnaHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Safety Stok</label>
                                        <input data-inputmask="
                                            'alias': 'numeric',
                                            'groupSeparator': ',',
                                            'rightAlign': 'false',
                                            'autoUnmask': 'true',
                                            'allowMinus': 'false',
                                            'removeMaskOnSubmit': 'true',
                                            'digits': '0',
                                            'min': '1'" 
                                            aria-describedby="safetyHelp" class="form-control @error('safety_stok') is-invalid @enderror" name="safety_stok" value="{{ old('safety_stok', $produk->safety_stok) }}">

                                        @error('safety_stok')
                                            <small id="safetyHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Harga Jual</label>
                                        <input data-inputmask="
                                            'alias': 'currency', 
                                            'prefix': 'Rp ',
                                            'placeholder': 'Rp 0',
                                            'rightAlign': 'true',
                                            'autoUnmask': 'true',
                                            'allowMinus': 'false',
                                            'removeMaskOnSubmit': 'true',
                                            'digits': '0',
                                            'min': '0'" 
                                            aria-describedby="hargaJualHepl" class="form-control @error('harga_jual') is-invalid @enderror" name="harga_jual" value="{{ old('harga_jual', $produk->harga_jual) }}">

                                        @error('harga_jual')
                                            <small id="hargaJualHepl" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Satuan Produk</label>
                                        <input type="text" aria-describedby="satuanHelp" class="form-control @error('satuan_produk') is-invalid @enderror" name="satuan_produk" placeholder="Masukan Satuan Produk" value="{{ old('satuan_produk', $produk->satuan_produk) }}">

                                        @error('satuan_produk')
                                            <small id="satuanHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Gambar (Opsional)</label>
                                        <input type="file" aria-describedby="gambarHelp" class="form-control @error('gambar_produk') is-invalid @enderror" name="gambar_produk">
                                        
                                        @error('gambar_produk')
                                            <small id="gambarHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Keterangan (Opsional)</label>
                                        <textarea class="form-control" name="keterangan" rows="3" placeholder="Keterangan (Opsional)">{{ old('keterangan') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row ">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-warning">Ubah Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js" 
    integrity="sha512-6Jym48dWwVjfmvB0Hu3/4jn4TODd6uvkxdi9GNbBHwZ4nGcRxJUCaTkL3pVY6XUQABqFo3T58EMXFQztbjvAFQ==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/bindings/inputmask.binding.js" 
    integrity="sha512-J6WEJE0No+5Qqm9/T93q88yRQjvoAioXG4gzJ+eqZtLi+ZBgimZDkTiLWiljwrwnoQw+xwECQm282RJ6CrJnlw==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection