@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Pemesanan Material</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Pemesanan Material
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-datatable">
    @can('pemesananMaterial create')
        <div class="invoice-create-btn mb-1">
            <a href="{{ route('pemesananMaterials.create') }}" class="btn btn-primary btn-sm round glow align-items-center">Buat Pemesanan Material</a>
        </div>
    @endcan
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-body card-dashboard">
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>supplier</th>
                                    <th>tgl</th>
                                    <th>jth tempo</th>
                                    <th>status nota
                                        <i class="bx bx-info-circle" 
                                            data-toggle="popover" data-placement="left" 
                                            data-container="body" data-original-title="Status Nota" 
                                            data-content=
                                                "<small><strong>Draft:</strong> Nota dalam kondisi penawaran.</small> <br />
                                                <small><strong>Approved:</strong> Nota disetujui Direktur.</small> <br />
                                                <small><strong>Open:</strong> Produk telah dipesan ke Supplier.</small> <br />
                                                <small><strong>Done:</strong> Produk telah sampai.</small>"
                                            data-html="true" data-trigger="hover">
                                        </i>
                                    </th>
                                    <th>pembayaran
                                        <i class="bx bx-info-circle"
                                            data-toggle="popover" data-placement="left" 
                                            data-container="body" data-original-title="Status Nota" 
                                            data-content=
                                                "<small><strong>Unpaid:</strong> Nota belum dibayar.</small> <br />
                                                <small><strong>Partially Paid:</strong> Baru dibayar sebagian.</small> <br />
                                                <small><strong>Paid:</strong> Nota telah dibayar sepenuhnya.</small> <br />
                                                <small><strong>Overdue:</strong> Telah melewati jatuh tempo.</small>"
                                            data-html="true" data-trigger="hover">                                            
                                        </i>
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pemesananMaterials as $pemesananMaterial)
                                    <tr>
                                        <td>{{ $pemesananMaterial->no_nota }}</td>
                                        <td>
                                            <small class="text-muted">{{ $pemesananMaterial->supplier->nama_supplier }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">@datetime($pemesananMaterial->created_at)</small>
                                        </td>
                                        <td>
                                            @switch($pemesananMaterial->tanggal_jatuh_tempo)
                                                @case(null)
                                                    <span class="badge badge-light-warning badge-pill">Belum ditetapkan</span>
                                                    @break
                                                @default
                                                    <small class="text-muted">{{ $pemesananMaterial->tanggal_jatuh_tempo }}</small>
                                            @endswitch
                                        </td>
                                        <td>
                                            @switch($pemesananMaterial->status_nota)
                                                @case('confirmed')
                                                    <span class="badge badge-light-primary badge-pill">{{ $pemesananMaterial->status_nota }}</span>
                                                    @break
                                                @case('done')
                                                    <span class="badge badge-light-success badge-pill">{{ $pemesananMaterial->status_nota }}</span>
                                                    @break
                                                @default
                                                    <span class="badge badge-light-info badge-pill">{{ $pemesananMaterial->status_nota }}</span>
                                            @endswitch
                                        </td>
                                        <td>
                                            @switch($pemesananMaterial->status_pembayaran)
                                                @case('partially paid')
                                                    <span class="badge badge-light-info badge-pill">{{ $pemesananMaterial->status_pembayaran }}</span>
                                                    @break
                                                @case('paid')
                                                    <span class="badge badge-light-success badge-pill">{{ $pemesananMaterial->status_pembayaran }}</span>
                                                    @break
                                                @case('overdue')
                                                    <span class="badge badge-light-warning badge-pill">{{ $pemesananMaterial->status_pembayaran }}</span>
                                                    @break
                                                @default
                                                    <span class="badge badge-light-danger badge-pill">{{ $pemesananMaterial->status_pembayaran }}</span>
                                            @endswitch
                                        </td>
                                        <td>
                                            <div class="invoice-action">
                                                <a href="{{ route('pemesananMaterials.show', $pemesananMaterial) }}" class="invoice-action-view cursor-pointer">
                                                    <i class="bx bx-show-alt"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/datatables/datatable.js') }}"></script>
<script src="{{ asset('app-assets/js/scripts/popover/popover.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if(Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif

    @if(Session::has('error'))
        toastr['error']("{{ session('error') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection