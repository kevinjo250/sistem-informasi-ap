@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-invoice.css') }}">
@endsection

@section('content')
<section class="invoice-edit-wrapper">
    <div class="row">
        <!-- invoice view page -->
        <div class="col-xl-12 col-md-12 col-12">
            <form class="invoice-item-repeater" action="{{ route('pemesananmaterials.confirm', $pemesananmaterial) }}" method="post">
                @csrf
                @method('patch')

                <div class="card">
                    <div class="card-body pb-0 mx-25">
                        <!-- header section -->
                        <div class="row mx-0">
                            <div class="col-xl-4 col-md-12 d-flex align-items-center pl-0">
                                <h6 class="invoice-number mb-0 mr-75">{{ $pemesananmaterial->no_nota }}</h6>
                            </div>
                            <div class="col-xl-8 col-md-12 px-0 pt-xl-0 pt-1">
                                <div class="invoice-date-picker d-flex align-items-center justify-content-xl-end flex-wrap">
                                    <div class="d-flex align-items-center">
                                        <small class="text-muted mr-75">Jatuh Tempo: </small>
                                        <fieldset class="d-flex">
                                            <input type="date" class="form-control mb-50 mb-sm-0 @error('tanggal_jatuh_tempo') is-invalid @enderror" name="tanggal_jatuh_tempo">
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <!-- invoice address and contact -->
                        <div class="row invoice-info">
                            <div class="col-4 mt-25">
                                <h6 class="invoice-to">Data Supplier</h6>
                                <fieldset class="invoice-address form-group">
                                    <select aria-describedby="supplierHelp" class="select2 form-control @error('supplier_id') is-invalid @enderror" name="supplier_id" id="supplier_id">
                                        <option></option>
                                        @foreach ($suppliers as $supplier)
                                        <option value="{{ $supplier->id }}" data-alamat="{{ $supplier->alamat_supplier }}" data-kota="{{ $supplier->kota_supplier }}" {{ $pemesananmaterial->supplier_id == $supplier->id ? 'selected' : '' }}>{{ $supplier->nama_supplier }}</option>
                                        @endforeach
                                    </select>

                                    @error('supplier_id')
                                    <small id="supplierHelp" class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </fieldset>
                                <fieldset class="invoice-address form-group">
                                    <textarea class="form-control" id="alamat_supplier" rows="4" placeholder="{{ $pemesananmaterial->supplier->alamat_supplier }} {{ $pemesananmaterial->supplier->kota_supplier }}" style="margin-top: 0px; margin-bottom: 0px; height: 101px;" readonly></textarea>
                                </fieldset>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="card-body pt-50">
                        <!-- product details table-->
                        <div class="invoice-product-details ">
                            <!-- table head dark -->
                            <div class="table-responsive">
                                <table class="table mb-0" id="produk_table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>Produk</th>
                                            <th>Sisa Stok</th>
                                            <th>Harga</th>
                                            <th>Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pemesananmaterial->produks as $key => $detail)
                                        <tr class="invoice-item-filed" id="produk{{ $loop->index }}">
                                            <td>
                                                <select class="select2 form-control invoice-item-select" name="produk_id[]">
                                                    <option></option>
                                                    @foreach ($produks as $produk)
                                                    <option value="{{ $produk->id }}" data-stok = {{ $produk->stok_produk }} {{ $detail->pivot->produk_id == $produk->id ? 'selected' : '' }}>
                                                        {{ $produk->nama_produk }} - {{ $produk->tipe_produk }} - {{ $produk->ukuran_produk }} - {{ $produk->warna_produk }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control" placeholder="{{ $detail->stok_produk }}" readonly>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control invoice-item-price" name="harga[]" placeholder="0">
                                            </td>
                                            <td>
                                                <input type="number" class="form-control invoice-item-qty" name="kuantitas[]" placeholder="0">
                                            </td>
                                        </tr>
                                        @endforeach
                                        <tr class="invoice-item-filed" id="produk{{ count($pemesananmaterial->produks) }}">
                                            <td>
                                                <select class="select2 form-control invoice-item-select" name="produk_id[]">
                                                    <option></option>
                                                    @foreach ($produks as $produk)
                                                    <option value="{{ $produk->id }}" data-stok = {{ $produk->stok_produk }}>
                                                        {{ $produk->nama_produk }} - {{ $produk->tipe_produk }} - {{ $produk->ukuran_produk }} - {{ $produk->warna_produk }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control" placeholder="0" readonly>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control invoice-item-price" name="harga[]" placeholder="0">
                                            </td>
                                            <td>
                                                <input type="number" class="form-control invoice-item-qty" name="kuantitas[]" placeholder="0">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <button class="btn btn-light-primary" id="add_row">Tambah Produk</button>
                                </div>
                                <div class="col-6 d-flex justify-content-end">
                                    <button class="btn btn-danger" id='delete_row'>Hapus Produk</button>
                                </div>
                            </div>
                        </div>
                        <!-- invoice subtotal -->
                        <hr>
                        <div class="invoice-subtotal pt-50">
                            <div class="row">
                                <div class="col-md-5 col-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="keterangan" cols="30" rows="5" placeholder="Keterangan (Opsional)">{{ $pemesananmaterial->keterangan }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">                
                        <div class="row">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-success">Konfirmasi</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js') }}"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/pages/app-invoice.js') }}"></script>
<script src="{{ asset('app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    $( document ).ready(function() {
        //Trigger New Produk Row
        let row_number = {{ count($pemesananmaterial->produks) + 1 }};
        $("#add_row").on("click", function(e){
        e.preventDefault();
            $('#produk_table').append('<tr class="invoice-item-filed" id="produk' + (row_number) + '"></tr>');
            $('#produk' + row_number).html($('#produk' + (row_number - 1)).html()).find('td:first-child');
            row_number++;
        
            $(".select2-container").remove();
            $(".select2").select2({
                placeholder: "Pilih Data",
                dropdownAutoWidth: true,
                width: '100%'
            });
        });
    
        //Remove Last Produk Row
        $("#delete_row").on("click", function(e){
            e.preventDefault();
            if(row_number > 1){
                $("#produk" + (row_number - 1)).remove();
                row_number--;
            }
        });
    
        // on product change also change size, color, price
        $(document).on("change", ".invoice-item-select", function (e) {
            var stok = this.options[e.target.selectedIndex].getAttribute('data-stok');
            $(e.target)
                .closest(".invoice-item-filed")
                .find(".invoice-item-qty")
                .val(stok);
        });
    
        //populate customer field based on selected option
        $(document).on("change", "#supplier_id", function(){
            var alamat = $(this).find('option:selected').data('alamat');
            var kota = $(this).find('option:selected').data('kota');
        
            $('#alamat_supplier').val(alamat + ' ' + kota);
        });
    });
</script>
@endsection