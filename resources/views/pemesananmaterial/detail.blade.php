@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-invoice.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Detail Pemesanan Material</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Pemesanan Material</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section class="invoice-view-wrapper">
    <div class="row">
        <!-- invoice view page -->
        <div class="col-xl-12 col-md-12 col-12">
            <div class="card invoice-print-area">
                <div class="card-body pb-0 mx-25">
                    <!-- header section -->
                    <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <span class="invoice-number mr-50">{{ $pemesananMaterial->no_nota }}</span>
                            @switch($pemesananMaterial->status_nota)
                                @case('approved')
                                    <span class="badge badge-light-primary badge-pill">{{ $pemesananMaterial->status_nota }}</span>
                                    @break
                                @case('done')
                                    <span class="badge badge-light-success badge-pill">{{ $pemesananMaterial->status_nota }}</span>
                                    @break
                                @case('open')
                                    <span class="badge badge-light-secondary badge-pill">{{ $pemesananMaterial->status_nota }}</span>
                                    @break
                                @default
                                    <span class="badge badge-light-info badge-pill">{{ $pemesananMaterial->status_nota }}</span>
                            @endswitch
                            @switch($pemesananMaterial->status_pembayaran)
                                @case('partially paid')
                                    <span class="badge badge-light-info badge-pill">{{ $pemesananMaterial->status_pembayaran }}</span>
                                    @break
                                @case('paid')
                                    <span class="badge badge-light-success badge-pill">{{ $pemesananMaterial->status_pembayaran }}</span>
                                    @break
                                @case('overdue')
                                    <span class="badge badge-light-warning badge-pill">{{ $pemesananMaterial->status_pembayaran }}</span>
                                    @break
                                @default
                                    <span class="badge badge-light-danger badge-pill">{{ $pemesananMaterial->status_pembayaran }}</span>
                            @endswitch
                        </div>
                        <div class="col-lg-8 col-md-12">
                            <div class="d-flex align-items-center justify-content-lg-end flex-wrap">
                                <div>
                                    <small class="text-muted">Tanggal Jatuh Tempo:</small>
                                    @switch($pemesananMaterial->tanggal_jatuh_tempo)
                                        @case(null)
                                            <span class="badge badge-light-warning badge-pill">Belum ditetapkan</span>
                                            @break
                                        @default
                                            <span>@date($pemesananMaterial->tanggal_jatuh_tempo)</span>
                                    @endswitch
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <!-- invoice address and contact -->
                    <div class="row invoice-info">
                        <div class="col-sm-6 col-12 mt-1">
                            <h6 class="invoice-from">Data Supplier</h6>
                            <div class="mb-1">
                                <span>{{ $pemesananMaterial->supplier->nama_supplier }}</span>
                            </div>
                            <div class="mb-1">
                                <span>{{ $pemesananMaterial->supplier->alamat_supplier }}, {{ $pemesananMaterial->supplier->kota_supplier }}</span>
                            </div>
                            <div class="mb-1">
                                <span>{{ $pemesananMaterial->supplier->no_telepon }}</span>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <!-- product details table-->
                <div class="invoice-product-details table-responsive">
                    <table class="table table-borderless mb-0">
                        <thead>
                            <tr class="border-0">
                                <th scope="col">Produk</th>
                                <th scope="col">Ukuran</th>
                                <th scope="col">Warna</th>
                                <th scope="col" class="text-right">Qty</th>
                                <th scope="col">Satuan</th>
                                <th scope="col" class="text-right">Harga</th>
                                <th scope="col" class="text-right">Sub Total</th>
                                <th scope="col">Status
                                    <i class="bx bx-info-circle" 
                                        data-toggle="popover" data-placement="left" 
                                        data-container="body" data-original-title="Status Material" 
                                        data-content=
                                            "<small><strong>Pending:</strong> Material belum disetujui Direktur.</small> <br />
                                            <small><strong>Approved:</strong> Material telah disetujui Direktur.</small> <br />
                                            <small><strong>Open:</strong> Material dalam proses pengiriman.</small> <br />
                                            <small><strong>Partial:</strong> Material telah datang sebagian.</small> <br />
                                            <small><strong>Done:</strong> Pesanan telah selesai.</small>"
                                        data-html="true" data-trigger="hover">
                                    </i>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pemesananMaterial->produks as $detail)
                                <tr>
                                    <td>
                                        <small class="text-muted">{{ $detail->nama_produk }}</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->ukuran_produk }}</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->warna_produk }}</small>
                                    </td>
                                    <td class="text-right">
                                        <small class="text-muted">@number($detail->pivot->kuantitas)</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->satuan_produk }}</small>
                                    </td>
                                    <td class="text-right">
                                        <small class="text-muted">@money($detail->pivot->harga)</small>
                                    </td>
                                    <td class="text-right">
                                        <small class="text-muted">@money($detail->pivot->sub_total)</small>
                                    </td>
                                    <td>
                                        @switch($detail->pivot->status_material)
                                            @case('approved')
                                                <span class="badge badge-light-primary badge-pill">{{ $detail->pivot->status_material }}</span>
                                                @break
                                            @case('done')
                                                <span class="badge badge-light-success badge-pill">{{ $detail->pivot->status_material }}</span>
                                                @break
                                            @default
                                                <span class="badge badge-light-info badge-pill">{{ $detail->pivot->status_material }}</span>
                                        @endswitch
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <!-- invoice subtotal -->
                <div class="card-body pt-0 mx-25">
                    <hr>
                    <div class="row">
                        <div class="col-4 col-sm-6 col-12 mt-75">
                            <p>{{ $pemesananMaterial->keterangan }}</p>
                        </div>
                        <div class="col-8 col-sm-6 col-12 d-flex justify-content-end mt-75">
                            <div class="invoice-subtotal">
                                <div class="invoice-calc d-flex justify-content-between">
                                    <span class="invoice-title">Total</span>
                                    <span class="invoice-value">@money($pemesananMaterial->total)</span>
                                </div>
                                <div class="invoice-calc d-flex justify-content-between">
                                    <span class="invoice-title">Diskon</span>
                                    <span class="invoice-value">- @money($pemesananMaterial->diskon)</span>
                                </div>
                                <hr>
                                <div class="invoice-calc d-flex justify-content-between">
                                    <span class="invoice-title">Grand Total</span>
                                    <span class="invoice-value">@money($pemesananMaterial->grand_total)</span>
                                </div>
                                @foreach ($pemesananMaterial->notaPelunasanSuppliers as $notaPelunasanSupplier)
                                    <hr>
                                    <div class="invoice-calc d-flex justify-content-between">
                                        <button type="button" class="btn btn-icon rounded-circle btn-outline mr-1" 
                                            data-toggle="popover" data-placement="left" 
                                            data-container="body" data-original-title="Info Pembayaran" 
                                            data-content=
                                                "<b>Nominal:</b> @money($notaPelunasanSupplier->nominal_pelunasan) <br />
                                                <b>Keterangan:</b> {{ $notaPelunasanSupplier->keterangan }} <br />
                                                <b>Tanggal:</b> @date($notaPelunasanSupplier->created_at) <br />
                                                <b>Cara Bayar:</b> {{ $notaPelunasanSupplier->cara_bayar }}"
                                            data-html="true" data-trigger="hover">
                                            <i class="bx bx-info-circle"></i>
                                        </button>
                                        <span class="invoice-title"><i>Dibayar pada @date($notaPelunasanSupplier->created_at)</i></span>
                                        <span class="invoice-value">@money($notaPelunasanSupplier->nominal_pelunasan)</span>
                                    </div>
                                @endforeach    
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="card-footer">
                    @if ($pemesananMaterial->status_nota == 'draft')
                        <div class="row">
                            <div class="col-6">
                                @can('pemesananMaterial edit')
                                    <a href="{{ route('pemesananMaterials.edit', $pemesananMaterial) }}" class="btn btn-warning">Ubah Data</a>
                                @endcan
                            </div>

                            <div class="col-6 d-flex justify-content-end">
                                @can('pemesananMaterial confirm')
                                    <a href="#KonfirmasiModal" class="btn btn-success" data-toggle="modal">Konfirmasi Pemesanan</a>
                                @endcan
                            </div>          

                            {{-- BEGIN KONFIRMASI MODAL --}}
                            <div class="modal fade text-left" id="KonfirmasiModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header bg-success">
                                            <h5 class="modal-title white" id="myModalLabel160">Konfirmasi Pemesanan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i class="bx bx-x"></i>
                                            </button>
                                        </div>
                                        <form action="{{ route('pemesananMaterials.confirm', $pemesananMaterial) }}" method="post">
                                            @csrf
                                            @method('patch')

                                            <div class="modal-body">
                                                <h6 class="text-muted">Pastikan seluruh data telah terisi dengan benar!</h6>
                                                <h6 class="text-muted">Apakah anda yakin untuk menyetujui pemesanan ini?</h6>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-light-danger" data-dismiss="modal">
                                                    <i class="bx bx-x d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Batal</span>
                                                </button>
                                                <button type="submit" class="btn btn-success ml-1">
                                                    <i class="bx bx-check d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Konfirmasi</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            {{-- END KONFIRMASI MODAL --}}
                        </div>

                        <div class="row mt-2">
                            <div class="col-6">
                                @can('pemesananMaterial delete')
                                    <a href="#BatalModal" class="btn btn-danger" data-toggle="modal">Batalkan Pemesanan</a>
                                @endcan
                            </div>

                            {{-- BEGIN BATAL MODAL --}}
                            <div class="modal fade text-left" id="BatalModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header bg-danger">
                                            <h5 class="modal-title white" id="myModalLabel160">Pembatalan Pemesanan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i class="bx bx-x"></i>
                                            </button>
                                        </div>
                                        <form action="{{ route('pemesananMaterials.destroy', $pemesananMaterial) }}" method="post">
                                            @csrf
                                            @method('delete')

                                            <div class="modal-body">
                                                <h6 class="text-muted">Seluruh data pada pemesanan ini akan dihapus!</h6>
                                                <h6 class="text-muted">Apakah anda yakin untuk membatalkan pemesanan ini?</h6>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-light-danger" data-dismiss="modal">
                                                    <i class="bx bx-x d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Batal</span>
                                                </button>
                                                <button type="submit" class="btn btn-danger ml-1">
                                                    <i class="bx bx-check d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Konfirmasi</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            {{-- END BATAL MODAL --}}
                        </div>
                    @endif

                    @if ($pemesananMaterial->status_nota == 'approved')
                        <div class="row">
                            <div class="col-6">
                                @can('pemesananMaterial edit')
                                    <a href="{{ route('pemesananMaterials.edit', $pemesananMaterial) }}" class="btn btn-warning">Ubah Data</a>
                                @endcan
                            </div>

                            <div class="col-6 d-flex justify-content-end">
                                @can('pemesananMaterial selesai')
                                    <a href="#SelesaiPemesananModal" class="btn btn-success" data-toggle="modal">Selesai Pemesanan ke Supplier</a>
                                @endcan
                            </div>

                            {{-- BEGIN Selesai Pemesanan MODAL --}}
                            <div class="modal fade text-left" id="SelesaiPemesananModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header bg-success">
                                            <h5 class="modal-title white" id="myModalLabel160">Selesai Pemesanan ke Supplier</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i class="bx bx-x"></i>
                                            </button>
                                        </div>
                                        <form action="{{ route('pemesananMaterials.open', $pemesananMaterial) }}" method="post">
                                            @csrf
                                            @method('patch')

                                            <div class="modal-body">
                                                <h6 class="text-muted">Apakah anda yakin telah melakukan pemesanan kepada supplier terkait?</h6>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-light-danger" data-dismiss="modal">
                                                    <i class="bx bx-x d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Batal</span>
                                                </button>
                                                <button type="submit" class="btn btn-success ml-1">
                                                    <i class="bx bx-check d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Konfirmasi</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            {{-- END Selesai Pemesanan MODAL --}}
                        </div>
                    @endif

                    @if ($pemesananMaterial->status_nota == 'open')
                        <div class="row">
                            <div class="col-6">
                                @can('pemesananMaterial edit')
                                    <a href="{{ route('pemesananMaterials.edit', $pemesananMaterial) }}" class="btn btn-warning">Ubah Data</a>
                                @endcan
                            </div>

                            <div class="col-6 d-flex justify-content-end">
                                @can('penerimaanMaterial create')
                                    <a href="{{ route('penerimaanMaterials.create', $pemesananMaterial) }}" class="btn btn-info">Buat Penerimaan</a>
                                @endcan
                            </div>
                        </div>
                    @endif

                    @if ($pemesananMaterial->status_nota == 'done' && $pemesananMaterial->status_pembayaran != 'paid')
                        <div class="row">
                            <div class="col-6">
                                @can('pemesananMaterial edit')
                                    <a href="{{ route('pemesananMaterials.edit', $pemesananMaterial) }}" class="btn btn-warning">Ubah Data</a>
                                @endcan
                            </div>

                            <div class="col-6 d-flex justify-content-end">
                                @can('notaPelunasanSupplier create')
                                    <a href="#PelunasanModal" class="btn btn-secondary" data-toggle="modal">Buat Pelunasan</a>
                                @endcan
                            </div>

                            {{-- BEGIN PELUNASAN MODAL --}}
                            <div class="modal fade text-left" id="PelunasanModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header bg-secondary">
                                            <h5 class="modal-title white" id="myModalLabel160">Pelunasan Supplier</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i class="bx bx-x"></i>
                                            </button>
                                        </div>
                                        <form action="{{ route('notaPelunasanSuppliers.store', $pemesananMaterial) }}" method="post">
                                            @csrf

                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label>Nomor Nota</label>
                                                    <input type="text" class="form-control" value="{{ $pemesananMaterial->no_nota }}" readonly>
                                                </div>
                                                <div class="form-group mb-0">
                                                    <label>Nominal Pembelian</label>
                                                    <input data-inputmask="
                                                        'alias': 'currency', 
                                                        'prefix': 'Rp ',
                                                        'placeholder': 'Rp 0',
                                                        'rightAlign': 'true',
                                                        'autoUnmask': 'true',
                                                        'allowMinus': 'false',
                                                        'removeMaskOnSubmit': 'true',
                                                        'digits': '0'" 
                                                        class="form-control" value="{{ $pemesananMaterial->grand_total }}" readonly>
                                                </div>
                                                <div class="form-group mb-0">
                                                    <label>Sisa Tagihan</label>
                                                    <input data-inputmask="
                                                        'alias': 'currency', 
                                                        'prefix': 'Rp ',
                                                        'placeholder': 'Rp 0',
                                                        'rightAlign': 'true',
                                                        'autoUnmask': 'true',
                                                        'allowMinus': 'false',
                                                        'removeMaskOnSubmit': 'true',
                                                        'digits': '0'" 
                                                        class="form-control" value="{{ $pemesananMaterial->grand_total - $pemesananMaterial->nominal_pelunasan }}" readonly>
                                                </div>
                                                <div class="form-group mb-0">
                                                    <label>Cara Pembayaran</label>
                                                    <select aria-describedby="caraBayarHelp" class="form-control" name="cara_bayar">
                                                        <option hidden>Pilih Cara Pembayaran</option>
                                                        <option value="Tunai" {{ old('cara_bayar') == 'Tunai' ? 'selected' : '' }}>Tunai</option>
                                                        <option value="Transfer" {{ old('cara_bayar') == 'Transfer' ? 'selected' : '' }}>Transfer</option>
                                                    </select>

                                                    @error('cara_bayar', 'store-nota-pelunasan-supplier')
                                                        <small id="caraBayarHelp" class="form-text text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                                <div class="form-group mb-0">
                                                    <label>Nominal Pelunasan</label>
                                                    <input data-inputmask="
                                                        'alias': 'currency', 
                                                        'prefix': 'Rp ',
                                                        'placeholder': 'Rp 0',
                                                        'rightAlign': 'true',
                                                        'autoUnmask': 'true',
                                                        'allowMinus': 'false',
                                                        'removeMaskOnSubmit': 'true',
                                                        'digits': '0', 
                                                        'max': {{ $pemesananMaterial->grand_total - $pemesananMaterial->nominal_pelunasan }}" 
                                                        aria-describedby="nominalPelunasanHelp" class="form-control @error('nominal_pelunasan', 'store-nota-pelunasan-supplier') is-invalid @enderror" name="nominal_pelunasan" value="{{ old('nominal_pelunasan', 0) }}">

                                                    @error('nominal_pelunasan', 'store-nota-pelunasan-supplier')
                                                        <small id="nominalPelunasanHelp" class="form-text text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                                <div class="form-group mb-0">
                                                    <label>Keterangan (Opsional)</label>
                                                    <textarea class="form-control" name="keterangan" cols="30" rows="5" placeholder="Keterangan (Opsional)">{{ old('keterangan') }}</textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-light-danger" data-dismiss="modal">
                                                    <i class="bx bx-x d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Batal</span>
                                                </button>
                                                <button type="submit" class="btn btn-secondary ml-1">
                                                    <i class="bx bx-check d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Buat Pelunasan</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            {{-- END PELUNASAN MODAL --}}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js" 
    integrity="sha512-6Jym48dWwVjfmvB0Hu3/4jn4TODd6uvkxdi9GNbBHwZ4nGcRxJUCaTkL3pVY6XUQABqFo3T58EMXFQztbjvAFQ==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/bindings/inputmask.binding.js" 
    integrity="sha512-J6WEJE0No+5Qqm9/T93q88yRQjvoAioXG4gzJ+eqZtLi+ZBgimZDkTiLWiljwrwnoQw+xwECQm282RJ6CrJnlw==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/popover/popover.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if(Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif

    @if(Session::has('error'))
        toastr['error']("{{ session('error') }}", {
            tapToDismiss: true,
        });
    @endif

    @if ($errors->hasBag('store-nota-pelunasan-supplier'))
        $("#PelunasanModal").modal("show");
    @endif
</script>
@endsection