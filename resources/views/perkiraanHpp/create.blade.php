@extends('layouts.app')

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Buat Perkiraan HPP</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Perkiraan HPP
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-vertical-layouts">
    <div class="row match-height">
        <div class="col-md-4 col-12">
            <div class="card">
                <form class="form form-vertical" action="{{ route('perkiraanHpps.store', $billOfMaterial) }}" method="post">
                    @csrf

                    <div class="card-header">
                        <small class="text-muted">Catatan: Perkiraan Hpp yang dibutuhkan untuk memproduksi 1 barang.</small>
                    </div>
                    <div class="card-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Produk</label>
                                        <input type="text" class="form-control" value="{{ $billOfMaterial->produk->nama_produk }}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Ukuran & Warna</label>
                                        <input type="text" class="form-control" value="{{ $billOfMaterial->produk->ukuran_produk }} - {{ $billOfMaterial->produk->warna_produk }}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Biaya Material</label>
                                        <input data-inputmask="
                                            'alias': 'currency', 
                                            'prefix': 'Rp ',
                                            'placeholder': 'Rp 0',
                                            'rightAlign': 'true',
                                            'autoUnmask': 'true',
                                            'allowMinus': 'false',
                                            'removeMaskOnSubmit': 'true',
                                            'digits': '0'" 
                                            class="form-control" value="{{ $biayaMaterial }}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Biaya Pekerja</label>
                                        <input data-inputmask="
                                            'alias': 'currency', 
                                            'prefix': 'Rp ',
                                            'placeholder': 'Rp 0',
                                            'rightAlign': 'true',
                                            'autoUnmask': 'true',
                                            'allowMinus': 'false',
                                            'removeMaskOnSubmit': 'true',
                                            'digits': '0',
                                            'min': '1'" 
                                            aria-describedby="biayaPekerjaHelp" class="form-control @error('biaya_pekerja') is-invalid @enderror" name="biaya_pekerja" value="{{ old('biaya_pekerja', 1) }}">

                                        @error('biaya_pekerja')
                                            <small id="biayaPekerjaHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Biaya Overhead Pabrik</label>
                                        <input data-inputmask="
                                            'alias': 'currency', 
                                            'prefix': 'Rp ',
                                            'placeholder': 'Rp 0',
                                            'rightAlign': 'true',
                                            'autoUnmask': 'true',
                                            'allowMinus': 'false',
                                            'removeMaskOnSubmit': 'true',
                                            'digits': '0',
                                            'min': '1'" 
                                            aria-describedby="biayaOverheadHelp" class="form-control @error('biaya_overhead') is-invalid @enderror" name="biaya_overhead" value="{{ old('biaya_overhead', 1) }}">

                                        @error('biaya_overhead')
                                            <small id="biayaOverheadHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row ">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Simpan Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js" 
    integrity="sha512-6Jym48dWwVjfmvB0Hu3/4jn4TODd6uvkxdi9GNbBHwZ4nGcRxJUCaTkL3pVY6XUQABqFo3T58EMXFQztbjvAFQ==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/bindings/inputmask.binding.js" 
    integrity="sha512-J6WEJE0No+5Qqm9/T93q88yRQjvoAioXG4gzJ+eqZtLi+ZBgimZDkTiLWiljwrwnoQw+xwECQm282RJ6CrJnlw==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection