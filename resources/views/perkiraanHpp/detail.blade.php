@extends('layouts.app')

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-invoice.css') }}">
@endsection


@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Detail Perkiraan HPP</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Perkiraan HPP
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section class="invoice-view-wrapper">
    <div class="row">
        <!-- invoice view page -->
        <div class="col-xl-10 col-md-12 col-12">
            <div class="card invoice-print-area">
                <div class="card-header">
                    <small class="text-muted">Catatan: Perkiraan Hpp yang dibutuhkan untuk memproduksi 1 barang.</small>
                </div>
                <div class="card-body pb-0 mx-25">
                    <div class="row invoice-info">
                        <div class="col-sm-6 col-12 mt-1">                            
                            <h6 class="invoice-from">Spesifikasi Produk</h6>
                            <div class="mb-1">
                                <span>Nama Produk: {{ $perkiraanHpp->billOfMaterial->produk->nama_produk }}</span>
                            </div>
                            <div class="mb-1">
                                <span>Ukuran: {{ $perkiraanHpp->billOfMaterial->produk->ukuran_produk }}</span>
                            </div>
                            <div class="mb-1">
                                <span>Warna: {{ $perkiraanHpp->billOfMaterial->produk->warna_produk }}</span>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <!-- product details table-->
                <div class="invoice-product-details table-responsive">
                    <table class="table table-borderless mb-0">
                        <thead>
                            <tr class="border-0">
                                <th scope="col">#</th>
                                <th scope="col">Material</th>
                                <th scope="col">Ukuran</th>
                                <th scope="col">Warna</th>
                                <th scope="col" class="text-right">Qty</th>
                                <th scope="col" class="text-right">Harga</th>
                                <th scope="col">Satuan</th>
                                <th scope="col" class="text-right">Sub Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($perkiraanHpp->billOfMaterial->produks as $detail)
                                <tr>
                                    <td>
                                        <small class="text-muted">{{ $loop->index +1 }}</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->nama_produk }}</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->ukuran_produk }}</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->warna_produk }}</small>
                                    </td>
                                    <td class="text-right">
                                        <small class="text-muted">@number($detail->pivot->kuantitas)</small>
                                    </td>
                                    <td class="text-right">
                                        <small class="text-muted">@money($detail->harga_pokok)</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->satuan_produk }}</small>
                                    </td>
                                    <td class="text-right">
                                        <small class="text-muted">@money($detail->pivot->kuantitas * $detail->harga_pokok)</small>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-body pt-0 mx-25">
                    <hr>
                    <div class="row">
                        <div class="col-4 col-sm-6 col-12 mt-75">

                        </div>
                        <div class="col-8 col-sm-6 col-12 d-flex justify-content-end mt-75">
                            <div class="invoice-subtotal">
                                <div class="invoice-calc d-flex justify-content-between">
                                    <span class="invoice-title">Biaya Material</span>
                                    <span class="invoice-value">@money($perkiraanHpp->biaya_material)</span>
                                </div>
                                <div class="invoice-calc d-flex justify-content-between">
                                    <span class="invoice-title">Biaya Pekerja</span>
                                    <span class="invoice-value">@money($perkiraanHpp->biaya_pekerja)</span>
                                </div>
                                <div class="invoice-calc d-flex justify-content-between">
                                    <span class="invoice-title">Biaya Overhead</span>
                                    <span class="invoice-value">@money($perkiraanHpp->biaya_overhead)</span>
                                </div>    
                                <hr>
                                <div class="invoice-calc d-flex justify-content-between">
                                    <span class="invoice-title">Harga Pokok Produksi</span>
                                    <span class="invoice-value">@money($perkiraanHpp->nominalHpp)</span>
                                </div>                               
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row ">
                        <div class="col-6">
                            <a href="{{ route('perkiraanHpps.edit', $perkiraanHpp) }}" class="btn btn-warning">Ubah Data</a>
                        </div>
                        <div class="col-6 d-flex justify-content-end">
                            <a href="#DeleteModal" data-toggle="modal" class="btn btn-danger">Hapus Data</a>
                        </div>

                        {{-- BEGIN DELETE MODAL --}}
                        <div class="modal fade text-left" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h5 class="modal-title white" id="myModalLabel160">Hapus Perkiraan Hpp</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="bx bx-x"></i>
                                        </button>
                                    </div>
                                    <form action="{{ route('perkiraanHpps.destroy', $perkiraanHpp) }}" method="post">
                                        @csrf
                                        @method('delete')

                                        <div class="modal-body">
                                            <small>Apakah anda yakin untuk melakukannya?</small>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                                <i class="bx bx-x d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block">Batal</span>
                                            </button>
                                            <button type="submit" class="btn btn-danger ml-1">
                                                <i class="bx bx-check d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block">Hapus Data</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        {{-- END DELETE MODAL --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection


@section('script')
<script type="text/javascript">
    @if (Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection