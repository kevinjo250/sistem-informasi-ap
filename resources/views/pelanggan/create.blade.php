@extends('layouts.app')

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Buat Pelanggan</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Pelanggan
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-md-8 col-sm-12">
            <div class="card">
                <form class="form" action="{{ route('pelanggans.store') }}" method="post">
                    @csrf
                    
                    <div class="card-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Nama Pelanggan</label>
                                        <input type="text" aria-describedby="namaHelp" class="form-control @error('nama_pelanggan') is-invalid @enderror" name="nama_pelanggan" placeholder="Masukan Nama Pelanggan" value="{{ old('nama_pelanggan') }}">

                                        @error('nama_pelanggan')
                                            <small id="namaHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Email Pelanggan (Opsional)</label>
                                        <input type="email" aria-describedby="emailHelp" class="form-control @error('email_pelanggan') is-invalid @enderror" name="email_pelanggan" placeholder="Masukan Email Pelanggan (Opsional)" value="{{ old('email_pelanggan') }}">

                                        @error('email_pelanggan')
                                            <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Alamat Pelanggan</label>
                                        <input type="text" aria-describedby="alamatHelp" class="form-control @error('alamat_pelanggan') is-invalid @enderror" name="alamat_pelanggan" placeholder="Masukan Alamat Pelanggan" value="{{ old('alamat_pelanggan') }}">

                                        @error('alamat_pelanggan')
                                            <small id="alamatHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Kota Pelanggan</label>
                                        <input type="text" aria-describedby="kotaHelp" class="form-control @error('kota_pelanggan') is-invalid @enderror" name="kota_pelanggan" placeholder="Masukan Kota Pelanggan" value="{{ old('kota_pelanggan') }}">

                                        @error('kota_pelanggan')
                                            <small id="kotaHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Nomor KTP</label>
                                        <input type="number" aria-describedby="noKtpHelp" class="form-control @error('no_ktp') is-invalid @enderror" name="no_ktp" placeholder="Nomor KTP memiliki 16 digit" value="{{ old('no_ktp') }}">

                                        @error('no_ktp')
                                            <small id="noKtpHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Nomor NPWP (Opsional)</label>
                                        <input type="number" aria-describedby="noNpwpHelp" class="form-control @error('no_npwp') is-invalid @enderror" name="no_npwp" placeholder="Nomor NPWP memiliki 15 digit" value="{{ old('no_npwp') }}">

                                        @error('no_npwp')
                                            <small id="noNpwpHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Nomor Telepon</label>
                                        <input type="tel" aria-describedby="telpHelp" class="form-control @error('no_telepon') is-invalid @enderror" name="no_telepon" placeholder="Contoh: 081233911680" value="{{ old('no_telepon') }}">

                                        @error('no_telepon')
                                            <small id="telpHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row ">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Simpan Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection