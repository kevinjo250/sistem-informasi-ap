@extends('layouts.app')

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-invoice.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Ubah Surat Jalan</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Surat Jalan</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section class="invoice-edit-wrapper">
    <div class="row">
        <!-- invoice view page -->
        <div class="col-xl-12 col-md-12 col-12">
            <form class="invoice-item-repeater" action="{{ route('suratJalans.update', $suratJalan) }}" method="post">
                @csrf
                @method('patch')

                <div class="card">
                    <div class="card-body pb-0 mx-25">
                        <!-- header section -->
                        <div class="row mx-0">
                            <div class="col-xl-4 col-md-12 d-flex align-items-center pl-0">
                                <h6 class="invoice-number mb-0 mr-75">{{ $suratJalan->no_nota }}</h6>
                            </div>
                            <div class="col-xl-8 col-md-12 px-0 pt-xl-0 pt-1">
                                <div class="invoice-date-picker d-flex align-items-center justify-content-xl-end flex-wrap">
                                    <div class="d-flex align-items-center">
                                        <small class="text-muted mr-75">Tanggal Kirim: </small>
                                        <fieldset class="d-flex ">
                                            <input type="date" class="form-control mr-2 mb-50 mb-sm-0 @error('tanggal_kirim') is-invalid @enderror" name="tanggal_kirim" value="{{ $suratJalan->tanggal_kirim }}">
                                        </fieldset>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <!-- invoice address and contact -->
                        <div class="row invoice-info">
                            <div class="col-sm-6 col-12 mt-1">
                                <h6 class="invoice-from">Data Pelanggan</h6>
                                <div class="mb-1">
                                    <span>{{ $suratJalan->salesOrder->pelanggan->nama_pelanggan }}</span>
                                </div>
                                <div class="mb-1">
                                    <span>{{ $suratJalan->salesOrder->pelanggan->alamat_pelanggan }} {{ $suratJalan->salesOrder->pelanggan->kota_pelanggan }}</span>
                                </div>
                                <div class="mb-1">
                                    <span>{{ $suratJalan->salesOrder->pelanggan->email_pelanggan }}</span>
                                </div>
                                <div class="mb-1">
                                    <span>{{ $suratJalan->salesOrder->pelanggan->no_telepon }}</span>
                                </div>
                            </div>
                            <div class="col-sm-6 col-12 mt-1">
                                <h6 class="invoice-from">Data Sales Order</h6>
                                <div class="mb-1">
                                    <span>No Sales Order: <a href="{{ route('salesOrders.show', $suratJalan->sales_order_id) }}">{{ $suratJalan->salesOrder->no_nota }}</a></span>
                                </div>
                                <div class="mb-1">
                                    <span>Estimasi Pengiriman: @date($suratJalan->salesOrder->tanggal_kirim)</span>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="card-body pt-50">
                        <!-- product details table-->
                        <div class="invoice-product-details ">
                            <!-- table head dark -->
                            <div class="table-responsive">
                                <table class="table mb-0">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>Nama</th>
                                            <th>Ukuran</th>
                                            <th>Warna</th>
                                            <th>Max Qty Kirim</th>
                                            <th>Qty Kirim</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($suratJalan->produks as $detail)
                                            <tr>
                                                <td>
                                                    <input type="text" class="form-control" value="{{ $detail->nama_produk }}" readonly>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" value="{{ $detail->ukuran_produk }}" readonly>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" value="{{ $detail->warna_produk }}" readonly>
                                                </td>
                                                <td>
                                                    <input data-inputmask="
                                                        'alias': 'numeric',
                                                        'groupSeparator': ',',
                                                        'rightAlign': 'true',
                                                        'autoUnmask': 'true',
                                                        'allowMinus': 'false',
                                                        'removeMaskOnSubmit': 'true',
                                                        'digits': '0'" 
                                                        class="form-control" value="{{ array_key_exists($detail->pivot->produk_id, $shipped) ? $detail->pivot->kuantitas + $shipped[$detail->pivot->produk_id]['kuantitas_beli'] - $shipped[$detail->pivot->produk_id]['kuantitas'] : '' }}" readonly>
                                                </td>
                                                <td>
                                                    <input data-inputmask="
                                                        'alias': 'numeric',
                                                        'groupSeparator': ',',
                                                        'rightAlign': 'true',
                                                        'autoUnmask': 'true',
                                                        'allowMinus': 'false',
                                                        'removeMaskOnSubmit': 'true',
                                                        'digits': '0',
                                                        'min': '1',
                                                        'max': '{{ array_key_exists($detail->pivot->produk_id, $shipped) ? $detail->pivot->kuantitas + $shipped[$detail->pivot->produk_id]['kuantitas_beli'] - $shipped[$detail->pivot->produk_id]['kuantitas'] : '' }}'" 
                                                        aria-describedby="kuantitasHelp" class="form-control @error('kuantitas.' . $loop->index) is-invalid @enderror" name="kuantitas[]" value="{{ old('kuantitas.' . $loop->index, $detail->pivot->kuantitas) }}">

                                                    @error('kuantitas.' . $loop->index)
                                                        <small id="kuantitasHelp" class="form-text text-danger">{{ $message }}</small>
                                                    @enderror
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- invoice subtotal -->
                        <hr>
                        <div class="invoice-subtotal pt-50">
                            <div class="row">
                                <div class="col-md-5 col-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="keterangan" cols="30" rows="5" placeholder="Keterangan (Opsional)">{{ $suratJalan->keterangan }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">                
                        <div class="row">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-warning">Ubah Data</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js" 
    integrity="sha512-6Jym48dWwVjfmvB0Hu3/4jn4TODd6uvkxdi9GNbBHwZ4nGcRxJUCaTkL3pVY6XUQABqFo3T58EMXFQztbjvAFQ==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/bindings/inputmask.binding.js" 
    integrity="sha512-J6WEJE0No+5Qqm9/T93q88yRQjvoAioXG4gzJ+eqZtLi+ZBgimZDkTiLWiljwrwnoQw+xwECQm282RJ6CrJnlw==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/pages/app-invoice.js') }}"></script>
@endsection