@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Buat Bill of Material</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Bill of Material</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-vertical-layouts">
    <div class="row match-height">
        <div class="col-sm-12 col-md-8">
            <div class="card">
                <form class="form form-vertical" action="{{ route('billOfMaterials.store') }}" method="post">
                    @csrf
                    
                    <div class="card-header">
                        <small>Catatan: 1 Bill of Material digunakan untuk memproduksi 1 barang.</small>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <label for="produk-vertical">Produk</label>
                                <select aria-describedby="produkHelp" class="select2 form-control" name="produk_id" id="produk-vertical">
                                    <option></option>
                                    @foreach ($produks as $produk)
                                        @if ($produk->jenis_produk == 'finished product')
                                            <option value="{{ $produk->id }}" {{ old('produk_id') == $produk->id ? 'selected' : '' }}>
                                                {{ $produk->nama_produk }} - {{ $produk->ukuran_produk }} - {{ $produk->warna_produk }}
                                            </option>
                                        @endif                                    
                                    @endforeach
                                </select>

                                @error('produk_id')
                                    <small id="produkHelp" class="form-text text-danger">{{ $message}}</small>
                                @enderror
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-12">
                                <!-- table head dark -->
                                <div class="table-responsive">
                                    <table class="table mb-0" id="material-table">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th>Material</th>
                                                <th>Qty</th>
                                                <th>Sat</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach (old('material_id', ['']) as $item)
                                                <tr class="invoice-item-filled" id="material{{ $loop->index }}">
                                                    <td>
                                                        <select aria-describedby="materialHelp" class="select2 form-control" name="material_id[]">
                                                            <option></option>
                                                            @foreach ($produks as $material)
                                                                @if ($material->jenis_produk != 'finished product')
                                                                    <option value="{{ $material->id }}" 
                                                                        {{ old('material_id.' . $loop->parent->index) == $material->id ? 'selected' : '' }}
                                                                        data-satuan="{{ $material->satuan_produk }}">
                                                                        {{ $material->nama_produk }} - {{ $material->ukuran_produk }} - {{ $material->warna_produk }}
                                                                    </option>
                                                                @endif                                                            
                                                            @endforeach
                                                        </select>

                                                        @error('material_id.' . $loop->index)
                                                            <small id="materialHelp" class="form-text text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <input data-inputmask="
                                                            'alias': 'numeric',
                                                            'groupSeparator': ',',
                                                            'rightAlign': 'true',
                                                            'autoUnmask': 'true',
                                                            'allowMinus': 'false',
                                                            'removeMaskOnSubmit': 'true',
                                                            'digits': '0',
                                                            'min': '1'" 
                                                            aria-describedby="kuantitasHelp" class="form-control kuantitas @error('kuantitas') is-invalid @enderror" name="kuantitas[]" value="{{ old('kuantitas.' . $loop->index, 1) }}">

                                                        @error('kuantitas.' . $loop->index)
                                                            <small id="kuantitasHelp" class="form-text text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control satuan-produk" placeholder="Satuan Material" readonly>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-icon btn-danger text-nowrap px-1 hapus-material">
                                                            <i class="bx bx-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="col p-0">
                                        <button type="button" class="btn btn-info" id="tambah-material">
                                            Tambah Material
                                        </button>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <hr>
                    <div class="card-footer">
                        <div class="row ">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Simpan Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js" 
    integrity="sha512-6Jym48dWwVjfmvB0Hu3/4jn4TODd6uvkxdi9GNbBHwZ4nGcRxJUCaTkL3pVY6XUQABqFo3T58EMXFQztbjvAFQ==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/bindings/inputmask.binding.js" 
    integrity="sha512-J6WEJE0No+5Qqm9/T93q88yRQjvoAioXG4gzJ+eqZtLi+ZBgimZDkTiLWiljwrwnoQw+xwECQm282RJ6CrJnlw==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        //Trigger New Material
        let row_number = 1;

        $("#tambah-material").on("click", function(e){
            e.preventDefault();

            $('#material-table').append('<tr class="invoice-item-filled" id="material' + (row_number) + '"></tr>');

            $('#material' + row_number).html($('#material' + (row_number - 1)).html()).find('td:first-child');
        
            $(".select2-container").remove();

            $(".select2").select2({
                placeholder: "Pilih Data",
                dropdownAutoWidth: true,
                width: '100%'
            });

            $("#material" + row_number + " .kuantitas").inputmask({
                alias: 'numeric',
                groupSeparator: ',',
                rightAlign: true,
                autoUnmask: true,
                allowMinus: false,
                removeMaskOnSubmit: true,
                digits: '0',
                min: '0', 
            });

            $("#material" + row_number + " .select2").val(null).trigger("change.select2");

            $("#material" + row_number + " .text-danger").hide();

            $("#material" + row_number + " .satuan-produk").val("");
            
            row_number++;
        });

        //Remove Selected Material
        $(document).on('click', '.hapus-material', function () {
            $(this).parents('tr').remove();

            row_number--;
        });        

        $(document).on("change", ".select2", function (e) {
            var satuan = this.options[e.target.selectedIndex].getAttribute('data-satuan');

            $(e.target)
                .closest(".invoice-item-filled")
                .find(".satuan-produk")
                .val(satuan);
        });
    });
</script>
@endsection