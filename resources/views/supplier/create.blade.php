@extends('layouts.app')

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Buat Supplier</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Supplier
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12 col-md-8">
            <div class="card">
                <form class="form" action="{{ route('suppliers.store') }}" method="post">
                    @csrf
                    
                    <div class="card-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Nama Supplier</label>
                                        <input type="text" aria-describedby="namaHelp" class="form-control @error('nama_supplier') is-invalid @enderror" name="nama_supplier" placeholder="Masukan Nama Supplier" value="{{ old('nama_supplier') }}">

                                        @error('nama_supplier')
                                            <small id="namaHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Email Supplier (Opsional)</label>
                                        <input type="email" aria-describedby="emailHelp" class="form-control @error('email_supplier') is-invalid @enderror" name="email_supplier" placeholder="Masukan Email Supplier (Opsional)" value="{{ old('email_supplier') }}">

                                        @error('email_supplier')
                                            <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Alamat Supplier</label>
                                        <input type="text" aria-describedby="alamatHelp" class="form-control @error('alamat_supplier') is-invalid @enderror" name="alamat_supplier" placeholder="Masukan Alamat Supplier" value="{{ old('alamat_supplier') }}">

                                        @error('alamat_supplier')
                                            <small id="alamatHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Kota Supplier</label>
                                        <input type="text" aria-describedby="kotaHelp" class="form-control @error('kota_supplier') is-invalid @enderror" name="kota_supplier" placeholder="Masukan Kota Supplier" value="{{ old('kota_supplier') }}">

                                        @error('kota_supplier')
                                            <small id="kotaHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Nomor Telepon</label>
                                        <input type="tel" aria-describedby="telpHelp" class="form-control @error('no_telepon') is-invalid @enderror" name="no_telepon" placeholder="Contoh: 081233911680" value="{{ old('no_telepon') }}">

                                        @error('no_telepon')
                                            <small id="telpHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row ">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Simpan Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection