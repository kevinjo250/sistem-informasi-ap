@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-invoice.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Detail Nota Pembayaran</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Nota Pembayaran
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section class="invoice-view-wrapper">
    <div class="row">
        <!-- invoice view page -->
        <div class="col-xl-6 col-md-12 col-12">
            <div class="card invoice-print-area">
                <div class="card-body pb-0 mx-25">
                    <!-- header section -->
                    <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <span class="invoice-number mr-50">{{ $notaPembayaran->no_nota }}</span>
                        </div>
                        <div class="col-lg-8 col-md-12">
                            <div class="d-flex align-items-center justify-content-lg-end flex-wrap">
                                <div>
                                    <small class="text-muted">Tanggal Pembayaran:</small>
                                    <span>@datetime($notaPembayaran->created_at)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <!-- invoice address and contact -->
                    <div class="row invoice-info">
                        <div class="col-sm-6 col-12 mt-1">
                            <h6 class="invoice-from">Data Pelanggan</h6>
                            <div class="mb-1">
                                <span>{{ $notaPembayaran->salesOrder->pelanggan->nama_pelanggan }}</span>
                            </div>
                            <div class="mb-1">
                                <span>{{ $notaPembayaran->salesOrder->pelanggan->alamat_pelanggan }}, {{ $notaPembayaran->salesOrder->pelanggan->kota_pelanggan }}</span>
                            </div>
                            <div class="mb-1">
                                <span>{{ $notaPembayaran->salesOrder->pelanggan->no_telepon }}</span>
                            </div>
                        </div>
                        <div class="col-sm-6 col-12 mt-1">
                            <h6 class="invoice-from">Data Karyawan</h6>
                            <div class="mb-1">
                                <span>{{ $notaPembayaran->user->nama_karyawan }}</span>
                            </div>
                            <div class="mb-1">
                                <span>{{ $notaPembayaran->user->jabatan->nama_jabatan }}</span>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <!-- product details table-->
                <div class="invoice-product-details table-responsive">
                    <table class="table table-borderless mb-0">
                        <thead>
                            <tr class="border-0">
                                <th scope="col">Nomor Sales Order</th>
                                <th scope="col">Keterangan</th>
                                <th scope="col">Cara Pembayaran</th>
                                <th scope="col" class="text-right">Nominal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <a href="{{ route('salesOrders.show', $notaPembayaran->sales_order_id) }}">{{ $notaPembayaran->salesOrder->no_nota }}</a>
                                </td>
                                <td>
                                    @switch($notaPembayaran->keterangan)
                                        @case(null)
                                            <small class="text-muted">-</small>
                                            @break
                                        @default
                                            <small class="text-muted">{{ $notaPembayaran->keterangan }}</small>
                                    @endswitch
                                </td>
                                <td>
                                    <small class="text-muted">{{ $notaPembayaran->cara_bayar }}</small>
                                </td>
                                <td class="text-right">
                                    <small class="text-muted">@money($notaPembayaran->nominal_bayar)</small>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-6">
                            @can('notaPembayaran edit')
                                <a href="{{ route('notaPembayarans.edit', $notaPembayaran) }}" class="btn btn-warning">Ubah Data</a>
                            @endcan
                        </div>
                        <div class="col-6 d-flex justify-content-end">
                            @can('notaPembayaran delete')
                                <a href="#DeleteModal" class="btn btn-danger" data-toggle="modal">Hapus Data</a>
                            @endcan
                        </div>                        

                        {{-- BEGIN DELETE MODAL --}}
                        <div class="modal fade text-left" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h5 class="modal-title white" id="myModalLabel160">Hapus Nota Pembayaran</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="bx bx-x"></i>
                                        </button>
                                    </div>
                                    <form action="{{ route('notaPembayarans.destroy', $notaPembayaran) }}" method="post">
                                        @csrf
                                        @method('delete')

                                        <div class="modal-body">
                                            <p>Apakah anda yakin untuk menghapus transaksi tersebut?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                                <i class="bx bx-x d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block">Batal</span>
                                            </button>
                                            <button type="submit" class="btn btn-danger ml-1">
                                                <i class="bx bx-check d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block">Hapus Data</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        {{-- END DELETE MODAL --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if(Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection