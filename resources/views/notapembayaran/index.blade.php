@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Nota Pembayaran</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Nota Pembayaran
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-datatable">
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-body card-dashboard">
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>nomor SO</th>
                                    <th>pelanggan</th>
                                    <th>tgl</th>
                                    <th>nominal</th>
                                    <th>karyawan</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($notaPembayarans as $notaPembayaran)
                                    <tr>
                                        <td>{{ $notaPembayaran->no_nota }}</td>
                                        <td>
                                            <a href="{{ route('salesOrders.show', $notaPembayaran->sales_order_id) }}">{{ $notaPembayaran->salesOrder->no_nota }}</a>                                        
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $notaPembayaran->salesOrder->pelanggan->nama_pelanggan }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">@datetime($notaPembayaran->created_at)</small>
                                        </td>
                                        <td class="text-right">
                                            <small class="text-muted">@money($notaPembayaran->nominal_bayar)</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $notaPembayaran->user->nama_karyawan }}</small>
                                        </td>
                                        <td>
                                            <div class="invoice-action">
                                                <a href="{{ route('notaPembayarans.show', $notaPembayaran) }}" class="invoice-action-view cursor-pointer">
                                                    <i class="bx bx-show-alt"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/datatables/datatable.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if(Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection