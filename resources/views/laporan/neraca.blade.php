@extends('layouts.app')

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Laporan Neraca</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Laporan Keuangan
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row" id="table-bordered">
    <div class="col-4">
        <div class="card">
            <div class="table-responsive">
                <table class="table table-bordered mb-0">
                    <thead>
                        <tr>
                            <td colspan="2">Aktiva</td>
                        </tr>
                        <tr>
                            <td>Nama Akun</td>
                            <td class="text-right">Saldo</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($akuns as $akun)
                            @if ($akun->urutan_akun == 1)
                                <tr>
                                    <td>{{ $akun->nama_akun }}</td>
                                    <td class="text-right">@money($akun->periodes[0]->pivot->saldo_awal + $akun->sum_debet - $akun->sum_kredit)</td>
                                </tr>
                            @endif
                        @endforeach
                        <tr>
                            <td class="text-right">
                                <strong>Total Aktiva</strong>
                            </td>
                            <td class="text-right">
                                <strong>@money($totalAktiva)</strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="card">
            <div class="table-responsive">
                <table class="table table-bordered mb-0">
                    <thead>
                        <tr>
                            <td colspan="2">Pasiva</td>
                        </tr>
                        <tr>
                            <td>Nama Akun</td>
                            <td class="text-right">Saldo</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($akuns as $akun)
                            @if ($akun->urutan_akun == 2)
                                <tr>
                                    <td>{{ $akun->nama_akun }}</td>
                                    <td class="text-right">@money($akun->periodes[0]->pivot->saldo_awal + $akun->sum_kredit - $akun->sum_debet)</td>
                                </tr>
                            @endif
                        @endforeach
                        @foreach ($akuns as $akun)
                            @if ($akun->urutan_akun == 3 && $akun->id != 302)
                                <tr>
                                    <td>{{ $akun->nama_akun }}</td>
                                    <td class="text-right">@money($modal - $prive + $labaRugi)</td>
                                </tr>
                                @break
                            @endif
                        @endforeach
                        <tr>
                            <td class="text-right">
                                <strong>Total Pasiva</strong>
                            </td>
                            <td class="text-right">
                                <strong>@money($totalPasiva)</strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection