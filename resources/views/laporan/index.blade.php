@extends('layouts.app')

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Laporan Keuangan</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Laporan Keuangan
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-vertical-layouts">
    <div class="row match-height">
        <div class="col-md-6 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Laporan Keuangan</h4>
                </div>
                <form class="form form-vertical" action="{{ route('laporanKeuangans.find') }}" method="post">
                    @csrf
                    
                    <div class="card-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Periode Akuntansi</label>
                                        <input type="month" class="form-control @error('periode_akuntansi') is-invalid @enderror" aria-describedby="periodeHelp" name="periode_akuntansi">

                                        @error('periode_akuntansi')
                                            <small id="periodeHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Jenis Laporan</label>
                                        <select class="form-control @error('jenis_laporan') is-invalid @enderror" aria-describedby="jenisHelp" name="jenis_laporan">
                                            <option value="neraca" selected>Laporan Neraca</option>
                                            <option value="labaRugi">Laporan Laba/Rugi</option>
                                            <option value="perubahanEkuitas">Laporan Perubahan Ekuitas</option>
                                        </select>

                                        @error('jenis_laporan')
                                            <small id="jenisHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection