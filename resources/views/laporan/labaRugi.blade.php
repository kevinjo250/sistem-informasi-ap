@extends('layouts.app')

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Laporan Laba Rugi</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Laporan Keuangan
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row" id="table-borderless">
    <div class="col-6">
        <div class="card">
            <div class="card-header justify-content-center">
                <h4 class="card-title">Laporan Laba/Rugi</h4>
            </div>
            <div class="card-body">
                <p class="card-text text-center">Periode @periode($periode)</p>
                <div class="table-responsive">
                    <table class="table table-borderless mb-0">
                        <tbody>
                            @foreach ($akuns as $akun)
                                @if ($akun->urutan_akun == 4 && $akun->id == 401)
                                    <tr>
                                        <td>{{ $akun->nama_akun }}</td>
                                        <td></td>
                                        <td class="text-right">@money($akun->sum_kredit - $akun->sum_debet)</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-right"><strong>Total Pendapatan</strong></td>
                                        <td class="text-right"><strong>@money($totalPendapatan)</strong></td>
                                    </tr>
                                    @break
                                @endif
                            @endforeach
                            @foreach ($akuns as $akun)
                                @if ($akun->urutan_akun == 5 ||$akun->id == 402)
                                    <tr>
                                        <td>{{ $akun->nama_akun }}</td>
                                        <td class="text-right">@money($akun->sum_debet - $akun->sum_kredit)</td>
                                        <td></td>
                                    </tr>
                                @endif
                            @endforeach
                            <tr>
                                <td class="text-right"><strong>Total Biaya</strong></td>
                                <td class="text-right"><strong>@money($totalBiaya)</strong></td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right"><strong>Laba/Rugi Bersih</strong></td>
                                <td class="text-right"><strong>@money($labaRugi)</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection