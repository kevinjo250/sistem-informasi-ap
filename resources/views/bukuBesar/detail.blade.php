@extends('layouts.app')

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Detail Buku Besar</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Buku Besar
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
    @foreach ($akuns as $akun)
        @if($akun->id != 504)
            <div class="row" id="table-bordered">
                <div class="col-12">
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table table-bordered mb-0">
                                <thead>
                                    <tr>
                                        <th colspan="4" class="text-center">Akun: {{ $akun->nama_akun }} ({{ $akun->id }})</th>
                                        <th rowspan="2" class="text-center align-middle">Debet</th>
                                        <th rowspan="2" class="text-center align-middle">Kredit</th>
                                    </tr>
                                    <tr>
                                        <th>#</th>
                                        <th>Tanggal</th>
                                        <th>No Bukti</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($akun->periodes))
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <small class="text-muted">{{ $akun->periodes[0]->tanggal_awal }}</small>
                                            </td>
                                            <td>
                                                <small class="text-muted">-</small>
                                            </td>
                                            <td>
                                                <small class="text-muted">Saldo Awal</small>
                                            </td>
                                            @switch($akun->tipe_akun)
                                                @case(0)
                                                    <td class="text-right">
                                                        <small class="text-muted">@money(0)</small>
                                                    </td>
                                                    <td class="text-right">
                                                        <small class="text-muted">@money($akun->periodes[0]->pivot->saldo_awal)</small>
                                                    </td>
                                                    @break
                                                @default
                                                    <td class="text-right">
                                                        <small class="text-muted">@money($akun->periodes[0]->pivot->saldo_awal)</small>
                                                    </td>
                                                    <td class="text-right">
                                                        <small class="text-muted">@money(0)</small>
                                                    </td>
                                            @endswitch
                                        </tr>
                                        @foreach ($akun->jurnals as $detail)
                                            <tr>
                                                <td>{{ $loop->index +2 }}</td>
                                                <td>
                                                    <small class="text-muted">{{ $detail->tanggal }}</small>
                                                </td>
                                                <td>
                                                    @switch($detail->jenis_transaksi)
                                                        @case('penjualan')                                            
                                                            <a href="{{ route('salesOrders.show', $salesOrders->where('no_nota', $detail->nomor_bukti)->first()->id) }}">{{ $detail->nomor_bukti }}</a>
                                                            @break
                                                        @case('pembatalan')
                                                            <a href="#">{{ $detail->nomor_bukti }}</a>
                                                            @break
                                                        @case('pembayaran pelanggan')                                            
                                                            <a href="#">{{ $detail->nomor_bukti }}</a>
                                                            @break
                                                        @case('pengiriman')                                            
                                                            <a href="{{ route('suratJalans.show', $suratJalans->where('no_nota', $detail->nomor_bukti)->first()->id) }}">{{ $detail->nomor_bukti }}</a>
                                                            @break
                                                        @case('pembelian')                                            
                                                            <a href="{{ route('pemesananMaterials.show', $pemesananMaterials->where('no_nota', $detail->nomor_bukti)->first()->id) }}">{{ $detail->nomor_bukti }}</a>
                                                            @break
                                                        @case('penerimaan material')                                            
                                                            <a href="{{ route('penerimaanMaterials.show', $penerimaanMaterials->where('no_nota', $detail->nomor_bukti)->first()->id) }}">{{ $detail->nomor_bukti }}</a>
                                                            @break
                                                        @case('pelunasan supplier')                                            
                                                            <a href="#">{{ $detail->nomor_bukti }}</a>
                                                            @break
                                                        @case('permintaan material')                                            
                                                            <a href="{{ route('permintaanMaterials.show', $permintaanMaterials->where('no_nota', $detail->nomor_bukti)->first()->id) }}">{{ $detail->nomor_bukti }}</a>
                                                            @break
                                                        @case('produksi')
                                                            <a href="{{ route('realisasiProduksis.show', $realisasiProduksis->where('no_nota', $detail->nomor_bukti)->first()->id) }}">{{ $detail->nomor_bukti }}</a>
                                                            @break
                                                        @default
                                                            <small class="text-muted">-</small>
                                                    @endswitch
                                                </td>
                                                <td>
                                                    <small class="text-muted">{{ $detail->keterangan }}</small>
                                                </td>
                                                <td class="text-right">
                                                    <small class="text-muted">@money($detail->pivot->debet)</small>
                                                </td>
                                                <td class="text-right">
                                                    <small class="text-muted">@money($detail->pivot->kredit)</small>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="4" class="text-right">
                                                <strong>Total Saldo</strong>
                                            </td>
                                            @switch($akun->tipe_akun)
                                                @case(0)                                        
                                                    <td class="text-right">
                                                        <strong>@money(0)</strong>
                                                    </td>
                                                    <td class="text-right">
                                                        <strong>@money(($akun->periodes[0]->pivot->saldo_awal + $akun->sum_kredit) - $akun->sum_debet)</strong>
                                                    </td>
                                                    @break
                                                @default
                                                    <td class="text-right">
                                                        <strong>@money($akun->periodes[0]->pivot->saldo_awal + $akun->sum_debet - $akun->sum_kredit)</strong>
                                                    </td>
                                                    <td class="text-right">
                                                        <strong>@money(0)</strong>
                                                    </td>
                                            @endswitch
                                        </tr>
                                        <tr>
                                            <td colspan="6"></td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td colspan="6" class="text-center">
                                                <small class="text-muted">No Data Available.</small>
                                            </td>
                                        </tr>
                                    @endif                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@endsection