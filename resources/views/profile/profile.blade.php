@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Pengaturan Akun</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Informasi Akun
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="page-account-settings">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <!-- left menu section -->
                <div class="col-md-3 mb-2 mb-md-0 pills-stacked">
                    <ul class="nav nav-pills flex-column">
                        <li class="nav-item">
                            <a class="nav-link d-flex align-items-center {{ $errors->hasBag('update-password') ? '' : 'active' }}" id="account-pill-general" data-toggle="pill" href="#account-vertical-general" aria-expanded="true">
                                <i class="bx bx-cog"></i>
                                <span>Data Pribadi</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex align-items-center {{ $errors->hasBag('update-password') ? 'active' : '' }}" id="account-pill-password" data-toggle="pill" href="#account-vertical-password" aria-expanded="false">
                                <i class="bx bx-lock"></i>
                                <span>Ubah Password</span>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane {{ $errors->hasBag('update-password') ? 'fade' : 'active' }}" id="account-vertical-general" aria-labelledby="account-pill-general" aria-expanded="true">
                                    <form class="validate-form" action="{{ route('profile.update', $user) }}" method="post" novalidate>
                                        @csrf
                                        @method('patch')

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Username</label>
                                                        <input type="text" class="form-control @error('username', 'update-profile') is-invalid @enderror" placeholder="Masukkan Username" value="{{ old('username', $user->username) }}" name="username" aria-describedby="usernameHelp" required>

                                                        @error('username', 'update-profile')
                                                            <small id="usernameHelp" class="form-text text-danger">{{ $message}}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Nama Lengkap</label>
                                                        <input type="text" class="form-control @error('nama_karyawan', 'update-profile') is-invalid @enderror" placeholder="Masukkan Nama Lengkap" value="{{ old('nama_karyawan', $user->nama_karyawan) }}" name="nama_karyawan" aria-describedby="namaHelp" required>

                                                        @error('nama_karyawan', 'update-profile')
                                                            <small id="namaHelp" class="form-text text-danger">{{ $message}}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Alamat Tinggal</label>
                                                        <input type="text" class="form-control @error('alamat_karyawan', 'update-profile') is-invalid @enderror" placeholder="Masukkan Alamat Tinggal" value="{{ old('alamat_karyawan', $user->alamat_karyawan) }}" name="alamat_karyawan" aria-describedby="alamatHelp" required>

                                                        @error('alamat_karyawan', 'update-profile')
                                                            <small id="alamatHelp" class="form-text text-danger">{{ $message}}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>E-mail (Opsional)</label>
                                                        <input type="email" class="form-control @error('email_karyawan', 'update-profile') is-invalid @enderror" placeholder="Contoh: karyawan@gmail.com" value="{{ old('email_karyawan', $user->email_karyawan) }}" name="email_karyawan" aria-describedby="emailHelp">

                                                        @error('email_karyawan', 'update-profile')
                                                            <small id="emailHelp" class="form-text text-danger">{{ $message}}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label>Nomor KTP</label>
                                                    <input type="text" class="form-control @error('no_ktp', 'update-profile') is-invalid @enderror" placeholder="Nomor KTP memiliki 16 digit" value="{{ old('no_ktp', $user->no_ktp) }}" name="no_ktp" aria-describedby="ktpHelp" required>

                                                    @error('no_ktp', 'update-profile')
                                                        <small id="ktpHelp" class="form-text text-danger">{{ $message}}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label>Nomor Telepon</label>
                                                    <input type="text" class="form-control @error('no_telepon', 'update-profile') is-invalid @enderror" placeholder="Masukkan Nomor Telepon" value="{{ old('no_telepon', $user->no_telepon) }}" name="no_telepon" aria-describedby="telpHelp" required>

                                                    @error('no_telepon', 'update-profile')
                                                        <small id="telpHelp" class="form-text text-danger">{{ $message}}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Tanggal Lahir</label>
                                                        <input type="date" class="form-control @error('tanggal_lahir', 'update-profile') is-invalid @enderror" placeholder="Pilih Tanggal Lahir" value="{{ old('tanggal_lahir', $user->tanggal_lahir) }}" name="tanggal_lahir" aria-describedby="lahirHelp" required>

                                                        @error('tanggal_lahir', 'update-profile')
                                                            <small id="lahirHelp" class="form-text text-danger">{{ $message}}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-6">
                                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                                            </div>
                                            <div class="col-6 d-flex justify-content-end">
                                                <button type="submit" class="btn btn-warning">Ubah Data</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane {{ $errors->hasBag('update-password') ? 'active' : 'fade' }}" id="account-vertical-password" role="tabpanel" aria-labelledby="account-pill-password" aria-expanded="false">
                                    <form class="validate-form" action="{{ route('password.update', $user) }}" method="post" novalidate>
                                        @csrf
                                        @method('patch')

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Password Saat Ini</label>
                                                        <input type="password" class="form-control @error('current_password', 'update-password') is-invalid @enderror" placeholder="Masukkan Password Saat Ini" name="current_password" aria-describedby="cpwdHelp" required>

                                                        @error('current_password', 'update-password')
                                                            <small id="cpwdHelp" class="form-text text-danger">{{ $message}}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Password Baru</label>
                                                        <input type="password" class="form-control @error('password', 'update-password') is-invalid @enderror" placeholder="Masukkan Password Baru" name="password" aria-describedby="pwdHelp" required>

                                                        @error('password', 'update-password')
                                                            <small id="pwdHelp" class="form-text text-danger">{{ $message}}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Konfirmasi Password Baru</label>
                                                        <input type="password" class="form-control @error('password', 'update-password') is-invalid @enderror" data-validation-match-match="password" placeholder="Masukkan Konfirmasi Password Baru" name="password_confirmation" aria-describedby="pwdHelp" required>

                                                        @error('password', 'update-password')
                                                            <small id="pwdHelp" class="form-text text-danger">{{ $message}}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-6">
                                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                                            </div>
                                            <div class="col-6 d-flex justify-content-end">
                                                <button type="submit" class="btn btn-warning">Ubah Data</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/forms/validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('page js')
@endsection

@section('script')
<script type="text/javascript">
    @if(Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection