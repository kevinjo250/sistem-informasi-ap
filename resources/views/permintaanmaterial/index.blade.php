@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Permintaan Material</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Permintaan Material
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-datatable">
    @can('permintaanMaterial create')
        <div class="invoice-create-btn mb-1">
            <a href="{{ route('permintaanMaterials.create') }}" class="btn btn-primary btn-sm round glow align-items-center">Buat Permintaan Material</a>
        </div>
    @endcan
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body card-dashboard">
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nomor SPK</th>
                                    <th>Produk</th>
                                    <th>Ukuran</th>
                                    <th>Warna</th>
                                    <th>Created At</th>
                                    <th>Status
                                        <i class="bx bx-info-circle" 
                                            data-toggle="popover" data-placement="left" 
                                            data-container="body" data-original-title="Status Nota" 
                                            data-content=
                                                "<small><strong>Pending:</strong> Nota permintaan sedang direview.</small> <br />
                                                <small><strong>Approved:</strong> Nota permintaan telah disetujui.</small>"
                                            data-html="true" data-trigger="hover">
                                        </i>
                                    </th>
                                    <th>Karyawan</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($permintaanMaterials as $permintaanMaterial)
                                    <tr>
                                        <td>{{ $permintaanMaterial->no_nota }}</td>
                                        <td>
                                            <a href="{{ route('suratPerintahKerjas.show', $permintaanMaterial->surat_perintah_kerja_id) }}">{{ $permintaanMaterial->suratPerintahKerja->no_nota }}</a>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $permintaanMaterial->suratPerintahKerja->produkSalesOrder->produk->nama_produk }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $permintaanMaterial->suratPerintahKerja->produkSalesOrder->produk->ukuran_produk }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $permintaanMaterial->suratPerintahKerja->produkSalesOrder->produk->warna_produk }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">@datetime($permintaanMaterial->created_at)</small>
                                        </td>
                                        <td>
                                            @switch($permintaanMaterial->status_nota)
                                                @case('approved')
                                                    <span class="badge badge-light-success badge-pill">{{ $permintaanMaterial->status_nota }}</span>
                                                    @break
                                                @default
                                                    <span class="badge badge-light-info badge-pill">{{ $permintaanMaterial->status_nota }}</span>
                                            @endswitch
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $permintaanMaterial->user->nama_karyawan }}</small>
                                        </td>
                                        <td>
                                            <div class="invoice-action">
                                                <a href="{{ route('permintaanMaterials.show', $permintaanMaterial) }}" class="invoice-action-view cursor-pointer">
                                                    <i class="bx bx-show-alt"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/datatables/datatable.js') }}"></script>
<script src="{{ asset('app-assets/js/scripts/popover/popover.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if(Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection