@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-invoice.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Detail Permintaan Material</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Permintaan Material
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section class="invoice-view-wrapper">
    <div class="row">
        <!-- invoice view page -->
        <div class="col-xl-9 col-md-12 col-12">
            <div class="card invoice-print-area">
                <div class="card-body pb-0 mx-25">
                    <!-- header section -->
                    <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <span class="invoice-number mr-50">{{ $permintaanMaterial->no_nota }}</span>
                            @switch($permintaanMaterial->status_nota)
                                @case('approved')
                                    <span class="badge badge-light-success badge-pill">{{ $permintaanMaterial->status_nota }}</span>
                                    @break
                                @default
                                    <span class="badge badge-light-info badge-pill">{{ $permintaanMaterial->status_nota }}</span>
                            @endswitch
                        </div>
                        <div class="col-lg-8 col-md-12">
                            <div class="d-flex align-items-center justify-content-lg-end flex-wrap">
                                <div>
                                    <small class="text-muted">Tanggal:</small>
                                    <span>@datetime($permintaanMaterial->created_at)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <!-- invoice address and contact -->
                    <div class="row invoice-info">
                        <div class="col-sm-6 col-12 mt-1">
                            <h6 class="invoice-from">Data Surat Perintah Kerja</h6>
                            <div class="mb-1">
                                <span>No Surat Perintah Kerja: 
                                    <a href="{{ route('suratPerintahKerjas.show', $permintaanMaterial->surat_perintah_kerja_id) }}">{{ $permintaanMaterial->suratPerintahKerja->no_nota }}</a>
                                </span>
                            </div>
                            <div class="mb-1">
                                <span>Tanggal Produksi: @date($permintaanMaterial->suratPerintahKerja->tanggal_produksi)</span>
                            </div>
                        </div>
                        <div class="col-sm-6 col-12 mt-1">
                            <h6 class="invoice-from">Data Produk</h6>
                            <div class="mb-1">
                                <span>Nama Produk: {{ $permintaanMaterial->suratPerintahKerja->produkSalesOrder->produk->nama_produk }}</span>
                            </div>
                            <div class="mb-1">
                                <span>Ukuran & Warna: {{ $permintaanMaterial->suratPerintahKerja->produkSalesOrder->produk->ukuran_produk }} - {{ $permintaanMaterial->suratPerintahKerja->produkSalesOrder->produk->warna_produk }}</span>
                            </div>
                            <div class="mb-1">
                                <span>Kuantitas Produksi: @number($permintaanMaterial->suratPerintahKerja->produkSalesOrder->kuantitas)</span>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="invoice-product-details table-responsive">
                    <table class="table table-borderless mb-0">
                        <thead>
                            <tr class="border-0">
                                <th scope="col">Material</th>
                                <th scope="col">Ukuran</th>
                                <th scope="col">Warna</th>
                                @if ($permintaanMaterial->status_nota == 'pending')
                                    <th scope="col" class="text-right">Stok Gudang</th>
                                @endif
                                <th scope="col" class="text-right">Qty</th>
                                <th scope="col">Satuan</th>
                                @if ($permintaanMaterial->status_nota == 'pending')                                    
                                    <th scope="col">Status</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($permintaanMaterial->produks as $detail)
                                <tr>
                                    <td>
                                        <small class="text-muted">{{ $detail->nama_produk }}</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->ukuran_produk }}</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->warna_produk }}</small>
                                    </td>          
                                    @if ($permintaanMaterial->status_nota == 'pending')
                                        <td class="text-right">
                                            <small class="text-muted">@number($detail->stok_produk)</small>
                                        </td>
                                    @endif
                                    <td class="text-right">
                                        <small class="text-muted">@number($detail->pivot->kuantitas)</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->satuan_produk }}</small>
                                    </td>
                                    @if ($permintaanMaterial->status_nota == 'pending')
                                        <td>
                                            @if ($detail->stok_produk < $detail->pivot->kuantitas)
                                                <span class="badge badge-light-danger badge-pill">Not Ready</span>
                                            @elseif ($detail->stok_produk >= $detail->pivot->kuantitas)
                                                <span class="badge badge-light-primary badge-pill">Ready</span>
                                            @endif
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">                    
                    @if ($permintaanMaterial->status_nota == 'pending')
                        <div class="row">
                            <div class="col-6">
                                @can('permintaanMaterial delete')
                                    <a href="#HapusModal" class="btn btn-danger" data-toggle="modal">Hapus Data</a>
                                @endcan
                            </div>

                            {{-- BEGIN HAPUS MODAL --}}
                            <div class="modal fade text-left" id="HapusModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header bg-danger">
                                            <h5 class="modal-title white" id="myModalLabel160">Hapus Permintaan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i class="bx bx-x"></i>
                                            </button>
                                        </div>
                                        <form action="{{ route('permintaanMaterials.destroy', $permintaanMaterial) }}" method="post">
                                            @csrf
                                            @method('delete')

                                            <div class="modal-body">
                                                <p>Apakah anda yakin untuk menghapus permintaan ini?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-light-danger" data-dismiss="modal">
                                                    <i class="bx bx-x d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Batal</span>
                                                </button>
                                                <button type="submit" class="btn btn-danger ml-1">
                                                    <i class="bx bx-check d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Hapus Permintaan</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            {{-- END HAPUS MODAL --}}

                            <div class="col-6 d-flex justify-content-end">
                                @can('permintaanMaterial confirm')
                                    <a href="#ConfirmModal" class="btn btn-success" data-toggle="modal">Konfirmasi Permintaan</a>
                                @endcan
                            </div>

                            {{-- BEGIN CONFIRM MODAL --}}
                            <div class="modal fade text-left" id="ConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header bg-success">
                                            <h5 class="modal-title white" id="myModalLabel160">Konfirmasi Permintaan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i class="bx bx-x"></i>
                                            </button>
                                        </div>
                                        <form action="{{ route('permintaanMaterials.confirm', $permintaanMaterial) }}" method="post">
                                            @csrf
                                            @method('patch')

                                            <div class="modal-body">
                                                <p>Apakah anda yakin untuk mengkonfirmasi permintaan ini?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-light-danger" data-dismiss="modal">
                                                    <i class="bx bx-x d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Batal</span>
                                                </button>
                                                <button type="submit" class="btn btn-success ml-1">
                                                    <i class="bx bx-check d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Konfirmasi</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            {{-- END CONFIRM MODAL --}}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if (Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif

    @if (Session::has('error'))
        toastr['error']("{{ session('error') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection