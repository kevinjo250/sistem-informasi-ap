@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Buat Permintaan Material</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Permintaan Material
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-vertical-layouts">
    <div class="row match-height">
        <div class="col-md-8 col-12">
            <div class="card">
                <form class="form form-vertical" action="{{ route('permintaanMaterials.store') }}" method="post">
                    @csrf

                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <label>Nomor Surat Perintah Kerja</label>
                                <select aria-describedby="spkHelp" class="select2 form-control" name="surat_perintah_kerja_id">
                                    <option></option>
                                    @foreach ($suratPerintahKerjas as $suratPerintahKerja)
                                        <option value="{{ $suratPerintahKerja->id }}" 
                                            data-qty="{{ $suratPerintahKerja->kuantitas }}" 
                                            data-produk="{{ $suratPerintahKerja->produkSalesOrder->produk->nama_produk }}" 
                                            data-tambahan="{{ $suratPerintahKerja->produkSalesOrder->produk->ukuran_produk }} - {{ $suratPerintahKerja->produkSalesOrder->produk->warna_produk }}" 
                                            {{ old('surat_perintah_kerja_id') == $suratPerintahKerja->id ? 'selected' : '' }}>
                                            {{ $suratPerintahKerja->no_nota }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('surat_perintah_kerja_id')
                                    <small id="spkHelp" class="form-text text-danger">{{ $message}}</small>
                                @enderror
                            </div>
                            <div class="col-6">
                                <label>Jumlah Produksi</label>
                                <input data-inputmask="
                                    'alias': 'numeric',
                                    'groupSeparator': ',',
                                    'rightAlign': 'true',
                                    'autoUnmask': 'true',
                                    'allowMinus': 'false',
                                    'removeMaskOnSubmit': 'true',
                                    'digits': '0',
                                    'min': '0'" 
                                    class="form-control qty" value="0" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <label>Produk</label>
                                <input type="text" class="form-control produk" placeholder="Nama Produk" readonly>
                            </div>
                            <div class="col-6">
                                <label>Ukuran - Warna</label>
                                <input type="text" class="form-control tambahan" placeholder="Ukuran - Warna Produk" readonly>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-12">
                                <!-- table head dark -->
                                <div class="table-responsive">
                                    <table class="table table-bordered mb-0" id="material-table">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th>Material</th>
                                                <th>Ukuran</th>
                                                <th>Warna</th>
                                                <th>Qty</th>
                                                <th>Sat</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="text-center">
                                                <td colspan="5">No data available in table</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row ">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Simpan Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js" 
    integrity="sha512-6Jym48dWwVjfmvB0Hu3/4jn4TODd6uvkxdi9GNbBHwZ4nGcRxJUCaTkL3pVY6XUQABqFo3T58EMXFQztbjvAFQ==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/bindings/inputmask.binding.js" 
    integrity="sha512-J6WEJE0No+5Qqm9/T93q88yRQjvoAioXG4gzJ+eqZtLi+ZBgimZDkTiLWiljwrwnoQw+xwECQm282RJ6CrJnlw==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="surat_perintah_kerja_id"]').on('change', function(e) {

            let qty = this.options[e.target.selectedIndex].getAttribute('data-qty');
            let produk = this.options[e.target.selectedIndex].getAttribute('data-produk');
            let tambahan = this.options[e.target.selectedIndex].getAttribute('data-tambahan');

            $(".qty").val(qty);
            $(".produk").val(produk);
            $(".tambahan").val(tambahan);

            let surat_perintah_kerja_id = $(this).val();

            $('#material-table tbody').remove();

            $.ajax({
               url: '/permintaanMaterials/getBillOfMaterials/' + surat_perintah_kerja_id,
               type: "GET",
               dataType: "json",
               success: function(data) {
                    //console.log(data);
                    $.each(data, function(i, value) {
                        $.each(value.produks, function(j, text) {

                            $('<tbody>').append(
                                $('<td>').text(text.nama_produk),
                                $('<td>').text(text.ukuran_produk),
                                $('<td>').text(text.warna_produk),
                                $('<td>').text((text.pivot['kuantitas'] * qty).toLocaleString('en-GB')),
                                $('<td>').text(text.satuan_produk),
                            ).appendTo("#material-table");
                        });
                    });
               }
            });
        });
    });
</script>
@endsection