@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-invoice.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Ubah Sales Order</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Sales Order
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section class="invoice-edit-wrapper">
    <div class="row">
        <!-- invoice view page -->
        <div class="col-xl-12 col-md-12 col-12">
            <form class="invoice-item-repeater" action="{{ route('salesOrders.update', $salesOrder) }}" method="post">
                @csrf
                @method('patch')

                <div class="card">
                    <div class="card-body pb-0 mx-25">
                        <!-- header section -->
                        <div class="row mx-0">
                            <div class="col-xl-4 col-md-12 d-flex align-items-center pl-0">
                                <h6 class="invoice-number mb-0 mr-75">{{ $salesOrder->no_nota }}</h6>
                            </div>
                            <div class="col-xl-8 col-md-12 px-0 pt-xl-0 pt-1">
                                <div class="invoice-date-picker d-flex align-items-center justify-content-xl-end flex-wrap">
                                    <div class="d-flex align-items-center">
                                        <small class="text-muted mr-75">Estimasi Kirim: </small>
                                        <fieldset class="d-flex ">
                                            <input type="date" class="form-control mr-2 mb-50 mb-sm-0 @error('tanggal_kirim') is-invalid @enderror" name="tanggal_kirim" value="{{ old('tanggal_kirim', $salesOrder->tanggal_kirim) }}">
                                        </fieldset>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <small class="text-muted mr-75">Estimasi Jth Tempo: </small>
                                        <fieldset class="d-flex">
                                            <input type="date" class="form-control mr-2 mb-50 mb-sm-0 @error('tanggal_jatuh_tempo') is-invalid @enderror" name="tanggal_jatuh_tempo" value="{{ old('tanggal_jatuh_tempo', $salesOrder->tanggal_jatuh_tempo) }}">
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <!-- invoice address and contact -->
                        <div class="row invoice-info">
                            <div class="col-lg-4 col-md-6 mt-25">
                                <h6 class="invoice-to">Data Pelanggan</h6>
                                <fieldset class="invoice-address form-group">
                                    @if ($salesOrder->status_nota != 'draft')
                                        <input type="text" class="form-control" value="{{ $salesOrder->pelanggan->nama_pelanggan }}" readonly>
                                    @else
                                        <select aria-describedby="pelangganHelp" class="select2 form-control @error('pelanggan_id') is-invalid @enderror" name="pelanggan_id" id="pelanggan_id">
                                            <option></option>
                                            @foreach ($pelanggans as $pelanggan)
                                                <option value="{{ $pelanggan->id }}" 
                                                    data-alamat="{{ $pelanggan->alamat_pelanggan }}" 
                                                    data-kota="{{ $pelanggan->kota_pelanggan }}" 
                                                    {{ old('pelanggan_id', $salesOrder->pelanggan_id) == $pelanggan->id ? 'selected' : '' }}>{{ $pelanggan->nama_pelanggan }}
                                                </option>
                                            @endforeach
                                        </select>

                                        @error('pelanggan_id')
                                            <small id="pelangganHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    @endif
                                </fieldset>
                                <fieldset class="invoice-address form-group">
                                    <textarea class="form-control" name="alamat_lengkap" id="alamat_pelanggan" rows="4" placeholder="Alamat Lengkap" style="margin-top: 0px; margin-bottom: 0px; height: 101px;" readonly>{{ old('alamat_lengkap', $salesOrder->pelanggan->alamat_pelanggan . ' ' . $salesOrder->pelanggan->kota_pelanggan) }}</textarea>
                                </fieldset>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="card-body pt-50">
                        <!-- product details table-->
                        <div class="invoice-product-details ">
                            <!-- table head dark -->
                            <div class="table-responsive">
                                <table class="table mb-0" id="produk-table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>Produk</th>
                                            <th>Harga</th>
                                            <th>Qty</th>
                                            <th>SubTotal</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($salesOrder->produks as $detail)
                                            <tr class="invoice-item-filed" id="produk{{ $loop->index }}">
                                                <td>
                                                    <select aria-describedby="produkHelp" class="select2 form-control invoice-item-select @error('produk_id.' . $loop->index) is-invalid @enderror" name="produk_id[]">
                                                        <option></option>
                                                        @foreach ($produks as $produk)
                                                            <option value="{{ $produk->id }}" 
                                                                data-harga = {{ $produk->harga_jual }} 
                                                                {{ old('produk_id.' . $loop->parent->index, $detail->pivot->produk_id) == $produk->id ? 'selected' : '' }}>
                                                                {{ $produk->nama_produk }} - {{ $produk->ukuran_produk }} - {{ $produk->warna_produk }}
                                                            </option>
                                                        @endforeach
                                                    </select>

                                                    @error('produk_id.' . $loop->index)
                                                        <small id="produkHelp" class="form-text text-danger">{{ $message }}</small>
                                                    @enderror
                                                </td>
                                                <td>
                                                    <input data-inputmask="
                                                        'alias': 'currency', 
                                                        'prefix': 'Rp ',
                                                        'placeholder': 'Rp 0',
                                                        'rightAlign': 'true',
                                                        'autoUnmask': 'true',
                                                        'allowMinus': 'false',
                                                        'removeMaskOnSubmit': 'true',
                                                        'digits': '0'" 
                                                        class="form-control invoice-item-price" name="harga[]" value="{{ old('harga.' . $loop->index, $detail->pivot->harga) }}" readonly>
                                                </td>
                                                <td>
                                                    <input data-inputmask="
                                                        'alias': 'numeric',
                                                        'groupSeparator': ',',
                                                        'rightAlign': 'true',
                                                        'autoUnmask': 'true',
                                                        'allowMinus': 'false',
                                                        'removeMaskOnSubmit': 'true',
                                                        'digits': '0',
                                                        'min': '1'" 
                                                        aria-describedby="kuantitasHelp" class="form-control invoice-item-qty @error('kuantitas.' . $loop->index) is-invalid @enderror" name="kuantitas[]" value="{{ old('kuantitas.' . $loop->index, $detail->pivot->kuantitas) }}" @if ($salesOrder->status_nota != 'draft') readonly @endif>

                                                    @error('kuantitas.' . $loop->index)
                                                        <small id="kuantitasHelp" class="form-text text-danger">{{ $message }}</small>
                                                    @enderror
                                                </td>
                                                <td>
                                                    <input data-inputmask="
                                                        'alias': 'currency', 
                                                        'prefix': 'Rp ',
                                                        'placeholder': 'Rp 0',
                                                        'rightAlign': 'true',
                                                        'autoUnmask': 'true',
                                                        'allowMinus': 'false',
                                                        'removeMaskOnSubmit': 'true',
                                                        'digits': '0'" 
                                                        class="form-control align-middle invoice-item-sub-total" name="sub_total[]" value="{{ old('sub_total.' . $loop->index, $detail->pivot->sub_total) }}" readonly>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-icon btn-danger text-nowrap px-1 hapus-produk">
                                                        <i class="bx bx-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>                            
                            <div class="col p-0">
                                <button type="button" class="btn btn-info" id="tambah-produk">
                                    Tambah Produk
                                </button>
                            </div>
                        </div>
                        <!-- invoice subtotal -->
                        <hr>
                        <div class="invoice-subtotal pt-50">
                            <div class="row">
                                <div class="col-md-5 col-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="keterangan" cols="30" rows="5" placeholder="Keterangan (Opsional)">{{ $salesOrder->keterangan }}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-7 offset-lg-2 col-12">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item d-flex justify-content-between border-0 pb-0">
                                            <div class="col-6">
                                                <span class="invoice-subtotal-title">Total</span>
                                            </div>
                                            <div class="col-6">
                                                <input data-inputmask="
                                                    'alias': 'currency', 
                                                    'prefix': 'Rp ',
                                                    'placeholder': 'Rp 0',
                                                    'rightAlign': 'true',
                                                    'autoUnmask': 'true',
                                                    'allowMinus': 'false',
                                                    'removeMaskOnSubmit': 'true',
                                                    'digits': '0'" 
                                                    class="form-control" name="total" id="invoice-total" value="{{ old('total', $salesOrder->total) }}" readonly>
                                            </div>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between border-0 pb-0">
                                            <div class="col-6">
                                                <span class="invoice-subtotal-title">Nominal Diskon</span>
                                            </div>
                                            <div class="col-6">
                                                <input data-inputmask="
                                                    'alias': 'currency', 
                                                    'prefix': 'Rp ',
                                                    'placeholder': 'Rp 0',
                                                    'rightAlign': 'true',
                                                    'autoUnmask': 'true',
                                                    'allowMinus': 'false',
                                                    'removeMaskOnSubmit': 'true',
                                                    'digits': '0'" 
                                                    aria-describedby="diskonHelp" class="form-control @error('diskon') is-invalid @enderror" name="diskon" id="invoice-diskon" value="{{ old('diskon', $salesOrder->diskon) }}" @if ($salesOrder->status_nota != 'draft') readonly @endif>

                                                @error('diskon')
                                                    <small id="diskonHelp" class="form-text text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </li>
                                        <li class="list-group-item py-0 border-0 mt-25">
                                            <hr>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between border-0 py-0">
                                            <div class="col-6">
                                                <span class="invoice-subtotal-title">Grand Total</span>
                                            </div>
                                            <div class="col-6">
                                                <input data-inputmask="
                                                    'alias': 'currency', 
                                                    'prefix': 'Rp ',
                                                    'placeholder': 'Rp 0',
                                                    'rightAlign': 'true',
                                                    'autoUnmask': 'true',
                                                    'allowMinus': 'false',
                                                    'removeMaskOnSubmit': 'true',
                                                    'digits': '0'" 
                                                    class="form-control" name="grand_total" id="invoice-grand-total" value="{{ old('grand_total', $salesOrder->grand_total) }}" readonly>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">                
                        <div class="row ">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-warning">Ubah Data</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js" 
    integrity="sha512-6Jym48dWwVjfmvB0Hu3/4jn4TODd6uvkxdi9GNbBHwZ4nGcRxJUCaTkL3pVY6XUQABqFo3T58EMXFQztbjvAFQ==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/bindings/inputmask.binding.js" 
    integrity="sha512-J6WEJE0No+5Qqm9/T93q88yRQjvoAioXG4gzJ+eqZtLi+ZBgimZDkTiLWiljwrwnoQw+xwECQm282RJ6CrJnlw==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/pages/app-invoice.js') }}"></script>
<script src="{{ asset('app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    $( document ).ready(function() {
        //Trigger New Produk Row
        let row_number = {{ count($salesOrder->produks) }};

        $("#tambah-produk").on("click", function(e) {
            e.preventDefault();

            $('#produk-table').append('<tr class="invoice-item-filed" id="produk' + (row_number) + '"></tr>');

            $('#produk' + row_number).html($('#produk' + (row_number - 1)).html()).find('td:first-child');

            $("#produk" + row_number + " .invoice-item-price").inputmask({
                alias: 'currency',
                prefix: 'Rp ',
                placeholder: 'Rp 0',
                rightAlign: true,
                autoUnmask: true,
                allowMinus: false,
                removeMaskOnSubmit: true,
                digits: '0',
            });

            $("#produk" + row_number + " .invoice-item-qty").inputmask({
                alias: 'numeric',
                groupSeparator: ',',
                rightAlign: true,
                autoUnmask: true,
                allowMinus: false,
                removeMaskOnSubmit: true,
                digits: '0',
                min: '1', 
            });

            $("#produk" + row_number + " .invoice-item-sub-total").inputmask({
                alias: 'currency',
                prefix: 'Rp ',
                placeholder: 'Rp 0',
                rightAlign: true,
                autoUnmask: true,
                allowMinus: false,
                removeMaskOnSubmit: true,
                digits: '0',
            });
        
            $(".select2-container").remove();

            $(".select2").select2({
                placeholder: "Pilih Data",
                dropdownAutoWidth: true,
                width: '100%',
            });
            
            $("#produk" + row_number + " .text-danger").hide();

            $("#produk" + row_number + " .invoice-item-qty").removeClass("is-invalid");

            $("#produk" + row_number + " .invoice-item-price").val(0);

            $("#produk" + row_number + " .invoice-item-qty").val(0);

            $("#produk" + row_number + " .invoice-item-sub-total").val(0);

            $("#produk" + row_number + " .select2").val(null).trigger('change.select2');
            
            row_number++;
        });
    
        //Remove Last Produk Row
        $(document).on('click', '.hapus-produk', function () {
            $(this).parents('tr').remove();

            row_number--;

            calculateTotal();
        });
    
        // on product change also change size, color, price
        $(document).on("change", ".invoice-item-select", function (e) {
            var harga = this.options[e.target.selectedIndex].getAttribute('data-harga');

            $(e.target)
                .closest(".invoice-item-filed")
                .find(".invoice-item-price")
                .val(harga);
        });
    
        //on change qty, count sub total
        $(document).on("keyup", ".invoice-item-qty", function (e) {
            var harga = $(e.target).closest(".invoice-item-filed").find(".invoice-item-price").val();

            var qty = $(e.target).closest(".invoice-item-filed").find(".invoice-item-qty").val();

            var result = harga * qty;
        
            $(e.target)
                .closest(".invoice-item-filed")
                .find(".invoice-item-sub-total")
                .val(result);
        
            calculateTotal()
        });
        
        //function to count total orders
        function calculateTotal() {
            var list_sub_total = document.getElementsByClassName("invoice-item-sub-total");

            var total = 0;
        
            for (var i = 0; i < list_sub_total.length; i++) {
                total += parseInt(list_sub_total[i].value);
            }

            $("#invoice-total").val(total);
        
            calculateGrandTotal();
        }
    
        //calculate grand total
        function calculateGrandTotal() {
            var total = document.getElementById("invoice-total").value;

            var diskon = document.getElementById("invoice-diskon").value;
        
            $("#invoice-grand-total").val(total - diskon);
        }
        
        //on change diskon, recount grand total
        $(document).on("keyup", "#invoice-diskon", function() {
            if (parseInt($(this).val()) > parseInt(document.getElementById("invoice-total").value)) 
            {
                $(this).val({{ $salesOrder->diskon }});

                Swal.fire({
                    title: 'Error!',
                    text: "Diskon tidak dapat melebihi nominal pembelian!",
                    icon: 'error',
                    confirmButtonText: 'Tutup'
                });
            }
            
            calculateGrandTotal();
        });
    
        //populate customer field based on selected option
        $(document).on("change", "#pelanggan_id", function() {
            var alamat = $(this).find('option:selected').data('alamat');

            var kota = $(this).find('option:selected').data('kota');
        
            $('#alamat_pelanggan').val(alamat + ' ' + kota);
        });
    });
</script>
@endsection