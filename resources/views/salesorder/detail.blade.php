@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-invoice.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Detail Sales Order</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Sales Order
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section class="invoice-view-wrapper">
    <div class="row">
        <!-- invoice view page -->
        <div class="col-xl-10 col-md-12 col-12">
            <div class="card invoice-print-area">
                <div class="card-body pb-0 mx-25">
                    <!-- header section -->
                    <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <span class="invoice-number mr-50">{{ $salesOrder->no_nota }}</span>
                            @switch($salesOrder->status_nota)
                                @case('confirmed')
                                    <span class="badge badge-light-primary badge-pill">{{ $salesOrder->status_nota }}</span>
                                    @break
                                @case('cancel')
                                    <span class="badge badge-light-danger badge-pill">{{ $salesOrder->status_nota }}</span>
                                    @break
                                @case('done')
                                    <span class="badge badge-light-success badge-pill">{{ $salesOrder->status_nota }}</span>
                                    @break
                                @default
                                    <span class="badge badge-light-info badge-pill">{{ $salesOrder->status_nota }}</span>
                            @endswitch
                            @switch($salesOrder->status_pembayaran)
                                @case('partially paid')
                                    <span class="badge badge-light-info badge-pill">{{ $salesOrder->status_pembayaran }}</span>
                                    @break
                                @case('paid')
                                    <span class="badge badge-light-success badge-pill">{{ $salesOrder->status_pembayaran }}</span>
                                    @break
                                @case('overdue')
                                    <span class="badge badge-light-warning badge-pill">{{ $salesOrder->status_pembayaran }}</span>
                                    @break
                                @case('cancel')
                                    <span class="badge badge-light-danger badge-pill">{{ $salesOrder->status_pembayaran }}</span>
                                    @break
                                @default
                                    <span class="badge badge-light-danger badge-pill">{{ $salesOrder->status_pembayaran }}</span>
                            @endswitch
                        </div>
                        <div class="col-lg-8 col-md-12">
                            <div class="d-flex align-items-center justify-content-lg-end flex-wrap">
                                <div class="mr-3">
                                    <small class="text-muted">Estimasi Kirim:</small>
                                    <span>@date($salesOrder->tanggal_kirim)</span>
                                </div>
                                <div>
                                    <small class="text-muted">Estimasi Jth Tempo:</small>
                                    <span>@date($salesOrder->tanggal_jatuh_tempo)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <!-- invoice address and contact -->
                    <div class="row invoice-info">
                        <div class="col-sm-6 col-12 mt-1">
                            <h6 class="invoice-from">Data Pelanggan</h6>
                            <div class="mb-1">
                                <span>{{ $salesOrder->pelanggan->nama_pelanggan }}</span>
                            </div>
                            <div class="mb-1">
                                <span>{{ $salesOrder->pelanggan->alamat_pelanggan }}, {{ $salesOrder->pelanggan->kota_pelanggan }}</span>
                            </div>
                            <div class="mb-1">
                                <span>{{ $salesOrder->pelanggan->no_telepon }}</span>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <!-- product details table-->
                <div class="invoice-product-details table-responsive">
                    <table class="table table-borderless mb-0">
                        <thead>
                            <tr class="border-0">
                                <th scope="col">Produk</th>
                                <th scope="col">Ukuran</th>
                                <th scope="col">Warna</th>
                                <th scope="col" class="text-right">Qty</th>
                                <th scope="col">Sat</th>
                                <th scope="col" class="text-right">Harga</th>
                                <th scope="col" class="text-right">Sub Total</th>
                                <th scope="col">Status
                                    <i class="bx bx-info-circle" 
                                        data-toggle="popover" data-placement="right" 
                                        data-container="body" data-original-title="Status Nota" 
                                        data-content=
                                            "<small><strong>Pending:</strong> Produk belum diproses.</small> <br />
                                            <small><strong>Process:</strong> Produk sedang diproduksi.</small> <br />
                                            <small><strong>Done:</strong> Produk telah selesai diproduksi.</small> <br />
                                            <small><strong>Cancel:</strong> Produk telah dibatalkan.</small> <br />
                                            <small><strong>Partially Shipped:</strong> Sebagian produk telah dikirim.</small> <br />
                                            <small><strong>Shipped:</strong> Seluruh produk telah dikirim.</small> <br />"
                                        data-html="true" data-trigger="hover">
                                    </i>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($salesOrder->produks as $detail)
                                <tr>
                                    <td>
                                        <small class="text-muted">{{ $detail->nama_produk }}</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->ukuran_produk }}</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->warna_produk }}</small>
                                    </td>
                                    <td class="text-right">
                                        <small class="text-muted">@number($detail->pivot->kuantitas)</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->satuan_produk }}</small>
                                    </td>
                                    <td class="text-right">
                                        <small class="text-muted">@money($detail->pivot->harga)</small>
                                    </td>
                                    <td class="text-right">
                                        <small class="text-muted">@money($detail->pivot->sub_total)</small>
                                    </td>
                                    <td>
                                        @switch($detail->pivot->status_order)
                                            @case('process')
                                                <span class="badge badge-light-primary badge-pill">{{ $detail->pivot->status_order }}</span>
                                                @break
                                            @case('cancel')
                                                <span class="badge badge-light-danger badge-pill">{{ $detail->pivot->status_order }}</span>
                                                @break
                                            @case('done')
                                                <span class="badge badge-light-success badge-pill">{{ $detail->pivot->status_order }}</span>
                                                @break
                                            @default
                                                <span class="badge badge-light-info badge-pill">{{ $detail->pivot->status_order }}</span>
                                        @endswitch
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <!-- invoice subtotal -->
                <div class="card-body pt-0 mx-25">
                    <hr>
                    <div class="row">
                        <div class="col-4 col-sm-6 col-12 mt-75">
                            <p>{{ $salesOrder->keterangan }}</p>
                        </div>
                        <div class="col-8 col-sm-6 col-12 d-flex justify-content-end mt-75">
                            <div class="invoice-subtotal">
                                <div class="invoice-calc d-flex justify-content-between">
                                    <span class="invoice-title">Total</span>
                                    <span class="invoice-value">@money($salesOrder->total)</span>
                                </div>
                                <div class="invoice-calc d-flex justify-content-between">
                                    <span class="invoice-title">Diskon</span>
                                    <span class="invoice-value">- @money($salesOrder->diskon)</span>
                                </div>
                                <hr>
                                <div class="invoice-calc d-flex justify-content-between">
                                    <span class="invoice-title">Grand Total</span>
                                    <span class="invoice-value">@money($salesOrder->grand_total)</span>
                                </div>
                                @forelse ($salesOrder->notaPembayarans as $notaPembayaran)
                                    <hr>
                                    <div class="invoice-calc d-flex justify-content-between">
                                        <button type="button" class="btn btn-icon rounded-circle btn-outline mr-1" 
                                            data-toggle="popover" data-placement="left" 
                                            data-container="body" data-original-title="Info Pembayaran" 
                                            data-content=
                                                "<b>Nominal:</b> @money($notaPembayaran->nominal_bayar) <br />
                                                <b>Keterangan:</b> {{ $notaPembayaran->keterangan }} <br />
                                                <b>Tanggal:</b> @date($notaPembayaran->created_at) <br />
                                                <b>Cara Bayar:</b> {{ $notaPembayaran->cara_bayar }}"
                                            data-html="true" data-trigger="hover">
                                            <i class="bx bx-info-circle"></i>
                                        </button>
                                        <span class="invoice-title"><i>Dibayar pada @date($notaPembayaran->created_at)</i></span>
                                        <span class="invoice-value">@money($notaPembayaran->nominal_bayar)</span>
                                    </div>
                                @empty
                                    
                                @endforelse                                
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="card-footer">
                    @if ($salesOrder->status_nota == 'draft')
                        <div class="row">
                            <div class="col-6">
                                @can('salesOrder edit')
                                    <a href="{{ route('salesOrders.edit', $salesOrder) }}" class="btn btn-warning">Ubah Data</a>
                                @endcan
                            </div>

                            <div class="col-6 d-flex justify-content-end">
                                @can('salesOrder confirm')
                                    <a href="#ConfirmModal" class="btn btn-success" data-toggle="modal">Konfirmasi Pemesanan</a>
                                @endcan
                            </div>
    
                            {{-- BEGIN CONFIRM MODAL --}}
                            <div class="modal fade text-left" id="ConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header bg-success">
                                            <h5 class="modal-title white" id="myModalLabel160">Konfirmasi Pemesanan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i class="bx bx-x"></i>
                                            </button>
                                        </div>
                                        <form action="{{ route('salesOrders.confirm', $salesOrder) }}" method="post">
                                            @csrf
                                            @method('patch')
    
                                            <div class="modal-body">
                                                <p>Apakah anda yakin untuk mengkonfirmasi pesanan ini?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-light-danger" data-dismiss="modal">
                                                    <i class="bx bx-x d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Batal</span>
                                                </button>
                                                <button type="submit" class="btn btn-success ml-1">
                                                    <i class="bx bx-check d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Konfirmasi Pemesanan</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            {{-- END CONFIRM MODAL --}}
                        </div>

                        <div class="row mt-2">
                            <div class="col-6">
                                @can('salesOrder delete')
                                    <a href="#HapusModal" class="btn btn-danger" data-toggle="modal">Hapus Data</a>
                                @endcan
                            </div>
    
                            {{-- BEGIN HAPUS MODAL --}}
                            <div class="modal fade text-left" id="HapusModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header bg-danger">
                                            <h5 class="modal-title white" id="myModalLabel160">Hapus Sales Order</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i class="bx bx-x"></i>
                                            </button>
                                        </div>
                                        <form action="{{ route('salesOrders.destroy', $salesOrder) }}" method="post">
                                            @csrf
                                            @method('delete')
    
                                            <div class="modal-body">
                                                <p>Apakah anda yakin untuk menghapus sales order ini?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-light-danger" data-dismiss="modal">
                                                    <i class="bx bx-x d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Batal</span>
                                                </button>
                                                <button type="submit" class="btn btn-danger ml-1">
                                                    <i class="bx bx-check d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Hapus <Datag></Datag></span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            {{-- END HAPUS MODAL --}}
                        </div>
                    @endif

                    @if ($salesOrder->status_nota == 'confirmed')
                        <div class="row">
                            <div class="col-6">
                                @can('salesOrder edit')
                                    <a href="{{ route('salesOrders.edit', $salesOrder) }}" class="btn btn-warning">Ubah Data</a>
                                @endcan
                            </div>

                            <div class="col-6 d-flex justify-content-end">
                                @can('batalOrder create')
                                    <a href="#BatalModal" class="btn btn-danger" data-toggle="modal">Batalkan Pemesanan</a>
                                @endcan
                            </div>
    
                            {{-- BEGIN BATAL MODAL --}}
                            <div class="modal fade text-left" id="BatalModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header bg-danger">
                                            <h5 class="modal-title white" id="myModalLabel160">Pembatalan Order</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i class="bx bx-x"></i>
                                            </button>
                                        </div>
                                        <form action="{{ route('batalOrders.store', $salesOrder) }}" method="post">
                                            @csrf
    
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label>Nomor Nota Sales Order</label>
                                                    <input type="text" value="{{ $salesOrder->no_nota }}" class="form-control" readonly>
                                                </div>
                                                <div class="form-group mb-0">
                                                    <label>Keterangan (Opsional)</label>
                                                    <textarea name="keterangan" class="form-control" cols="30" rows="5" placeholder="Keterangan (Opsional)"></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-light-danger" data-dismiss="modal">
                                                    <i class="bx bx-x d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Batal</span>
                                                </button>
                                                <button type="submit" class="btn btn-danger ml-1">
                                                    <i class="bx bx-check d-block d-sm-none"></i>
                                                    <span class="d-none d-sm-block">Konfirmasi</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            {{-- END BATAL MODAL --}}
                        </div>

                        @if ($salesOrder->status_pembayaran != 'paid')
                            <div class="row mt-2">
                                <div class="col-6">
                                    @can('notaPembayaran create')
                                        <a href="#PembayaranModal" class="btn btn-secondary" data-toggle="modal">Buat Pembayaran</a>
                                    @endcan
                                </div>
                            </div>
                        @endif
                    @endif

                    @if ($salesOrder->status_nota == 'on going')
                        <div class="row">
                            <div class="col-6">
                                @can('salesOrder edit')
                                    <a href="{{ route('salesOrders.edit', $salesOrder) }}" class="btn btn-warning">Ubah Data</a>
                                @endcan
                            </div>

                            @if ($salesOrder->status_pembayaran != 'paid')
                                <div class="col-6 d-flex justify-content-end">
                                    @can('notaPembayaran create')
                                        <a href="#PembayaranModal" class="btn btn-secondary" data-toggle="modal">Buat Pembayaran</a>
                                    @endcan
                                </div>
                            @endif                            
                        </div>
                    @endif

                    @if ($salesOrder->status_nota == 'done')
                        <div class="row">
                            @if ($salesOrder->un_shipped_product)
                                <div class="col-6">
                                    @can('suratJalan create')
                                        <a href="{{ route('suratJalans.create', $salesOrder) }}" class="btn btn-info">Buat Surat Jalan</a>
                                    @endcan
                                </div>
                            @endif

                            <div class="col-6 @if ($salesOrder->un_shipped_product) d-flex justify-content-end @endif">
                                @can('notaPembayaran create')
                                    @if ($salesOrder->status_pembayaran != 'paid')
                                        <a href="#PembayaranModal" class="btn btn-secondary" data-toggle="modal">Buat Pembayaran</a>
                                    @endif
                                @endcan
                            </div>
                        </div>
                    @endif
    
                    {{-- BEGIN PEMBAYARAN MODAL --}}
                    <div class="modal fade text-left" id="PembayaranModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header bg-secondary">
                                    <h5 class="modal-title white" id="myModalLabel160">Pembayaran Order Pelanggan</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i class="bx bx-x"></i>
                                    </button>
                                </div>
                                <form action="{{ route('notaPembayarans.store', $salesOrder) }}" method="post">
                                    @csrf

                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label>Nomor Nota</label>
                                            <input type="text" class="form-control" value="{{ $salesOrder->no_nota }}" readonly>
                                        </div>
                                        <div class="form-group mb-0">
                                            <label>Nominal Pembelian</label>
                                            <input data-inputmask="
                                                'alias': 'currency', 
                                                'prefix': 'Rp ',
                                                'placeholder': 'Rp 0',
                                                'rightAlign': 'true',
                                                'autoUnmask': 'true',
                                                'allowMinus': 'false',
                                                'removeMaskOnSubmit': 'true',
                                                'digits': '0'" 
                                                class="form-control" value="{{ $salesOrder->grand_total }}" readonly>
                                        </div>
                                        <div class="form-group mb-0">
                                            <label>Sisa Tagihan</label>
                                            <input data-inputmask="
                                                'alias': 'currency', 
                                                'prefix': 'Rp ',
                                                'placeholder': 'Rp 0',
                                                'rightAlign': 'true',
                                                'autoUnmask': 'true',
                                                'allowMinus': 'false',
                                                'removeMaskOnSubmit': 'true',
                                                'digits': '0'" 
                                                class="form-control" value="{{ $salesOrder->grand_total - $salesOrder->nominal_bayar }}" readonly>
                                        </div>
                                        <div class="form-group mb-0">
                                            <label>Cara Pembayaran</label>
                                            <select aria-describedby="caraBayarHelp" class="form-control @error('cara_bayar', 'store-nota-pembayaran') is-invalid @enderror" name="cara_bayar">
                                                <option hidden>Pilih Cara Pembayaran</option>
                                                <option value="Tunai" {{ old('cara_bayar') == 'Tunai' ? 'selected' : '' }}>Tunai</option>
                                                <option value="Transfer" {{ old('cara_bayar') == 'Transfer' ? 'selected' : '' }}>Transfer</option>
                                            </select>

                                            @error('cara_bayar', 'store-nota-pembayaran')
                                                <small id="caraBayarHelp" class="form-text text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group mb-0">
                                            <label>Nominal Pembayaran</label>
                                            <input data-inputmask="
                                                'alias': 'currency', 
                                                'prefix': 'Rp ',
                                                'rightAlign': 'true',
                                                'autoUnmask': 'true',
                                                'allowMinus': 'false',
                                                'removeMaskOnSubmit': 'true',
                                                'digits': '0', 
                                                'max': {{ $salesOrder->grand_total - $salesOrder->nominal_bayar }}" 
                                                aria-describedby="nominalBayarHelp" class="form-control @error('cara_bayar_id', 'store-nota-pembayaran') is-invalid @enderror" name="nominal_bayar" value="{{ old('nominal_bayar') }}" placeholder="Rp 0">

                                            @error('nominal_bayar', 'store-nota-pembayaran')
                                                <small id="nominalBayarHelp" class="form-text text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group mb-0">
                                            <label>Keterangan (Opsional)</label>
                                            <textarea class="form-control" name="keterangan" cols="30" rows="5" placeholder="Keterangan (Opsional)">{{ old('keterangan') }}</textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light-danger" data-dismiss="modal">
                                            <i class="bx bx-x d-block d-sm-none"></i>
                                            <span class="d-none d-sm-block">Batal</span>
                                        </button>
                                        <button type="submit" class="btn btn-secondary ml-1">
                                            <i class="bx bx-check d-block d-sm-none"></i>
                                            <span class="d-none d-sm-block">Buat Pembayaran</span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {{-- END PEMBAYARAN MODAL --}}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js" 
    integrity="sha512-6Jym48dWwVjfmvB0Hu3/4jn4TODd6uvkxdi9GNbBHwZ4nGcRxJUCaTkL3pVY6XUQABqFo3T58EMXFQztbjvAFQ==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/bindings/inputmask.binding.js" 
    integrity="sha512-J6WEJE0No+5Qqm9/T93q88yRQjvoAioXG4gzJ+eqZtLi+ZBgimZDkTiLWiljwrwnoQw+xwECQm282RJ6CrJnlw==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/popover/popover.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if (Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif

    @if (Session::has('error'))
        toastr['error']("{{ session('error') }}", {
            tapToDismiss: true,
        });
    @endif

    @if ($errors->hasBag('store-nota-pembayaran'))
        $("#PembayaranModal").modal("show");
    @endif
</script>
@endsection