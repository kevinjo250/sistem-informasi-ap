@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Ubah Realisasi Produksi</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Realisasi Produksi
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-vertical-layouts">
    <div class="row match-height">
        <div class="col-xl-10 col-md-12 col-sm-12 col-12">
            <div class="card">
                <form class="form form-vertical" action="{{ route('realisasiProduksis.update', $realisasiProduksi) }}" method="post">
                    @csrf
                    @method('patch')
                    
                    <div class="card-body pb-0 mx-25">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Nomor Surat Perintah Kerja</label>
                                    <input type="text" class="form-control" name="surat_perintah_kerja_id" value="{{ old('surat_perintah_kerja_id', $realisasiProduksi->suratPerintahKerja->no_nota) }}" readonly>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Produk</label>
                                    <input type="text" class="form-control" name="produk" value="{{ old('produk', $realisasiProduksi->suratPerintahKerja->produkSalesOrder->produk->nama_produk) }}" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Ukuran</label>
                                    <input type="text" class="form-control" name="ukuran" value="{{ old('ukuran', $realisasiProduksi->suratPerintahKerja->produkSalesOrder->produk->ukuran_produk) }}" readonly>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Warna</label>
                                    <input type="text" class="form-control" name="warna" value="{{ old('warna', $realisasiProduksi->suratPerintahKerja->produkSalesOrder->produk->warna_produk) }}" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Tanggal Mulai Produksi</label>
                                    <input type="datetime-local" aria-describedby="tglMulaiHelp" class="form-control @error('tanggal_mulai') is-invalid @enderror" name="tanggal_mulai" value="{{ old('tanggal_mulai', date('Y-m-d\TH:i:s', strtotime($realisasiProduksi->tanggal_mulai))) }}" step="1">
    
                                    @error('tanggal_mulai')
                                        <small id="tglMulaiHelp" class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Tanggal Selesai Produksi</label>
                                    <input type="datetime-local" aria-describedby="tglSelesaiHelp" class="form-control @error('tanggal_selesai') is-invalid @enderror" name="tanggal_selesai" value="{{ old('tanggal_selesai', date('Y-m-d\TH:i:s', strtotime($realisasiProduksi->tanggal_selesai))) }}" step="1">
    
                                    @error('tanggal_selesai')
                                        <small id="tglSelesaiHelp" class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Kuantitas Selesai Produksi</label>
                                    <input data-inputmask="
                                        'alias': 'numeric',
                                        'groupSeparator': ',',
                                        'rightAlign': 'true',
                                        'autoUnmask': 'true',
                                        'allowMinus': 'false',
                                        'removeMaskOnSubmit': 'true',
                                        'digits': '0',
                                        'min': '0',
                                        'max': '{{ $realisasiProduksi->suratPerintahKerja->kuantitas - $realisasiProduksi->suratPerintahKerja->realisasiProduksis->sum('kuantitas_selesai') + $realisasiProduksi->kuantitas_selesai }}'" 
                                        aria-describedby="kuantitasSelesaiHelp" class="form-control kuantitas @error('kuantitas_selesai') is-invalid @enderror" name="kuantitas_selesai" value="{{ old('kuantitas_selesai', $realisasiProduksi->kuantitas_selesai) }}">
    
                                    @error('kuantitas_selesai')
                                        <small id="kuantitasSelesaiHelp" class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div><div class="col-6">
                                <div class="form-group">
                                    <label>Realisasi Biaya Material</label>
                                    <input data-inputmask="
                                        'alias': 'currency', 
                                        'prefix': 'Rp ',
                                        'placeholder': 'Rp 1',
                                        'rightAlign': 'true',
                                        'autoUnmask': 'true',
                                        'allowMinus': 'false',
                                        'removeMaskOnSubmit': 'true',
                                        'digits': '0',
                                        'min': '1'" 
                                        aria-describedby="biayaMaterialHelp" class="form-control realisasi-biaya-material @error('biaya_material') is-invalid @enderror" name="biaya_material" value="{{ old('biaya_material', $realisasiProduksi->biaya_material) }}" readonly>
    
                                    @error('biaya_material')
                                        <small id="biayaMaterialHelp" class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Realisasi Biaya Pekerja</label>
                                    <input data-inputmask="
                                        'alias': 'currency', 
                                        'prefix': 'Rp ',
                                        'placeholder': 'Rp 1',
                                        'rightAlign': 'true',
                                        'autoUnmask': 'true',
                                        'allowMinus': 'false',
                                        'removeMaskOnSubmit': 'true',
                                        'digits': '0',
                                        'min': '1'" 
                                        aria-describedby="biayaPekerjaHelp" class="form-control realisasi-biaya-pekerja @error('biaya_pekerja') is-invalid @enderror" name="biaya_pekerja" value="{{ old('biaya_pekerja', $realisasiProduksi->biaya_pekerja) }}">
    
                                    @error('biaya_pekerja')
                                        <small id="biayaPekerjaHelp" class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Realisasi Biaya Overhead Pabrik</label>
                                    <input data-inputmask="
                                        'alias': 'currency', 
                                        'prefix': 'Rp ',
                                        'placeholder': 'Rp 1',
                                        'rightAlign': 'true',
                                        'autoUnmask': 'true',
                                        'allowMinus': 'false',
                                        'removeMaskOnSubmit': 'true',
                                        'digits': '0',
                                        'min': '1'" 
                                        aria-describedby="biayaOverheadHelp" class="form-control realisasi-biaya-overhead @error('biaya_overhead') is-invalid @enderror" name="biaya_overhead" value="{{ old('biaya_overhead', $realisasiProduksi->biaya_overhead) }}">
    
                                    @error('biaya_overhead')
                                        <small id="biayaOverheadHelp" class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-50">
                        <div class="table-responsive">
                            <table class="table mb-0" id="material-table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Material</th>
                                        <th>Qty Rusak</th>
                                        <th>Qty Sisa</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($realisasiProduksi->produks as $detail)
                                        <tr id="material{{ $loop->index }}">
                                            <td>
                                                <select aria-describedby="produkHelp" class="select2 form-control @error('produk_id.' . $loop->index) is-invalid @enderror" name="produk_id[]">
                                                    <option></option>
                                                    @foreach ($materials as $material)
                                                        <option value="{{ $material->id }}" 
                                                            {{ $detail->pivot->produk_id == $material->id ? 'selected' : '' }}>
                                                            {{ $material->nama_produk }} - {{ $material->ukuran_produk }} - {{ $material->warna_produk }}
                                                        </option>
                                                    @endforeach
                                                </select>

                                                @error('produk_id.' . $loop->index)
                                                    <small id="produkHelp" class="form-text text-danger">{{ $message }}</small>
                                                @enderror
                                            </td>
                                            <td>
                                                <input data-inputmask="
                                                    'alias': 'numeric',
                                                    'groupSeparator': ',',
                                                    'rightAlign': 'true',
                                                    'autoUnmask': 'true',
                                                    'allowMinus': 'false',
                                                    'removeMaskOnSubmit': 'true',
                                                    'digits': '0',
                                                    'min': '0'" 
                                                    aria-describedby="kuantitasRusakHelp" class="form-control invoice-item-qty-rusak @error('kuantitas_rusak.' . $loop->index) is-invalid @enderror" name="kuantitas_rusak[]" value="{{ old('kuantitas_rusak.' . $loop->index, $detail->pivot->kuantitas_rusak) }}">

                                                @error('kuantitas_rusak.' . $loop->index)
                                                    <small id="kuantitasRusakHelp" class="form-text text-danger">{{ $message }}</small>
                                                @enderror
                                            </td>
                                            <td>
                                                <input data-inputmask="
                                                    'alias': 'numeric',
                                                    'groupSeparator': ',',
                                                    'rightAlign': 'true',
                                                    'autoUnmask': 'true',
                                                    'allowMinus': 'false',
                                                    'removeMaskOnSubmit': 'true',
                                                    'digits': '0',
                                                    'min': '0'" 
                                                    aria-describedby="kuantitasSisaHelp" class="form-control invoice-item-qty-sisa @error('kuantitas_sisa.' . $loop->index) is-invalid @enderror" name="kuantitas_sisa[]" value="{{ old('kuantitas_sisa.' . $loop->index, $detail->pivot->kuantitas_sisa) }}">

                                                @error('kuantitas_sisa.' . $loop->index)
                                                    <small id="kuantitasSisaHelp" class="form-text text-danger">{{ $message }}</small>
                                                @enderror
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-icon btn-danger text-nowrap px-1 hapus-material">
                                                    <i class="bx bx-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @empty
                                        @foreach (old('produk_id', ['']) as $detail)
                                            <tr id="material{{ $loop->index }}">
                                                <td>
                                                    <select aria-describedby="produkHelp" class="select2 form-control @error('produk_id.' . $loop->index) is-invalid @enderror" name="produk_id[]">
                                                        <option></option>
                                                        @foreach ($materials as $material)
                                                            <option value="{{ $material->id }}" 
                                                                {{ old('produk_id.' . $loop->parent->index) == $material->id ? 'selected' : '' }}>
                                                                {{ $material->nama_produk }} - {{ $material->ukuran_produk }} - {{ $material->warna_produk }}
                                                            </option>
                                                        @endforeach
                                                    </select>

                                                    @error('produk_id.' . $loop->index)
                                                        <small id="produkHelp" class="form-text text-danger">{{ $message }}</small>
                                                    @enderror
                                                </td>
                                                <td>
                                                    <input data-inputmask="
                                                        'alias': 'numeric',
                                                        'groupSeparator': ',',
                                                        'rightAlign': 'true',
                                                        'autoUnmask': 'true',
                                                        'allowMinus': 'false',
                                                        'removeMaskOnSubmit': 'true',
                                                        'digits': '0',
                                                        'min': '0'" 
                                                        aria-describedby="kuantitasRusakHelp" class="form-control invoice-item-qty-rusak @error('kuantitas_rusak.' . $loop->index) is-invalid @enderror" name="kuantitas_rusak[]" value="{{ old('kuantitas_rusak.' . $loop->index, 0) }}">

                                                    @error('kuantitas_rusak.' . $loop->index)
                                                        <small id="kuantitasRusakHelp" class="form-text text-danger">{{ $message }}</small>
                                                    @enderror
                                                </td>
                                                <td>
                                                    <input data-inputmask="
                                                        'alias': 'numeric',
                                                        'groupSeparator': ',',
                                                        'rightAlign': 'true',
                                                        'autoUnmask': 'true',
                                                        'allowMinus': 'false',
                                                        'removeMaskOnSubmit': 'true',
                                                        'digits': '0',
                                                        'min': '0'" 
                                                        aria-describedby="kuantitasSisaHelp" class="form-control invoice-item-qty-sisa @error('kuantitas_sisa.' . $loop->index) is-invalid @enderror" name="kuantitas_sisa[]" value="{{ old('kuantitas_sisa.' . $loop->index, 0) }}">

                                                    @error('kuantitas_sisa.' . $loop->index)
                                                        <small id="kuantitasSisaHelp" class="form-text text-danger">{{ $message }}</small>
                                                    @enderror
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-icon btn-danger text-nowrap px-1 hapus-material">
                                                        <i class="bx bx-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="col p-0">
                            <button type="button" class="btn btn-info" id="tambah-material">
                                Tambah Material
                            </button>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row ">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Simpan Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js" 
    integrity="sha512-6Jym48dWwVjfmvB0Hu3/4jn4TODd6uvkxdi9GNbBHwZ4nGcRxJUCaTkL3pVY6XUQABqFo3T58EMXFQztbjvAFQ==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/bindings/inputmask.binding.js" 
    integrity="sha512-J6WEJE0No+5Qqm9/T93q88yRQjvoAioXG4gzJ+eqZtLi+ZBgimZDkTiLWiljwrwnoQw+xwECQm282RJ6CrJnlw==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    $( document ).ready(function() {

        let biayaMaterial = {{ $realisasiProduksi->suratPerintahKerja->produkSalesOrder->produk->billOfMaterial->perkiraanHpp->biaya_material }};

        let biayaPekerja = {{ $realisasiProduksi->suratPerintahKerja->produkSalesOrder->produk->billOfMaterial->perkiraanHpp->biaya_pekerja }};

        let biayaOverhead = {{ $realisasiProduksi->suratPerintahKerja->produkSalesOrder->produk->billOfMaterial->perkiraanHpp->biaya_overhead }};

        $(".kuantitas").on("keyup", function(e) {
            let qtyRealisasi = parseInt($(this).val());

            $(".realisasi-biaya-material").val(qtyRealisasi * biayaMaterial);

            $(".realisasi-biaya-pekerja").val(qtyRealisasi * biayaPekerja);

            $(".realisasi-biaya-overhead").val(qtyRealisasi * biayaOverhead);
        });

        //Trigger New Material Row
        let row_number = {{ count($realisasiProduksi->produks) == 0 ? '1' : count($realisasiProduksi->produks) }};

        $("#tambah-material").on("click", function(e) {
            e.preventDefault();

            $('#material-table').append('<tr id="material' + (row_number) + '"></tr>');

            $('#material' + row_number).html($('#material' + (row_number - 1)).html()).find('td:first-child');            


            $("#material" + row_number + " .invoice-item-qty-rusak").inputmask({
                alias: 'numeric',
                groupSeparator: ',',
                rightAlign: true,
                autoUnmask: true,
                allowMinus: false,
                removeMaskOnSubmit: true,
                digits: '0',
                min: '0', 
            });

            $("#material" + row_number + " .invoice-item-qty-sisa").inputmask({
                alias: 'numeric',
                groupSeparator: ',',
                rightAlign: true,
                autoUnmask: true,
                allowMinus: false,
                removeMaskOnSubmit: true,
                digits: '0',
                min: '0', 
            });

            $(".select2-container").remove();

            $(".select2").select2({
                placeholder: "Pilih Data",
                dropdownAutoWidth: true,
                width: '100%'
            });

            $("#material" + row_number + " .invoice-item-qty-rusak").val(0);

            $("#material" + row_number + " .invoice-item-qty-sisa").val(0);

            $("#material" + row_number + " .select2").val(null).trigger('change.select2');

            row_number++;
        });

        //Remove Last Produk Row
        $(document).on('click', '.hapus-material', function () {
            $(this).parents('tr').remove();

            row_number--;
        });
    });
</script>
@endsection