@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-invoice.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Detail Realisasi Produksi</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Realisasi Produksi
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section class="invoice-view-wrapper">
    <div class="row">
        <div class="col-xl-9 col-md-12 col-12">
            <div class="card invoice-print-area">
                <div class="card-body pb-0 mx-25">
                    <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <span class="invoice-number mr-50">{{ $realisasiProduksi->no_nota }}</span>
                        </div>
                        <div class="col-lg-8 col-md-12">
                            <div class="d-flex align-items-center justify-content-lg-end flex-wrap">
                                <div>
                                    <small class="text-muted">Created At:</small>
                                    <span>@datetime($realisasiProduksi->created_at)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row invoice-info">
                        <div class="col-4 mt-1">
                            <h6 class="invoice-from">Data Produk</h6>
                            <div class="mb-1">
                                <span>Nama Produk: {{ $realisasiProduksi->suratPerintahKerja->produkSalesOrder->produk->nama_produk }}</span>
                            </div>
                            <div class="mb-1">
                                <span>Ukuran: {{ $realisasiProduksi->suratPerintahKerja->produkSalesOrder->produk->ukuran_produk }}</span>
                            </div>
                            <div class="mb-1">
                                <span>Warna: {{ $realisasiProduksi->suratPerintahKerja->produkSalesOrder->produk->warna_produk }}</span>
                            </div>
                        </div>
                        <div class="col-4 mt-1">
                            <h6 class="invoice-from">Data Surat Perintah Kerja</h6>
                            <div class="mb-1">
                                <span>No Surat Perintah Kerja: 
                                    <a href="{{ route('suratPerintahKerjas.show', $realisasiProduksi->surat_perintah_kerja_id) }}">{{ $realisasiProduksi->suratPerintahKerja->no_nota }}</a>
                                </span>
                            </div>
                            <div class="mb-1">
                                <span>Tanggal Rencana Produksi: @date($realisasiProduksi->suratPerintahKerja->tanggal_produksi)</span>
                            </div>
                        </div>
                        <div class="col-4 mt-1">
                            <h6 class="invoice-from">Data Realisasi Produksi</h6>
                            <div class="mb-1">
                                <span>Tanggal Mulai Produksi: @datetime($realisasiProduksi->tanggal_mulai)</span>
                            </div>
                            <div class="mb-1">
                                <span>Tanggal Selesai Produksi: @datetime($realisasiProduksi->tanggal_selesai)</span>
                            </div>
                            <div class="mb-1">
                                <span>Kuantitas Selesai Produksi: @number($realisasiProduksi->kuantitas_selesai)</span>
                            </div>
                            <div class="mb-1">
                                <span>Realisasi Biaya Material: @money($realisasiProduksi->biaya_material)</span>
                            </div>
                            <div class="mb-1">
                                <span>Realisasi Biaya Pekerja: @money($realisasiProduksi->biaya_pekerja)</span>
                            </div>
                            <div class="mb-1">
                                <span>Realisasi Biaya Overhead Produksi: @money($realisasiProduksi->biaya_overhead)</span>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="invoice-product-details table-responsive">
                    <table class="table table-borderless mb-0">
                        <thead>
                            <tr class="border-0">
                                <th scope="col">Material</th>
                                <th scope="col">Ukuran</th>
                                <th scope="col">Warna</th>
                                <th scope="col" class="text-right">Qty Rusak</th>
                                <th scope="col" class="text-right">Qty Sisa</th>
                                <th scope="col">Satuan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($realisasiProduksi->produks as $detail)
                                <tr>
                                    <td>
                                        <small class="text-muted">{{ $detail->nama_produk }}</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->ukuran_produk }}</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->warna_produk }}</small>
                                    </td>
                                    <td class="text-right">
                                        <small class="text-muted">@number($detail->pivot->kuantitas_rusak)</small>
                                    </td>
                                    <td class="text-right">
                                        <small class="text-muted">@number($detail->pivot->kuantitas_sisa)</small>
                                    </td>
                                    <td>
                                        <small class="text-muted">{{ $detail->satuan_produk }}</small>
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td colspan="6">No data available in table</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="row ">
                        <div class="col-6">
                            @can('realisasiProduksi edit')
                                <a href="{{ route('realisasiProduksis.edit', $realisasiProduksi) }}" class="btn btn-warning">Ubah Data</a>
                            @endcan
                        </div>

                        <div class="col-6 d-flex justify-content-end">
                            @can('realisasiProduksi delete')
                                <a href="#DeleteModal" data-toggle="modal" class="btn btn-danger">Hapus Data</a>
                            @endcan
                        </div>

                        {{-- BEGIN DELETE MODAL --}}
                        <div class="modal fade text-left" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h5 class="modal-title white" id="myModalLabel160">Hapus Data Realisasi Produksi</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="bx bx-x"></i>
                                        </button>
                                    </div>
                                    <form action="{{ route('realisasiProduksis.destroy', $realisasiProduksi) }}" method="post">
                                        @csrf
                                        @method('delete')

                                        <div class="modal-body">
                                            <p>Apakah anda yakin untuk menghapus data tersebut?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                                <i class="bx bx-x d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block">Batal</span>
                                            </button>
                                            <button type="submit" class="btn btn-danger ml-1">
                                                <i class="bx bx-check d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block">Hapus Data</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        {{-- END DELETE MODAL --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if (Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection