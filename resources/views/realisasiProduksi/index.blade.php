@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Realisasi Produksi</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Realisasi Produksi
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-datatable">
    @can('realisasiProduksi create')
        <div class="invoice-create-btn mb-1">
            <a href="{{ route('realisasiProduksis.create') }}" class="btn btn-primary btn-sm round glow align-items-center">Buat Realisasi Produksi</a>
        </div>
    @endcan
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body card-dashboard">
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nomor SPK</th>
                                    <th>Tgl Mulai</th>
                                    <th>Tgl Selesai</th>
                                    <th>Produk</th>
                                    <th>Ukuran</th>
                                    <th>Warna</th>
                                    <th>Qty</th>
                                    <th>Karyawan</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($realisasiProduksis as $realisasiProduksi)
                                    <tr>
                                        <td>{{ $loop->index +1 }}</td>
                                        <td>
                                            <a href="{{ route('suratPerintahKerjas.show', $realisasiProduksi->surat_perintah_kerja_id) }}">{{ $realisasiProduksi->suratPerintahKerja->no_nota }}</a>
                                        </td>
                                        <td>
                                            <small class="text-muted">@datetime($realisasiProduksi->tanggal_mulai)</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">@datetime($realisasiProduksi->tanggal_selesai)</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $realisasiProduksi->suratPerintahKerja->produkSalesOrder->produk->nama_produk }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $realisasiProduksi->suratPerintahKerja->produkSalesOrder->produk->ukuran_produk }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $realisasiProduksi->suratPerintahKerja->produkSalesOrder->produk->warna_produk }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">@number($realisasiProduksi->kuantitas_selesai)</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $realisasiProduksi->user->nama_karyawan }}</small>
                                        </td>
                                        <td>
                                            <div class="invoice-action">
                                                <a href="{{ route('realisasiProduksis.show', $realisasiProduksi) }}" class="invoice-action-view cursor-pointer">
                                                    <i class="bx bx-show-alt"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/datatables/datatable.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if(Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection