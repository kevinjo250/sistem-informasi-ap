@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Buat Realisasi Produksi</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Realisasi Produksi
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-vertical-layouts">
    <div class="row match-height">
        <div class="col-xl-10 col-md-12 col-sm-12 col-12">
            <div class="card">
                <form class="form form-vertical" action="{{ route('realisasiProduksis.store') }}" method="post">
                    @csrf
                    
                    <div class="card-body pb-0 mx-25">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Nomor Surat Perintah Kerja</label>
                                    <select aria-describedby="suratPerintahKerjaHelp" class="select2 form-control invoice-item-select @error('surat_perintah_kerja_id') is-invalid @enderror" name="surat_perintah_kerja_id">
                                        <option></option>
                                        @foreach ($suratPerintahKerjas as $suratPerintahKerja)
                                            <option value="{{ $suratPerintahKerja->id }}" {{ old('surat_perintah_kerja_id') == $suratPerintahKerja->id ? 'selected' : '' }} 
                                                data-produk="{{ $suratPerintahKerja->produkSalesOrder->produk->nama_produk }}" 
                                                data-ukuran="{{ $suratPerintahKerja->produkSalesOrder->produk->ukuran_produk }}" 
                                                data-warna="{{ $suratPerintahKerja->produkSalesOrder->produk->warna_produk }}"
                                                data-qty="{{ $suratPerintahKerja->kuantitas }}" 
                                                data-qty-selesai="{{ $suratPerintahKerja->realisasiProduksis->sum('kuantitas_selesai') }}"
                                                data-biaya-material="{{ $suratPerintahKerja->permintaanMaterial->biaya_material / $suratPerintahKerja->kuantitas }}"
                                                data-biaya-pekerja="{{ $suratPerintahKerja->produkSalesOrder->produk->billOfMaterial->perkiraanHpp->biaya_pekerja }}"
                                                data-biaya-overhead="{{ $suratPerintahKerja->produkSalesOrder->produk->billOfMaterial->perkiraanHpp->biaya_overhead }}">{{ $suratPerintahKerja->no_nota }}
                                            </option>
                                        @endforeach
                                    </select>
    
                                    @error('surat_perintah_kerja_id')
                                        <small id="suratPerintahKerjaHelp" class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Produk</label>
                                    <input type="text" class="form-control produk" name="produk" value="{{ old('produk') }}" placeholder="Nama Produk" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Ukuran</label>
                                    <input type="text" class="form-control ukuran" name="ukuran" value="{{ old('ukuran') }}" placeholder="Ukuran Produk" readonly>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Warna</label>
                                    <input type="text" class="form-control warna" name="warna" value="{{ old('warna') }}" placeholder="Warna Produk" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Tanggal Mulai Produksi</label>
                                    <input type="datetime-local" aria-describedby="tglMulaiHelp" class="form-control @error('tanggal_mulai') is-invalid @enderror" name="tanggal_mulai" value="{{ old('tanggal_mulai') }}" step="1">
    
                                    @error('tanggal_mulai')
                                        <small id="tglMulaiHelp" class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Tanggal Selesai Produksi</label>
                                    <input type="datetime-local" aria-describedby="tglSelesaiHelp" class="form-control @error('tanggal_selesai') is-invalid @enderror" name="tanggal_selesai" value="{{ old('tanggal_selesai') }}" step="1">
    
                                    @error('tanggal_selesai')
                                        <small id="tglSelesaiHelp" class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Kuantitas Selesai Produksi</label>
                                    <input data-inputmask="
                                        'alias': 'numeric',
                                        'groupSeparator': ',',
                                        'rightAlign': 'true',
                                        'autoUnmask': 'true',
                                        'allowMinus': 'false',
                                        'removeMaskOnSubmit': 'true',
                                        'digits': '0',
                                        'min': '0'" 
                                        aria-describedby="kuantitasSelesaiHelp" class="form-control kuantitas @error('kuantitas_selesai') is-invalid @enderror" name="kuantitas_selesai" value="{{ old('kuantitas_selesai', 0) }}">
    
                                    @error('kuantitas_selesai')
                                        <small id="kuantitasSelesaiHelp" class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Realisasi Biaya Material</label>
                                    <input data-inputmask="
                                        'alias': 'currency', 
                                        'prefix': 'Rp ',
                                        'placeholder': 'Rp 1',
                                        'rightAlign': 'true',
                                        'autoUnmask': 'true',
                                        'allowMinus': 'false',
                                        'removeMaskOnSubmit': 'true',
                                        'digits': '0',
                                        'min': '1'" 
                                        aria-describedby="biayaMaterialHelp" class="form-control realisasi-biaya-material @error('biaya_material') is-invalid @enderror" name="biaya_material" value="{{ old('biaya_material', 1) }}" readonly>
    
                                    @error('biaya_material')
                                        <small id="biayaMaterialHelp" class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Realisasi Biaya Pekerja</label>
                                    <input data-inputmask="
                                        'alias': 'currency', 
                                        'prefix': 'Rp ',
                                        'placeholder': 'Rp 1',
                                        'rightAlign': 'true',
                                        'autoUnmask': 'true',
                                        'allowMinus': 'false',
                                        'removeMaskOnSubmit': 'true',
                                        'digits': '0',
                                        'min': '1'" 
                                        aria-describedby="biayaPekerjaHelp" class="form-control realisasi-biaya-pekerja @error('biaya_pekerja') is-invalid @enderror" name="biaya_pekerja" value="{{ old('biaya_pekerja', 1) }}">
    
                                    @error('biaya_pekerja')
                                        <small id="biayaPekerjaHelp" class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Realisasi Biaya Overhead Pabrik</label>
                                    <input data-inputmask="
                                        'alias': 'currency', 
                                        'prefix': 'Rp ',
                                        'placeholder': 'Rp 1',
                                        'rightAlign': 'true',
                                        'autoUnmask': 'true',
                                        'allowMinus': 'false',
                                        'removeMaskOnSubmit': 'true',
                                        'digits': '0',
                                        'min': '1'" 
                                        aria-describedby="biayaOverheadHelp" class="form-control realisasi-biaya-overhead @error('biaya_overhead') is-invalid @enderror" name="biaya_overhead" value="{{ old('biaya_overhead', 1) }}">
    
                                    @error('biaya_overhead')
                                        <small id="biayaOverheadHelp" class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-50">
                        <div class="table-responsive">
                            <table class="table mb-0" id="material-table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Material</th>
                                        <th>Qty Rusak</th>
                                        <th>Qty Sisa</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach (old('produk_id', ['']) as $detail)
                                        <tr id="material{{ $loop->index }}">
                                            <td>
                                                <select aria-describedby="produkHelp" class="select2 form-control @error('produk_id.' . $loop->index) is-invalid @enderror" name="produk_id[]">
                                                    <option></option>
                                                    @foreach ($materials as $material)
                                                        <option value="{{ $material->id }}" 
                                                            {{ old('produk_id.' . $loop->parent->index) == $material->id ? 'selected' : '' }}>
                                                            {{ $material->nama_produk }} - {{ $material->ukuran_produk }} - {{ $material->warna_produk }}
                                                        </option>
                                                    @endforeach
                                                </select>

                                                @error('produk_id.' . $loop->index)
                                                    <small id="produkHelp" class="form-text text-danger">{{ $message }}</small>
                                                @enderror
                                            </td>
                                            <td>
                                                <input data-inputmask="
                                                    'alias': 'numeric',
                                                    'groupSeparator': ',',
                                                    'rightAlign': 'true',
                                                    'autoUnmask': 'true',
                                                    'allowMinus': 'false',
                                                    'removeMaskOnSubmit': 'true',
                                                    'digits': '0',
                                                    'min': '0'" 
                                                    aria-describedby="kuantitasRusakHelp" class="form-control invoice-item-qty-rusak @error('kuantitas_rusak.' . $loop->index) is-invalid @enderror" name="kuantitas_rusak[]" value="{{ old('kuantitas_rusak.' . $loop->index, 0) }}">

                                                @error('kuantitas_rusak.' . $loop->index)
                                                    <small id="kuantitasRusakHelp" class="form-text text-danger">{{ $message }}</small>
                                                @enderror
                                            </td>
                                            <td>
                                                <input data-inputmask="
                                                    'alias': 'numeric',
                                                    'groupSeparator': ',',
                                                    'rightAlign': 'true',
                                                    'autoUnmask': 'true',
                                                    'allowMinus': 'false',
                                                    'removeMaskOnSubmit': 'true',
                                                    'digits': '0',
                                                    'min': '0'" 
                                                    aria-describedby="kuantitasSisaHelp" class="form-control invoice-item-qty-sisa @error('kuantitas_sisa.' . $loop->index) is-invalid @enderror" name="kuantitas_sisa[]" value="{{ old('kuantitas_sisa.' . $loop->index, 0) }}">

                                                @error('kuantitas_sisa.' . $loop->index)
                                                    <small id="kuantitasSisaHelp" class="form-text text-danger">{{ $message }}</small>
                                                @enderror
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-icon btn-danger text-nowrap px-1 hapus-material">
                                                    <i class="bx bx-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col p-0">
                            <button type="button" class="btn btn-info" id="tambah-material">
                                Tambah Material
                            </button>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row ">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Simpan Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js" 
    integrity="sha512-6Jym48dWwVjfmvB0Hu3/4jn4TODd6uvkxdi9GNbBHwZ4nGcRxJUCaTkL3pVY6XUQABqFo3T58EMXFQztbjvAFQ==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/bindings/inputmask.binding.js" 
    integrity="sha512-J6WEJE0No+5Qqm9/T93q88yRQjvoAioXG4gzJ+eqZtLi+ZBgimZDkTiLWiljwrwnoQw+xwECQm282RJ6CrJnlw==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    $( document ).ready(function() {

        var biayaMaterial = 0;

        var biayaPekerja = 0;

        var biayaOverhead = 0;

        $(".invoice-item-select").on("change", function(e) {
            var produk = this.options[e.target.selectedIndex].getAttribute('data-produk');

            var ukuran = this.options[e.target.selectedIndex].getAttribute('data-ukuran');

            var warna = this.options[e.target.selectedIndex].getAttribute('data-warna');

            var qty = this.options[e.target.selectedIndex].getAttribute('data-qty');

            var qtySelesai = this.options[e.target.selectedIndex].getAttribute('data-qty-selesai');

            biayaMaterial = this.options[e.target.selectedIndex].getAttribute('data-biaya-material');

            biayaPekerja = this.options[e.target.selectedIndex].getAttribute('data-biaya-pekerja');

            biayaOverhead = this.options[e.target.selectedIndex].getAttribute('data-biaya-overhead');
            
            let max = qty - qtySelesai;
            
            $(".produk").val(produk);

            $(".ukuran").val(ukuran);

            $(".warna").val(warna);

            $(".kuantitas").val(max);

            $(".kuantitas").inputmask("numeric", {
                max: max
            });

            $(".realisasi-biaya-material").val(max * biayaMaterial);

            $(".realisasi-biaya-pekerja").val(max * biayaPekerja);

            $(".realisasi-biaya-overhead").val(max * biayaOverhead);
        });

        $(".kuantitas").on("keyup", function(e) {
            let qtyRealisasi = parseInt($(this).val());

            $(".realisasi-biaya-material").val(qtyRealisasi * biayaMaterial);

            $(".realisasi-biaya-pekerja").val(qtyRealisasi * biayaPekerja);

            $(".realisasi-biaya-overhead").val(qtyRealisasi * biayaOverhead);
        });

        //Trigger New Material Row
        let row_number = 1;

        $("#tambah-material").on("click", function(e) {
            e.preventDefault();

            let new_row_number = row_number - 1;

            $('#material-table').append('<tr id="material' + (row_number) + '"></tr>');

            $('#material' + row_number).html($('#material' + new_row_number).html()).find('td:first-child');            


            $("#material" + row_number + " .invoice-item-qty-rusak").inputmask({
                alias: 'numeric',
                groupSeparator: ',',
                rightAlign: true,
                autoUnmask: true,
                allowMinus: false,
                removeMaskOnSubmit: true,
                digits: '0',
                min: '0', 
            });

            $("#material" + row_number + " .invoice-item-qty-sisa").inputmask({
                alias: 'numeric',
                groupSeparator: ',',
                rightAlign: true,
                autoUnmask: true,
                allowMinus: false,
                removeMaskOnSubmit: true,
                digits: '0',
                min: '0', 
            });

            $(".select2-container").remove();

            $(".select2").select2({
                placeholder: "Pilih Data",
                dropdownAutoWidth: true,
                width: '100%'
            });

            $("#material" + row_number + " .text-danger").hide();

            row_number++;
        });

        //Remove Last Produk Row
        $(document).on('click', '.hapus-material', function () {
            $(this).parents('tr').remove();

            row_number--;
        });
    });
</script>
@endsection