@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Ubah Karyawan</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Karyawan
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-sm-12 col-8">
            <div class="card">
                <form class="form" action="{{ route('users.update', $user) }}" method="post">
                    @csrf
                    @method('patch')

                    <div class="card-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Divisi</label>
                                        <select aria-describedby="divisiHelp" class="select2 form-control" name="divisi_id">
                                            @foreach ($divisis as $divisi)
                                                <option value="{{ $divisi->id }}" {{ old('divisi_id', $user->divisi_id) == $divisi->id ? 'selected' : ''}}>
                                                    {{ $divisi->nama_divisi }}
                                                </option>
                                            @endforeach
                                        </select>

                                        @error('divisi_id')
                                            <small id="divisiHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Jabatan</label>
                                        <select aria-describedby="jabatanHelp" class="select2 form-control" name="jabatan_id">
                                            @foreach ($jabatans as $jabatan)
                                                <option value="{{ $jabatan->id }}" {{ old('jabatan_id', $user->jabatan_id) == $jabatan->id ? 'selected' : ''}}>
                                                    {{ $jabatan->nama_jabatan }}
                                                </option>
                                            @endforeach
                                        </select>

                                        @error('jabatan_id')
                                            <small id="jabatanHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" value="{{ $user->username }}" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Nomor KTP</label>
                                        <input type="number" class="form-control" value="{{ $user->no_ktp }}" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Nama Karyawan</label>
                                        <input type="text" class="form-control" value="{{ $user->nama_karyawan }}" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Nomor Telepon</label>
                                        <input type="tel" class="form-control" value="{{ $user->no_telepon }}" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Alamat Karyawan</label>
                                        <input type="text" class="form-control" value="{{ $user->alamat_karyawan }}" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <input type="date" class="form-control" value="{{ $user->tanggal_lahir }}" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Email Karyawan (Opsional)</label>
                                        <input type="email" class="form-control" placeholder="Masukan Email Karyawan (Opsional)" value="{{ $user->email_karyawan }}" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Tanggal Masuk</label>
                                        <input type="date" aria-describedby="masukHelp" class="form-control @error('tanggal_masuk') is-invalid @enderror" name="tanggal_masuk" value="{{ old('tanggal_masuk', $user->tanggal_masuk) }}">

                                        @error('tanggal_masuk')
                                            <small id="masukHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Status Karyawan</label>
                                        <select aria-describedby="statusHelp" class="form-control @error('status_karyawan') is-invalid @enderror" name="status_karyawan">
                                            <option value="aktif" {{ old('status_karyawan', $user->status_karyawan) == 'aktif' ? 'selected' : ''}}>Aktif</option>
                                            <option value="non aktif" {{ old('status_karyawan', $user->status_karyawan) == 'non aktif' ? 'selected' : ''}}>Non Aktif</option>
                                        </select>

                                        @error('status_karyawan')
                                            <small id="statusHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row ">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-warning">Ubah Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
@endsection