@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Buat Karyawan</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Karyawan
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-sm-12 col-8">
            <div class="card">
                <form class="form" action="{{ route('users.store') }}" method="post">
                    @csrf
                   
                    <div class="card-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Divisi</label>
                                        <select aria-describedby="divisiHelp" class="select2 form-control" name="divisi_id">
                                            <option></option>
                                            @foreach ($divisis as $divisi)
                                                <option value="{{ $divisi->id }}" {{ old('divisi_id') == $divisi->id ? 'selected' : '' }}>
                                                    {{ $divisi->nama_divisi }}
                                                </option>
                                            @endforeach
                                        </select>

                                        @error('divisi_id')
                                            <small id="divisiHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Jabatan</label>
                                        <select aria-describedby="jabatanHelp" class="select2 form-control" name="jabatan_id">
                                            <option></option>
                                            @foreach ($jabatans as $jabatan)
                                                <option value="{{ $jabatan->id }}" {{ old('jabatan_id') == $jabatan->id ? 'selected' : '' }}>
                                                    {{ $jabatan->nama_jabatan }}
                                                </option>
                                            @endforeach
                                        </select>

                                        @error('jabatan_id')
                                            <small id="jabatanHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" aria-describedby="usernameHelp" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Masukan Username" value="{{ old('username') }}">

                                        @error('username')
                                            <small id="usernameHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Nomor KTP</label>
                                        <input type="number" aria-describedby="noKtpHelp" class="form-control @error('no_ktp') is-invalid @enderror" name="no_ktp" placeholder="Nomor KTP memiliki 16 digit" value="{{ old('no_ktp') }}">

                                        @error('no_ktp')
                                            <small id="noKtpHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Nama Karyawan</label>
                                        <input type="text" aria-describedby="namaHelp" class="form-control @error('nama_karyawan') is-invalid @enderror" name="nama_karyawan" placeholder="Masukan Nama Karyawan" value="{{ old('nama_karyawan') }}">

                                        @error('nama_karyawan')
                                            <small id="namaHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Nomor Telepon</label>
                                        <input type="tel" aria-describedby="telpHelp" class="form-control @error('no_telepon') is-invalid @enderror" name="no_telepon" placeholder="Contoh: 081233911680" value="{{ old('no_telepon') }}">

                                        @error('no_telepon')
                                            <small id="telpHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Alamat Karyawan</label>
                                        <input type="text" aria-describedby="alamatHelp" class="form-control @error('alamat_karyawan') is-invalid @enderror" name="alamat_karyawan" placeholder="Masukan Alamat Karyawan" value="{{ old('alamat_karyawan') }}">

                                        @error('alamat_karyawan')
                                            <small id="alamatHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <input type="date" aria-describedby="lahirHelp" class="form-control @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" value="{{ old('tanggal_lahir') }}">

                                        @error('tanggal_lahir')
                                            <small id="lahirHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Email Karyawan (Opsional)</label>
                                        <input type="email" aria-describedby="emailHelp" class="form-control @error('email_karyawan') is-invalid @enderror" name="email_karyawan" placeholder="Masukan Email Karyawan (Opsional)" value="{{ old('email_karyawan') }}">

                                        @error('email_karyawan')
                                            <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Tanggal Masuk</label>
                                        <input type="date" aria-describedby="masukHelp" class="form-control @error('tanggal_masuk') is-invalid @enderror" name="tanggal_masuk" value="{{ old('tanggal_masuk') }}">

                                        @error('tanggal_masuk')
                                            <small id="masukHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" aria-describedby="passwordHelp" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password Minimal Memiliki 8 Karakter">

                                        @error('password')
                                            <small id="passwordHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Konfirmasi Password</label>
                                        <input type="password" aria-describedby="cpasswordHelp" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" placeholder="Masukkan Konfirmasi Password">
                                        
                                        @error('password_confirmation')
                                            <small id="cpasswordHelp" class="form-text text-danger">{{ $message}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Simpan Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
@endsection