@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Karyawan</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Karyawan
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-datatable">
    @can('karyawan create')
        <div class="invoice-create-btn mb-1">
            <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm round glow align-items-center">Buat Karyawan</a>
        </div>
    @endcan
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body card-dashboard">
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>divisi</th>
                                    <th>jabatan</th>
                                    <th>nama</th>
                                    <th>alamat</th>
                                    <th>email</th>
                                    <th>no KTP</th>
                                    <th>telp</th>
                                    <th>tgl lahir</th>
                                    <th>tgl msk</th>
                                    <th>status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>
                                            <small class="text-muted">{{ $user->divisi->nama_divisi }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $user->jabatan->nama_jabatan }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $user->nama_karyawan }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $user->alamat_karyawan }}</small>
                                        </td>
                                        <td>
                                            @switch($user->email_karyawan)
                                                @case(null)
                                                    <span class="badge badge-light-info badge-pill">not found</span>
                                                    @break
                                                @default
                                                    <small class="text-muted">{{ $user->email_karyawan }}</small>
                                            @endswitch
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $user->no_ktp }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $user->no_telepon }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">@date($user->tanggal_lahir)</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">@date($user->tanggal_masuk)</small>
                                        </td>
                                        <td>
                                            @switch($user->status_karyawan)
                                                @case('non aktif')
                                                    <span class="badge badge-light-danger badge-pill">{{ $user->status_karyawan }}</span>
                                                    @break
                                                @default
                                                    <span class="badge badge-light-primary badge-pill">{{ $user->status_karyawan }}</span>
                                            @endswitch
                                        </td>
                                        <td>
                                            @can('karyawan edit')
                                                @if ($user->username != 'administrator')
                                                    <div class="invoice-action">                                                    
                                                        <a href="{{ route('users.edit', $user) }}" class="invoice-action-edit cursor-pointer">
                                                            <i class="bx bx-edit-alt"></i>
                                                        </a>
                                                    </div>
                                                @endif
                                            @endcan                                        
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/datatables/datatable.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if(Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection