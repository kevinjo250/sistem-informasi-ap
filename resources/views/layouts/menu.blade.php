<li class="{{ Request::routeIs('dashboard') ? 'active' : ''}}">
    <a href="{{ route('dashboard') }}">
        <i class="menu-livicon" data-icon="desktop"></i><span class="menu-title text-truncate" data-i18n="Dashboard">Dashboard</span>
    </a>
</li>
@can('akuntansi access')
    <li class="nav-item">
        <a href="#">
            <i class="menu-livicon" data-icon="bar-chart"></i><span class="menu-title text-truncate" data-i18n="Akuntansi">Akuntansi</span>
        </a>
        <ul class="menu-content">
            <li class="{{ Request::routeIs('jurnals*') ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{ route('jurnals.index') }}">
                    <i class="bx bx-book-open"></i><span class="menu-item text-truncate" data-i18n="Jurnal">Jurnal</span>
                </a>
            </li>
            <li class="{{ Request::routeIs('bukuBesars*') ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{ route('bukuBesars.index') }}">
                    <i class="bx bx-receipt"></i><span class="menu-item text-truncate" data-i18n="Buku Besar">Buku Besar</span>
                </a>
            </li>
            <li class="{{ Request::routeIs('laporanKeuangans*') ? 'active' : ''}}">
                <a class="d-flex align-items-center" href="{{ route('laporanKeuangans.index') }}">
                    <i class="bx bxs-report"></i><span class="menu-item text-truncate" data-i18n="Laporan Keuangan">Laporan Keuangan</span>
                </a>
            </li>
        </ul>
    </li>
@endcan
<li class="nav-item">
    <a href="#">
        <i class="menu-livicon" data-icon="notebook"></i><span class="menu-title text-truncate" data-i18n="Kontak">Kontak</span>
    </a>
    <ul class="menu-content">
        <li class="{{ Request::routeIs('pelanggans*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('pelanggans.index') }}">
                <i class="bx bx-group"></i><span class="menu-item text-truncate" data-i18n="Pelanggan">Pelanggan</span>
            </a>
        </li>
        <li class="{{ Request::routeIs('suppliers*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('suppliers.index') }}">
                <i class="bx bx-id-card"></i><span class="menu-item text-truncate" data-i18n="Supplier">Supplier</span>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item">
    <a href="#">
        <i class="menu-livicon" data-icon="shoppingcart-out"></i><span class="menu-title text-truncate" data-i18n="Penjualan">Penjualan</span>
    </a>
    <ul class="menu-content">
        <li class="{{ Request::routeIs('salesOrders*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('salesOrders.index') }}">
                <i class="bx bx-receipt"></i><span class="menu-item text-truncate" data-i18n="Sales Order">Sales Order</span>
            </a>
        </li>
        <li class="{{ Request::routeIs('batalOrders*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('batalOrders.index') }}">
                <i class="bx bx-receipt"></i><span class="menu-item text-truncate" data-i18n="Batal Order">Batal Order</span>
            </a>
        </li>
        <li class="{{ Request::routeIs('notaPembayarans*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('notaPembayarans.index') }}">
                <i class="bx bx-money"></i><span class="menu-item text-truncate" data-i18n="Nota Pembayaran">Nota Pembayaran</span>
            </a>
        </li>
        <li class="{{ Request::routeIs('suratJalans*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('suratJalans.index') }}">
                <i class="bx bxs-truck"></i><span class="menu-item text-truncate" data-i18n="Surat Jalan">Surat Jalan</span>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item">
    <a href="#">
        <i class="menu-livicon" data-icon="shoppingcart-in"></i><span class="menu-title text-truncate" data-i18n="Pembelian">Pembelian</span>
    </a>
    <ul class="menu-content">
        <li class="{{ Request::routeIs('pemesananMaterials*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('pemesananMaterials.index') }}">
                <i class="bx bx-receipt"></i><span class="menu-item text-truncate" data-i18n="Pemesanan Material">Pemesanan Material</span>
            </a>
        </li>
        <li class="{{ Request::routeIs('penerimaanMaterials*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('penerimaanMaterials.index') }}">
                <i class="bx bx-file"></i><span class="menu-item text-truncate" data-i18n="Penerimaan Material">Penerimaan Material</span>
            </a>
        </li>
        <li class="{{ Request::routeIs('notaPelunasanSuppliers*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('notaPelunasanSuppliers.index') }}">
                <i class="bx bx-money"></i><span class="menu-item text-truncate" data-i18n="Nota Pelunasan">Nota Pelunasan</span>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item">
    <a href="#">
        <i class="menu-livicon" data-icon="gears"></i><span class="menu-title text-truncate" data-i18n="Produksi">Produksi</span>
    </a>
    <ul class="menu-content">
        <li class="{{ Request::routeIs('billOfMaterials*') || Request::routeIs('perkiraanHpps*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('billOfMaterials.index') }}">
                <i class="bx bx-spreadsheet"></i><span class="menu-item text-truncate" data-i18n="Bill of Material">Bill of Material</span>
            </a>
        </li>
        <li class="{{ Request::routeIs('suratPerintahKerjas*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('suratPerintahKerjas.index') }}">
                <i class="bx bx-task"></i><span class="menu-item text-truncate" data-i18n="Surat Perintah Kerja">Surat Perintah Kerja</span>
            </a>
        </li>
        <li class="{{ Request::routeIs('permintaanMaterials*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('permintaanMaterials.index') }}">
                <i class="bx bx-file"></i><span class="menu-item text-truncate" data-i18n="Permintaan Material">Permintaan Material</span>
            </a>
        </li>
        <li class="{{ Request::routeIs('realisasiProduksis*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('realisasiProduksis.index') }}">
                <i class="bx bx-cog"></i><span class="menu-item text-truncate" data-i18n="Realisasi Produksi">Realisasi Produksi</span>
            </a>
        </li>
    </ul>
</li>
<li class="{{ Request::routeIs('produks*') ? 'active' : ''}}">
    <a href="{{ route('produks.index') }}">
        <i class="menu-livicon" data-icon="box"></i><span class="menu-title text-truncate" data-i18n="Produk">Produk</span>
    </a>
</li>
<li class="nav-item">
    <a href="#">
        <i class="menu-livicon" data-icon="users"></i><span class="menu-title text-truncate" data-i18n="Manajemen User">Manajemen User</span>
    </a>
    <ul class="menu-content">
        <li class="{{ Request::routeIs('divisis*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('divisis.index') }}">
                <i class="bx bxs-user-pin"></i><span class="menu-item text-truncate" data-i18n="Divisi">Divisi</span>
            </a>
        </li>
        <li class="{{ Request::routeIs('jabatans*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('jabatans.index') }}">
                <i class="bx bxs-user-badge"></i><span class="menu-item text-truncate" data-i18n="Jabatan">Jabatan</span>
            </a>
        </li>
        <li class="{{ Request::routeIs('otorisasis*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('otorisasis.index') }}">
                <i class="bx bx-user-check"></i><span class="menu-item text-truncate" data-i18n="Otorisasi">Otorisasi</span>
            </a>
        </li>
        <li class="{{ Request::routeIs('users*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('users.index') }}">
                <i class="bx bx-group"></i><span class="menu-item text-truncate" data-i18n="Karyawan">Karyawan</span>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item">
    <a href="#">
        <i class="menu-livicon" data-icon="wrench"></i><span class="menu-title text-truncate" data-i18n="Pengaturan Lainnya">Pengaturan Lainnya</span>
    </a>
    <ul class="menu-content">
        <li class="{{ Request::routeIs('akuns*') ? 'active' : ''}}">
            <a class="d-flex align-items-center" href="{{ route('akuns.index') }}">
                <i class="bx bx-list-ul"></i><span class="menu-item text-truncate" data-i18n="Daftar Akun">Daftar Akun</span>
            </a>
        </li>
    </ul>
</li>