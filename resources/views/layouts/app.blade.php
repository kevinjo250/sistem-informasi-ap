<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="Frest admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Frest admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Sistem Informasi Akuntansi & Produksi</title>
    <link rel="apple-touch-icon" href={{ asset('app-assets/images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('app-assets/images/ico/favicon.ico') }}">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700') }}" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
    @yield('vendor css')
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/semi-dark-layout.css') }}">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    @yield('page css')
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

    <!-- BEGIN: Header-->
    <div class="header-navbar-shadow"></div>
    <nav class="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top ">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        
                    </div>
                    <ul class="nav navbar-nav float-right">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon bx bx-fullscreen"></i></a></li>
                        <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="javascript:void(0);" data-toggle="dropdown"><i class="ficon bx bx-bell bx-tada bx-flip-horizontal"></i><span class="badge badge-pill badge-danger badge-up notification-number">{{ auth()->user()->unreadNotifications->count() }}</span></a>
                            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                <li class="dropdown-menu-header">
                                    <div class="dropdown-header px-1 py-75 d-flex justify-content-between"><span class="notification-title">{{ auth()->user()->unreadNotifications->count() }} Notifications</span></div>
                                </li>
                                <li class="scrollable-container media-list">                                    
                                    @forelse (auth()->user()->unreadNotifications as $notification)
                                        <a class="d-flex justify-content-between cursor-pointer notification-alert" href="#" data-id="{{ $notification->id }}">
                                            <div class="media d-flex align-items-center border-0">                                                
                                                @switch($notification->type)
                                                    @case('App\Notifications\LowMaterialStockNotification')
                                                        <div class="media-left pr-0">
                                                            <div class="avatar bg-rgba-danger m-0 mr-1 p-25">
                                                                <div class="avatar-content"><i class="bx bx-package text-danger"></i></div>
                                                            </div>
                                                        </div>
                                                        <div class="media-body">
                                                            <h6 class="media-heading"><span class="text-bold-500">Stok material {{ $notification->data['nama_produk'] }} menipis!</span></h6>
                                                            <h6 class="media-heading">Ukuran: {{ $notification->data['ukuran_produk'] }}, Warna: {{ $notification->data['warna_produk'] }} </h6>
                                                            <small class="notification-text">Created at: @datetime($notification->created_at)</small>
                                                        </div>
                                                        <div class="media-right pl-0">
                                                            <div class="row border-left text-center">
                                                                <div class="col-12 px-50 py-75">
                                                                    <div class="avatar-content">
                                                                        <small>Mark as Read</small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @break
                                                    @case('App\Notifications\PemesananMaterialConfirmedNotification')
                                                        <div class="media-left pr-0">
                                                            <div class="avatar bg-rgba-danger m-0 mr-1 p-25">
                                                                <div class="avatar-content"><i class="bx bx-task text-success"></i></div>
                                                            </div>
                                                        </div>
                                                        <div class="media-body">
                                                            <h6 class="media-heading"><span class="text-bold-500">Nota {{ $notification->data['no_nota'] }} telah dikonfirmasi!</span></h6>
                                                            <h6 class="media-heading">Dikonfirmasi oleh Direktur Perusahaan </h6>
                                                            <small class="notification-text">Created at: @datetime($notification->created_at)</small>
                                                        </div>
                                                        <div class="media-right pl-0">
                                                            <div class="row border-left text-center">
                                                                <div class="col-12 px-50 py-75">
                                                                    <div class="avatar-content">
                                                                        <small>Mark as Read</small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @break
                                                    @case('App\Notifications\SalesOrderDueDateNotification')
                                                        <div class="media-left pr-0">
                                                            <div class="avatar bg-rgba-danger m-0 mr-1 p-25">
                                                                <div class="avatar-content"><i class="bx bx-credit-card text-danger"></i></div>
                                                            </div>
                                                        </div>
                                                        <div class="media-body">
                                                            <h6 class="media-heading"><span class="text-bold-500">Nota {{ $notification->data['no_nota'] }} telah jatuh tempo!</span></h6>
                                                            <small class="notification-text">Created at: @datetime($notification->created_at)</small>
                                                        </div>
                                                        <div class="media-right pl-0">
                                                            <div class="row border-left text-center">
                                                                <div class="col-12 px-50 py-75">
                                                                    <div class="avatar-content">
                                                                        <small>Mark as Read</small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @break
                                                    @case('App\Notifications\PemesananMaterialDueDateNotification')
                                                        <div class="media-left pr-0">
                                                            <div class="avatar bg-rgba-danger m-0 mr-1 p-25">
                                                                <div class="avatar-content"><i class="bx bx-credit-card text-danger"></i></div>
                                                            </div>
                                                        </div>
                                                        <div class="media-body">
                                                            <h6 class="media-heading"><span class="text-bold-500">Nota {{ $notification->data['no_nota'] }} telah jatuh tempo!</span></h6>
                                                            <small class="notification-text">Created at: @datetime($notification->created_at)</small>
                                                        </div>
                                                        <div class="media-right pl-0">
                                                            <div class="row border-left text-center">
                                                                <div class="col-12 px-50 py-75">
                                                                    <div class="avatar-content">
                                                                        <small>Mark as Read</small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @break
                                                    @default
                                                        <div class="media-left pr-0">
                                                            <div class="avatar bg-rgba-danger m-0 mr-1 p-25">
                                                                <div class="avatar-content"><i class="bx bx-task text-primary"></i></div>
                                                            </div>
                                                        </div>
                                                        <div class="media-body">
                                                            <h6 class="media-heading"><span class="text-bold-500">Nota {{ $notification->data['no_nota'] }} telah dibuat!</span></h6>
                                                            <h6 class="media-heading">Dibuat oleh {{ $notification->data['karyawan'] }} </h6>
                                                            <small class="notification-text">Created at: @datetime($notification->created_at)</small>
                                                        </div>
                                                        <div class="media-right pl-0">
                                                            <div class="row border-left text-center">
                                                                <div class="col-12 px-50 py-75">
                                                                    <div class="avatar-content">
                                                                        <small>Mark as Read</small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                @endswitch
                                            </div>
                                        </a>
                                    @empty
                                        <div class="media d-flex justify-content-center">
                                            <h5 class="media-heading">No Notifications!</h5>
                                        </div>
                                    @endforelse
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-user nav-item">
                            <a class="dropdown-toggle nav-link dropdown-user-link" href="javascript:void(0);" data-toggle="dropdown">
                                <div class="user-nav d-sm-flex d-none">
                                    <span class="user-name">{{ auth()->user()->nama_karyawan }}</span>
                                    <span class="user-status text-muted">Active</span>
                                </div>
                                <span><img class="round" src="{{ asset('app-assets/images/portrait/small/avatar-s-11.jpg') }}" alt="avatar" height="40" width="40"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right pb-0">
                                <a class="dropdown-item" href="{{ route('profile.edit', auth()->user()) }}">
                                    <i class="bx bx-user mr-50"></i> Ubah Akun
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    <i class="bx bx-power-off mr-50"></i> Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="post" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="{{ route('dashboard') }}">
                        <div class="brand-logo">
                            <svg class="logo" width="26px" height="26px" viewbox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <title>icon</title>
                                <defs>
                                    <lineargradient id="linearGradient-1" x1="50%" y1="0%" x2="50%" y2="100%">
                                        <stop stop-color="#5A8DEE" offset="0%"></stop>
                                        <stop stop-color="#699AF9" offset="100%"></stop>
                                    </lineargradient>
                                    <lineargradient id="linearGradient-2" x1="0%" y1="0%" x2="100%" y2="100%">
                                        <stop stop-color="#FDAC41" offset="0%"></stop>
                                        <stop stop-color="#E38100" offset="100%"></stop>
                                    </lineargradient>
                                </defs>
                                <g id="Sprite" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="sprite" transform="translate(-69.000000, -61.000000)">
                                        <g id="Group" transform="translate(17.000000, 15.000000)">
                                            <g id="icon" transform="translate(52.000000, 46.000000)">
                                                <path id="Combined-Shape" d="M13.5909091,1.77272727 C20.4442608,1.77272727 26,7.19618701 26,13.8863636 C26,20.5765403 20.4442608,26 13.5909091,26 C6.73755742,26 1.18181818,20.5765403 1.18181818,13.8863636 C1.18181818,13.540626 1.19665566,13.1982714 1.22574292,12.8598734 L6.30410592,12.859962 C6.25499466,13.1951893 6.22958398,13.5378796 6.22958398,13.8863636 C6.22958398,17.8551125 9.52536149,21.0724191 13.5909091,21.0724191 C17.6564567,21.0724191 20.9522342,17.8551125 20.9522342,13.8863636 C20.9522342,9.91761479 17.6564567,6.70030817 13.5909091,6.70030817 C13.2336969,6.70030817 12.8824272,6.72514561 12.5388136,6.77314791 L12.5392575,1.81561642 C12.8859498,1.78721495 13.2366963,1.77272727 13.5909091,1.77272727 Z"></path>
                                                <path id="Combined-Shape" d="M13.8863636,4.72727273 C18.9447899,4.72727273 23.0454545,8.82793741 23.0454545,13.8863636 C23.0454545,18.9447899 18.9447899,23.0454545 13.8863636,23.0454545 C8.82793741,23.0454545 4.72727273,18.9447899 4.72727273,13.8863636 C4.72727273,13.5378966 4.74673291,13.1939746 4.7846258,12.8556254 L8.55057141,12.8560055 C8.48653249,13.1896162 8.45300462,13.5340745 8.45300462,13.8863636 C8.45300462,16.887125 10.8856023,19.3197227 13.8863636,19.3197227 C16.887125,19.3197227 19.3197227,16.887125 19.3197227,13.8863636 C19.3197227,10.8856023 16.887125,8.45300462 13.8863636,8.45300462 C13.529522,8.45300462 13.180715,8.48740462 12.8430777,8.55306931 L12.8426531,4.78608796 C13.1851829,4.7472336 13.5334422,4.72727273 13.8863636,4.72727273 Z" fill="#4880EA"></path>
                                                <path id="Combined-Shape" d="M13.5909091,1.77272727 C20.4442608,1.77272727 26,7.19618701 26,13.8863636 C26,20.5765403 20.4442608,26 13.5909091,26 C6.73755742,26 1.18181818,20.5765403 1.18181818,13.8863636 C1.18181818,13.540626 1.19665566,13.1982714 1.22574292,12.8598734 L6.30410592,12.859962 C6.25499466,13.1951893 6.22958398,13.5378796 6.22958398,13.8863636 C6.22958398,17.8551125 9.52536149,21.0724191 13.5909091,21.0724191 C17.6564567,21.0724191 20.9522342,17.8551125 20.9522342,13.8863636 C20.9522342,9.91761479 17.6564567,6.70030817 13.5909091,6.70030817 C13.2336969,6.70030817 12.8824272,6.72514561 12.5388136,6.77314791 L12.5392575,1.81561642 C12.8859498,1.78721495 13.2366963,1.77272727 13.5909091,1.77272727 Z" fill="url(#linearGradient-1)"></path>
                                                <rect id="Rectangle" x="0" y="0" width="7.68181818" height="7.68181818"></rect>
                                                <rect id="Rectangle" fill="url(#linearGradient-2)" x="0" y="0" width="7.68181818" height="7.68181818"></rect>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                        <h2 class="brand-text mb-0">SI-AP</h2>
                    </a></li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary"></i><i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block primary" data-ticon="bx-disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">

                {{-- START SIDE MENU CONTENT --}}
                @include('layouts.menu')  
                {{-- END SIDE MENU CONTENT --}}

            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                @yield('breadcrumb')
            </div>
            <div class="content-body">

                {{-- START BODY CONTENT --}}
                @yield('content')
                {{-- END BODY CONTENT --}}

            </div>
        </div>
    </div>
    <!-- END: Content-->
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix mb-0"><span class="float-left d-inline-block">2021 &copy; PIXINVENT</span><span class="float-right d-sm-inline-block d-none">Crafted with<i class="bx bxs-heart pink mx-50 font-small-3"></i>by<a class="text-uppercase" href="https://1.envato.market/pixinvent_portfolio" target="_blank">Pixinvent</a></span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    @yield('page vendor js')
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
    <script src="{{ asset('app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/footer.js') }}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    @yield('page js')
    <!-- END: Page JS-->

    {{-- BEGIN Custom Script --}}
    <script class="text\javascript">
        $( document ).ready(function() {
            function sendMarkRequest(id) {
                return $.ajax("{{ route('markNotification') }}", {
                    method: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id
                    }
                });
            }

            var totalNotifications = {{ auth()->user()->unreadNotifications->count() }};

            $(".notification-alert").on("click", function(e) {
                let request = sendMarkRequest($(this).data("id"));

                request.done(() => {
                    totalNotifications -= 1;

                    if (parseInt(totalNotifications) == 0) {
                        $(this).html("<div class='media d-flex justify-content-center'><h5 class='media-heading'>No Notifications!</h5></div>");
                    } else {
                        $(this).remove();
                    }

                    $(".notification-title").text(totalNotifications + " Notifications");

                    $(".notification-number").text(totalNotifications);
                })
            });
        });
    </script>

    @yield('script')
    {{-- END Custom Script --}}

</body>
<!-- END: Body-->

</html>