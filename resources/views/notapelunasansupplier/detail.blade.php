@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-invoice.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Detail Nota Pelunasan Supplier</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Nota Pelunasan Supplier
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section class="invoice-view-wrapper">
    <div class="row">
        <!-- invoice view page -->
        <div class="col-xl-6 col-md-12 col-12">
            <div class="card invoice-print-area">
                <div class="card-body pb-0 mx-25">
                    <!-- header section -->
                    <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <span class="invoice-number mr-50">{{ $notaPelunasanSupplier->no_nota }}</span>
                        </div>
                        <div class="col-lg-8 col-md-12">
                            <div class="d-flex align-items-center justify-content-lg-end flex-wrap">
                                <div>
                                    <small class="text-muted">Tanggal Pelunasan:</small>
                                    <span>@datetime($notaPelunasanSupplier->created_at)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <!-- invoice address and contact -->
                    <div class="row invoice-info">
                        <div class="col-sm-6 col-12 mt-1">
                            <h6 class="invoice-from">Data Supplier</h6>
                            <div class="mb-1">
                                <span>{{ $notaPelunasanSupplier->pemesananMaterial->supplier->nama_supplier }}</span>
                            </div>
                            <div class="mb-1">
                                <span>{{ $notaPelunasanSupplier->pemesananMaterial->supplier->alamat_supplier }}, {{ $notaPelunasanSupplier->pemesananMaterial->supplier->kota_supplier }}</span>
                            </div>
                            <div class="mb-1">
                                <span>{{ $notaPelunasanSupplier->pemesananMaterial->supplier->no_telepon }}</span>
                            </div>
                        </div>
                        <div class="col-sm-6 col-12 mt-1">
                            <h6 class="invoice-from">Data Karyawan</h6>
                            <div class="mb-1">
                                <span>{{ $notaPelunasanSupplier->user->nama_karyawan }}</span>
                            </div>
                            <div class="mb-1">
                                <span>{{ $notaPelunasanSupplier->user->jabatan->nama_jabatan }}</span>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <!-- product details table-->
                <div class="invoice-product-details table-responsive">
                    <table class="table table-borderless mb-0">
                        <thead>
                            <tr class="border-0">
                                <th scope="col">Nomor Pemesanan Material</th>
                                <th scope="col">Keterangan</th>
                                <th scope="col">Cara Pembayaran</th>
                                <th scope="col" class="text-right">Nominal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <a href="{{ route('pemesananMaterials.show', $notaPelunasanSupplier->pemesanan_material_id) }}">{{ $notaPelunasanSupplier->pemesananMaterial->no_nota }}</a>
                                </td>
                                <td>
                                    @switch($notaPelunasanSupplier->keterangan)
                                        @case(null)
                                            <small class="text-muted">-</small>
                                            @break
                                        @default
                                            <small class="text-muted">{{ $notaPelunasanSupplier->keterangan }}</small>
                                    @endswitch
                                </td>
                                <td>
                                    <small class="text-muted">{{ $notaPelunasanSupplier->cara_bayar }}</small>
                                </td>
                                <td class="text-right">
                                    <small class="text-muted">@money($notaPelunasanSupplier->nominal_pelunasan)</small>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-6">
                            @can('notaPelunasanSupplier edit')
                                <a href="{{ route('notaPelunasanSuppliers.edit', $notaPelunasanSupplier) }}" class="btn btn-warning">Ubah Data</a>
                            @endcan
                        </div>
                        <div class="col-6 d-flex justify-content-end">
                            @can('notaPelunasanSupplier delete')
                                <a href="#DeleteModal" class="btn btn-danger" data-toggle="modal">Hapus Data</a>
                            @endcan
                        </div>                        

                        {{-- BEGIN DELETE MODAL --}}
                        <div class="modal fade text-left" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h5 class="modal-title white" id="myModalLabel160">Hapus Nota Pelunasan Supplier</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="bx bx-x"></i>
                                        </button>
                                    </div>
                                    <form action="{{ route('notaPelunasanSuppliers.destroy', $notaPelunasanSupplier) }}" method="post">
                                        @csrf
                                        @method('delete')

                                        <div class="modal-body">
                                            <p>Apakah anda yakin untuk menghapus transaksi tersebut?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                                <i class="bx bx-x d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block">Batal</span>
                                            </button>
                                            <button type="submit" class="btn btn-danger ml-1">
                                                <i class="bx bx-check d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block">Hapus Data</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        {{-- END DELETE MODAL --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if(Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection