@extends('layouts.app')

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Ubah Nota Pelunasan Supplier</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Nota Pelunasan Supplier
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-vertical-layouts">
    <div class="row match-height">
        <div class="col-md-6 col-12">
            <div class="card">
                <form class="form form-vertical" action="{{ route('notaPelunasanSuppliers.update', $notaPelunasanSupplier) }}" method="post">
                    @csrf
                    @method('patch')

                    <div class="card-body">                        
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Nomor Nota</label>
                                        <input type="text" class="form-control" value="{{ $notaPelunasanSupplier->no_nota }}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Nomor Nota Permintaan Material</label>
                                        <input type="text" class="form-control" value="{{ $notaPelunasanSupplier->pemesananMaterial->no_nota }}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Nominal Pembelian</label>
                                        <input data-inputmask="
                                            'alias': 'currency', 
                                            'prefix': 'Rp ',
                                            'placeholder': 'Rp 0',
                                            'rightAlign': 'true',
                                            'autoUnmask': 'true',
                                            'allowMinus': 'false',
                                            'removeMaskOnSubmit': 'true',
                                            'digits': '0'" 
                                            class="form-control" value="{{ $notaPelunasanSupplier->pemesananMaterial->grand_total }}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Sisa Tagihan</label>
                                        <input data-inputmask="
                                            'alias': 'currency', 
                                            'prefix': 'Rp ',
                                            'placeholder': 'Rp 0',
                                            'rightAlign': 'true',
                                            'autoUnmask': 'true',
                                            'allowMinus': 'false',
                                            'removeMaskOnSubmit': 'true',
                                            'digits': '0'" 
                                            class="form-control" value="{{ $notaPelunasanSupplier->pemesananMaterial->grand_total - $notaPelunasanSupplier->pemesananMaterial->nominal_pelunasan }}" readonly>
                                    </div>
                                    <div class="form-group mb-0">
                                        <label>Cara Pembayaran</label>
                                        <select aria-describedby="caraBayarHelp" class="form-control" name="cara_bayar">
                                            <option hidden>Pilih Cara Pembayaran</option>
                                            <option value="Tunai" {{ old('cara_bayar', $notaPelunasanSupplier->cara_bayar) == 'Tunai' ? 'selected' : '' }}>Tunai</option>
                                            <option value="Transfer" {{ old('cara_bayar', $notaPelunasanSupplier->cara_bayar) == 'Transfer' ? 'selected' : '' }}>Transfer</option>
                                        </select>

                                        @error('cara_bayar')
                                            <small id="caraBayarHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-0">
                                        <label>Nominal Pelunasan</label>
                                        <input data-inputmask="
                                            'alias': 'currency', 
                                            'prefix': 'Rp ',
                                            'placeholder': 'Rp 0',
                                            'rightAlign': 'true',
                                            'autoUnmask': 'true',
                                            'allowMinus': 'false',
                                            'removeMaskOnSubmit': 'true',
                                            'digits': '0', 
                                            'max': {{ $notaPelunasanSupplier->nominal_pelunasan + $notaPelunasanSupplier->pemesananMaterial->grand_total - $notaPelunasanSupplier->pemesananMaterial->nominal_pelunasan }}" 
                                            aria-describedby="nominalPelunasanHelp" class="form-control @error('nominal_pelunasan') is-invalid @enderror" name="nominal_pelunasan" value="{{ old('nominal_pelunasan', $notaPelunasanSupplier->nominal_pelunasan) }}">

                                        @error('nominal_pelunasan')
                                            <small id="nominalPelunasanHelp" class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Keterangan</label>
                                        <textarea class="form-control" name="keterangan" cols="30" rows="5">{{ old('keterangan', $notaPelunasanSupplier->keterangan) }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row ">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-warning">Ubah Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js" 
    integrity="sha512-6Jym48dWwVjfmvB0Hu3/4jn4TODd6uvkxdi9GNbBHwZ4nGcRxJUCaTkL3pVY6XUQABqFo3T58EMXFQztbjvAFQ==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/bindings/inputmask.binding.js" 
    integrity="sha512-J6WEJE0No+5Qqm9/T93q88yRQjvoAioXG4gzJ+eqZtLi+ZBgimZDkTiLWiljwrwnoQw+xwECQm282RJ6CrJnlw==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection