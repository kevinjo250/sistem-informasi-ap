@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Surat Perintah Kerja</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Surat Perintah Kerja
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-datatable">
    @can('suratPerintahKerja create')
        <div class="invoice-create-btn mb-1">
            <a href="{{ route('suratPerintahKerjas.create') }}" class="btn btn-primary btn-sm round glow align-items-center">Buat Surat Perintah Kerja</a>
        </div>
    @endcan
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body card-dashboard">
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>tgl produksi</th>
                                    <th>nomor SO</th>
                                    <th>produk</th>
                                    <th>ukuran</th>
                                    <th>warna</th>
                                    <th>qty</th>
                                    <th>status produksi
                                        <i class="bx bx-info-circle" 
                                            data-toggle="popover" data-placement="left" 
                                            data-container="body" data-original-title="Status Produksi" 
                                            data-content=
                                                "<small><strong>Pending:</strong> Menunggu permintaan material.</small> <br />
                                                <small><strong>Waiting for Materials:</strong> Menunggu persetujuan material.</small> <br />
                                                <small><strong>In Production:</strong> Dalam proses produksi.</small> <br />
                                                <small><strong>Done:</strong> Proses produksi telah selesai.</small>"
                                            data-html="true" data-trigger="hover">
                                        </i>
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($suratPerintahKerjas as $suratPerintahKerja)
                                    <tr>
                                        <td>{{ $suratPerintahKerja->no_nota }}</td>
                                        <td>
                                            <small class="text-muted">@date($suratPerintahKerja->tanggal_produksi)</small>
                                        </td>
                                        <td>
                                            <a href="{{ route('salesOrders.show', $suratPerintahKerja->produkSalesOrder->sales_order_id) }}">{{ $suratPerintahKerja->produkSalesOrder->salesOrder->no_nota }}</a>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $suratPerintahKerja->produkSalesOrder->produk->nama_produk }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $suratPerintahKerja->produkSalesOrder->produk->ukuran_produk }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">{{ $suratPerintahKerja->produkSalesOrder->produk->warna_produk }}</small>
                                        </td>
                                        <td class="text-right">
                                            <small class="text-muted">@number($suratPerintahKerja->kuantitas)</small>
                                        </td>
                                        <td>
                                            @switch($suratPerintahKerja->status_produksi)
                                                @case('waiting for materials')
                                                    <span class="badge badge-light-secondary badge-pill">{{ $suratPerintahKerja->status_produksi }}</span>
                                                    @break
                                                @case('in production')
                                                    <span class="badge badge-light-primary badge-pill">{{ $suratPerintahKerja->status_produksi }}</span>
                                                    @break
                                                @case('done')
                                                    <span class="badge badge-light-success badge-pill">{{ $suratPerintahKerja->status_produksi }}</span>
                                                    @break
                                                @default
                                                    <span class="badge badge-light-info badge-pill">{{ $suratPerintahKerja->status_produksi }}</span>
                                            @endswitch
                                        </td>
                                        <td>
                                            <div class="invoice-action">
                                                <a href="{{ route('suratPerintahKerjas.show', $suratPerintahKerja) }}" class="invoice-action-view cursor-pointer">
                                                    <i class="bx bx-show-alt"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('page js')
<script src="{{ asset('app-assets/js/scripts/datatables/datatable.js') }}"></script>
<script src="{{ asset('app-assets/js/scripts/popover/popover.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if(Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif

    @if(Session::has('error'))
        toastr['error']("{{ session('error') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection