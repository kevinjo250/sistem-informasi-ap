@extends('layouts.app')

@section('vendor css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css') }}">
@endsection

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Ubah Surat Perintah Kerja</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Surat Perintah Kerja
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="basic-vertical-layouts">
    <div class="row match-height">
        <div class="col-xl-6 col-md-10 col-12">
            <div class="card">
                <form class="form form-vertical" action="{{ route('suratPerintahKerjas.update', $suratPerintahKerja) }}" method="post">
                    @csrf
                    @method('patch')

                    <div class="card-header">
                        <small>Catatan: 1 SPK digunakan untuk 1 jenis produk.</small>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Produk</label>
                                    <input type="text" class="form-control" value="{{ $suratPerintahKerja->produkSalesOrder->salesOrder->no_nota }} - {{ $suratPerintahKerja->produkSalesOrder->produk->nama_produk }}} - {{ $suratPerintahKerja->produkSalesOrder->produk->ukuran_produk }} - {{ $suratPerintahKerja->produkSalesOrder->produk->warna_produk }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Tanggal Produksi</label>
                                    <input type="date" aria-describedby="tglProduksiHelp" class="form-control @error('tanggal_produksi') is-invalid @enderror" name="tanggal_produksi" value="{{ old('tanggal_produksi', $suratPerintahKerja->tanggal_produksi) }}">
    
                                    @error('tanggal_produksi')
                                        <small id="tglProduksiHelp" class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Kuantitas Produksi</label>
                                    <input data-inputmask="
                                        'alias': 'numeric',
                                        'groupSeparator': ',',
                                        'rightAlign': 'true',
                                        'autoUnmask': 'true',
                                        'allowMinus': 'false',
                                        'removeMaskOnSubmit': 'true',
                                        'digits': '0',
                                        'min': '1'" 
                                        aria-describedby="kuantitasHelp" class="form-control kuantitas @error('kuantitas') is-invalid @enderror" name="kuantitas" value="{{ old('kuantitas', $suratPerintahKerja->kuantitas) }}">
    
                                    @error('kuantitas')
                                        <small id="kuantitasHelp" class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Keterangan (Opsional)</label>
                                    <textarea class="form-control" name="keterangan" cols="30" rows="5" placeholder="Masukan keterangan">{{ old('keterangan', $suratPerintahKerja->keterangan) }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row ">
                            <div class="col-6">
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-warning">Ubah Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page vendor js')
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('script')
<script type="text/javascript">
    @if(Session::has('error'))
        toastr['error']("{{ session('error') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection