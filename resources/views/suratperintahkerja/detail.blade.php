@extends('layouts.app')

@section('page css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-invoice.css') }}">
@endsection

@section('breadcrumb')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="breadcrumbs-top">
        <h5 class="content-header-title float-left pr-1 mb-0">Detail Surat Perintah Kerja</h5>
        <div class="breadcrumb-wrapper d-none d-sm-block">
            <ol class="breadcrumb p-0 mb-0 pl-1">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active">Surat Perintah Kerja
                </li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<section class="invoice-view-wrapper">
    <div class="row">
        <!-- invoice view page -->
        <div class="col-8">
            <div class="card invoice-print-area">
                <div class="card-body pb-0 mx-25">
                    <!-- header section -->
                    <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <h4>{{ $suratPerintahKerja->no_nota }}</h4>
                        </div>
                        <div class="col-lg-8 col-md-12">
                            <div class="d-flex align-items-center justify-content-lg-end flex-wrap">
                                <span>Tanggal Produksi: @date($suratPerintahKerja->tanggal_produksi)</span>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="invoice-product-details table-responsive">
                    <table class="table table-borderless mb-0">
                        <thead>
                            <tr class="border-0">
                                <th scope="col">Produk</th>
                                <th scope="col">Ukuran</th>
                                <th scope="col">Warna</th>
                                <th scope="col">Qty</th>
                                <th scope="col">Satuan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $suratPerintahKerja->produkSalesOrder->produk->nama_produk }}</td>
                                <td>{{ $suratPerintahKerja->produkSalesOrder->produk->ukuran_produk }}</td>
                                <td>{{ $suratPerintahKerja->produkSalesOrder->produk->warna_produk }}</td>
                                <td>{{ $suratPerintahKerja->kuantitas }}</td>
                                <td>{{ $suratPerintahKerja->produkSalesOrder->produk->satuan_produk }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-body pt-0 mx-25">
                    <hr>
                    <div class="row">
                        <div class="col-4 col-sm-6 col-12 mt-75">
                            <p>{{ $suratPerintahKerja->keterangan }}</p>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-6">
                            @can('suratPerintahKerja edit')
                                <a href="{{ route('suratPerintahKerjas.edit', $suratPerintahKerja) }}" class="btn btn-warning">Ubah Data</a>
                            @endcan
                        </div>
                        <div class="col-6 d-flex justify-content-end">
                            @can('suratPerintahKerja delete')
                                <a href="#DeleteModal" data-toggle="modal" class="btn btn-danger">Hapus Data</a>
                            @endcan
                        </div>

                        {{-- BEGIN DELETE MODAL --}}
                        <div class="modal fade text-left" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h5 class="modal-title white" id="myModalLabel160">Hapus Data Surat Perintah Kerja</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="bx bx-x"></i>
                                        </button>
                                    </div>
                                    <form action="{{ route('suratPerintahKerjas.destroy', $suratPerintahKerja) }}" method="post">
                                        @csrf
                                        @method('delete')

                                        <div class="modal-body">
                                            <p>Apakah anda yakin untuk menghapus data tersebut?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                                <i class="bx bx-x d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block">Batal</span>
                                            </button>
                                            <button type="submit" class="btn btn-danger ml-1">
                                                <i class="bx bx-check d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block">Hapus Data</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        {{-- END DELETE MODAL --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script type="text/javascript">
    @if(Session::has('success'))
        toastr['success']("{{ session('success') }}", {
            tapToDismiss: true,
        });
    @endif
</script>
@endsection