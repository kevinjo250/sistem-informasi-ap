<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePemesananMaterialProdukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanan_material_produk', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pemesanan_material_id')->constrained();
            $table->foreignId('produk_id')->constrained();
            $table->integer('kuantitas')->default(0);
            $table->integer('harga')->default(0);
            $table->enum('status_material', ['pending', 'approved', 'open', 'partial', 'done'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemesanan_material_produk');
    }
}
