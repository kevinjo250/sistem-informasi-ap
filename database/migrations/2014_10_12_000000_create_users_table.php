<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->unique();
            $table->string('nama_karyawan');
            $table->string('alamat_karyawan');
            $table->string('email_karyawan')->unique()->nullable();
            $table->string('no_ktp', 16)->unique();
            $table->string('no_telepon', 12)->unique();
            $table->date('tanggal_lahir');
            $table->date('tanggal_masuk');
            $table->enum('status_karyawan', ['aktif', 'non aktif'])->default('aktif');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
