<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePemesananMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanan_materials', function (Blueprint $table) {
            $table->id();
            $table->string('no_nota')->unique();
            $table->date('tanggal_jatuh_tempo')->nullable();
            $table->integer('total')->default(0);
            $table->integer('diskon')->default(0);
            $table->mediumText('keterangan')->nullable();
            $table->enum('status_nota', ['draft', 'approved', 'open', 'done'])->default('draft');
            $table->enum('status_pembayaran', ['unpaid', 'partially paid', 'paid', 'overdue'])->default('unpaid');
            $table->foreignId('supplier_id')->constrained();
            $table->foreignId('user_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemesanan_materials');
    }
}
