<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produks', function (Blueprint $table) {
            $table->id();
            $table->string('nama_produk');
            $table->enum('jenis_produk', ['material', 'finished product', 'reject']);
            $table->string('ukuran_produk');
            $table->string('warna_produk');
            $table->integer('safety_stok');
            $table->integer('stok_produk')->default(0);
            $table->string('satuan_produk');
            $table->integer('harga_pokok')->default(0);            
            $table->integer('harga_jual');
            $table->mediumText('keterangan')->nullable();
            $table->string('gambar_produk')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produks');
    }
}
