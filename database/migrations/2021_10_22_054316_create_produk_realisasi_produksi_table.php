<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdukRealisasiProduksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk_realisasi_produksi', function (Blueprint $table) {
            $table->id();
            $table->foreignId('produk_id')->constrained();
            $table->foreignId('realisasi_produksi_id')->constrained();
            $table->integer('kuantitas_rusak');
            $table->integer('kuantitas_sisa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk_realisasi_produksi');
    }
}
