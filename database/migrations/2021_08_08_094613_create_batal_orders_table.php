<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatalOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batal_orders', function (Blueprint $table) {
            $table->id();
            $table->string('no_nota')->unique();
            $table->mediumText('keterangan')->nullable();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('sales_order_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batal_orders');
    }
}
