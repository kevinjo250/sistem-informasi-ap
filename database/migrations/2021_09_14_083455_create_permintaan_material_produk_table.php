<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanMaterialProdukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_material_produk', function (Blueprint $table) {
            $table->id();
            $table->foreignId('permintaan_material_id')->constrained();
            $table->foreignId('produk_id')->constrained();
            $table->integer('kuantitas');
            $table->integer('hpp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_material_produk');
    }
}
