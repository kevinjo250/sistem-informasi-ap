<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJurnalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jurnals', function (Blueprint $table) {
            $table->id();
            $table->dateTime('tanggal');
            $table->enum('jenis_jurnal', ['JU', 'JT']);
            $table->enum('jenis_transaksi', ['penjualan', 'pembatalan', 'pembayaran pelanggan', 'pengiriman', 'pembelian', 'penerimaan material', 'pelunasan supplier', 'permintaan material', 'produksi', 'lain-lain', 'penutup']);
            $table->string('nomor_bukti');
            $table->mediumText('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jurnals');
    }
}
