<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_orders', function (Blueprint $table) {
            $table->id();
            $table->string('no_nota')->unique();
            $table->date('tanggal_kirim');
            $table->date('tanggal_jatuh_tempo');
            $table->integer('total');
            $table->integer('diskon');
            $table->mediumText('keterangan')->nullable();
            $table->enum('status_nota', ['draft', 'confirmed', 'on going', 'cancel', 'done'])->default('draft');
            $table->enum('status_pembayaran', ['unpaid', 'partially paid', 'paid', 'overdue', 'cancel'])->default('unpaid');
            $table->foreignId('user_id')->constrained();
            $table->foreignId('pelanggan_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_orders');
    }
}
