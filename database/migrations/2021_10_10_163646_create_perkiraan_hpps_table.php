<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerkiraanHppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perkiraan_hpps', function (Blueprint $table) {
            $table->id();
            $table->foreignId('bill_of_material_id')->constrained();
            $table->integer('biaya_material');
            $table->integer('biaya_pekerja');
            $table->integer('biaya_overhead');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perhitungan_hpps');
    }
}
