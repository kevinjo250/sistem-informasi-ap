<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotaPelunasanSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nota_pelunasan_suppliers', function (Blueprint $table) {
            $table->id();
            $table->string('no_nota')->unique();
            $table->enum('cara_bayar', ['Tunai', 'Transfer']);
            $table->integer('nominal_pelunasan');
            $table->mediumText('keterangan')->nullable();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('pemesanan_material_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nota_pelunasan_suppliers');
    }
}
