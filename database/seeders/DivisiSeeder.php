<?php

namespace Database\Seeders;

use App\Models\Divisi;
use Illuminate\Database\Seeder;

class DivisiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Divisi::firstOrCreate([
            'nama_divisi' => 'Administrator'
        ]);

        Divisi::firstOrCreate([
            'nama_divisi' => 'Direktur'
        ]);

        Divisi::firstOrCreate([
            'nama_divisi' => 'Kantor Pemasaran'
        ]);

        Divisi::firstOrCreate([
            'nama_divisi' => 'Pabrik Produksi'
        ]);
    }
}
