<?php

namespace Database\Seeders;

use App\Models\BillOfMaterial;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BillOfMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BillOfMaterial::firstOrCreate([
            'produk_id' => 1
        ]);

        BillOfMaterial::firstOrCreate([
            'produk_id' => 2
        ]);

        DB::table('bill_of_material_produk')->insert([
            [
                'bill_of_material_id' => 1,
                'produk_id' => 3,
                'kuantitas' => 50,
            ],
            [
                'bill_of_material_id' => 1,
                'produk_id' => 4,
                'kuantitas' => 50,
            ],
            [
                'bill_of_material_id' => 1,
                'produk_id' => 5,
                'kuantitas' => 10,
            ],
            [
                'bill_of_material_id' => 2,
                'produk_id' => 3,
                'kuantitas' => 50,
            ],
            [
                'bill_of_material_id' => 2,
                'produk_id' => 4,
                'kuantitas' => 50,
            ],
            [
                'bill_of_material_id' => 2,
                'produk_id' => 5,
                'kuantitas' => 10,
            ]
        ]);
    }
}
