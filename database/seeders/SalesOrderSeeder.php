<?php

namespace Database\Seeders;

use App\Models\SalesOrder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SalesOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SalesOrder::firstOrCreate([
            'no_nota' => 'SO/2021-10-22/1',
            'tanggal_kirim' => '2021-10-30',
            'tanggal_jatuh_tempo' => '2021-11-30',
            'total' => 5000000,
            'diskon' => 0,
            'status_nota' => 'done',
            'status_pembayaran' => 'paid',
            'user_id' => 1,
            'pelanggan_id' => 1,
            'created_at' => '2021-10-22 04:06:16'
        ]);

        SalesOrder::firstOrCreate([
            'no_nota' => 'SO/2021-10-22/2',
            'tanggal_kirim' => '2021-10-29',
            'tanggal_jatuh_tempo' => '2021-11-29',
            'total' => 3000000,
            'diskon' => 0,
            'status_nota' => 'done',
            'status_pembayaran' => 'paid',
            'user_id' => 1,
            'pelanggan_id' => 2,
            'created_at' => '2021-10-22 04:56:45'
        ]);

        DB::table('produk_sales_order')->insert([
            [
                'sales_order_id' => 1,
                'produk_id' => 1,
                'kuantitas' => 3,
                'harga' => 1000000,
                'status_order' => 'shipped',
            ],
            [
                'sales_order_id' => 1,
                'produk_id' => 2,
                'kuantitas' => 2,
                'harga' => 1000000,
                'status_order' => 'shipped',
            ],
            [
                'sales_order_id' => 2,
                'produk_id' => 1,
                'kuantitas' => 3,
                'harga' => 1000000,
                'status_order' => 'shipped',
            ]
        ]);
    }
}
