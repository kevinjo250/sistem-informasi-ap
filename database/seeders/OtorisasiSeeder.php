<?php

namespace Database\Seeders;

use App\Models\Otorisasi;
use Illuminate\Database\Seeder;

class OtorisasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'nama_otorisasi' => 'supplier access'],
            ['id' => 2, 'nama_otorisasi' => 'supplier create'],
            ['id' => 3, 'nama_otorisasi' => 'supplier edit'],
            ['id' => 4, 'nama_otorisasi' => 'supplier delete'],
            ['id' => 5, 'nama_otorisasi' => 'pelanggan access'],
            ['id' => 6, 'nama_otorisasi' => 'pelanggan create'],
            ['id' => 7, 'nama_otorisasi' => 'pelanggan edit'],
            ['id' => 8, 'nama_otorisasi' => 'pelanggan delete'],
            ['id' => 9, 'nama_otorisasi' => 'produk access'],
            ['id' => 10, 'nama_otorisasi' => 'produk create'],
            ['id' => 11, 'nama_otorisasi' => 'produk edit'],
            ['id' => 12, 'nama_otorisasi' => 'divisi access'],
            ['id' => 13, 'nama_otorisasi' => 'divisi create'],
            ['id' => 14, 'nama_otorisasi' => 'divisi edit'],
            ['id' => 15, 'nama_otorisasi' => 'divisi delete'],
            ['id' => 16, 'nama_otorisasi' => 'jabatan access'],
            ['id' => 17, 'nama_otorisasi' => 'jabatan create'],
            ['id' => 18, 'nama_otorisasi' => 'jabatan edit'],
            ['id' => 19, 'nama_otorisasi' => 'jabatan delete'],
            ['id' => 20, 'nama_otorisasi' => 'otorisasi access'],
            ['id' => 21, 'nama_otorisasi' => 'karyawan access'],
            ['id' => 22, 'nama_otorisasi' => 'karyawan create'],
            ['id' => 23, 'nama_otorisasi' => 'karyawan edit'],
            ['id' => 24, 'nama_otorisasi' => 'akun access'],
            ['id' => 25, 'nama_otorisasi' => 'salesOrder access'],
            ['id' => 26, 'nama_otorisasi' => 'salesOrder create'],
            ['id' => 27, 'nama_otorisasi' => 'salesOrder edit'],
            ['id' => 28, 'nama_otorisasi' => 'salesOrder delete'],
            ['id' => 29, 'nama_otorisasi' => 'salesOrder confirm'],            
            ['id' => 30, 'nama_otorisasi' => 'batalOrder access'],            
            ['id' => 31, 'nama_otorisasi' => 'batalOrder create'],            
            ['id' => 32, 'nama_otorisasi' => 'batalOrder edit'],            
            ['id' => 33, 'nama_otorisasi' => 'notaPembayaran access'],            
            ['id' => 34, 'nama_otorisasi' => 'notaPembayaran create'],            
            ['id' => 35, 'nama_otorisasi' => 'notaPembayaran edit'],            
            ['id' => 36, 'nama_otorisasi' => 'notaPembayaran delete'],            
            ['id' => 37, 'nama_otorisasi' => 'suratJalan access'],            
            ['id' => 38, 'nama_otorisasi' => 'suratJalan create'],            
            ['id' => 39, 'nama_otorisasi' => 'suratJalan edit'],            
            ['id' => 40, 'nama_otorisasi' => 'suratJalan delete'],            
            ['id' => 41, 'nama_otorisasi' => 'pemesananMaterial access'],            
            ['id' => 42, 'nama_otorisasi' => 'pemesananMaterial create'],            
            ['id' => 43, 'nama_otorisasi' => 'pemesananMaterial edit'],
            ['id' => 44, 'nama_otorisasi' => 'pemesananMaterial delete'],
            ['id' => 45, 'nama_otorisasi' => 'pemesananMaterial confirm'],
            ['id' => 46, 'nama_otorisasi' => 'pemesananMaterial selesai'],            
            ['id' => 47, 'nama_otorisasi' => 'penerimaanMaterial access'],            
            ['id' => 48, 'nama_otorisasi' => 'penerimaanMaterial create'],            
            ['id' => 49, 'nama_otorisasi' => 'penerimaanMaterial edit'],
            ['id' => 50, 'nama_otorisasi' => 'penerimaanMaterial delete'],            
            ['id' => 51, 'nama_otorisasi' => 'notaPelunasanSupplier access'],            
            ['id' => 52, 'nama_otorisasi' => 'notaPelunasanSupplier create'],            
            ['id' => 53, 'nama_otorisasi' => 'notaPelunasanSupplier edit'],
            ['id' => 54, 'nama_otorisasi' => 'notaPelunasanSupplier delete'],            
            ['id' => 55, 'nama_otorisasi' => 'billOfMaterial access'],            
            ['id' => 56, 'nama_otorisasi' => 'billOfMaterial create'],            
            ['id' => 57, 'nama_otorisasi' => 'billOfMaterial edit'],
            ['id' => 58, 'nama_otorisasi' => 'billOfMaterial delete'],            
            ['id' => 59, 'nama_otorisasi' => 'perkiraanHpp access'],         
            ['id' => 60, 'nama_otorisasi' => 'suratPerintahKerja access'],            
            ['id' => 61, 'nama_otorisasi' => 'suratPerintahKerja create'],            
            ['id' => 62, 'nama_otorisasi' => 'suratPerintahKerja edit'],
            ['id' => 63, 'nama_otorisasi' => 'suratPerintahKerja delete'],            
            ['id' => 64, 'nama_otorisasi' => 'permintaanMaterial access'],            
            ['id' => 65, 'nama_otorisasi' => 'permintaanMaterial create'],
            ['id' => 66, 'nama_otorisasi' => 'permintaanMaterial delete'],
            ['id' => 67, 'nama_otorisasi' => 'permintaanMaterial confirm'],            
            ['id' => 68, 'nama_otorisasi' => 'realisasiProduksi access'],            
            ['id' => 69, 'nama_otorisasi' => 'realisasiProduksi create'],            
            ['id' => 70, 'nama_otorisasi' => 'realisasiProduksi edit'],
            ['id' => 71, 'nama_otorisasi' => 'realisasiProduksi delete'],            
            ['id' => 72, 'nama_otorisasi' => 'akuntansi access'], 
        ];

        foreach ($items as $item) {
            Otorisasi::create($item);
        }
    }
}
