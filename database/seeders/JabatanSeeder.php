<?php

namespace Database\Seeders;

use App\Models\Jabatan;
use Illuminate\Database\Seeder;

class JabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jabatan::firstOrCreate([
            'nama_jabatan' => 'Administrator',
        ]);

        Jabatan::firstOrCreate([
            'nama_jabatan' => 'Direktur',
        ]);

        Jabatan::firstOrCreate([
            'nama_jabatan' => 'Kepala Kantor',
        ]);

        Jabatan::firstOrCreate([
            'nama_jabatan' => 'Admin Kas',
        ]);

        Jabatan::firstOrCreate([
            'nama_jabatan' => 'Admin Presensi & Purchasing',
        ]);

        Jabatan::firstOrCreate([
            'nama_jabatan' => 'Admin Pengiriman',
        ]);

        Jabatan::firstOrCreate([
            'nama_jabatan' => 'Admin Cetak Faktur',
        ]);

        Jabatan::firstOrCreate([
            'nama_jabatan' => 'Manajer Produksi',
        ]);

        Jabatan::firstOrCreate([
            'nama_jabatan' => 'Kepala Gudang',
        ]);
    }
}
