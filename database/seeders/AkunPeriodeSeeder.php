<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AkunPeriodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'akun_id' => '101', 'periode_id' => 1, 'saldo_awal' => 0, 'saldo_akhir' => 5000000],
            ['id' => 2, 'akun_id' => '102', 'periode_id' => 1, 'saldo_awal' => 0, 'saldo_akhir' => 3000000],
            ['id' => 3, 'akun_id' => '103', 'periode_id' => 1, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 4, 'akun_id' => '104', 'periode_id' => 1, 'saldo_awal' => 0, 'saldo_akhir' => 3680000],
            ['id' => 5, 'akun_id' => '105', 'periode_id' => 1, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 6, 'akun_id' => '106', 'periode_id' => 1, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 7, 'akun_id' => '107', 'periode_id' => 1, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 8, 'akun_id' => '201', 'periode_id' => 1, 'saldo_awal' => 0, 'saldo_akhir' => 4480000],
            ['id' => 9, 'akun_id' => '202', 'periode_id' => 1, 'saldo_awal' => 0, 'saldo_akhir' => 3200000],
            ['id' => 10, 'akun_id' => '301', 'periode_id' => 1, 'saldo_awal' => 0, 'saldo_akhir' => 4000000],
            ['id' => 11, 'akun_id' => '302', 'periode_id' => 1, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 12, 'akun_id' => '401', 'periode_id' => 1, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 13, 'akun_id' => '402', 'periode_id' => 1, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 14, 'akun_id' => '501', 'periode_id' => 1, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 15, 'akun_id' => '502', 'periode_id' => 1, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 16, 'akun_id' => '101', 'periode_id' => 2, 'saldo_awal' => 5000000, 'saldo_akhir' => 0],
            ['id' => 17, 'akun_id' => '102', 'periode_id' => 2, 'saldo_awal' => 3000000, 'saldo_akhir' => 0],
            ['id' => 18, 'akun_id' => '103', 'periode_id' => 2, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 19, 'akun_id' => '104', 'periode_id' => 2, 'saldo_awal' => 3680000, 'saldo_akhir' => 0],
            ['id' => 20, 'akun_id' => '105', 'periode_id' => 2, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 21, 'akun_id' => '106', 'periode_id' => 2, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 22, 'akun_id' => '107', 'periode_id' => 2, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 23, 'akun_id' => '201', 'periode_id' => 2, 'saldo_awal' => 4480000, 'saldo_akhir' => 0],
            ['id' => 24, 'akun_id' => '202', 'periode_id' => 2, 'saldo_awal' => 3200000, 'saldo_akhir' => 0],
            ['id' => 25, 'akun_id' => '301', 'periode_id' => 2, 'saldo_awal' => 4000000, 'saldo_akhir' => 0],
            ['id' => 26, 'akun_id' => '302', 'periode_id' => 2, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 27, 'akun_id' => '401', 'periode_id' => 2, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 28, 'akun_id' => '402', 'periode_id' => 2, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 29, 'akun_id' => '501', 'periode_id' => 2, 'saldo_awal' => 0, 'saldo_akhir' => 0],
            ['id' => 30, 'akun_id' => '502', 'periode_id' => 2, 'saldo_awal' => 0, 'saldo_akhir' => 0],
        ];

        DB::table('akun_periode')->insert($items);
    }
}
