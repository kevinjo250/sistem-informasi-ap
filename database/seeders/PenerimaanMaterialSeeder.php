<?php

namespace Database\Seeders;

use App\Models\PenerimaanMaterial;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PenerimaanMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PenerimaanMaterial::firstOrCreate([
            'no_nota' => 'PS/2021-10-22/1',
            'pemesanan_material_id' => 1,
            'user_id' => 1,
            'created_at' => '2021-10-22 04:01:51'
        ]);

        DB::table('penerimaan_material_produk')->insert([
            [
                'penerimaan_material_id' => 1,
                'produk_id' => 3,
                'kuantitas' => 2000
            ],
            [
                'penerimaan_material_id' => 1,
                'produk_id' => 4,
                'kuantitas' => 2000
            ],
            [
                'penerimaan_material_id' => 1,
                'produk_id' => 5,
                'kuantitas' => 2000
            ],
        ]);
    }
}
