<?php

namespace Database\Seeders;

use App\Models\Pelanggan;
use Illuminate\Database\Seeder;

class PelangganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pelanggan::firstOrCreate([
            'nama_pelanggan' => 'PT. ABC',
            'alamat_pelanggan' => 'Jalan Ahmad Yani 12',
            'kota_pelanggan' => 'Surabaya',
            'email_pelanggan' => 'pelanggan1@pelanggan.com',
            'no_ktp' => '1897598623805690',
            'no_npwp' => null,
            'no_telepon' => '081278011849',
        ]);

        Pelanggan::firstOrCreate([
            'nama_pelanggan' => 'Toko Makmur',
            'alamat_pelanggan' => 'Jalan Mawar 20',
            'kota_pelanggan' => 'Pasuruan',
            'email_pelanggan' => 'pelanggan2@pelanggan.com',
            'no_ktp' => '1897597853805790',
            'no_npwp' => null,
            'no_telepon' => '081275311849',
        ]);
    }
}
