<?php

namespace Database\Seeders;

use App\Models\SuratPerintahKerja;
use Illuminate\Database\Seeder;

class SuratPerintahKerjaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SuratPerintahKerja::firstOrCreate([
            'no_nota' => 'SPK/2021-10-22/1',
            'tanggal_produksi' => '2021-10-23',
            'produk_sales_order_id' => 1,
            'kuantitas' => 3,
            'status_produksi' => 'done',
            'created_at' => '2021-10-22 04:47:45'
        ]);

        SuratPerintahKerja::firstOrCreate([
            'no_nota' => 'SPK/2021-10-22/2',
            'tanggal_produksi' => '2021-11-23',
            'produk_sales_order_id' => 2,
            'kuantitas' => 2,
            'status_produksi' => 'done',
            'created_at' => '2021-10-22 04:47:59'
        ]);

        SuratPerintahKerja::firstOrCreate([
            'no_nota' => 'SPK/2021-10-22/3',
            'tanggal_produksi' => '2021-10-24',
            'produk_sales_order_id' => 3,
            'kuantitas' => 3,
            'status_produksi' => 'done',
            'created_at' => '2021-10-22 04:57:58'
        ]);
    }
}
