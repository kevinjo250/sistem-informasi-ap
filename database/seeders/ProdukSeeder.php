<?php

namespace Database\Seeders;

use App\Models\Produk;
use Illuminate\Database\Seeder;

class ProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Produk::firstOrCreate([
            'nama_produk' => 'Full Latex - Essential Latex',
            'jenis_produk' => 'finished product',
            'ukuran_produk' => '200x180x180',
            'warna_produk' => 'Hitam',
            'safety_stok' => 5,
            'stok_produk' => 0,
            'satuan_produk' => 'pcs',
            'harga_pokok' => 500000,
            'harga_jual' => 1000000,
            'keterangan' => '',
        ]);

        Produk::firstOrCreate([
            'nama_produk' => 'Full Latex - Essential Latex',
            'jenis_produk' => 'finished product',
            'ukuran_produk' => '200x180x180',
            'warna_produk' => 'Gold',
            'safety_stok' => 5,
            'stok_produk' => 0,
            'satuan_produk' => 'pcs',
            'harga_pokok' => 500000,
            'harga_jual' => 1000000,
            'keterangan' => '',
        ]);

        Produk::firstOrCreate([
            'nama_produk' => 'Kawat - Kawat Kecil',
            'jenis_produk' => 'material',
            'ukuran_produk' => '1x1x2',
            'warna_produk' => 'Hitam',
            'safety_stok' => 100,
            'stok_produk' => 1600,
            'satuan_produk' => 'pcs',
            'harga_pokok' => 500,
            'harga_jual' => 0,
            'keterangan' => '',
        ]);

        Produk::firstOrCreate([
            'nama_produk' => 'Kawat - Kawat Besar',
            'jenis_produk' => 'material',
            'ukuran_produk' => '1x1x2',
            'warna_produk' => 'Hitam',
            'safety_stok' => 100,
            'stok_produk' => 1600,
            'satuan_produk' => 'pcs',
            'harga_pokok' => 600,
            'harga_jual' => 0,
            'keterangan' => '',
        ]);

        Produk::firstOrCreate([
            'nama_produk' => 'Spons - Spons Latex',
            'jenis_produk' => 'material',
            'ukuran_produk' => '10x20x1 meter',
            'warna_produk' => 'Hitam',
            'safety_stok' => 100,
            'stok_produk' => 1920,
            'satuan_produk' => 'lembar',
            'harga_pokok' => 1000,
            'harga_jual' => 0,
            'keterangan' => '',
        ]);
    }
}
