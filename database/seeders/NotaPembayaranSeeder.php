<?php

namespace Database\Seeders;

use App\Models\NotaPembayaran;
use Illuminate\Database\Seeder;

class NotaPembayaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        NotaPembayaran::firstOrCreate([
            'no_nota' => 'NPO/2021-10-22/1',
            'cara_bayar' => 'Tunai',
            'nominal_bayar' => 5000000,
            'keterangan' => 'lunas',
            'user_id' => 1,
            'sales_order_id' => 1,
            'created_at' => '2021-10-22 04:50:11'
        ]);

        NotaPembayaran::firstOrCreate([
            'no_nota' => 'NPO/2021-10-22/2',
            'cara_bayar' => 'Transfer',
            'nominal_bayar' => 3000000,
            'keterangan' => 'lunas',
            'user_id' => 1,
            'sales_order_id' => 2,
            'created_at' => '2021-10-22 05:29:39'
        ]);
    }
}
