<?php

namespace Database\Seeders;

use App\Models\Periode;
use Illuminate\Database\Seeder;

class PeriodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Periode::firstOrCreate([
            'tanggal_awal' => '2021-10-01',
            'tanggal_akhir' => '2021-10-31',
        ]);

        Periode::firstOrCreate([
            'tanggal_awal' => '2021-11-01',
            'tanggal_akhir' => '2021-11-30',
        ]);
    }
}
