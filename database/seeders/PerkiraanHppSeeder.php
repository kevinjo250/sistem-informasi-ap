<?php

namespace Database\Seeders;

use App\Models\PerkiraanHpp;
use Illuminate\Database\Seeder;

class PerkiraanHppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PerkiraanHpp::firstOrCreate([
            'bill_of_material_id' => 1,
            'biaya_material' => 65000,
            'biaya_pekerja' => 400000,
            'biaya_overhead' => 35000
        ]);

        PerkiraanHpp::firstOrCreate([
            'bill_of_material_id' => 2,
            'biaya_material' => 65000,
            'biaya_pekerja' => 400000,
            'biaya_overhead' => 35000
        ]);
    }
}
