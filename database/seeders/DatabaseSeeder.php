<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            OtorisasiSeeder::class,
            JabatanSeeder::class,
            JabatanOtorisasiSeeder::class,
            DivisiSeeder::class,
            UserSeeder::class,
            SupplierSeeder::class,
            PelangganSeeder::class,
            // AkunSeeder::class,
            // ProdukSeeder::class,
            // PeriodeSeeder::class,
            // AkunPeriodeSeeder::class,
            // BillOfMaterialSeeder::class,
            // PerkiraanHppSeeder::class,
            // PemesananMaterialSeeder::class,
            // PenerimaanMaterialSeeder::class,
            // SalesOrderSeeder::class,
            // SuratPerintahKerjaSeeder::class,
            // PermintaanMaterialSeeder::class,
            // RealisasiProduksiSeeder::class,
            // SuratJalanSeeder::class,
            // NotaPembayaranSeeder::class,
            // JurnalSeeder::class,
        ]);
    }
}
