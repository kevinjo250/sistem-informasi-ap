<?php

namespace Database\Seeders;

use App\Models\PermintaanMaterial;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermintaanMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PermintaanMaterial::firstOrCreate([
            'no_nota' => 'PM/2021-10-22/1',
            'surat_perintah_kerja_id' => 1,
            'user_id' => 1,
            'status_nota' => 'approved',
            'created_at' => '2021-10-22 04:48:13'
        ]);

        PermintaanMaterial::firstOrCreate([
            'no_nota' => 'PM/2021-10-22/2',
            'surat_perintah_kerja_id' => 2,
            'user_id' => 1,
            'status_nota' => 'approved',
            'created_at' => '2021-10-22 04:48:29'
        ]);

        PermintaanMaterial::firstOrCreate([
            'no_nota' => 'PM/2021-10-22/3',
            'surat_perintah_kerja_id' => 3,
            'user_id' => 1,
            'status_nota' => 'approved',
            'created_at' => '2021-10-22 04:58:10'
        ]);

        DB::table('permintaan_material_produk')->insert([
            [
                'permintaan_material_id' => 1,
                'produk_id' => 3,
                'kuantitas' => 150,
                'hpp' => 500
            ],
            [
                'permintaan_material_id' => 1,
                'produk_id' => 4,
                'kuantitas' => 150,
                'hpp' => 600
            ],
            [
                'permintaan_material_id' => 1,
                'produk_id' => 5,
                'kuantitas' => 30,
                'hpp' => 1000
            ],
            [
                'permintaan_material_id' => 2,
                'produk_id' => 3,
                'kuantitas' => 100,
                'hpp' => 500
            ],
            [
                'permintaan_material_id' => 2,
                'produk_id' => 4,
                'kuantitas' => 100,
                'hpp' => 600
            ],
            [
                'permintaan_material_id' => 2,
                'produk_id' => 5,
                'kuantitas' => 20,
                'hpp' => 1000
            ],
            [
                'permintaan_material_id' => 3,
                'produk_id' => 3,
                'kuantitas' => 150,
                'hpp' => 500
            ],
            [
                'permintaan_material_id' => 3,
                'produk_id' => 4,
                'kuantitas' => 150,
                'hpp' => 600
            ],
            [
                'permintaan_material_id' => 3,
                'produk_id' => 5,
                'kuantitas' => 30,
                'hpp' => 1000
            ],
        ]);
    }
}
