<?php

namespace Database\Seeders;

use App\Models\Otorisasi;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JabatanOtorisasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $otorisasis = Otorisasi::all();

        foreach($otorisasis as $otorisasi)
        {            
            DB::table('jabatan_otorisasi')->insert([
                'jabatan_id' => 1,
                'otorisasi_id' => $otorisasi->id,
            ]);
        }

        foreach($otorisasis as $otorisasi)
        {            
            DB::table('jabatan_otorisasi')->insert([
                'jabatan_id' => 2,
                'otorisasi_id' => $otorisasi->id,
            ]);
        }
    }
}
