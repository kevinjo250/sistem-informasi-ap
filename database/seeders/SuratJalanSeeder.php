<?php

namespace Database\Seeders;

use App\Models\SuratJalan;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuratJalanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SuratJalan::firstOrCreate([
            'no_nota' => 'SJ/2021-10-22/1',
            'tanggal_kirim' => '2021-10-24',
            'sales_order_id' => 1,
            'user_id' => 1,
            'created_at' => '2021-10-22 04:50:29'
        ]);

        SuratJalan::firstOrCreate([
            'no_nota' => 'SJ/2021-10-22/2',
            'tanggal_kirim' => '2021-10-25',
            'sales_order_id' => 2,
            'user_id' => 1,
            'created_at' => '2021-11-22 05:29:54'
        ]);

        DB::table('produk_surat_jalan')->insert([
            [
                'surat_jalan_id' => 1,
                'produk_id' => 1,
                'kuantitas' => 3
            ],
            [
                'surat_jalan_id' => 1,
                'produk_id' => 2,
                'kuantitas' => 2
            ],
            [
                'surat_jalan_id' => 2,
                'produk_id' => 1,
                'kuantitas' => 3
            ],
        ]);
    }
}
