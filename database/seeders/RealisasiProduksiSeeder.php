<?php

namespace Database\Seeders;

use App\Models\RealisasiProduksi;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RealisasiProduksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RealisasiProduksi::firstOrCreate([
            'no_nota' => 'RP/2021-10-22/1',
            'surat_perintah_kerja_id' => 1,
            'tanggal_mulai' => '2021-10-23 11:48:45',
            'tanggal_selesai' => '2021-10-23 15:48:48',
            'kuantitas_selesai' => 3,
            'biaya_material' => 195000,
            'biaya_pekerja' => 1200000,
            'biaya_overhead' => 105000,
            'user_id' => 1,
            'created_at' => '2021-10-22 04:48:55'
        ]);

        RealisasiProduksi::firstOrCreate([
            'no_nota' => 'RP/2021-10-22/2',
            'surat_perintah_kerja_id' => 2,
            'tanggal_mulai' => '2021-10-23 15:49:06',
            'tanggal_selesai' => '2021-10-23 17:49:16',
            'kuantitas_selesai' => 2,
            'biaya_material' => 130000,
            'biaya_pekerja' => 800000,
            'biaya_overhead' => 70000,
            'user_id' => 1,
            'created_at' => '2021-10-22 04:49:22'
        ]);

        RealisasiProduksi::firstOrCreate([
            'no_nota' => 'RP/2021-10-22/3',
            'surat_perintah_kerja_id' => 3,
            'tanggal_mulai' => '2021-10-24 11:58:23',
            'tanggal_selesai' => '2021-10-24 14:58:26',
            'kuantitas_selesai' => 3,
            'biaya_material' => 195000,
            'biaya_pekerja' => 1200000,
            'biaya_overhead' => 105000,
            'user_id' => 1,
            'created_at' => '2021-10-22 04:58:31'
        ]);
    }
}
