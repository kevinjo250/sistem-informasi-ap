<?php

namespace Database\Seeders;

use App\Models\Supplier;
use Illuminate\Database\Seeder;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Supplier::firstOrCreate([
            'nama_supplier' => 'PT. XYZ',
            'alamat_supplier' => 'Jalan Ahmad Yani 12',
            'kota_supplier' => 'Surabaya',
            'email_supplier' => 'supplier1@supplier.com',
            'no_telepon' => '081233911849',
        ]);

        Supplier::firstOrCreate([
            'nama_supplier' => 'PT. Anugerah Jaya',
            'alamat_supplier' => 'Jalan Ngagel Utara 22',
            'kota_supplier' => 'Surabaya',
            'email_supplier' => 'supplier2@supplier.com',
            'no_telepon' => '081233911000',
        ]);

        Supplier::firstOrCreate([
            'nama_supplier' => 'UD. Kayu Jati',
            'alamat_supplier' => 'Jalan Mojosari 29',
            'kota_supplier' => 'Mojokerto',
            'email_supplier' => 'supplier3@supplier.com',
            'no_telepon' => '081233587000',
        ]);
    }
}
