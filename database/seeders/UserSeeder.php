<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
            'username' => 'administrator',
            'nama_karyawan' => 'Kevin Jonathan',
            'alamat_karyawan' => 'Rungkut Tengah V Surabaya',
            'email_karyawan' => 'kevinjo250@gmail.com',
            'no_ktp' => '0123456789012345',
            'no_telepon' => '081233911680',
            'tanggal_lahir' => '1998-09-25',
            'tanggal_masuk' => '2019-05-25',
            'password' => '$2y$10$uw33P2mYmKOo4C7gkoE.meJMLdyWPhlXhub22dXAkia8QqJpTkhwW', //administrator123
            'status_karyawan' => 'aktif',
            'divisi_id' => 1,
            'jabatan_id'=> 1,
        ]);

        User::firstOrCreate([
            'username' => 'bob',
            'nama_karyawan' => 'Bobby Mandala',
            'alamat_karyawan' => 'Citraland VV Surabaya',
            'no_ktp' => '0123456789012344',
            'no_telepon' => '081245911678',
            'tanggal_lahir' => '1990-09-25',
            'tanggal_masuk' => '2010-05-25',
            'password' => '$2y$10$MqsQQvEUVqyHHmT7yMe9KuwAh7NYSpEa1Ze8Q/RFX138rdMJN2s1m', //password123
            'status_karyawan' => 'aktif',
            'divisi_id' => 2,
            'jabatan_id'=> 2,
        ]);

        User::firstOrCreate([
            'username' => 'abigail',
            'nama_karyawan' => 'Abigail Otwell',
            'alamat_karyawan' => 'Ngagel Jaya Utara X Surabaya',
            'no_ktp' => '0123456186712344',
            'no_telepon' => '081297811678',
            'tanggal_lahir' => '1995-01-11',
            'tanggal_masuk' => '2015-05-01',
            'password' => '$2y$10$MqsQQvEUVqyHHmT7yMe9KuwAh7NYSpEa1Ze8Q/RFX138rdMJN2s1m', //password123
            'status_karyawan' => 'aktif',
            'divisi_id' => 3,
            'jabatan_id'=> 3,
        ]);

        User::firstOrCreate([
            'username' => 'alice',
            'nama_karyawan' => 'Alice Surya',
            'alamat_karyawan' => 'Raya Kenjeran IX Surabaya',
            'no_ktp' => '5891456186712344',
            'no_telepon' => '081297855878',
            'tanggal_lahir' => '1992-02-02',
            'tanggal_masuk' => '2010-12-01',
            'password' => '$2y$10$MqsQQvEUVqyHHmT7yMe9KuwAh7NYSpEa1Ze8Q/RFX138rdMJN2s1m', //password123
            'status_karyawan' => 'aktif',
            'divisi_id' => 3,
            'jabatan_id'=> 4,
        ]);

        User::firstOrCreate([
            'username' => 'steven',
            'nama_karyawan' => 'Steven W. L.',
            'alamat_karyawan' => 'Tunjungan XV/2 Surabaya',
            'no_ktp' => '5891456186724664',
            'no_telepon' => '081297855318',
            'tanggal_lahir' => '1988-06-16',
            'tanggal_masuk' => '2005-11-15',
            'password' => '$2y$10$MqsQQvEUVqyHHmT7yMe9KuwAh7NYSpEa1Ze8Q/RFX138rdMJN2s1m', //password123
            'status_karyawan' => 'aktif',
            'divisi_id' => 3,
            'jabatan_id'=> 5,
        ]);

        User::firstOrCreate([
            'username' => 'dodit',
            'nama_karyawan' => 'Dodit L. T.',
            'alamat_karyawan' => 'Raya Gempol 225 Sidoarjo',
            'no_ktp' => '5896677186724664',
            'no_telepon' => '081681055318',
            'tanggal_lahir' => '1991-08-08',
            'tanggal_masuk' => '2009-11-01',
            'password' => '$2y$10$MqsQQvEUVqyHHmT7yMe9KuwAh7NYSpEa1Ze8Q/RFX138rdMJN2s1m', //password123
            'status_karyawan' => 'aktif',
            'divisi_id' => 3,
            'jabatan_id'=> 6,
        ]);

        User::firstOrCreate([
            'username' => 'alexa',
            'nama_karyawan' => 'Alexius S. B.',
            'alamat_karyawan' => 'Raya Gempol 15 Sidoarjo',
            'no_ktp' => '5869007186724664',
            'no_telepon' => '081681036988',
            'tanggal_lahir' => '1994-03-08',
            'tanggal_masuk' => '2012-05-29',
            'password' => '$2y$10$MqsQQvEUVqyHHmT7yMe9KuwAh7NYSpEa1Ze8Q/RFX138rdMJN2s1m', //password123
            'status_karyawan' => 'aktif',
            'divisi_id' => 3,
            'jabatan_id'=> 7,
        ]);

        User::firstOrCreate([
            'username' => 'billy',
            'nama_karyawan' => 'Billy P. P.',
            'alamat_karyawan' => 'Kendangsari XII Surabaya',
            'no_ktp' => '5822007186724964',
            'no_telepon' => '083961036988',
            'tanggal_lahir' => '1994-09-22',
            'tanggal_masuk' => '2014-05-30',
            'password' => '$2y$10$MqsQQvEUVqyHHmT7yMe9KuwAh7NYSpEa1Ze8Q/RFX138rdMJN2s1m', //password123
            'status_karyawan' => 'aktif',
            'divisi_id' => 3,
            'jabatan_id'=> 8,
        ]);

        User::firstOrCreate([
            'username' => 'nicholas',
            'nama_karyawan' => 'Nicholas L. A.',
            'alamat_karyawan' => 'Raya Manyar II Surabaya',
            'no_ktp' => '5822006686584964',
            'no_telepon' => '083308606988',
            'tanggal_lahir' => '1994-01-28',
            'tanggal_masuk' => '2015-06-22',
            'password' => '$2y$10$MqsQQvEUVqyHHmT7yMe9KuwAh7NYSpEa1Ze8Q/RFX138rdMJN2s1m', //password123
            'status_karyawan' => 'aktif',
            'divisi_id' => 3,
            'jabatan_id'=> 9,
        ]);
    }
}
