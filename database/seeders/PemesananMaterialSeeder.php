<?php

namespace Database\Seeders;

use App\Models\PemesananMaterial;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PemesananMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PemesananMaterial::firstOrCreate([
            'no_nota' => 'PM/2021-10-22/1',
            'total' => 4200000,
            'diskon' => 0,
            'status_nota' => 'done',
            'status_pembayaran' => 'unpaid',
            'supplier_id' => 2,
            'user_id' => 1,
            'created_at' => '2021-10-22 03:59:38',
        ]);

        DB::table('pemesanan_material_produk')->insert([
            [
                'pemesanan_material_id' => 1,
                'produk_id' => 3,
                'kuantitas' => 2000,
                'harga' => 500,
                'status_material'=> 'done'
            ],
            [
                'pemesanan_material_id' => 1,
                'produk_id' => 4,
                'kuantitas' => 2000,
                'harga' => 600,
                'status_material'=> 'done'
            ],
            [
                'pemesanan_material_id' => 1,
                'produk_id' => 5,
                'kuantitas' => 2000,
                'harga' => 1000,
                'status_material'=> 'done'
            ],
        ]);
    }
}
