<?php

namespace Database\Seeders;

use App\Models\Jurnal;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JurnalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:01:51',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'pembelian',
            'nomor_bukti' => 'PM/2021-10-22/1',
            'keterangan' => 'Mencatat pembelian material kepada supplier. ',
            'created_at' => '2021-10-22 04:01:51'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:06:23',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'penjualan',
            'nomor_bukti' => 'SO/2021-10-22/1',
            'keterangan' => 'Mencatat transaksi penjualan kepada pelanggan. ',
            'created_at' => '2021-10-22 04:06:23'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:48:19',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'permintaan material',
            'nomor_bukti' => 'PM/2021-10-22/1',
            'keterangan' => 'Mencatat penggunaan bahan baku untuk produksi.',
            'created_at' => '2021-10-22 04:48:19'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:48:33',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'permintaan material',
            'nomor_bukti' => 'PM/2021-10-22/2',
            'keterangan' => 'Mencatat penggunaan bahan baku untuk produksi.',
            'created_at' => '2021-10-22 04:48:33'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:48:55',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'produksi',
            'nomor_bukti' => 'RP/2021-10-22/1',
            'keterangan' => 'Mencatat biaya gaji ke barang dalam proses.',
            'created_at' => '2021-10-22 04:48:55'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:48:55',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'produksi',
            'nomor_bukti' => 'RP/2021-10-22/1',
            'keterangan' => 'Mencatat biaya overhead ke hutang usaha.',
            'created_at' => '2021-10-22 04:48:55'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:48:55',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'produksi',
            'nomor_bukti' => 'RP/2021-10-22/1',
            'keterangan' => 'Mencatat biaya overhead ke barang dalam proses.',
            'created_at' => '2021-10-22 04:48:55'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:48:55',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'produksi',
            'nomor_bukti' => 'RP/2021-10-22/1',
            'keterangan' => 'Mencatat barang jadi yang ditransfer ke gudang barang jadi.',
            'created_at' => '2021-10-22 04:48:55'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:49:22',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'produksi',
            'nomor_bukti' => 'RP/2021-10-22/2',
            'keterangan' => 'Mencatat biaya gaji ke barang dalam proses.',
            'created_at' => '2021-10-22 04:49:22'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:49:22',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'produksi',
            'nomor_bukti' => 'RP/2021-10-22/2',
            'keterangan' => 'Mencatat biaya overhead ke hutang usaha.',
            'created_at' => '2021-10-22 04:49:22'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:49:22',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'produksi',
            'nomor_bukti' => 'RP/2021-10-22/2',
            'keterangan' => 'Mencatat biaya overhead ke barang dalam proses.',
            'created_at' => '2021-10-22 04:49:22'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:49:22',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'produksi',
            'nomor_bukti' => 'RP/2021-10-22/2',
            'keterangan' => 'Mencatat barang jadi yang ditransfer ke gudang barang jadi.',
            'created_at' => '2021-10-22 04:49:22'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:50:11',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'pembayaran pelanggan',
            'nomor_bukti' => 'NPO/2021-10-22/1',
            'keterangan' => 'Mencatat pembayaran dari pelanggan. lunas',
            'created_at' => '2021-10-22 04:50:11'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:50:29',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'pengiriman',
            'nomor_bukti' => 'SJ/2021-10-22/1',
            'keterangan' => 'Mencatat pengiriman produk ke pelanggan. ',
            'created_at' => '2021-10-22 04:50:29'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:56:49',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'penjualan',
            'nomor_bukti' => 'SO/2021-10-22/2',
            'keterangan' => 'Mencatat transaksi penjualan kepada pelanggan. ',
            'created_at' => '2021-10-22 04:56:49'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:58:14',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'permintaan material',
            'nomor_bukti' => 'PM/2021-10-22/3',
            'keterangan' => 'Mencatat penggunaan bahan baku untuk produksi.',
            'created_at' => '2021-10-22 04:58:14'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:58:31',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'produksi',
            'nomor_bukti' => 'RP/2021-10-22/3',
            'keterangan' => 'Mencatat biaya gaji ke barang dalam proses.',
            'created_at' => '2021-10-22 04:58:31'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:58:31',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'produksi',
            'nomor_bukti' => 'RP/2021-10-22/3',
            'keterangan' => 'Mencatat biaya overhead ke hutang usaha.',
            'created_at' => '2021-10-22 04:58:31'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:58:31',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'produksi',
            'nomor_bukti' => 'RP/2021-10-22/3',
            'keterangan' => 'Mencatat biaya overhead ke barang dalam proses.',
            'created_at' => '2021-10-22 04:58:31'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 04:58:31',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'produksi',
            'nomor_bukti' => 'RP/2021-10-22/3',
            'keterangan' => 'Mencatat barang jadi yang ditransfer ke gudang barang jadi.',
            'created_at' => '2021-10-22 04:58:31'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 05:29:39',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'pembayaran pelanggan',
            'nomor_bukti' => 'NPO/2021-10-22/2',
            'keterangan' => 'Mencatat pembayaran dari pelanggan. lunas',
            'created_at' => '2021-10-22 05:29:39'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-22 05:29:54',
            'jenis_jurnal' => 'JU',
            'jenis_transaksi' => 'pengiriman',
            'nomor_bukti' => 'SJ/2021-10-22/2',
            'keterangan' => 'Mencatat pengiriman produk ke pelanggan. ',
            'created_at' => '2021-10-22 05:29:54'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-30 05:32:25',
            'jenis_jurnal' => 'JT',
            'jenis_transaksi' => 'penutup',
            'nomor_bukti' => '-',
            'keterangan' => 'Penutupan - Pendapatan',
            'created_at' => '2021-10-30 05:32:25'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-30 05:32:25',
            'jenis_jurnal' => 'JT',
            'jenis_transaksi' => 'penutup',
            'nomor_bukti' => '-',
            'keterangan' => 'Penutupan - Biaya',
            'created_at' => '2021-10-30 05:32:25'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-30 05:32:25',
            'jenis_jurnal' => 'JT',
            'jenis_transaksi' => 'penutup',
            'nomor_bukti' => '-',
            'keterangan' => 'Penutupan - Modal dan Laba Rugi',
            'created_at' => '2021-10-30 05:32:25'
        ]);

        Jurnal::firstOrCreate([
            'tanggal' => '2021-10-30 05:32:25',
            'jenis_jurnal' => 'JT',
            'jenis_transaksi' => 'penutup',
            'nomor_bukti' => '-',
            'keterangan' => 'Penutupan - Modal dan Prive',
            'created_at' => '2021-10-30 05:32:25'
        ]);

        $items = [
            ['akun_id' => 104, 'jurnal_id' => 1, 'debet' => 4200000, 'kredit' => 0],
            ['akun_id' => 201, 'jurnal_id' => 1, 'debet' => 0, 'kredit' => 4200000],
            ['akun_id' => 103, 'jurnal_id' => 2, 'debet' => 5000000, 'kredit' => 0],
            ['akun_id' => 401, 'jurnal_id' => 2, 'debet' => 0, 'kredit' => 5000000],
            ['akun_id' => 105, 'jurnal_id' => 3, 'debet' => 195000, 'kredit' => 0],
            ['akun_id' => 104, 'jurnal_id' => 3, 'debet' => 0, 'kredit' => 195000],
            ['akun_id' => 105, 'jurnal_id' => 4, 'debet' => 130000, 'kredit' => 0],
            ['akun_id' => 104, 'jurnal_id' => 4, 'debet' => 0, 'kredit' => 130000],
            ['akun_id' => 105, 'jurnal_id' => 5, 'debet' => 1200000, 'kredit' => 0],
            ['akun_id' => 202, 'jurnal_id' => 5, 'debet' => 0, 'kredit' => 1200000],
            ['akun_id' => 502, 'jurnal_id' => 6, 'debet' => 105000, 'kredit' => 0],
            ['akun_id' => 201, 'jurnal_id' => 6, 'debet' => 0, 'kredit' => 105000],
            ['akun_id' => 105, 'jurnal_id' => 7, 'debet' => 105000, 'kredit' => 0],
            ['akun_id' => 502, 'jurnal_id' => 7, 'debet' => 0, 'kredit' => 105000],
            ['akun_id' => 106, 'jurnal_id' => 8, 'debet' => 1500000, 'kredit' => 0],
            ['akun_id' => 105, 'jurnal_id' => 8, 'debet' => 0, 'kredit' => 1500000],
            ['akun_id' => 105, 'jurnal_id' => 9, 'debet' => 800000, 'kredit' => 0],
            ['akun_id' => 202, 'jurnal_id' => 9, 'debet' => 0, 'kredit' => 800000],
            ['akun_id' => 502, 'jurnal_id' => 10, 'debet' => 70000, 'kredit' => 0],
            ['akun_id' => 201, 'jurnal_id' => 10, 'debet' => 0, 'kredit' => 70000],
            ['akun_id' => 105, 'jurnal_id' => 11, 'debet' => 70000, 'kredit' => 0],
            ['akun_id' => 502, 'jurnal_id' => 11, 'debet' => 0, 'kredit' => 70000],
            ['akun_id' => 106, 'jurnal_id' => 12, 'debet' => 1000000, 'kredit' => 0],
            ['akun_id' => 105, 'jurnal_id' => 12, 'debet' => 0, 'kredit' => 1000000],
            ['akun_id' => 101, 'jurnal_id' => 13, 'debet' => 5000000, 'kredit' => 0],
            ['akun_id' => 103, 'jurnal_id' => 13, 'debet' => 0, 'kredit' => 5000000],
            ['akun_id' => 501, 'jurnal_id' => 14, 'debet' => 2500000, 'kredit' => 0],
            ['akun_id' => 106, 'jurnal_id' => 14, 'debet' => 0, 'kredit' => 2500000],
            ['akun_id' => 103, 'jurnal_id' => 15, 'debet' => 3000000, 'kredit' => 0],
            ['akun_id' => 401, 'jurnal_id' => 15, 'debet' => 0, 'kredit' => 3000000],
            ['akun_id' => 105, 'jurnal_id' => 16, 'debet' => 195000, 'kredit' => 0],
            ['akun_id' => 104, 'jurnal_id' => 16, 'debet' => 0, 'kredit' => 195000],
            ['akun_id' => 105, 'jurnal_id' => 17, 'debet' => 1200000, 'kredit' => 0],
            ['akun_id' => 202, 'jurnal_id' => 17, 'debet' => 0, 'kredit' => 1200000],
            ['akun_id' => 502, 'jurnal_id' => 18, 'debet' => 105000, 'kredit' => 0],
            ['akun_id' => 201, 'jurnal_id' => 18, 'debet' => 0, 'kredit' => 105000],
            ['akun_id' => 105, 'jurnal_id' => 19, 'debet' => 105000, 'kredit' => 0],
            ['akun_id' => 502, 'jurnal_id' => 19, 'debet' => 0, 'kredit' => 105000],
            ['akun_id' => 106, 'jurnal_id' => 20, 'debet' => 1500000, 'kredit' => 0],
            ['akun_id' => 105, 'jurnal_id' => 20, 'debet' => 0, 'kredit' => 1500000],
            ['akun_id' => 102, 'jurnal_id' => 21, 'debet' => 3000000, 'kredit' => 0],
            ['akun_id' => 103, 'jurnal_id' => 21, 'debet' => 0, 'kredit' => 3000000],
            ['akun_id' => 501, 'jurnal_id' => 22, 'debet' => 1500000, 'kredit' => 0],
            ['akun_id' => 106, 'jurnal_id' => 22, 'debet' => 0, 'kredit' => 1500000],
            ['akun_id' => 401, 'jurnal_id' => 23, 'debet' => 8000000, 'kredit' => 0],
            ['akun_id' => 504, 'jurnal_id' => 23, 'debet' => 0, 'kredit' => 8000000],
            ['akun_id' => 504, 'jurnal_id' => 24, 'debet' => 4000000, 'kredit' => 0],
            ['akun_id' => 402, 'jurnal_id' => 24, 'debet' => 0, 'kredit' => 0],
            ['akun_id' => 501, 'jurnal_id' => 24, 'debet' => 0, 'kredit' => 4000000],
            ['akun_id' => 502, 'jurnal_id' => 24, 'debet' => 0, 'kredit' => 0],
            ['akun_id' => 504, 'jurnal_id' => 25, 'debet' => 4000000, 'kredit' => 0],
            ['akun_id' => 301, 'jurnal_id' => 25, 'debet' => 0, 'kredit' => 4000000],
            ['akun_id' => 301, 'jurnal_id' => 26, 'debet' => 0, 'kredit' => 0],
            ['akun_id' => 302, 'jurnal_id' => 26, 'debet' => 0, 'kredit' => 0],
        ];

        DB::table('akun_jurnal')->insert($items);
    }
}
