<?php

namespace Database\Seeders;

use App\Models\Akun;
use Illuminate\Database\Seeder;

class AkunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 101, 'nama_akun' => 'Kas', 'tipe_akun' => '1', 'urutan_akun' => 1],
            ['id' => 102, 'nama_akun' => 'Rekening Bank', 'tipe_akun' => '1', 'urutan_akun' => 1],
            ['id' => 103, 'nama_akun' => 'Piutang Usaha', 'tipe_akun' => '1', 'urutan_akun' => 1],
            ['id' => 104, 'nama_akun' => 'Sediaan Bahan Baku', 'tipe_akun' => '1', 'urutan_akun' => 1],
            ['id' => 105, 'nama_akun' => 'Sediaan Dalam Proses', 'tipe_akun' => '1', 'urutan_akun' => 1],
            ['id' => 106, 'nama_akun' => 'Sediaan Barang Jadi', 'tipe_akun' => '1', 'urutan_akun' => 1],
            ['id' => 107, 'nama_akun' => 'Sediaan Barang Cacat', 'tipe_akun' => 1, 'urutan_akun' => 1],
            ['id' => 201, 'nama_akun' => 'Hutang Usaha', 'tipe_akun' => '0', 'urutan_akun' => 2],
            ['id' => 202, 'nama_akun' => 'Hutang Gaji', 'tipe_akun' => 0, 'urutan_akun' => 2],
            ['id' => 301, 'nama_akun' => 'Modal', 'tipe_akun' => '0', 'urutan_akun' => 3],
            ['id' => 302, 'nama_akun' => 'Prive', 'tipe_akun' => '1', 'urutan_akun' => 3],
            ['id' => 401, 'nama_akun' => 'Penjualan', 'tipe_akun' => '0', 'urutan_akun' => 4],
            ['id' => 402, 'nama_akun' => 'Diskon Penjualan', 'tipe_akun' => '1', 'urutan_akun' => 4],
            ['id' => 501, 'nama_akun' => 'HPP', 'tipe_akun' => '1', 'urutan_akun' => 5],
            ['id' => 502, 'nama_akun' => 'Biaya Overhead Pabrik', 'tipe_akun' => '1', 'urutan_akun' => 5],
            ['id' => 504, 'nama_akun' => 'Ihtisar Laba Rugi']
        ];

        foreach ($items as $item) {
            Akun::create($item);
        }
    }
}
