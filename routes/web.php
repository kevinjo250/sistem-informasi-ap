<?php

use App\Http\Controllers\{
    AkunController,
    BatalOrderController,
    BillOfMaterialController,
    BukuBesarController,
    DashboardController,
    DivisiController,
    JabatanController,
    JurnalController,
    LaporanKeuanganController,
    OtorisasiController,
    PasswordController,
    PelangganController,
    PemesananMaterialController,
    PenerimaanMaterialController,
    ProdukController,
    ProfileController,
    SalesOrderController,
    SupplierController,
    SuratJalanController,
    SuratPerintahKerjaController,
    UserController,
    NotaPelunasanSupplierController,
    NotaPembayaranController,
    PerkiraanHppController,
    PermintaanMaterialController,
    RealisasiProduksiController,
};
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false,
    'verify'   => false,
]);

Route::middleware('auth')->group(function () {
    Route::get('/', [DashboardController::class, 'index'])
        ->name('dashboard');

    Route::post('markNotification', [DashboardController::class, 'markNotification'])
        ->name('markNotification');

    Route::resource('divisis', DivisiController::class)
        ->except(['show']);

    Route::resource('jabatans', JabatanController::class)
        ->except('show');

    Route::resource('users', UserController::class)
        ->except(['show', 'destroy']);

    Route::resource('pelanggans', PelangganController::class)
        ->except(['show']);

    Route::resource('suppliers', SupplierController::class)
        ->except(['show']);

    Route::get('otorisasis', [OtorisasiController::class, 'index'])
        ->name('otorisasis.index');

    Route::resource('produks', ProdukController::class)
        ->except(['show', 'destroy']);

    Route::get('akuns', [AkunController::class, 'index'])
        ->name('akuns.index');

    Route::prefix('profile')->group(function () {
        Route::get('/{user}', [ProfileController::class, 'edit'])
            ->name('profile.edit');

        Route::patch('/{user}', [ProfileController::class, 'update'])
            ->name('profile.update');

        Route::patch('/passwordUpdate/{user}', [PasswordController::class, 'update'])
            ->name('password.update');
    });

    Route::patch('salesOrders/{salesOrder}/confirm', [SalesOrderController::class, 'confirm'])
        ->name('salesOrders.confirm');

    Route::resource('salesOrders', SalesOrderController::class);

    Route::post('batalOrders/{salesOrder}', [BatalOrderController::class, 'store'])
            ->name('batalOrders.store');

    Route::resource('batalOrders', BatalOrderController::class)
        ->except(['create', 'store', 'show', 'destroy']);

    Route::post('notaPembayarans/{salesOrder}', [NotaPembayaranController::class, 'store'])
        ->name('notaPembayarans.store');

    Route::resource('notaPembayarans', NotaPembayaranController::class)
        ->except(['create', 'store']);

    Route::get('suratJalans/create/{salesOrder}', [SuratJalanController::class, 'create'])
        ->name('suratJalans.create');

    Route::post('suratJalans/{salesOrder}', [SuratJalanController::class, 'store'])
        ->name('suratJalans.store');

    Route::resource('suratJalans', SuratJalanController::class)
        ->except(['create', 'store']);

    Route::patch('pemesananMaterials/confirm/{pemesananMaterial}', [PemesananMaterialController::class, 'confirm'])
        ->name('pemesananMaterials.confirm');

    Route::patch('pemesananMaterials/open/{pemesananMaterial}', [PemesananMaterialController::class, 'open'])
        ->name('pemesananMaterials.open');

    Route::resource('pemesananMaterials', PemesananMaterialController::class);

    Route::get('penerimaanMaterials/create/{pemesananMaterial}', [PenerimaanMaterialController::class, 'create'])
        ->name('penerimaanMaterials.create');

    Route::post('penerimaanMaterials/store/{pemesananMaterial}', [PenerimaanMaterialController::class, 'store'])
        ->name('penerimaanMaterials.store');

    Route::resource('penerimaanMaterials', PenerimaanMaterialController::class)
        ->except(['create', 'store']);

    Route::post('notaPelunasanSuppliers/store/{pemesananMaterial}', [NotaPelunasanSupplierController::class, 'store'])
        ->name('notaPelunasanSuppliers.store');

    Route::resource('notaPelunasanSuppliers', NotaPelunasanSupplierController::class)
        ->except(['create', 'store']);

    Route::resource('billOfMaterials', BillOfMaterialController::class);

    Route::resource('suratPerintahKerjas', SuratPerintahKerjaController::class);

    Route::get('permintaanMaterials/getBillOfMaterials/{id}', [PermintaanMaterialController::class, 'getBillOfMaterials']);

    Route::patch('permintaanMaterials/confirm/{permintaanMaterial}', [PermintaanMaterialController::class, 'confirm'])
        ->name('permintaanMaterials.confirm');
    
    Route::resource('permintaanMaterials', PermintaanMaterialController::class)
        ->except(['edit', 'update']);

    Route::get('perkiraanHpps/create/{billOfMaterial}', [PerkiraanHppController::class, 'create'])
        ->name('perkiraanHpps.create');

    Route::post('perkiraanHpps/store/{billOfMaterial}', [PerkiraanHppController::class, 'store'])
        ->name('perkiraanHpps.store');

    Route::resource('perkiraanHpps', PerkiraanHppController::class)
        ->except(['create', 'store']);

    Route::resource('realisasiProduksis', RealisasiProduksiController::class);

    Route::post('jurnals/find', [JurnalController::class, 'find'])
        ->name('jurnals.find');

    Route::get('jurnals/close', [JurnalController::class, 'close'])
        ->name('jurnals.close');

    Route::resource('jurnals', JurnalController::class)
        ->only(['index', 'create', 'store']);

    Route::get('bukuBesars', [BukuBesarController::class, 'index'])
        ->name('bukuBesars.index');

    Route::post('bukuBesars/find', [BukuBesarController::class, 'find'])
        ->name('bukuBesars.find');

    Route::get('laporanKeuangans', [LaporanKeuanganController::class, 'index'])
        ->name('laporanKeuangans.index');

    Route::post('laporanKeuangans/find', [LaporanKeuanganController::class, 'find'])
        ->name('laporanKeuangans.find');
});
